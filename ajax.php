<?php
empty($_GET['action']) && exit;
$action = $_GET['action'];
require('includes/global_config.php');
$ajax_lang_filename = DIR_WS_LANGUAGES . $language . '/ajax/' . $action . '.php';
if (is_file($ajax_lang_filename)) require($ajax_lang_filename);

$post = $_POST;

switch ($action) {
	case 'send_mail':
		empty($_GET['m']) || !is_numeric($_GET['m']) && exit('no');
		!tep_session_is_registered('partner_id') && tep_exit('no');
		$mail_query = tep_db_query('select * from ' . TABLE_MARKETING_MAILS . ' where material_id = ' . (int)$_GET['m'] . ' and material_status = 1');
		tep_db_num_rows($mail_query) != 1 && tep_exit('no');
		$mail = tep_db_fetch_array($mail_query);

		$user_info_query = tep_db_query('select partners_firstname, partners_lastname, partners_email_address from ' . TABLE_PARTNERS . ' where partners_id = ' . (int)$partner_id);
		tep_db_num_rows($user_info_query) != 1 && tep_exit('no');

		$user_info = tep_db_fetch_array($user_info_query);
		$options = array(
			'to' => array(
				'address' => $user_info['partners_email_address'],
				'name' => $user_info['partners_firstname'] . ' ' . $user_info['partners_lastname']
			),
			'subject' => $mail['material_title'],
			'message' => $mail['material_html']
		);

		tep_exit(tep_send_mail($options) ? 'ok' : 'no');
		break;

	case 'ck':
		if (tep_session_is_registered('visit_info') && !tep_session_is_registered('visit_id')) {
			if (!$IS_BOT) {
				$sql_data_visit = $visit_info;
				tep_db_perform(TABLE_VISITS, $sql_data_visit);
				$visit_id = tep_db_insert_id();
				tep_session_register('visit_id');
			}
		}
		tep_session_unregister('visit_info');
		break;

	case 'get_aff_hist':
		if (!$PARTNER || !tep_session_is_registered('partner_id')) exit;
		$res = array();
		$res['html'] = '';

		if (isset($_GET['from']) && isset($_GET['to']) && is_numeric($_GET['from']) && is_numeric($_GET['to']) && $_GET['from'] <= $_GET['to']) {
			$start_date = intval($_GET['from'] / 1000);
			$end_date = intval($_GET['to'] / 1000);


			$date_filter = ' and ph.date_added >= "' . date(MYSQL_FORMAT_DATETIME, $start_date) . '" and ph.date_added <= "' . date(MYSQL_FORMAT_DATETIME, $end_date) . '"';
			$history_query = tep_db_query('select ph.orders_id, ph.date_added, ph.affiliates_share, ph.affiliates_commission, ph.commission_status, group_concat(distinct pd.products_name SEPARATOR \', \') as products from ' . TABLE_PARTNERS_HISTORY . ' as ph join ' . TABLE_ORDERS_PRODUCTS . ' as op on(ph.partners_id = ' . (int)$partner_id . ' and op.orders_id = ph.orders_id' . $date_filter . ') join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd on(op.products_id = pd.products_id and pd.language_id = ' . (int)$languages_id . ') group by(ph.orders_id) order by ph.date_added desc');

			if (tep_db_num_rows($history_query) > 0) {
				$res['html'] .= '<table class="affiliates-history-table">';
				$res['html'] .= '<tr>';
				$res['html'] .= '<th>' . HISTORY_HEAD_ORDER_ID . '</th>';
				$res['html'] .= '<th>' . HISTORY_HEAD_RECEIVED . '</th>';
				$res['html'] .= '<th>' . HISTORY_HEAD_DATE . '</th>';
				$res['html'] .= '<th>' . HISTORY_HEAD_SHARE . '</th>';
				$res['html'] .= '<th>' . HISTORY_HEAD_COMMISSION . '</th>';
				$res['html'] .= '<th>' . HISTORY_HEAD_PRODUCTS . '</th>';
				$res['html'] .= '</tr>';
				while ($hst = tep_db_fetch_array($history_query)) {
					$res['html'] .= '<tr>';
					$res['html'] .= '<td>' . $hst['orders_id'] . '</td>';
					$res['html'] .= '<td>' . ($hst['commission_status'] ? 'YES' : 'NO') . '</td>';
					$res['html'] .= '<td>' . $hst['date_added'] . '</td>';
					$res['html'] .= '<td>' . $hst['affiliates_share'] . '</td>';
					$res['html'] .= '<td>' . $hst['affiliates_commission'] . '</td>';
					$res['html'] .= '<td>' . $hst['products'] . '</td>';
					$res['html'] .= '</tr>';
				}

				$res['html'] .= '</table>';
			} else {
				$res['html'] .= TEXT_NO_DATA;
			}

		} else {
			$res['html'] .= TEXT_NO_DATA;
		}

		$res = json_encode($res);
		if (isset($_GET['callback'])) {
			$res = $_GET['callback'] . '(' . $res . ');';
		}
		exit($res);
		break;

	case 'get_aff_hist_bounds':
		if (!$PARTNER || !tep_session_is_registered('partner_id')) exit;
		$res = array();
		$bounds_query = tep_db_query('select unix_timestamp(min(date_added)) * 1000 as minDate, unix_timestamp(max(date_added)) * 1000 as maxDate from ' . TABLE_PARTNERS_HISTORY . ' where partners_id = ' . (int)$partner_id);
		if (tep_db_num_rows($bounds_query) === 1) {
			$res = tep_db_fetch_array($bounds_query);
		}
		$res = json_encode($res);
		if (isset($_GET['callback'])) {
			$res = $_GET['callback'] . '(' . $res . ');';
		}
		exit($res);
		break;

	case 'cart':
		$response = array();

		$act = empty($_GET['act']) ? '' : $_GET['act'];
		switch ($act) {
			case 'add':
				if (empty($post['id']) || !is_numeric($post['id']) || $post['id'] < 1 || (intval($post['id']) != $post['id'])) exit('{}');
				$id = intval($post['id']);
				$qty = (isset($post['qty']) ? intval($post['qty']) : 1);
				$qty = $qty < 1 ? 1 : $qty;

				$check_product = tep_db_query('select p.products_id, p.products_quantity, p.products_price, p.products_image, pd.products_name from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd on(p.products_id = pd.products_id) where p.products_id = ' . intval($id) . ' and pd.lannguage_id = ' . intval($languages_id) . ' and p.products_status = 1');
				if (tep_db_num_rows($check_product) !== 1) {
					exit();
				}

				$product = tep_db_fetch_array($check_product);

				if ($cart->in_cart($product['products_id'])) {
					$exs_qty = $cart->get_quantity($product['products_id']);
				} else {
					$exs_qty = 0;
				}

				$exs_qty = filter_var($exs_qty, FILTER_VALIDATE_INT) && $exs_qty > 0 ? (int)$exs_qty : 0;

				if ((STOCK_CHECK == 'true') && (STOCK_ALLOW_CHECKOUT != 'true')) {
					if ($product['products_quantity'] < $qty + $exs_qty) {
						$qty = $product['products_quantity'];
					} else {
						$qty += $exs_qty;
					}
				} else {
					$qty += $exs_qty;
				}

				if ($cart->in_cart($product['products_id'])) {
					$cart->update_quantity($product['products_id'], $qty, $product['products_id']);
				} else {
					$cart->add_cart($product['products_id'], $qty, $product['products_id']);
				}

				$html = '      <tr>';

				$html .= '<table border="0" cellspacing="2" cellpadding="2">' .
					'  <tr>' .
					'    <td align="center"><a href="' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product['products_name'])) . '">' . tep_image(DIR_WS_IMAGES_PRODUCTS . $product['products_image'] . '-2' . IMAGE_EXTENSION, $product['products_name'], SMALL_IMAGE_WIDTH) . '</a></td>' .
					'    <td valign="top"><a href="' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product['products_name'])) . '"><strong>' . $product['products_name'] . '</strong></a>';

				/*if (STOCK_CHECK == 'true') {
					//$stock_check = tep_check_stock($product['id'], $product['quantity']);
					if ($product['quantity'] > $product['stock']) {
						$html .= '<span class="markProductOutOfStock">' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '</span>';
					}
				}*/

				$html .= '<br /><br />' . tep_draw_input_field('products_quantity[' . $product['products_id'] . ']', 0, 'size="4" class="jg-basket-updater jg-basket-qty jg-basket-updater-qty" data-jg-basket-prod-id="' . $product['products_id'] . '" data-jg-basket-updater-id="' . $product['id'] . '" data-jg-basket-prod-price="' . $product['products_price'] . '"') . '<button type="button" class="jg-basket-remover" data-jg-basket-prod-id="' . $product['products_id'] . '" title="' . TEXT_REMOVE . '">' . TEXT_REMOVE . '</button>';

				$html .= '    </td>' .
					'  </tr>' .
					'</table>';

				/*$html .= '        <td valign="top">' . $html . '</td>' .
					'        <td align="right" valign="top"><strong>' . $currencies->display_price($product['final_price'], tep_get_tax_rate($product['tax_class_id'])) . '</strong></td>' .
					'      </tr>';*/

				$html .= '        <td valign="top">' . $html . '</td>' .
					'        <td align="right" valign="top"><strong>' . $product['products_price'] . '</strong></td>' .
					'      </tr>';

				exit(json_encode(array(
					'id' => $id,
					'qty' => $qty - $exs_qty,
					'html' => $html
				)));
				break;

			case 'remove':
				if (empty($post['id']) || !is_numeric($post['id']) || $post['id'] < 1 || (intval($post['id']) != $post['id'])) exit('{}');
				$id = intval($post['id']);

				$cart->remove($id);

				exit('OK');
				break;

			case 'update':
				if (empty($post['id']) || !is_numeric($post['id']) || $post['id'] < 1 || (intval($post['id']) != $post['id'])) exit('{}');
				$id = intval($post['id']);
				$qty = (isset($post['qty']) ? intval($post['qty']) : 1);
				$qty = $qty < 1 ? 1 : $qty;

				$check_product = tep_db_query('select p.products_id, p.products_quantity, p.products_price, p.products_image, pd.products_name from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd on(p.products_id = pd.products_id) where p.products_id = ' . intval($id) . ' and pd.language_id = ' . intval($languages_id) . ' and p.products_status = 1');
				if (tep_db_num_rows($check_product) !== 1) {
					exit();
				}

				$product = tep_db_fetch_array($check_product);

				if ((STOCK_CHECK == 'true') && (STOCK_ALLOW_CHECKOUT != 'true')) {
					if ($product['products_quantity'] < $qty) {
						$qty = $product['products_quantity'];
					}
				}

				if ($cart->in_cart($product['products_id'])) {
					$cart->update_quantity($product['products_id'], $qty, $product['products_id']);
				} else {
					$cart->add_cart($product['products_id'], $qty, $product['products_id']);
				}

				include(DIR_WS_CLASSES . 'payment.php');
				$payment_modules = new payment;


				//--------------------------------------- checkout-shipping-----------------------------------------------------

				require('includes/classes/http_client.php');


// if the customer is not logged on, redirect them to the login page
				/*if (!tep_session_is_registered('customer_id')) {
					$navigation->set_snapshot();
					tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
				}*/


// if there is nothing in the customers cart, redirect them to the shopping cart page

				/*$select_str = 'select ab.address_book_id';
				if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
				if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
				if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
				if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
				if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
				if (ACCOUNT_STATE == 'true') {
					$select_str .= ', ab.entry_state';
					$select_str .= ', ab.entry_zone_id';
				}
				if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
				if (ACCOUNT_TELEPHONE_NUMBER == 'true') $select_str .= ', c.customers_telephone';
				$def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
				$def_addr_book = tep_db_fetch_array($def_addr_book_query);
				if (empty($def_addr_book['entry_state']) && empty($def_addr_book['entry_zone_id'])) {
					tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
				}
				unset($def_addr_book['entry_state']);
				unset($def_addr_book['entry_zone_id']);
				if(in_array(null, $def_addr_book) || in_array('',$def_addr_book) || in_array('0', $def_addr_book)) {
					tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
				}

	// if no shipping destination address was selected, use the customers own address as default
				if (!tep_session_is_registered('sendto')) {
					tep_session_register('sendto');
					$sendto = $customer_default_address_id;
				} else {
	// verify the selected shipping address
					if ( (is_array($sendto) && empty($sendto)) || is_numeric($sendto) ) {
						$check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$sendto . "'");
						$check_address = tep_db_fetch_array($check_address_query);

						if ($check_address['total'] != '1') {
							$sendto = $customer_default_address_id;
							if (tep_session_is_registered('shipping')) tep_session_unregister('shipping');
						}
					}
				}*/

				require(DIR_WS_CLASSES . 'order.php');
				$order = new order;

// register a random ID in the session to check throughout the checkout procedure
// against alterations in the shopping cart contents
				if (!tep_session_is_registered('cartID')) {
					tep_session_register('cartID');
				} elseif (($cartID != $cart->cartID) && tep_session_is_registered('shipping')) {
					tep_session_unregister('shipping');
				}

				$cartID = $cart->cartID = $cart->generate_cart_id();

// if the order contains only virtual products, forward the customer to the billing page as
// a shipping address is not needed
				if ($order->content_type == 'virtual') {
					if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
					$shipping = false;
					$sendto = false;
					tep_redirect(tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
				}

				$total_weight = $cart->show_weight();
				$total_count = $cart->count_contents();

// load all enabled shipping modules
				require(DIR_WS_CLASSES . 'shipping.php');
				$shipping_modules = new shipping;

				if (defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true')) {
					$pass = false;

					switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
						case 'national':
							if ($order->delivery['country_id'] == STORE_COUNTRY) {
								$pass = true;
							}
							break;
						case 'international':
							if ($order->delivery['country_id'] != STORE_COUNTRY) {
								$pass = true;
							}
							break;
						case 'both':
							$pass = true;
							break;
					}

					$free_shipping = false;
					if (($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) {
						$free_shipping = true;

						include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
					}
				} else {
					$free_shipping = false;
				}

// get all available shipping quotes
				$quotes = $shipping_modules->quote();
				$jsquotes = array();

				for ($i = 0, $n = sizeof($quotes); $i < $n; $i++) {
					if (isset($quotes[$i]['error'])) {
					} else {
						for ($j = 0, $n2 = sizeof($quotes[$i]['methods']); $j < $n2; $j++) {
							//echo $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0)));
							$jsquotes['#quotes-' . $i . '-' . $j] = $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0)));
						}
					}
				}

				exit(json_encode($jsquotes));
				break;
			case 'load':
				$products = $cart->get_products();
				!is_array($products) && $products = null;

				if (!empty($products)) {
					$prods = array();
					$prods['prepend'] = '';
					$total_price = 0;
					foreach ($products as $product) {
						$prods[$product['id']] = array(
							'id' => $product['id'],
							'qty' => $product['quantity']
						);

						$html = '<tr>';

						$products_name = '<table border="0" cellspacing="2" cellpadding="2">' .
							'  <tr>' .
							'    <td align="center"><a href="' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product['name'])) . '">' . tep_image(DIR_WS_IMAGES_PRODUCTS . $product['image'] . '-2' . IMAGE_EXTENSION, $product['name'], SMALL_IMAGE_WIDTH) . '</a></td>' .
							'    <td valign="top"><a href="' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product['name'])) . '"><strong>' . $product['name'] . '</strong></a>';

						/*if (STOCK_CHECK == 'true') {
							//$stock_check = tep_check_stock($product['id'], $product['quantity']);
							if ($product['quantity'] > $product['stock']) {
								$html .= '<span class="markProductOutOfStock">' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '</span>';
							}
						}*/

						$products_name .= '<br /><br />' . tep_draw_input_field('products_quantity[' . $product['id'] . ']', 0, 'size="4" class="jg-basket-updater jg-basket-qty jg-basket-updater-qty" data-jg-basket-prod-id="' . $product['id'] . '" data-jg-basket-updater-id="' . $product['id'] . '" data-jg-basket-prod-price="' . $product['final_price'] . '"') . '<span>$</span><span class="jg-basket-prod-total-price" data-jg-basket-prod-id="' . $product['id'] . '" data-jg-basket-prod-price="' . $product['final_price'] . '"></span><button type="button" class="jg-basket-remover" data-jg-basket-prod-id="' . $product['id'] . '" title="' . TEXT_REMOVE . '">' . TEXT_REMOVE . '</button>';

						$products_name .= '</td>' .
							'</tr>' .
							'</table>';

						$html .= '<td valign="top">' . $products_name . '</td>' .
							'<td align="right" valign="top"><strong>' . $currencies->display_price($product['final_price'], tep_get_tax_rate($product['tax_class_id'])) . '</strong></td>' .
							'</tr>';

						$prods[$product['id']]['html'] = $html;
						$total_price += intval($product['quantity'] * $product['final_price']);
					}

					/*$prods['postpend'] = '<li class="flycardfooter price">
							  <div class="total">Total: <span class="jg-basket-total-price">' . number_format($total_price, '0', '.', ' ') . '</span><span class="dram"></span></div>
							  <button id="btnCartCheckout" type="button" class="icon55 icon20 ibutton" onclick="gotocart();"> Купить</button>
							  <button id="btnGotoCart" type="button" class="icon55 icon20 ibutton" onclick="gotocart();"> Открыть корзину</button>
				    		</li>';*/
					exit(json_encode($prods));
				}
				exit();
				break;

		}
		break;
	case "feedback":

		$message = "Name: " . $post['name'];
		$message .= "<br>Phone: " . $post['email'];
		$message .= "<br>Message: " . $post['message'];



		$options = array(
			'to' => array(
				'address' => 'info@dena.am',
				'name' => 'admin'
			),
			'subject' => 'New feedback email',
			'message' =>  $message
		);

		$res = tep_send_mail($options);

		header('Cache-Control: no-cache, must-revalidate');
		header('Content-Type: application/json');
		echo json_encode(['success'=>$res, 'options'=>$options]);
		break;
    case "payment":

        $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        $data = file_get_contents('php://input');
        $verified = verify_webhook($data, $hmac_header);
        error_log('Webhook verified: '.var_export($verified, true)); //check error.log to see the result
        error_log('data is '. $data);
        break;
}

function verify_webhook($data, $hmac_header)
{
    $calculated_hmac = base64_encode(hash_hmac('sha256', $data, 'e855d04b05f69550b4c853a566fae8c61577bd52de293eebc9c1be242ef22640', true));
    return ($hmac_header == $calculated_hmac);
}