<?php
//------------google -------------------

require_once DIR_WS_MODULES . 'socials/google_plus/google-api-php-client/src/Google_Client.php';
require_once DIR_WS_MODULES . 'socials/google_plus/google-api-php-client/src/contrib/Google_Oauth2Service.php';

$client = new Google_Client();

$oauth2 = new Google_Oauth2Service($client);

$authUrl = $client->createAuthUrl();


//-----google end------------------
?>
<div id="loginTabs">
    <ul>
        <li data-tab="signInTab"><a><?php echo SIGN_IN_HEADING_TITLE;?></a></li>
        <li data-tab="signUpTab"><a><?php echo SIGN_UP_HEADING_TITLE;?></a></li>
        <li data-tab="passwordForgottenTab"><a><?php echo PASSWORD_FORGOTTEN_HEADING_TITLE;?></a></li>
    </ul>
    <div>
        <div id="signInTab">
            <div id="signInMessages">
            <?php
            if ($messageStack->size('login') > 0) {
                echo $messageStack->output('login');
            }?>
            </div>

                <div class="contentText">

                    <?php echo tep_draw_form('login', tep_href_link(LOGIN_URL, 'action=process', 'SSL'), 'post', 'id="signInForm"', true); ?>

                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                            <td class="fieldValue"><?php echo tep_draw_input_field('email_address'); ?></td>
                        </tr>
                        <tr>
                            <td class="fieldKey"><?php echo ENTRY_PASSWORD; ?></td>
                            <td class="fieldValue"><?php echo tep_draw_password_field('password'); ?></td>
                        </tr>
                    </table>


                    <p align="right">
                        <?php echo tep_draw_button(IMAGE_BUTTON_LOGIN, 'key', null, 'primary'); ?>
                        <img src="images/loader.gif" style="display: none;" class="login-loading"/>
                    </p>

                    </form>

                    <a href="<?=$authUrl?>"><img src="<?php echo DIR_WS_IMAGES . 'google/google-sign-in-btn.png'?>"/></a>
                    <div id='fb-login-btn1'></div>

                </div>
        </div>
        <div id="signUpTab">
        <?php
//        require('includes/form_check.js.php');
        ?>


        <div id="createAccountMessages">
        </div>

<!--        --><?php //echo tep_draw_form('create_account', tep_href_link(CREATE_ACCOUNT_URL, '', 'SSL'), 'post', 'onsubmit="return check_form(create_account);" id="createAccountForm"', true) . tep_draw_hidden_field('action', 'process'); ?>
        <?php echo tep_draw_form('create_account', tep_href_link(CREATE_ACCOUNT_URL, '', 'SSL'), 'post', 'id="createAccountForm"', true) . tep_draw_hidden_field('action', 'process'); ?>

        <div class="contentContainer">
        <div>
            <!--<span class="inputRequirement" style="float: right;"><?php echo FORM_REQUIRED_INFORMATION; ?></span>-->
<!--            <h2>--><?php //echo CATEGORY_PERSONAL; ?><!--</h2>-->
        </div>

        <div class="contentText">
            <table border="0" cellspacing="2" cellpadding="2" width="100%">

                <?php
                if (ACCOUNT_GENDER == 'true' && REG_GENDER == 'true') {
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_GENDER; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_radio_field('gender', 'm') . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('gender', 'f') . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . (tep_not_null(ENTRY_GENDER_TEXT) ? '<span class="inputRequirement">' . ENTRY_GENDER_TEXT . '</span>': ''); ?></td>
                    </tr>

                <?php
                }
                ?>
                <tr>
                    <td class="fieldKey"><?php echo ENTRY_FIRST_NAME; ?></td>
                    <td class="fieldValue"><?php echo tep_draw_input_field('firstname') . '&nbsp;' . (tep_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_FIRST_NAME_TEXT . '</span>': ''); ?></td>
                </tr>
                <?php
                if (ACCOUNT_LAST_NAME == 'true' && REG_LAST_NAME == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_LAST_NAME; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('lastname') . '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_LAST_NAME_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }

                if (ACCOUNT_DOB == 'true' && REG_DOB == 'true') {
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_DATE_OF_BIRTH; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('dob', '', 'id="dob"') . '&nbsp;' . (tep_not_null(ENTRY_DATE_OF_BIRTH_TEXT) ? '<span class="inputRequirement">' . ENTRY_DATE_OF_BIRTH_TEXT . '</span>': ''); ?><script type="text/javascript">$('#dob').datepicker({dateFormat: '<?php echo JQUERY_DATEPICKER_FORMAT; ?>', changeMonth: true, changeYear: true, yearRange: '-100:+0'});</script></td>
                    </tr>

                <?php
                }
                ?>

                <tr>
                    <td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                    <td class="fieldValue"><?php echo tep_draw_input_field('email_address','') . '&nbsp;' . (tep_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>': ''); ?></td>
                </tr>
            </table>
        </div>

        <?php
        if (ACCOUNT_COMPANY == 'true' && REG_COMPANY == 'true') {
            ?>

<!--            <h2>--><?php //echo CATEGORY_COMPANY; ?><!--</h2>-->

            <div class="contentText">
                <table border="0" cellspacing="2" cellpadding="2" width="100%">
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_COMPANY; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('company') . '&nbsp;' . (tep_not_null(ENTRY_COMPANY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COMPANY_TEXT . '</span>': ''); ?></td>
                    </tr>
                </table>
            </div>

        <?php
        }
        ?>

<!--        <h2>--><?php //echo CATEGORY_ADDRESS; ?><!--</h2>-->

        <div class="contentText">
            <table border="0" cellspacing="2" cellpadding="2" width="100%">
                <?php
                if (ACCOUNT_STREET_ADDRESS == 'true' && REG_STREET_ADDRESS == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_STREET_ADDRESS; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('street_address') . '&nbsp;' . (tep_not_null(ENTRY_STREET_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_STREET_ADDRESS_TEXT . '</span>': ''); ?></td>
                    </tr>

                <?php
                }

                if (ACCOUNT_SUBURB == 'true' && REG_SUBURB == 'true') {
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_SUBURB; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('suburb') . '&nbsp;' . (tep_not_null(ENTRY_SUBURB_TEXT) ? '<span class="inputRequirement">' . ENTRY_SUBURB_TEXT . '</span>': ''); ?></td>
                    </tr>

                <?php
                }

                if (ACCOUNT_POST_CODE == 'true' && REG_POST_CODE == 'true') {
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_POST_CODE; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('postcode') . '&nbsp;' . (tep_not_null(ENTRY_POST_CODE_TEXT) ? '<span class="inputRequirement">' . ENTRY_POST_CODE_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }

                if (ACCOUNT_CITY == 'true' && REG_CITY == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_CITY; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('city') . '&nbsp;' . (tep_not_null(ENTRY_CITY_TEXT) ? '<span class="inputRequirement">' . ENTRY_CITY_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }

                if (ACCOUNT_STATE == 'true' && REG_STATE == 'true') {
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_STATE; ?></td>
                        <td class="fieldValue">
                            <?php
                            /*if ($process == true) {
                                if ($entry_state_has_zones == true) {
                                    $zones_array = array();
                                    $zones_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' order by zone_name");
                                    while ($zones_values = tep_db_fetch_array($zones_query)) {
                                        $zones_array[] = array('id' => $zones_values['zone_name'], 'text' => $zones_values['zone_name']);
                                    }
                                    echo tep_draw_pull_down_menu('state', $zones_array);
                                } else {
                                    echo tep_draw_input_field('state');
                                }
                            } else {*/
                            echo tep_draw_input_field('state');
                            //}

                            if (tep_not_null(ENTRY_STATE_TEXT)) echo '&nbsp;<span class="inputRequirement">' . ENTRY_STATE_TEXT . '</span>';
                            ?>
                        </td>
                    </tr>

                <?php
                }

                if (ACCOUNT_COUNTRY == 'true' && REG_COUNTRY == 'true') {
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_COUNTRY; ?></td>
                        <td class="fieldValue"><?php echo tep_get_country_list('country') . '&nbsp;' . (tep_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COUNTRY_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }
                ?>
            </table>
        </div>

<!--        <h2>--><?php //echo CATEGORY_CONTACT; ?><!--</h2>-->

        <div class="contentText">
            <table border="0" cellspacing="2" cellpadding="2" width="100%">
                <?php
                if (ACCOUNT_TELEPHONE_NUMBER == 'true' && REG_TELEPHONE_NUMBER == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_TELEPHONE_NUMBER; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('telephone') . '&nbsp;' . (tep_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }

                if (ACCOUNT_FAX_NUMBER == 'true' && REG_FAX_NUMBER == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_FAX_NUMBER; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('fax') . '&nbsp;' . (tep_not_null(ENTRY_FAX_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_FAX_NUMBER_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }

                if (ACCOUNT_NEWSLETTER == 'true' && REG_NEWSLETTER == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_NEWSLETTER; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_checkbox_field('newsletter', '1') . '&nbsp;' . (tep_not_null(ENTRY_NEWSLETTER_TEXT) ? '<span class="inputRequirement">' . ENTRY_NEWSLETTER_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }
                ?>
            </table>
        </div>

<!--        <h2>--><?php //echo CATEGORY_PASSWORD; ?><!--</h2>-->

        <div class="contentText">
            <table border="0" cellspacing="2" cellpadding="2" width="100%">
                <tr>
                    <td class="fieldKey"><?php echo ENTRY_PASSWORD; ?></td>
                    <td class="fieldValue"><?php echo tep_draw_password_field('password') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_TEXT) ? '<span class="inputRequirement">' . ENTRY_PASSWORD_TEXT . '</span>': ''); ?></td>
                </tr>
                <?php
                if (ACCOUNT_PASSWORD_CONFIRMATION == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_PASSWORD_CONFIRMATION; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_password_field('confirmation') . '&nbsp;' . (tep_not_null(ENTRY_PASSWORD_CONFIRMATION_TEXT) ? '<span class="inputRequirement">' . ENTRY_PASSWORD_CONFIRMATION_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }
                ?>
            </table>
        </div>

        <div align='right' class="buttonSet">
            <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_SIGN_UP, 'person', null, 'primary'); ?></span>
            <img src="images/loader.gif" style="display: none;" class="login-loading"/>
        </div>

        <a href="<?=$authUrl?>"><img src="<?php echo DIR_WS_IMAGES . 'google/google-sign-in-btn.png'?>"/></a>
        <div id='fb-login-btn2'></div>
        </div>
        </form>
        </div>
        <div id="passwordForgottenTab">

            <div id="passwordResetMessages"></div>

            <div id="pass_res_init">
                <?php echo TEXT_PASSWORD_RESET_INITIATED; ?>
            </div>

            <?php echo tep_draw_form('password_forgotten', tep_href_link(PASSWORD_FORGOTTEN_URL, 'action=process', 'SSL'), 'post', 'id="passwordResetForm"', true); ?>



            <div><?php echo TEXT_MAIN; ?></div>

            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                <tr>
                    <td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                    <td class="fieldValue"><?php echo tep_draw_input_field('email_address'); ?></td>
                </tr>
            </table>
            <div class="buttonSet">
                <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', null, 'primary'); ?></span>
            </div>

            </form>
        </div>
    </div>
</div>