<?php
require('includes/form_check.js.php');
?>
<script>
	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	$(function() {
		$('input[name="email_address"]').bind('input', function() {
			if (validateEmail(this.value)) {
				$('#mail-required').fadeOut();
			} else {
				$('#mail-required').fadeIn();
			}
		});
	});
</script>

<h1><?php echo HEADING_TITLE; ?></h1>

<?php
if ($messageStack->size('account_edit') > 0) {
    echo $messageStack->output('account_edit');
}
?>

<?php echo tep_draw_form('account_edit', tep_href_link(ACCOUNT_EDIT_URL, '', 'SSL'), 'post', 'onsubmit="return check_form(account_edit);"', true) . tep_draw_hidden_field('action', 'process'); ?>

<div class="contentContainer">
    <div>
        <div class="inputRequirement" style="float: right;"><?php echo FORM_REQUIRED_INFORMATION; ?></div>
        <h2><?php echo MY_ACCOUNT_TITLE; ?></h2>
    </div>

	<span id="mail-required" style="display: none; background-color: rgba(170, 0, 23, 0.84); color: #ffffff;"><?php echo ENTRY_EMAIL_ADDRESS_CHECK_ERROR; ?></span>

    <div class="contentText">
        <table border="0" cellspacing="2" cellpadding="2" width="100%">
            <?php
            if ($PARTNER) {
                /*if (ACCOUNT_GENDER == 'true') {
                    if (isset($gender)) {
                        $male = ($gender == 'm') ? true : false;
                    } else {
                        $male = ($account['customers_gender'] == 'm') ? true : false;
                    }
                    $female = !$male;
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_GENDER; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_radio_field('gender', 'm', $male) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('gender', 'f', $female) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . (tep_not_null(ENTRY_GENDER_TEXT) ? '<span class="inputRequirement">' . ENTRY_GENDER_TEXT . '</span>': ''); ?></td>
                    </tr>

                <?php
                }*/
                ?>

                <tr>
                    <td class="fieldKey"><?php echo ENTRY_FIRST_NAME; ?></td>
                    <td class="fieldValue"><?php echo tep_draw_input_field('firstname', $account['partners_firstname']) . '&nbsp;' . (tep_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_FIRST_NAME_TEXT . '</span>': ''); ?></td>
                </tr>
                <?php
                if (ACCOUNT_LAST_NAME == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_LAST_NAME; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('lastname', $account['partners_lastname']) . '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_LAST_NAME_TEXT . '</span>': ''); ?></td>
                    </tr>

                <?php
                }
                /*if (ACCOUNT_DOB == 'true') {
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_DATE_OF_BIRTH; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('dob', tep_date_short($account['customers_dob']), 'id="dob"') . '&nbsp;' . (tep_not_null(ENTRY_DATE_OF_BIRTH_TEXT) ? '<span class="inputRequirement">' . ENTRY_DATE_OF_BIRTH_TEXT . '</span>': ''); ?><script type="text/javascript">$('#dob').datepicker({dateFormat: '<?php echo JQUERY_DATEPICKER_FORMAT; ?>', changeMonth: true, changeYear: true, yearRange: '-100:+0'});</script></td>
                    </tr>

                <?php
                }*/
                ?>

                <tr>
                    <td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                    <td class="fieldValue"><?php echo tep_draw_input_field('email_address', $account['partners_email_address']) . '&nbsp;' . (tep_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>': ''); ?></td>
                </tr>
                <?php

            } else {
                if (ACCOUNT_GENDER == 'true') {
                    if (isset($gender)) {
                        $male = ($gender == 'm') ? true : false;
                        $female = ($gender == 'f') ? true : false;
                    } else {
                        $male = ($account['customers_gender'] == 'm') ? true : false;
                        $female = ($account['customers_gender'] == 'f') ? true : false;
                    }
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_GENDER; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_radio_field('gender', 'm', $male) . '&nbsp;&nbsp;' . MALE . '&nbsp;&nbsp;' . tep_draw_radio_field('gender', 'f', $female) . '&nbsp;&nbsp;' . FEMALE . '&nbsp;' . (tep_not_null(ENTRY_GENDER_TEXT) ? '<span class="inputRequirement">' . ENTRY_GENDER_TEXT . '</span>': ''); ?></td>
                    </tr>

                <?php
                }
                ?>

                <tr>
                    <td class="fieldKey"><?php echo ENTRY_FIRST_NAME; ?></td>
                    <td class="fieldValue"><?php echo tep_draw_input_field('firstname', $account['customers_firstname']) . '&nbsp;' . (tep_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_FIRST_NAME_TEXT . '</span>': ''); ?></td>
                </tr>
                <?php
                if (ACCOUNT_LAST_NAME == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_LAST_NAME; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('lastname', $account['customers_lastname']) . '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_LAST_NAME_TEXT . '</span>': ''); ?></td>
                    </tr>

                <?php
                }
                if (ACCOUNT_DOB == 'true') {
                    ?>

                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_DATE_OF_BIRTH; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('dob', tep_date_short($account['customers_dob']), 'id="dob"') . '&nbsp;' . (tep_not_null(ENTRY_DATE_OF_BIRTH_TEXT) ? '<span class="inputRequirement">' . ENTRY_DATE_OF_BIRTH_TEXT . '</span>': ''); ?><script type="text/javascript">$('#dob').datepicker({dateFormat: '<?php echo JQUERY_DATEPICKER_FORMAT; ?>', changeMonth: true, changeYear: true, yearRange: '-100:+0'});</script></td>
                    </tr>

                <?php
                }
                ?>

                <tr>
                    <td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                    <td class="fieldValue"><?php echo tep_draw_input_field('email_address', $account['customers_email_address']) . '&nbsp;' . (tep_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>': ''); ?></td>
                </tr>
                <?php
                if (ACCOUNT_TELEPHONE_NUMBER == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_TELEPHONE_NUMBER; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('telephone', $account['customers_telephone']) . '&nbsp;' . (tep_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }

                if (ACCOUNT_FAX_NUMBER == 'true') {
                    ?>
                    <tr>
                        <td class="fieldKey"><?php echo ENTRY_FAX_NUMBER; ?></td>
                        <td class="fieldValue"><?php echo tep_draw_input_field('fax', $account['customers_fax']) . '&nbsp;' . (tep_not_null(ENTRY_FAX_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_FAX_NUMBER_TEXT . '</span>': ''); ?></td>
                    </tr>
                <?php
                }
            }
            ?>
        </table>

        <br />

        <div class="buttonSet">
            <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', null, 'primary'); ?></span>

            <?php echo tep_draw_button(IMAGE_BUTTON_BACK, 'triangle-1-w', tep_href_link(ACCOUNT_URL, '', 'SSL')); ?>
        </div>
    </div>
</div>

</form>