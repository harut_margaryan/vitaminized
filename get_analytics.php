<?php

empty($_GET['for']) && exit;
$for = $_GET['for'];
require('includes/global_config.php');
!tep_session_is_registered('admin') && !($PARTNER && tep_session_is_registered('partner_id')) && tep_exit();
require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_GET_ANALYTICS);
switch($for) {
    case 'visits':
        $result = '{}';
        $date_filter = '';
        if (isset($_GET['start']) && isset($_GET['end']) && is_numeric($_GET['start']) && is_numeric($_GET['end']) && $_GET['start'] <= $_GET['end']) {
            $start_date = intval($_GET['start'] / 1000 - 24 * 3600);
            $end_date = intval($_GET['end'] / 1000 + 24 * 3600);

            $start_date = strtotime(date('Y-m-d', $start_date));
            $end_date = strtotime(date('Y-m-d', $end_date));

            $range = $end_date - $start_date;

            $date_filter = ' and v.first_online > "' . date('Y-m-d', $start_date) . '" and v.first_online < "' . date('Y-m-d', $end_date) . '"';
            if ($range > 12 * 30 * 24 * 3600) { // 12 months
                $date_format = "%Y-%m-01 00:00:00";
	            $by = 'months';
            } else {
	            $date_format = "%Y-%m-%d 00:00:00";
	            $by = 'days';
            }
        } else {
	        $date_format = "%Y-%m-01 00:00:00";
	        $by = 'months';
        }

        $visits_query = tep_db_query('SELECT UNIX_TIMESTAMP(DATE_FORMAT(v.first_online, "' . $date_format . '")) AS visits_date, COUNT(DISTINCT v.visit_id) AS visits,  COUNT(DISTINCT ph.visit_id) AS buys FROM ' . TABLE_VISITS . ' AS v LEFT JOIN ' . TABLE_PARTNERS_HISTORY . ' AS ph ON(ph.visit_id = v.visit_id) WHERE v.partners_id = ' . $partner_id . $date_filter . ' GROUP BY visits_date ORDER BY visits_date ASC');
        if ($rows = tep_db_num_rows($visits_query)) {
            $js_data_visits = '[';
            $js_data_purchases = '[';
            while($visit = tep_db_fetch_array($visits_query)) {
	            $prev_date = floatval($visit['visits_date']);
                $js_data_visits .= '[' . ($prev_date * 1000) . ',' . $visit['visits'] . '],';
                $js_data_purchases .= '[' . ($prev_date * 1000) . ',' . $visit['buys'] . '],';
            }

	        switch ($by) {
		        case 'months':
			        $prev_date += 32 * 3600 * 24;
			        $prev_date = strtotime(date('Y-m-01', $prev_date));
			        break;
		        case 'days':
			        $prev_date += 3600 * 24;
			        break;
	        }

	        $js_data_visits .= '[' . ($prev_date * 1000) . ',' . 0 . '],';
	        $js_data_purchases .= '[' . ($prev_date * 1000) . ',' . 0 . '],';

            $js_data_visits[strlen($js_data_visits) - 1] = ']';
            $js_data_purchases[strlen($js_data_purchases) - 1] = ']';

            $result = '{"title":"' . TEXT_VISITS_TITLE . '","series":[{"name":"' . TEXT_VISITS . '","data":' . $js_data_visits . '},{"name":"' . TEXT_BUYS . '","data":' . $js_data_purchases . '}]}';
        }

        if (isset($_GET['callback'])) {
            $result = $_GET['callback'] . '(' . $result . ');';
        }
        exit($result);
        break;

    case 'methods':
        $res = '{}';
        $methods_query = tep_db_query('SELECT/* IFNULL(v.method_id, 0) AS method_id,*/ COUNT(DISTINCT ph.visit_id) AS buys,  COUNT(DISTINCT v.visit_id) AS visits, IFNULL(mi.method_name, "' . METHOD_NAME_OTHER . '") AS method_name FROM ' . TABLE_VISITS . ' AS v LEFT JOIN ' . TABLE_MARKETING_METHODS_INFO . ' AS mi ON(v.method_id = mi.method_id AND mi.language_id = ' . (int)$languages_id . ') LEFT JOIN ' . TABLE_PARTNERS_HISTORY . ' AS ph ON(ph.visit_id = v.visit_id) WHERE v.partners_id = ' . (int)$partner_id . ' GROUP BY (v.method_id)');
        if ($rows = tep_db_num_rows($methods_query)) {
            $js_data_visits = '[';
            $js_data_purchases = '[';
            $js_data_methods = '[';
            while($method = tep_db_fetch_array($methods_query)) {
                $js_data_visits .= '{"name":"' . $method['method_name'] . '","y":' . $method['visits'] . '},';
                $js_data_purchases .= '{"name":"' . $method['method_name'] . '","y":' . $method['buys'] . '},';
                $js_data_methods .= '"' . $method['method_name'] . '",';
            }

            $js_data_methods[strlen($js_data_methods) - 1] = ']';
            $js_data_visits[strlen($js_data_visits) - 1] = ']';
            $js_data_purchases[strlen($js_data_purchases) - 1] = ']';
            $result = '{"title":"' . TEXT_METHODS_TITLE . '","series":[{"name":"' . TEXT_VISITS . '","data":' . $js_data_visits . '},{"name":"' . TEXT_BUYS . '","data":' . $js_data_purchases . '}],"categories":' . $js_data_methods . '}';
        }
        if (isset($_GET['callback'])) {
            $result = $_GET['callback'] . '(' . $result . ');';
        }
        exit($result);
        break;

    case 'products':
        $result = '{}';

        $products_query = tep_db_query(
            'SELECT ' .
                'v.products_id, ' .
                'v.products_name, ' .
                'v.visits, ' .
                'b.buys ' .
            'FROM (' .
                'SELECT ' .
                    'pd.products_id, ' .
                    'pd.products_name, ' .
                    'COUNT(DISTINCT v.visit_id) AS visits ' .
                'FROM ' . TABLE_PRODUCTS_DESCRIPTION . ' AS pd ' .
                'LEFT JOIN ' . TABLE_VISITS . ' AS v ' .
                    'ON(' .
                        'v.partners_id = ' . (int)$partner_id . ' ' .
                        'AND pd.products_id = v.products_id' .
                    ') ' .
                'WHERE ' .
                    'pd.language_id = ' . (int)$languages_id . ' ' .
                'GROUP BY(pd.products_id)' .
            ') AS v ' .
            'LEFT JOIN (' .
                'SELECT ' .
                    'op.products_id, ' .
                    'SUM(op.products_quantity) AS buys ' .
                'FROM ' . TABLE_PARTNERS_HISTORY . ' AS ph ' .
                'JOIN ' . TABLE_ORDERS_PRODUCTS . ' AS op ' .
                    'ON(' .
                        'ph.orders_id = op.orders_id ' .
                        'AND ph.partners_id = ' . (int)$partner_id .
                    ') ' .
                'GROUP BY(op.products_id)' .
            ') AS b ' .
                'ON(v.products_id = b.products_id) ' .
            'WHERE (' .
                'v.visits > 0 ' .
                'OR b.buys > 0' .
            ')'
        );

        if ($rows = tep_db_num_rows($products_query)) {
            $js_data_visits = '[';
            $js_data_purchases = '[';
            $js_data_products = '[';
            while ($products = tep_db_fetch_array($products_query)) {
                $js_data_visits .= '{"name":"' . $products['products_name'] . '","y":' . $products['visits'] . '},';
                $js_data_purchases .= '{"name":"' . $products['products_name'] . '","y":' . $products['buys'] . '},';
                $js_data_products .= '"' . $products['products_name'] . '",';
            }

            $js_data_products[strlen($js_data_products) - 1] = ']';
            $js_data_visits[strlen($js_data_visits) - 1] = ']';
            $js_data_purchases[strlen($js_data_purchases) - 1] = ']';

            $result = '{"title":"' . TEXT_PRODUCTS_TITLE . '","series":[{"name":"' . TEXT_VISITS . '","data":' . $js_data_visits . '},{"name":"' . TEXT_BUYS . '","data":' . $js_data_purchases . '}],"categories":' . $js_data_products . '}';
        }

        if (isset($_GET['callback'])) {
            $result = $_GET['callback'] . '(' . $result . ');';
        }
        exit($result);
        break;

    case 'method':
        $result = '{}';
        if (isset($_GET['m']) && is_numeric($_GET['m'])) {
            if ($_GET['m'] == 0) {
                $filter = ' AND v.method_id IS NULL';
            } else {
                $filter = ' AND v.method_id = ' . (int)$_GET['m'];
            }
            if (isset($_GET['start']) && isset($_GET['end']) && is_numeric($_GET['start']) && is_numeric($_GET['end']) && $_GET['start'] <= $_GET['end']) {
                $start_date = intval($_GET['start'] / 1000 - 24 * 3600);
                $end_date = intval($_GET['end'] / 1000 + 24 * 3600);

                $start_date = strtotime(date('Y-m-d', $start_date));
                $end_date = strtotime(date('Y-m-d', $end_date));

                $range = $end_date - $start_date;

                $filter .= ' and v.first_online > "' . date('Y-m-d', $start_date) . '" and v.first_online < "' . date('Y-m-d', $end_date) . '"';
	            if ($range > 12 * 30 * 24 * 3600) { // 12 months
		            $date_format = "%Y-%m-01 00:00:00";
		            $by = 'months';
	            } else {
		            $date_format = "%Y-%m-%d 00:00:00";
		            $by = 'days';
	            }
            } else {
	            $date_format = "%Y-%m-01 00:00:00";
	            $by = 'months';
            }

            $visits_query = tep_db_query('SELECT UNIX_TIMESTAMP(DATE_FORMAT(v.first_online, "' . $date_format . '")) AS visits_date, COUNT(DISTINCT v.visit_id) AS visits,  COUNT(DISTINCT ph.visit_id) AS buys FROM ' . TABLE_VISITS . ' AS v LEFT JOIN ' . TABLE_PARTNERS_HISTORY . ' AS ph ON(ph.visit_id = v.visit_id) WHERE v.partners_id = ' . $partner_id . $filter . ' GROUP BY visits_date ORDER BY visits_date ASC');
            if ($rows = tep_db_num_rows($visits_query)) {
                $js_data_visits = '[';
                $js_data_purchases = '[';

	            while ($visit = tep_db_fetch_array($visits_query)) {
		            $prev_date = floatval($visit['visits_date']);
		            $js_data_visits .= '[' . ($prev_date * 1000) . ',' . $visit['visits'] . '],';
		            $js_data_purchases .= '[' . ($prev_date * 1000) . ',' . $visit['buys'] . '],';
	            }

	            switch ($by) {
		            case 'months':
			            $prev_date += 32 * 3600 * 24;
			            $prev_date = strtotime(date('Y-m-01', $prev_date));
			            break;
		            case 'days':
			            $prev_date += 3600 * 24;
			            break;
	            }


	            $js_data_visits .= '[' . ($prev_date * 1000) . ',' . 0 . '],';
	            $js_data_purchases .= '[' . ($prev_date * 1000) . ',' . 0 . '],';

                $js_data_visits[strlen($js_data_visits) - 1] = ']';
                $js_data_purchases[strlen($js_data_purchases) - 1] = ']';

                $result = '{"series":[{"name":"' . TEXT_VISITS . '","data":' . $js_data_visits . '},{"name":"' . TEXT_BUYS . '","data":' . $js_data_purchases . '}]}';
            }
        }

        if (isset($_GET['callback'])) {
            $result = $_GET['callback'] . '(' . $result . ');';
        }
        exit($result);
        break;

    case 'product':
        $result = '{}';
        if (isset($_GET['p']) && is_numeric($_GET['p'])) {
            $visits_filter = '';
            $buys_filter = '';
            if (isset($_GET['start']) && isset($_GET['end']) && is_numeric($_GET['start']) && is_numeric($_GET['end']) && $_GET['start'] <= $_GET['end']) {
                $start_date = intval($_GET['start'] / 1000 - 24 * 3600);
                $end_date = intval($_GET['end'] / 1000 + 24 * 3600);

                $start_date = strtotime(date('Y-m-d', $start_date));
                $end_date = strtotime(date('Y-m-d', $end_date));

                $range = $end_date - $start_date;

                $visits_filter .= ' and v.first_online > "' . date('Y-m-d', $start_date) . '" and v.first_online < "' . date('Y-m-d', $end_date) . '"';
                $buys_filter .= ' and ph.date_added > "' . date('Y-m-d', $start_date) . '" and ph.date_added < "' . date('Y-m-d', $end_date) . '"';
	            if ($range > 12 * 30 * 24 * 3600) { // 12 months
		            $date_format = "%Y-%m-01 00:00:00";
		            $by = 'months';
	            } else {
		            $date_format = "%Y-%m-%d 00:00:00";
		            $by = 'days';
	            }
            } else {
	            $date_format = "%Y-%m-01 00:00:00";
	            $by = 'months';
            }

            $visits_query =
                'SELECT ' .
                    'UNIX_TIMESTAMP(' .
                        'DATE_FORMAT(' .
                            'v.first_online, ' .
                            '"' . $date_format . '"' .
                        ')' .
                    ') AS date_col, ' .
                    'COUNT(DISTINCT v.visit_id) AS visits ' .
                'FROM ' . TABLE_VISITS . ' AS v ' .
                'WHERE ' .
                    'v.partners_id = ' . (int)$partner_id . ' ' .
                    'AND v.products_id = ' . (int)$_GET['p'] . ' ' .
                    $visits_filter . ' ' .
                'GROUP BY date_col';

            $buys_query =
                'SELECT ' .
                    'UNIX_TIMESTAMP(' .
                        'DATE_FORMAT(' .
                            'ph.date_added, ' .
                            '"' . $date_format . '"' .
                        ')' .
                    ') AS date_col, ' .
                    'SUM(op.products_quantity) AS buys ' .
                'FROM ' . TABLE_PARTNERS_HISTORY . ' AS ph ' .
                'JOIN ' . TABLE_ORDERS_PRODUCTS . ' AS op ' .
                    'ON(' .
                        'ph.orders_id = op.orders_id ' .
                        'AND ph.partners_id = ' . (int)$partner_id . ' ' .
                        'AND op.products_id = ' . (int)$_GET['p'] .
                        $buys_filter . ' ' .
                    ') ' .
                'JOIN ' . TABLE_ORDERS . ' AS o ' .
                    'ON(o.orders_id = op.orders_id) ' .
                'GROUP BY date_col';


            $analytic_query = tep_db_query(
                '(' .
                    'SELECT ' .
                         'v.date_col, ' .
                         'v.visits, ' .
                         'b.buys ' .
                     'FROM (' . $visits_query . ') AS v ' .
                     'LEFT JOIN (' . $buys_query . ') AS b ' .
                         'ON (v.date_col = b.date_col) ' .
                ') UNION ALL (' .
                    'SELECT b.date_col, v.visits, b.buys ' .
                    'FROM (' . $visits_query . ') AS v ' .
                    'RIGHT JOIN (' . $buys_query . ') AS b ' .
                        'ON (v.date_col = b.date_col) ' .
                    'WHERE v.date_col IS NULL ' .
                ') ' .
                'ORDER BY date_col ASC'
            );

            if ($rows = tep_db_num_rows($analytic_query)) {
                $js_data_visits = '[';
                $js_data_purchases = '[';

	            while ($visit = tep_db_fetch_array($analytic_query)) {
		            $prev_date = floatval($visit['date_col']);
		            $js_data_visits .= '[' . ($prev_date * 1000) . ',' . intval($visit['visits']) . '],';
		            $js_data_purchases .= '[' . ($prev_date * 1000) . ',' . intval($visit['buys']) . '],';
	            }

	            switch ($by) {
		            case 'months':
			            $prev_date += 32 * 3600 * 24;
			            $prev_date = strtotime(date('Y-m-01', $prev_date));
			            break;
		            case 'days':
			            $prev_date += 3600 * 24;
			            break;
	            }


	            $js_data_visits .= '[' . ($prev_date * 1000) . ',' . 0 . '],';
	            $js_data_purchases .= '[' . ($prev_date * 1000) . ',' . 0 . '],';

                $js_data_visits[strlen($js_data_visits) - 1] = ']';
                $js_data_purchases[strlen($js_data_purchases) - 1] = ']';

                $result = '{"series":[{"name":"' . TEXT_VISITS . '","data":' . $js_data_visits . '},{"name":"' . TEXT_BUYS . '","data":' . $js_data_purchases . '}]}';
            }
        }

        if (isset($_GET['callback'])) {
            $result = $_GET['callback'] . '(' . $result . ');';
        }
        exit($result);
        break;

    case 'material':
        $result = '{}';
        if (!empty($_GET['m']) && is_numeric($_GET['m']) && isset($_GET['t']) && in_array($_GET['t'], array('method')) && isset($_GET['n']) && is_numeric($_GET['n'])) {
            switch($_GET['t']) {
                case 'method':
                    $date_filter = '';
                    if (isset($_GET['start']) && isset($_GET['end']) && is_numeric($_GET['start']) && is_numeric($_GET['end']) && $_GET['start'] <= $_GET['end']) {
                        $start_date = intval($_GET['start'] / 1000 - 24 * 3600);
                        $end_date = intval($_GET['end'] / 1000 + 24 * 3600);

                        $start_date = strtotime(date('Y-m-d', $start_date));
                        $end_date = strtotime(date('Y-m-d', $end_date));

                        $range = $end_date - $start_date;

                        $date_filter = ' and v.first_online > "' . date('Y-m-d', $start_date) . '" and v.first_online < "' . date('Y-m-d', $end_date) . '"';
	                    if ($range > 12 * 30 * 24 * 3600) { // 12 months
		                    $date_format = "%Y-%m-01 00:00:00";
		                    $by = 'months';
	                    } else {
		                    $date_format = "%Y-%m-%d 00:00:00";
		                    $by = 'days';
	                    }
                    } else {
	                    $date_format = "%Y-%m-01 00:00:00";
	                    $by = 'months';
                    }

                    $visits_query = tep_db_query('SELECT UNIX_TIMESTAMP(DATE_FORMAT(v.first_online, "' . $date_format . '")) AS visits_date, COUNT(DISTINCT v.visit_id) AS visits,  COUNT(DISTINCT ph.visit_id) AS buys FROM ' . TABLE_VISITS . ' AS v LEFT JOIN ' . TABLE_PARTNERS_HISTORY . ' AS ph ON(ph.visit_id = v.visit_id) WHERE v.partners_id = ' . $partner_id . ' and v.method_id = ' . (int)$_GET['n'] . ' and v.material_id = ' . (int)$_GET['m'] . $date_filter . ' GROUP BY visits_date ORDER BY visits_date ASC');
                    if ($rows = tep_db_num_rows($visits_query)) {
                        $js_data_visits = '[';
                        $js_data_purchases = '[';

	                    while ($visit = tep_db_fetch_array($visits_query)) {
		                    $prev_date = floatval($visit['visits_date']);
		                    $js_data_visits .= '[' . ($prev_date * 1000) . ',' . $visit['visits'] . '],';
		                    $js_data_purchases .= '[' . ($prev_date * 1000) . ',' . $visit['buys'] . '],';
	                    }

	                    switch ($by) {
		                    case 'months':
			                    $prev_date += 32 * 3600 * 24;
			                    $prev_date = strtotime(date('Y-m-01', $prev_date));
			                    break;
		                    case 'days':
			                    $prev_date += 3600 * 24;
			                    break;
	                    }


	                    $js_data_visits .= '[' . ($prev_date * 1000) . ',' . 0 . '],';
	                    $js_data_purchases .= '[' . ($prev_date * 1000) . ',' . 0 . '],';

                        $js_data_visits[strlen($js_data_visits) - 1] = ']';
                        $js_data_purchases[strlen($js_data_purchases) - 1] = ']';

                        $result = '{"series":[{"name":"' . TEXT_VISITS . '","data":' . $js_data_visits . '},{"name":"' . TEXT_BUYS . '","data":' . $js_data_purchases . '}]}';
                    }
                    break;
            }
        }
        if (isset($_GET['callback'])) {
            $result = $_GET['callback'] . '(' . $result . ');';
        }
        exit($result);
        break;

    case 'aff_hist':
        $result = '{}';
        $date_filter = '';
        if (isset($_GET['start']) && isset($_GET['end']) && is_numeric($_GET['start']) && is_numeric($_GET['end']) && $_GET['start'] <= $_GET['end']) {
            $start_date = intval($_GET['start'] / 1000 - 24 * 3600);
            $end_date = intval($_GET['end'] / 1000 + 24 * 3600);

            $start_date = strtotime(date('Y-m-d', $start_date));
            $end_date = strtotime(date('Y-m-d', $end_date));

            $range = $end_date - $start_date;

            $date_filter = ' and date_added > "' . date('Y-m-d', $start_date) . '" and date_added < "' . date('Y-m-d', $end_date) . '"';
	        if ($range > 12 * 30 * 24 * 3600) { // 12 months
		        $date_format = "%Y-%m-01 00:00:00";
		        $by = 'months';
	        } else {
		        $date_format = "%Y-%m-%d 00:00:00";
		        $by = 'days';
	        }
        } else {
	        $date_format = "%Y-%m-01 00:00:00";
	        $by = 'months';
        }

        $anltc_query = tep_db_query('SELECT UNIX_TIMESTAMP(DATE_FORMAT(date_added, "' . $date_format . '")) AS date_col, SUM(affiliates_share) AS total_share, count(orders_id) as orders FROM ' . TABLE_PARTNERS_HISTORY . ' WHERE partners_id = ' . $partner_id . $date_filter . ' GROUP BY date_col ORDER BY date_col ASC');
        if ($rows = tep_db_num_rows($anltc_query)) {
            $js_data_total = '[';
            while($anltc = tep_db_fetch_array($anltc_query)) {
	            $prev_date = floatval($anltc['date_col']);
                $js_data_total .= '{"x":' . $prev_date * 1000 . ',"y":' . round($anltc['total_share'], 2) . ',"orders":' . $anltc['orders'] . '},';
            }

	        switch ($by) {
		        case 'months':
			        $prev_date += 32 * 3600 * 24;
			        $prev_date = strtotime(date('Y-m-01', $prev_date));
			        break;
		        case 'days':
			        $prev_date += 3600 * 24;
			        break;
	        }

	        $js_data_total .= '{"x":' . $prev_date * 1000 . ',"y":' . 0 . ',"orders":' . 0 . '},';

            $js_data_total[strlen($js_data_total) - 1] = ']';

            $tooltip = '{"pointFormat": "{series.name}: <b>{point.y}$</b>({point.orders} ' . TEXT_ORDERS . ')<br/>"}';
            $result = '{"title":"' . TEXT_AFF_HIST_TITLE . '","series":[{"name":"' . TEXT_SHARE . '","data":' . $js_data_total . '}],"tooltip":' . $tooltip . '}';
        }

        if (isset($_GET['callback'])) {
            $result = $_GET['callback'] . '(' . $result . ');';
        }
        exit($result);
        break;
}