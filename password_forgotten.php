<h1><?php echo HEADING_TITLE_PASSWORD_FORGOTTEN; ?></h1>

<div id="passwordResetMessages"></div>

<div id="pass_res_init">
    <?php echo TEXT_PASSWORD_RESET_INITIATED; ?>
</div>

<?php echo tep_draw_form('password_forgotten', tep_href_link(PASSWORD_FORGOTTEN_URL, 'action=process', 'SSL'), 'post', 'id="passwordResetForm"', true); ?>



<div><?php echo TEXT_MAIN; ?></div>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
        <td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
        <td class="fieldValue"><?php echo tep_draw_input_field('email_address'); ?></td>
    </tr>
</table>
<div class="buttonSet">
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', null, 'primary'); ?></span>
</div>

</form>