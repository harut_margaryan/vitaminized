<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Marketing Materials');
define('HEADING_TITLE', 'Marketing Materials');
define('TEXT_SEND_MAIL', 'send mail');
define('TEXT_MAIL_SENT', 'mail sent');
define('TEXT_MAIL_NOT_SENT', 'mail is not send');