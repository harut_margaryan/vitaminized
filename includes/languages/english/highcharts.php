<?php
define('CHART_CONTEXT_BUTTON_TITLE', 'Chart context menu');
define('CHART_DECIMAL_POINT', '.');
define('CHART_DOWNLOAD_JPEG', 'Download JPEG image');
define('CHART_DOWNLOAD_PDF', 'Download PDF document');
define('CHART_DOWNLOAD_PNG', 'Download PNG image');
define('CHART_DOWNLOAD_SVG', 'Download SVG vector image');
define('CHART_DRILL_UP_TEXT', 'Back to {series.name}');
define('CHART_LOADING', 'Loading...');
define('CHART_MONTHS', '[ "January" , "February" , "March" , "April" , "May" , "June" , "July" , "August" , "September" , "October" , "November" , "December"]');
define('CHART_NUMERIC_SYMBOLS', '[ "k" , "M" , "G" , "T" , "P" , "E"]');
define('CHART_PRINT_CHART', 'Print chart');
define('CHART_RESET_ZOOM', 'Reset zoom');
define('CHART_RESET_ZOOM_TITLE', 'Reset zoom level 1:1');
define('CHART_SHORT_MONTHS', '[ "Jan" , "Feb" , "Mar" , "Apr" , "May" , "Jun" , "Jul" , "Aug" , "Sep" , "Oct" , "Nov" , "Dec"]');
define('CHART_THOUSANDS_SEP', ',');
define('CHART_WEEKDAYS', '["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]');
define('CHART_DATE_FROM', 'From');
define('CHART_DATE_TO', 'To');