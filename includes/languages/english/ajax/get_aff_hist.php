<?php
define('TEXT_NO_DATA', 'There are no history to view');
define('HISTORY_HEAD_ORDER_ID', 'Order ID');
define('HISTORY_HEAD_RECEIVED', 'Received');
define('HISTORY_HEAD_DATE', 'Date');
define('HISTORY_HEAD_SHARE', 'Share');
define('HISTORY_HEAD_COMMISSION', 'Commission');
define('HISTORY_HEAD_PRODUCTS', 'Products');