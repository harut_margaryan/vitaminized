<?php

define('NAVBAR_TITLE', 'Partners Analytic');
define('HEADING_TITLE', 'Partners Analytic');
define('TOTAL_SHARE', 'Total Share');
define('DATETIME_HEADING' , 'datetime');
define('PERCENT_HEADING' , 'percentage');
define('REFERER_HEADING' , 'referer');

define('TEXT_IN_DETAIL', 'In detail');
define('METHOD_NAME_OTHER', 'Other');
define('TEXT_NO_MATERIALS', 'There are no materials');
define('TEXT_NO_ANALYTIC', 'There are no analytic data');
define('MATERIAL_HEAD_TITLE', 'Material Title');
define('MATERIAL_HEAD_VISITS', 'Material Visits');
define('MATERIAL_HEAD_PURCHASES', 'Material Purchases');

//highcharts