<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Marketing Materials');
define('HEADING_TITLE', 'Marketing Materials');

define('TEXT_MARKETING_METHOD' , 'Method');
define('TEXT_MARKETING_PRODUCT' , 'Product');
define('TEXT_PRODUCT_GLOBAL' , 'Global');
define('TEXT_MARKETING_LANGUAGE' , 'Language');
define('TEXT_MARKETING_TAGS' , 'Tags');
define('TEXT_MARKETING_FILTER' , 'Filter');
