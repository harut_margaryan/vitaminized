<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
define('METHOD_NAME_OTHER', 'Other');
define('TEXT_VISITS', 'Visits');
define('TEXT_SHARE', 'Share');
define('TEXT_ORDERS', 'orders');
define('TEXT_BUYS', 'Purchases');
define('TEXT_VISITS_TITLE', 'Visits');
define('TEXT_AFF_HIST_TITLE', 'Affiliates History');
define('TEXT_METHODS_TITLE', 'Methods');
define('TEXT_PRODUCTS_TITLE', 'Products');