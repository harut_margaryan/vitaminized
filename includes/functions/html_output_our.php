<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 8/9/13
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates. *
 * */
function tep_htmlentities_decode($text) {
    $text = str_replace(
        array(
            '&quot;','&#039;','&#37;','&euro;','&amp;','&lt;','&gt;','&laquo;','&raquo;','&lsquo;','&rsquo;','&ldquo;','&rdquo;','&bdquo;','&Prime;','&not;','&ndash;','&#58;','&#43;','&#45;','&nbsp;','&ordf;','&bull;','&deg;','&reg;','&mdash;',
        ),
        array(
            '"',"'",'%','€','&','<','>','«','»','‘','’','"','"','"','"','','-',':','+','-',' ','`','.','','','-',
        ),
        $text );
    return html_entity_decode($text);
}

function tep_mark_search_string($str, $keywords) {
    $replaced_str = $str;
    if (is_array($keywords) && tep_not_null($keywords)) {
        uasort($keywords,
            create_function(
                '$a, $b',
                'return (mb_strlen($b) - mb_strlen($a));'
            )
        );

        $replaced_str = '';
        $keyword = array_shift($keywords);

        while (true) {
            if (($index = mb_stripos($str, $keyword)) === false) {
                $replaced_str .= tep_mark_search_string($str, $keywords);
                break;
            }
            if($index !== 0) {
                $replaced_str .= tep_mark_search_string(mb_substr($str, 0, $index), $keywords);
                $str = mb_substr($str, $index);
            }
            $length = mb_strlen($keyword);
            $replaced_str .= '<span class="search-mark">' . mb_substr($str, 0, $length) . '</span>';
            if(($str = mb_substr($str, $length)) === '') break;
        }
    }
    return $replaced_str;
}

function tep_htmlentities_2($text) {
    $text = str_replace(
        array(
            '%','€','«','»','‘','’','“','”','„','″','+',
        ),
        array(
            '&#37;','&euro;','&laquo;','&raquo;','&#039;','&#039;','&quot;','&quot;','&quot;','&quot;','&#43;',
        ),
        $text );
    return $text;
}

function tep_draw_meta_keywords($str) {	// cleaning text-names - for meta keywords

    $str = preg_replace("#[^(\w)|(\x7F-\xFF)|(\s)]#", " ", $str);
    $str = trim($str);
    $str = preg_replace('#[-_ ]+#', ',', $str);
    return $str;
}

function tep_prepare_html($text,$flag=0) {
    $pairs = array(
        " " => ' ',
        "&nbsp;" => ' ',
        "&lt;&lt;" => '&laquo;',
        "&gt;&gt;" => '&raquo;',
        "(" => ' (',
        "," => ', ',
        " ," => ','
    );
    $text = strtr($text, $pairs);
    if($flag != 2) $text = preg_replace('/[\r\n\t]+/', ' ', $text); // for about text
    $text = stripslashes($text);

    if($flag == 1) { // tinymce text

        $allowd = '<a><strong><em><u><sub><sup><ul><ol><li><br><p><div><table><tr><td>';
        $text = strip_tags($text, $allowd);

        $text = preg_replace("/<([a-z][a-z0-9]*)(?:[^>]*(\shref=['\"][^'\"]*['\"]))?[^>]*?(\/?)>/i",'<$1$2$3>', $text);
        $text = str_replace("<br>", '<br />', $text);
        $text = str_replace("<br/>", '<br />', $text);
//		$text = str_replace("<div>&nbsp;</div>", '<br />', $text);
//		$text = str_replace("<p>&nbsp;</p>", '<br />', $text);

        $text = htmlentities($text);

    } else { // simple text

        $text = strip_tags($text);
        $text = tep_htmlentities_decode($text);//navsyaki
        $text = htmlspecialchars($text, ENT_QUOTES, CHARSET);
        $text = tep_htmlentities_2($text);

    }

    $text = preg_replace('/\s+/', ' ', trim($text));

    return $text;
}

function tep_clean_text_int($str) {	// cleaning text-names - for url

    $str = tep_htmlentities_decode($str);
    $str = preg_replace("#[^(\w)|(\x7F-\xFF)|(\s)]#", " ", $str);
    $str = trim($str);
    $str = preg_replace('#[-_ ]+#', '-', $str);//tep_htmlentities_decode($text)
    return tep_strtolower_int($str, CHARSET);
}

function tep_strtolower_int($str) {
    return mb_convert_case($str, MB_CASE_LOWER);
}

function tep_break_text_int($str,$max_length) {
    if(mb_strlen($str) > $max_length) {
        $str = mb_substr($str,0,$max_length, CHARSET);
    }
    return $str;
}

function out_categories($cat, &$cats_out) {
    global $cPath_array;
    $link = '';
    if($cat->parent->id !== 0) {
        $cats_out .= '&nbsp;&nbsp;';
        $link = tep_clean_text_int($cat->parent->name) . '/';
    }
    $link .= tep_clean_text_int($cat->name);
    $name = $cat->name . ($cat->parent->id == 0 ? '-&gt;' : '');
    if (isset($cPath_array) && in_array($cat->id, $cPath_array)) {
        $name = '<strong>' . $name . '</strong>';
    }
    $cats_out .= '<a href="' . tep_href_link($link) . '">' . $name . '</a>';
    $cats_out .= '<br/>';
}