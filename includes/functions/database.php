<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

function tep_db_connect($server = DB_SERVER, $username = DB_SERVER_USERNAME, $password = DB_SERVER_PASSWORD, $database = DB_DATABASE, $mysqli_object = 'mysqli') {
    global $$mysqli_object;

    if (USE_PCONNECT == 'true') {
        $$mysqli_object = new mysqli("p:" . $server, $username, $password, $database);
    } else {
        $$mysqli_object = new mysqli($server, $username, $password, $database);
    }

    $$mysqli_object->set_charset("utf8");

    tep_db_query('SET time_zone = "+00:00"');

    return $mysqli_object;
}

function tep_db_close($mysqli_object = 'mysqli') {
    global $$mysqli_object;

    return $$mysqli_object->close();
}

function tep_db_error($query, $errno, $error) {
    die('<div style="color:#000000;"><strong>' . $errno . ' - ' . $error . '<br /><br />' . $query . '<br /><br /><small><div style="color:#000000;">[TEP STOP]</div></small><br /><br /></strong></div>');
}

function tep_db_query($query, $mysqli_object = 'mysqli') {
    global $$mysqli_object, $num_queries;

    if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
        error_log('QUERY ' . $query . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
    }

    $result = $$mysqli_object->query($query) or tep_db_error($query, $$mysqli_object->errno, $$mysqli_object->error);

    if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
        $result_error = mysql_error();
        error_log('RESULT ' . $result . ' ' . $result_error . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
    }

    $num_queries++;

    return $result;
}

function tep_db_perform($table, $data, $action = 'insert', $parameters = '', $mysqli_object = 'mysqli') {
    reset($data);
    if ($action == 'insert') {
        $query = 'insert into ' . $table . ' (';
        while (list($columns, ) = each($data)) {
            $query .= $columns . ', ';
        }
        $query = substr($query, 0, -2) . ') values (';
        reset($data);
        while (list(, $value) = each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query .= 'now(), ';
                    break;
                case 'null':
                    $query .= 'null, ';
                    break;
                default:
                    $query .= '\'' . tep_db_input($value) . '\', ';
                    break;
            }
        }
        $query = substr($query, 0, -2) . ')';
    } elseif ($action == 'update') {
        $query = 'update ' . $table . ' set ';
        while (list($columns, $value) = each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query .= $columns . ' = now(), ';
                    break;
                case 'null':
                    $query .= $columns .= ' = null, ';
                    break;
                default:
                    $query .= $columns . ' = \'' . tep_db_input($value) . '\', ';
                    break;
            }
        }
        $query = substr($query, 0, -2) . ' where ' . $parameters;
    }

    return tep_db_query($query, $mysqli_object);
}

function tep_db_fetch_array($result) {
    return $result->fetch_array(MYSQLI_ASSOC);
}

function tep_db_fetch_all($result) {
	if (method_exists('mysqli_result', 'fetch_all')) {
		$res = $result->fetch_all(MYSQLI_ASSOC);
	} else {
		tep_db_data_seek($result, 0);
		$res = array();
		while ( $tmp = tep_db_fetch_array($result)) {
			$res[] = $tmp;
		}
	}

	return $res;
}

function tep_db_num_rows($result) {
    return $result->num_rows;
}

function tep_db_data_seek($result, $row_number) {
    return $result->data_seek($row_number);
}

function tep_db_insert_id($mysqli_object = 'mysqli') {
    global $$mysqli_object;

    return $$mysqli_object->insert_id;
}

function tep_db_free_result($result) {
    return $result->free();
}

function tep_db_fetch_fields($result) {
    return $result->fetch_field();
}

function tep_db_output($string) {
    return htmlspecialchars($string);
}

function tep_db_input($string, $mysqli_object = 'mysqli') {
    global $$mysqli_object;

    if (is_null($string)) return $string;
    return $$mysqli_object->real_escape_string($string);
}

function tep_db_prepare_input($string) {
    if (is_string($string)) {
        return trim(stripslashes($string));
    } elseif (is_array($string)) {
        foreach ($string as &$value) {
            $value = tep_db_prepare_input($value);
        }
        return $string;
    } else {
        return $string;
    }
}

function tep_db_autocommit($mode, $mysqli_object = 'mysqli') {
    global $$mysqli_object;

    return $$mysqli_object->autocommit($mode);
}

function tep_db_commit($mysqli_object = 'mysqli') {
    global $$mysqli_object;

    return $$mysqli_object->commit();
}

function tep_db_rollback($mysqli_object = 'mysqli') {
    global $$mysqli_object;

    return $$mysqli_object->rollback();
}

function tep_db_transaction($queries, $mysqli_object = 'mysqli') {
    global $$mysqli_object;

    tep_db_autocommit(false);
    for ($i = 0, $n = sizeof($queries); $i < $n; ++$i) {
        if (!tep_db_query($queries[$i])) {
            tep_db_rollback();
            tep_db_autocommit(true);
            return;
        }
    }
    tep_db_commit();
    tep_db_autocommit(true);
    return;
}