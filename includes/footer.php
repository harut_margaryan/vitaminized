<?php

// require(DIR_WS_INCLUDES . 'counter.php');
if (!$PARTNER) {
?>
    <footer style="clear: both;">
	    <section class="contact-section">

        <div class="product-notation">
          These statements have not been evaluated by the Food and Drug Administration. These products are not intended to diagnose, treat, cure or prevent disease.
        </div>

	    	<div class="contacts-menu-head">
	    		<div class="contacts-menu">
	    			<div class="contacts-icon"><?= tep_image('contacts-icon.png', '', 32)?><span>CONTACTS</span></div>
	    			<ul>
	    				<!--<li><a>3411 Silverside Road, Wilmington, DE, 19810</a></li>
	    				<li><a>Tel.: +374 10 55 55</a></li>
	    				<li><a>Fax: +374 10 55 55 66 99</a></li>-->
	    				<li><a>E-mail: info@vitaminized.com</a></li>
	    			</ul>
	    			<div class="contacts-site-icon">
	    				<?= tep_image('fb-icon.png', '', 50) ?>
	    				<?= tep_image('gplus-icon.png', '',  50) ?>
	    			</div>
	    		</div>
	    		<div class="feedback-form">
	    			<div class="feedback-form-head"><?= tep_image('feedback-form-icon.png', '', 32)?><span>FEEDBACK</span></div>
	    			<form id="feedbackForm" action="#">
              <div id="submit-error"></div>
	    				<input name="name" type="text" class="dena-input" placeholder="name surname"/>
              <span class='error-message' id='name-error'></span>
	    				<input name="email" type="text" class="dena-input" placeholder="e-mail"/>
              <span class='error-message' id='email-error'></span>
	    				<textarea name="message" name="text" class="dena-input" placeholder="enquiry"></textarea>
              <span class='error-message' id='message-error'></span>
	    				<button type="button" class="feedback-button dena-btn dena-btn-blue" onclick='sendFeedback()'>SEND</button>
	    			</form>
	    		</div>
	    	</div>
	    </section>
	    <div class="dena-head-menu">
	    	<ul>
	    		<?php
	    		$company_links_query = tep_db_query("select p.pages_id, pd.pages_title from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) where (p.parents_id = 26 or p.pages_id = 26) and p.pages_status = 1 and pd.language_id = '" . (int)$languages_id . "' order by p.parents_id asc, p.pages_sort_order asc");
	    		$company_page = tep_db_fetch_array($company_links_query);
	    		while($cmp_link = tep_db_fetch_array($company_links_query)) {
	    			echo '<li><a href="' . tep_href_link(tep_clean_text_int($company_page['pages_title']) . '/' . tep_clean_text_int($cmp_link['pages_title'])) . '">' . $cmp_link['pages_title'] . '</a></li>';
	    		}
	    		?>
	    	</ul>
	    </div>
	    <div class="dena-footer">
	    	<div class="dena-footer-cont">
	    		<div class="footer-logo"><?= tep_image('dena-pharma-logo.png', 'logo', 140)?></div>
	    		<div class="footer-cont"><span>copyright&copy; “Dena International” LLC 2013-2016. All rights reserved.</span>
                    <span>Zealut-Dena<sup>&reg;</sup>, MegaEl-Dena<sup>&reg;</sup>, Cavsor<sup>&reg;</sup>, Elgenoflex<sup>&reg;</sup> ARE ALL REGISTERED TRADEMARKS OF DENA INTERNATIONAL LLC.</span></div>
	    	</div>
	    </div>
    </footer>
<?php } ?>

