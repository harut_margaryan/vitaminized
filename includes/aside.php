<!--<aside id="right-col-hidden">
</aside>-->

<aside id="right-col">

    <button type="button" id="hide-right-col" class="dena-btn dena-btn-alpha right-col-style">
        <?= tep_image('hide-menu-icon.png', 'menu', '', '', 'style="width:25px;"')?>
    </button>

    <div id="right-col2">
        <!--<div class="right-col-head"><span>HIDE MENU</span>
            <div></div>
        </div>-->
        <?= tep_draw_form('quick_find', tep_href_link(ADVANCED_SEARCH_RESULT_URL, '', 'NONSSL', false), 'get', 'id="search-form"'); ?>
        <i class="left-search-icon icon-search"></i>
        <input type="search" name="keywords" class="right-col-search" placeholder="Search" maxlength="300"/>
        <?= tep_hide_session_id() ?>
        </form>
        <?php $nav_links = '<div class="nav-links ">'; ?>
        <nav id="site-nav">
            <?php
            $prods_query = tep_db_query('select p.products_id, p.products_image, p.products_price, pd.products_name, pd.products_short_desc, pd.products_description from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd using(products_id) where p.products_status = 1 and pd.language_id = ' . intval($languages_id));
            if (tep_db_num_rows($prods_query)) {
                echo '<h3 class="nav-menu">Products</h3>';
                echo '<div>';
                echo '<ul>';
                while ($prod = tep_db_fetch_array($prods_query)) {
                    echo '<li>';
                    echo '<a href="' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($prod['products_name'])) . '">';
                    echo $prod['products_name'];
                    echo '</a>';
                    echo '</li>';
                }
                echo '</ul>';
                echo '</div>';
            }

            $pages_query = tep_db_query("select p.pages_id, pd.pages_title, (pd.pages_description is not null and pd.pages_description <> '') as page_has_content from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) WHERE p.parents_id = 0 and p.pages_status = 1 and pd.language_id = '" . (int)$languages_id . "' order by p.pages_sort_order asc");
            if (tep_db_num_rows($pages_query)) {
                while ($page = tep_db_fetch_array($pages_query)) {
                    $subpages_query = tep_db_query("select p.pages_id, pd.pages_title from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) where p.parents_id = " . intval($page['pages_id']) . " and p.parents_id != 26 and p.pages_status = 1 and pd.language_id = '" . (int)$languages_id . "' order by p.pages_sort_order, pd.pages_title asc");
                    if (tep_db_num_rows($subpages_query)) {
                        echo '<h3 class="nav-menu">' . $page['pages_title'] . '</h3>';
                        echo '<div>';
                        echo '<ul>';
                        while ($subpage = tep_db_fetch_array($subpages_query)) {
                            echo '<li>';
                            echo '<a href="' . tep_href_link(tep_clean_text_int($page['pages_title']) . '/' . tep_clean_text_int($subpage['pages_title'])) . '">';
                            echo $subpage['pages_title'];
                            echo '</a>';
                            echo '</li>';
                        }
                        echo '</ul>';
                        echo '</div>';
                    } else {
                        $nav_links .= '<a href="' . tep_href_link(tep_clean_text_int($page['pages_title'])) . '">' . $page['pages_title'] . '</a>';
                    }
                }
            }
            ?>
        </nav>
        <?= $nav_links . '</div>' ?>
        <!-- <div class="signin-form-cont">
        <div>Sign in</div>
        <form action="#">
            <input type="text" placeholder="Username"/>
            <input type="password" placeholder="Password"/>
            <div class="signin-buttons">
                <button type="button" class="dena-btn dena-btn-blue">SIGN iN</button>
                <a href="#">Registration</a>
                <a href="#">Sign up for news</a>
            </div>
        </form>
    </div>
    <div class="languages-box">
        <span>Language:</span>
		<?php
        //$lang_box = new bm_languages();
        //echo $lang_box->execute();
        ?>
    </div>-->

    </div>
</aside>
