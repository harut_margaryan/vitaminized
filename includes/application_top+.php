<?php

/*UPDATE visits SET partners_id = IF((SIN(2*PI()*visit_id/1)+SIN(2*PI()*visit_id/0.8)+SIN(2*PI()*visit_id/1.5)) > 1, NULL, 28)*/
//require(DIR_WS_INCLUDES . 'data_generator.php');

define('MAX_SHORT_DESCRIPTION_LENGTH', 300);
define('WITH_SHOPPING_CART', false);

$not_found = false;
$main_get = isset($_GET['main']) ? (string)$_GET['main'] : '';
$main_get = tep_strtolower_int($main_get);
$item_get = isset($_GET['item']) ? (string)$_GET['item'] : '';

if(isset($_GET['extra']) && intval($_GET['extra']) > 0){
	$_GET['p'] = $_GET['extra'];
	unset($_GET['extra']);
}

$extra_get = isset($_GET['extra']) ? (string)$_GET['extra'] : '';


$js_redirect = false;
$main = '';
$item = '';
$extra = '';
$path = '';
$fb_app_id = "654654";
//$og_pic = HTTP_SERVER . DIR_WS_CATALOG . DIR_WS_IMAGES . 'store_logo.png';
$og_pic = DIR_WS_IMAGES . 'store_logo.png';
$og_url = tep_href_link();
$og_desc = '';
$og_text = '';
$og_type = 'website';
$do_breadcrumb = '';


if (!tep_session_is_registered('visit_id')) {
    $visit_id = false;
    if (!$IS_BOT && !tep_session_is_registered('visit_info')) {
        $visit_info = array(
            'partners_id' => empty($AFFILIATE['partners_id']) ? 'null' : (int)$AFFILIATE['partners_id'],
            'method_id' => 'null',
            'material_id' => 'null',
            'products_id' => 'null',
            'from_url' => empty($_SERVER['HTTP_REFERER']) ? null : $_SERVER['HTTP_REFERER'],
            'to_url' => tep_get_current_url(),
            'last_url' => tep_get_current_url(),
            'first_online' => 'now()',
            'last_online' => 'now()',
            'user_agent' => tep_get_user_agent(),
            'ip_address' => tep_get_ip_address()
        );
        tep_session_register('visit_info');
    }
}

if (!$PARTNER) {
    include(DIR_WS_LANGUAGES . $language . '/modules/boxes/bm_categories.php');
    require(DIR_WS_MODULES . 'boxes/bm_categories.php');
}

if (tep_not_null($main_get)) {
    $main = $main_get;
    $og_url = tep_href_link($main . (empty($item_get) ? '' : '/' . $item_get));
    switch($main_get) {
        case ACCOUNT_URL:
            if ($PARTNER) {
                if (!tep_session_is_registered('partner_id')) {
                    $navigation->set_snapshot();
                    if (tep_session_is_registered('customer_id')) {
                        $PARTNER = false;
                        tep_redirect(tep_href_link(ACCOUNT_URL));
                    } else {
                        tep_redirect(tep_href_link()); // TODO : redirect to login page
                    }
                }
            } else {
                if (!tep_session_is_registered('customer_id')) {
                    $navigation->set_snapshot();
                    //if (tep_session_is_registered('partner_id')) {
                        $PARTNER = true;
                        tep_redirect(tep_href_link(ACCOUNT_URL));
                   /* } else {
                        tep_redirect(tep_href_link()); // TODO : redirect to login page
                    }*/
                }
            }

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_ACCOUNT);

            $breadcrumb->add(NAVBAR_TITLE, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            break;

        case ACCOUNT_EDIT_URL:


            if ($PARTNER) {
                if (!tep_session_is_registered('partner_id')) {
                    $navigation->set_snapshot();
                    if (tep_session_is_registered('customer_id')) {
                        $PARTNER = false;
                        tep_redirect(tep_href_link(ACCOUNT_EDIT_URL));
                    } else {
                        tep_redirect(tep_href_link()); // TODO : redirect to login page
                    }
                }
            } else {
                if (!tep_session_is_registered('customer_id')) {
                    $navigation->set_snapshot();
                    if (tep_session_is_registered('partner_id')) {
                        $PARTNER = true;
                        tep_redirect(tep_href_link(ACCOUNT_EDIT_URL));
                    } else {
                        tep_redirect(tep_href_link()); // TODO : redirect to login page
                    }
                }
            }

// needs to be included earlier to set the success message in the messageStack
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_ACCOUNT_EDIT);

            if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                if ($PARTNER) {
                    //if (ACCOUNT_GENDER == 'true') $gender = isset($_POST['gender']) ? tep_db_prepare_input($_POST['gender']) : '';
                    $firstname = tep_db_prepare_input($_POST['firstname']);
                    if (ACCOUNT_LAST_NAME == 'true') $lastname = isset($_POST['lastname']) ? tep_db_prepare_input($_POST['lastname']) : '';
                    //if (ACCOUNT_DOB == 'true') $dob = isset($_POST['dob']) ? tep_db_prepare_input($_POST['dob']) : '';
                    $email_address = tep_db_prepare_input($_POST['email_address']);
                    //if (ACCOUNT_FAX_NUMBER == 'true') $fax = isset($_POST['fax']) ? tep_db_prepare_input($_POST['fax']) : '';

                    $error = false;

                    /*if (ACCOUNT_GENDER == 'true') {
                        if ( ($gender != 'm') && ($gender != 'f') ) {
                            $error = true;

                            $messageStack->add('account_edit', ENTRY_GENDER_ERROR);
                        }
                    }*/

                    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('account_edit', ENTRY_FIRST_NAME_ERROR);
                    }

                    if (ACCOUNT_LAST_NAME == 'true') {
                        if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('account_edit', ENTRY_LAST_NAME_ERROR);
                        }
                    }



                    if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR);
                    }

                    if (!tep_validate_email($email_address)) {
                        $error = true;

                        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
                    }

                    $check_email_query = tep_db_query("select count(*) as total from " . TABLE_PARTNERS . " where partners_email_address = '" . tep_db_input($email_address) . "' and partners_id != '" . (int)$partner_id . "'");
                    $check_email = tep_db_fetch_array($check_email_query);
                    if ($check_email['total'] > 0) {
                        $error = true;

                        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
                    }


                    if ($error == false) {
                        $sql_data_array = array('partners_firstname' => $firstname,
                            'partners_email_address' => $email_address);
                        if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['partners_lastname'] = $lastname;

                        $ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                        $user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                        //if (ACCOUNT_FAX_NUMBER == 'true') $sql_data_array['customers_fax'] = $fax;
                        //if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
                        //if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($dob);

                        tep_db_perform(TABLE_PARTNERS, $sql_data_array, 'update', "partners_id = '" . (int)$partner_id . "'");


                        tep_db_query("update " . TABLE_PARTNERS_INFO . " set
                        partners_ip = '" . $ip . "',
                        partners_user_agent = '" . $user_agent . "',
                        partners_info_date_account_last_modified = now()
                        where partners_info_id = '" . (int)$partner_id . "'");

                        /*$sql_data_array = array('entry_firstname' => $firstname);
                        if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['entry_lastname'] = $lastname;

                        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$customer_default_address_id . "'");*/


// reset the session variables
                        $partner_first_name = $firstname;

                        $messageStack->add_session('account', SUCCESS_ACCOUNT_UPDATED, 'success');

                        tep_redirect(tep_href_link(ACCOUNT_URL, '', 'SSL'));
                    }
                } else {
                    if (ACCOUNT_GENDER == 'true') $gender = isset($_POST['gender']) ? tep_db_prepare_input($_POST['gender']) : '';
                    $firstname = tep_db_prepare_input($_POST['firstname']);
                    if (ACCOUNT_LAST_NAME == 'true') $lastname = isset($_POST['lastname']) ? tep_db_prepare_input($_POST['lastname']) : '';
                    if (ACCOUNT_DOB == 'true') $dob = isset($_POST['dob']) ? tep_db_prepare_input($_POST['dob']) : '';
                    $email_address = tep_db_prepare_input($_POST['email_address']);
                    if (ACCOUNT_TELEPHONE_NUMBER == 'true') $telephone = isset($_POST['telephone']) ? tep_db_prepare_input($_POST['telephone']) : '';
                    if (ACCOUNT_FAX_NUMBER == 'true') $fax = isset($_POST['fax']) ? tep_db_prepare_input($_POST['fax']) : '';

                    $error = false;

                    if (ACCOUNT_GENDER == 'true') {
                        if ( ($gender != 'm') && ($gender != 'f') ) {
                            $error = true;

                            $messageStack->add('account_edit', ENTRY_GENDER_ERROR);
                        }
                    }

                    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('account_edit', ENTRY_FIRST_NAME_ERROR);
                    }

                    if (ACCOUNT_LAST_NAME == 'true') {
                        if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('account_edit', ENTRY_LAST_NAME_ERROR);
                        }
                    }

                    if (ACCOUNT_DOB == 'true') {
                        if ((is_numeric(tep_date_raw($dob)) == false) || (@checkdate(substr(tep_date_raw($dob), 4, 2), substr(tep_date_raw($dob), 6, 2), substr(tep_date_raw($dob), 0, 4)) == false)) {
                            $error = true;

                            $messageStack->add('account_edit', ENTRY_DATE_OF_BIRTH_ERROR);
                        }
                    }

                    if (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR);
                    }

                    if (!tep_validate_email($email_address)) {
                        $error = true;

                        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
                    }

                    $check_email_query = tep_db_query("select count(*) as total from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "' and customers_id != '" . (int)$customer_id . "'");
                    $check_email = tep_db_fetch_array($check_email_query);
                    if ($check_email['total'] > 0) {
                        $error = true;

                        $messageStack->add('account_edit', ENTRY_EMAIL_ADDRESS_ERROR_EXISTS);
                    }

                    if (ACCOUNT_TELEPHONE_NUMBER == 'true') {
                        if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('account_edit', ENTRY_TELEPHONE_NUMBER_ERROR);
                        }
                    }

                    if ($error == false) {
                        $sql_data_array = array('customers_firstname' => $firstname,
                            'customers_email_address' => $email_address);
                        if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['customers_lastname'] = $lastname;
                        if (ACCOUNT_TELEPHONE_NUMBER == 'true') $sql_data_array['customers_telephone'] = $telephone;
                        if (ACCOUNT_FAX_NUMBER == 'true') $sql_data_array['customers_fax'] = $fax;
                        if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
                        if (ACCOUNT_DOB == 'true') $sql_data_array['customers_dob'] = tep_date_raw($dob);

                        tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "'");

                        $customers_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                        $customers_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                        tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_ip = '" . $customers_ip . "', customers_user_agent = '" . $customers_user_agent . "', customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");

                        $sql_data_array = array('entry_firstname' => $firstname);
                        if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['entry_lastname'] = $lastname;

                        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$customer_default_address_id . "'");

                        // reset the session variables
                        $customer_first_name = $firstname;

                        $messageStack->add_session('account', SUCCESS_ACCOUNT_UPDATED, 'success');

                        tep_redirect(tep_href_link(ACCOUNT_URL, '', 'SSL'));
                    }
                }
            }

            if ($PARTNER) {
                $account_query = tep_db_query("select partners_firstname, partners_lastname, partners_email_address, partners_telephone from " . TABLE_PARTNERS . " where partners_id = '" . (int)$partner_id . "'");
            } else {
                $account_query = tep_db_query("select customers_gender, customers_firstname, customers_lastname, customers_dob, customers_email_address, customers_telephone, customers_fax from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
            }
            $account = tep_db_fetch_array($account_query);

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ACCOUNT_EDIT_URL, '', 'SSL'));
            break;

        case ACCOUNT_HISTORY_URL:
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_ACCOUNT_HISTORY);

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ACCOUNT_HISTORY_URL, '', 'SSL'));
            break;

        case ACCOUNT_HISTORY_INFO_URL:
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

            if (!isset($_GET['order_id']) || (isset($_GET['order_id']) && !is_numeric($_GET['order_id']))) {
                tep_redirect(tep_href_link(ACCOUNT_HISTORY_URL, '', 'SSL'));
            }

            $customer_info_query = tep_db_query("select o.customers_id from " . TABLE_ORDERS . " o join " . TABLE_ORDERS_STATUS . " s on(o.orders_status = s.orders_status_id) where o.orders_id = '". (int)$_GET['order_id'] . "' and s.language_id = '" . (int)$languages_id . "' and s.public_flag = '1'");
            $customer_info = tep_db_fetch_array($customer_info_query);
            if ($customer_info['customers_id'] != $customer_id) {
                tep_redirect(tep_href_link(ACCOUNT_HISTORY_URL, '', 'SSL'));
            }

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_ACCOUNT_HISTORY_INFO);

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ACCOUNT_HISTORY_URL, '', 'SSL'));
            $breadcrumb->add(sprintf(NAVBAR_TITLE_3, $_GET['order_id']), tep_href_link(ACCOUNT_HISTORY_INFO_URL, 'order_id=' . $_GET['order_id'], 'SSL'));

            require(DIR_WS_CLASSES . 'order.php');
            $order = new order($_GET['order_id']);
            break;

        case ACCOUNT_NEWSLETTERS_URL:
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

// needs to be included earlier to set the success message in the messageStack
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_ACCOUNT_NEWSLETTERS);

            $newsletter_query = tep_db_query("select customers_newsletter from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
            $newsletter = tep_db_fetch_array($newsletter_query);

            if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                if (isset($_POST['newsletter_general']) && is_numeric($_POST['newsletter_general'])) {
                    $newsletter_general = tep_db_prepare_input($_POST['newsletter_general']);
                } else {
                    $newsletter_general = '0';
                }

                if ($newsletter_general != $newsletter['customers_newsletter']) {
                    $newsletter_general = (($newsletter['customers_newsletter'] == '1') ? '0' : '1');

                    tep_db_query("update " . TABLE_CUSTOMERS . " set customers_newsletter = '" . (int)$newsletter_general . "' where customers_id = '" . (int)$customer_id . "'");
                }

                $messageStack->add_session('account', SUCCESS_NEWSLETTER_UPDATED, 'success');

                tep_redirect(tep_href_link(ACCOUNT_URL, '', 'SSL'));
            }

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ACCOUNT_NEWSLETTERS_URL, '', 'SSL'));
            break;

        case ACCOUNT_NOTIFICATIONS_URL:
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

// needs to be included earlier to set the success message in the messageStack
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_ACCOUNT_NOTIFICATIONS);

            $global_query = tep_db_query("select global_product_notifications from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customer_id . "'");
            $global = tep_db_fetch_array($global_query);

            if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                if (isset($_POST['product_global']) && is_numeric($_POST['product_global'])) {
                    $product_global = tep_db_prepare_input($_POST['product_global']);
                } else {
                    $product_global = '0';
                }

                (array)$products = $_POST['products'];

                if ($product_global != $global['global_product_notifications']) {
                    $product_global = (($global['global_product_notifications'] == '1') ? '0' : '1');

                    tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set global_product_notifications = '" . (int)$product_global . "' where customers_info_id = '" . (int)$customer_id . "'");
                } elseif (sizeof($products) > 0) {
                    $products_parsed = array();
                    reset($products);
                    while (list(, $value) = each($products)) {
                        if (is_numeric($value)) {
                            $products_parsed[] = $value;
                        }
                    }

                    if (sizeof($products_parsed) > 0) {
                        $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_NOTIFICATIONS . " where customers_id = '" . (int)$customer_id . "' and products_id not in (" . implode(',', $products_parsed) . ")");
                        $check = tep_db_fetch_array($check_query);

                        if ($check['total'] > 0) {
                            tep_db_query("delete from " . TABLE_PRODUCTS_NOTIFICATIONS . " where customers_id = '" . (int)$customer_id . "' and products_id not in (" . implode(',', $products_parsed) . ")");
                        }
                    }
                } else {
                    $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_NOTIFICATIONS . " where customers_id = '" . (int)$customer_id . "'");
                    $check = tep_db_fetch_array($check_query);

                    if ($check['total'] > 0) {
                        tep_db_query("delete from " . TABLE_PRODUCTS_NOTIFICATIONS . " where customers_id = '" . (int)$customer_id . "'");
                    }
                }

                $messageStack->add_session('account', SUCCESS_NOTIFICATIONS_UPDATED, 'success');

                tep_redirect(tep_href_link(ACCOUNT_URL, '', 'SSL'));
            }

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ACCOUNT_NOTIFICATIONS_URL, '', 'SSL'));
            break;

        case ACCOUNT_PASSWORD_URL:

            if ($PARTNER) {
                if (!tep_session_is_registered('partner_id')) {
                    $navigation->set_snapshot();
                    if (tep_session_is_registered('customer_id')) {
                        $PARTNER = false;
                        tep_redirect(tep_href_link(ACCOUNT_PASSWORD_URL));
                    } else {
                        tep_redirect(tep_href_link()); // TODO : redirect to login page
                    }
                }
            } else {
                if (!tep_session_is_registered('customer_id')) {
                    $navigation->set_snapshot();
                    if (tep_session_is_registered('partner_id')) {
                        $PARTNER = true;
                        tep_redirect(tep_href_link(ACCOUNT_PASSWORD_URL));
                    } else {
                        tep_redirect(tep_href_link()); // TODO : redirect to login page
                    }
                }
            }

// needs to be included earlier to set the success message in the messageStack
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_ACCOUNT_PASSWORD);

            if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {

                $has_password = tep_check_has_password();

                if ($has_password) {
                    if (isset($_POST['password_current'])) {
                        $password_current = tep_db_prepare_input($_POST['password_current']);
                    } else {
                        $error = true;

                        $messageStack->add('account_password', ERROR_CURRENT_PASSWORD_NOT_MATCHING);
                    }
                }
                if (isset($_POST['password_new'])) {
                    $password_new = tep_db_prepare_input($_POST['password_new']);
                } else {
                    $error = true;

                    $messageStack->add('account_password', ENTRY_PASSWORD_NEW_ERROR);
                }
                if (isset($_POST['password_new'])) {
                    $password_confirmation = tep_db_prepare_input($_POST['password_confirmation']);
                } else {
                    $error = true;

                    $messageStack->add('account_password', ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING);
                }

                $error = false;

                if (strlen($password_new) < ENTRY_PASSWORD_MIN_LENGTH) {
                    $error = true;

                    $messageStack->add('account_password', ENTRY_PASSWORD_NEW_ERROR);
                } elseif ($password_new != $password_confirmation) {
                    $error = true;

                    $messageStack->add('account_password', ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING);
                }

                if ($error == false) {
                    if ($PARTNER) {
                        $true_password = true;
                        if ($has_password) {
                            $check_partner_query = tep_db_query("select partners_password from " . TABLE_PARTNERS . " where partners_id = '" . (int)$partner_id . "'");
                            $check_partner = tep_db_fetch_array($check_partner_query);
                            $true_password = tep_validate_password($password_current, $check_partner['partners_password']);
                        }

                        if ($true_password) {
                            tep_db_query("update " . TABLE_PARTNERS . " set partners_password = '" . tep_encrypt_password($password_new) . "' where partners_id = '" . (int)$partner_id . "'");

                            $partners_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                            $partners_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                            tep_db_query("update " . TABLE_PARTNERS_INFO . " set partners_ip = '" . $partners_ip . "', partners_user_agent = '" . $partners_user_agent . "', partners_info_date_account_last_modified = now() where partners_info_id = '" . (int)$partner_id . "'");

                            $messageStack->add_session('account', SUCCESS_PASSWORD_UPDATED, 'success');

                            tep_redirect(tep_href_link(ACCOUNT_URL, '', 'SSL'));
                        } else {
                            $error = true;

                            $messageStack->add('account_password', ERROR_CURRENT_PASSWORD_NOT_MATCHING);
                        }

                    } else {
                        $true_password = true;
                        if ($has_password) {
                            $check_customer_query = tep_db_query("select customers_password from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
                            $check_customer = tep_db_fetch_array($check_customer_query);
                            $true_password = tep_validate_password($password_current, $check_customer['customers_password']);
                        }

                        if ($true_password) {
                            tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_encrypt_password($password_new) . "' where customers_id = '" . (int)$customer_id . "'");

                            $customers_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                            $customers_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                            tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_ip = '" . $customers_ip . "', customers_user_agent = '" . $customers_user_agent . "', customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");

                            $messageStack->add_session('account', SUCCESS_PASSWORD_UPDATED, 'success');

                            tep_redirect(tep_href_link(ACCOUNT_URL, '', 'SSL'));
                        } else {
                            $error = true;

                            $messageStack->add('account_password', ERROR_CURRENT_PASSWORD_NOT_MATCHING);
                        }
                    }
                }
            }

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ACCOUNT_PASSWORD_URL, '', 'SSL'));
            break;

        case ADDRESS_BOOK_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }


            $addresses_query = tep_db_query("select ab.entry_firstname as firstname, ab.entry_lastname as lastname, ab.entry_company as company, ab.entry_street_address as street_address, ab.entry_suburb as suburb, ab.entry_city as city, ab.address_book_id, ab.entry_postcode as postcode, ab.entry_state as state, ab.entry_zone_id as zone_id, ab.entry_country_id as country_id, c.address_format_id as format_id, af.address_format as format from " . TABLE_ADDRESS_BOOK . " as ab left join " . TABLE_COUNTRIES . " as c on(c.countries_id = ab.entry_country_id) left join " . TABLE_ADDRESS_FORMAT . " as af on(af.address_format_id = c.address_format_id) where ab.customers_id = '" . (int)$customer_id . "'");
            $addresses_array = array();
            while($addresses_array[] = tep_db_fetch_array($addresses_query));
            array_pop($addresses_array);

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_ADDRESS_BOOK);

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));
            break;

        case ADDRESS_BOOK_PROCESS_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

// needs to be included earlier to set the success message in the messageStack
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_ADDRESS_BOOK_PROCESS);

            if (isset($_GET['action']) && ($_GET['action'] == 'deleteconfirm') && isset($_GET['delete']) && is_numeric($_GET['delete']) && isset($_GET['formid']) && ($_GET['formid'] == md5($sessiontoken))) {
                if ((int)$_GET['delete'] == $customer_default_address_id) {
                    $messageStack->add_session('addressbook', WARNING_PRIMARY_ADDRESS_DELETION, 'warning');
                } else {
                    tep_db_query("delete from " . TABLE_ADDRESS_BOOK . " where address_book_id = '" . (int)$_GET['delete'] . "' and customers_id = '" . (int)$customer_id . "'");

                    $messageStack->add_session('addressbook', SUCCESS_ADDRESS_BOOK_ENTRY_DELETED, 'success');
                }

                tep_redirect(tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));
            }

// error checking when updating or adding an entry
            $process = false;
            if (isset($_POST['action']) && (($_POST['action'] == 'process') || ($_POST['action'] == 'update')) && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                $process = true;
                $error = false;

                if (ACCOUNT_GENDER == 'true') $gender = isset($_POST['gender']) ? tep_db_prepare_input($_POST['gender']) : '';
                if (ACCOUNT_COMPANY == 'true') $company = isset($_POST['company']) ? tep_db_prepare_input($_POST['company']) : '';
                $firstname = isset($_POST['firstname']) ? tep_db_prepare_input($_POST['firstname']) : '';
                if (ACCOUNT_LAST_NAME == 'true') $lastname = isset($_POST['lastname']) ? tep_db_prepare_input($_POST['lastname']) : '';
                if (ACCOUNT_STREET_ADDRESS == 'true') $street_address = isset($_POST['street_address']) ? tep_db_prepare_input($_POST['street_address']) : '';
                if (ACCOUNT_SUBURB == 'true') $suburb = isset($_POST['suburb']) ? tep_db_prepare_input($_POST['suburb']) : '';
                if (ACCOUNT_POST_CODE == 'true') $postcode = isset($_POST['postcode']) ? tep_db_prepare_input($_POST['postcode']) : '';
                if (ACCOUNT_CITY == 'true') $city = isset($_POST['city']) ? tep_db_prepare_input($_POST['city']) : '';
                if (ACCOUNT_COUNTRY == 'true') $country = isset($_POST['country']) ? tep_db_prepare_input($_POST['country']) : '';
                if (ACCOUNT_STATE == 'true') {
                    if (isset($_POST['zone_id'])) {
                        $zone_id = tep_db_prepare_input($_POST['zone_id']);
                    } else {
                        $zone_id = false;
                    }
                    $state = tep_db_prepare_input($_POST['state']);
                }

                if (ACCOUNT_GENDER == 'true') {
                    if ( ($gender != 'm') && ($gender != 'f') ) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_GENDER_ERROR);
                    }
                }

                if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
                    $error = true;

                    $messageStack->add('addressbook', ENTRY_FIRST_NAME_ERROR);
                }

                if (ACCOUNT_LAST_NAME == 'true') {
                    if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_LAST_NAME_ERROR);
                    }
                }

                if (ACCOUNT_STREET_ADDRESS == 'true') {
                    if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_STREET_ADDRESS_ERROR);
                    }
                }

                if (ACCOUNT_POST_CODE == 'true') {
                    if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_POST_CODE_ERROR);
                    }
                }

                if (ACCOUNT_CITY == 'true') {
                    if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_CITY_ERROR);
                    }
                }

                if (ACCOUNT_COUNTRY == 'true') {
                    if (!is_numeric($country)) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_COUNTRY_ERROR);
                    }
                }

                if (ACCOUNT_STATE == 'true') {
                    $zone_id = 0;
                    $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
                    $check = tep_db_fetch_array($check_query);
                    $entry_state_has_zones = ($check['total'] > 0);
                    if ($entry_state_has_zones == true) {
                        $zone_query = tep_db_query("select distinct zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and (zone_name = '" . tep_db_input($state) . "' or zone_code = '" . tep_db_input($state) . "')");
                        if (tep_db_num_rows($zone_query) == 1) {
                            $zone = tep_db_fetch_array($zone_query);
                            $zone_id = $zone['zone_id'];
                        } else {
                            $error = true;

                            $messageStack->add('addressbook', ENTRY_STATE_ERROR_SELECT);
                        }
                    } else {
                        if (strlen($state) < ENTRY_STATE_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('addressbook', ENTRY_STATE_ERROR);
                        }
                    }
                }

                if ($error == false) {
                    $sql_data_array = array('entry_firstname' => $firstname);

                    if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['entry_lastname'] = $lastname;
                    if (ACCOUNT_STREET_ADDRESS == 'true') $sql_data_array['entry_street_address'] = $street_address;
                    if (ACCOUNT_POST_CODE == 'true') $sql_data_array['entry_postcode'] = $postcode;
                    if (ACCOUNT_CITY == 'true') $sql_data_array['entry_city'] = $city;
                    if (ACCOUNT_COUNTRY == 'true') $sql_data_array['entry_country_id'] = (int)$country;
                    if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
                    if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
                    if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb;
                    if (ACCOUNT_STATE == 'true') {
                        if ($zone_id > 0) {
                            $sql_data_array['entry_zone_id'] = (int)$zone_id;
                            $sql_data_array['entry_state'] = '';
                        } else {
                            $sql_data_array['entry_zone_id'] = '0';
                            $sql_data_array['entry_state'] = $state;
                        }
                    }

                    if ($_POST['action'] == 'update') {
                        $check_query = tep_db_query("select address_book_id from " . TABLE_ADDRESS_BOOK . " where address_book_id = '" . (int)$_GET['edit'] . "' and customers_id = '" . (int)$customer_id . "' limit 1");
                        if (tep_db_num_rows($check_query) == 1) {
                            tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "address_book_id = '" . (int)$_GET['edit'] . "' and customers_id ='" . (int)$customer_id . "'");

// reregister session variables
                            if ( (isset($_POST['primary']) && ($_POST['primary'] == 'on')) || ($_GET['edit'] == $customer_default_address_id) ) {
                                $customer_first_name = $firstname;
                                $customer_country_id = $country;
                                $customer_zone_id = (($zone_id > 0) ? (int)$zone_id : '0');
                                $customer_default_address_id = (int)$_GET['edit'];

                                $sql_data_array = array('customers_firstname' => $firstname,
                                    'customers_default_address_id' => (int)$_GET['edit']);

                                if(ACCOUNT_LAST_NAME == 'true') $sql_data_array['customers_lastname'] = $lastname;
                                if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;

                                tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "'");

                                $customers_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                                $customers_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                                tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_ip = '" . $customers_ip . "', customers_user_agent = '" . $customers_user_agent . "', customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");
                            }

                            $messageStack->add_session('addressbook', SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED, 'success');
                        }
                    } else {
                        if (tep_count_customer_address_book_entries() < MAX_ADDRESS_BOOK_ENTRIES) {
                            $sql_data_array['customers_id'] = (int)$customer_id;
                            tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);

                            $new_address_book_id = tep_db_insert_id();

// reregister session variables
                            if (isset($_POST['primary']) && ($_POST['primary'] == 'on')) {
                                $customer_first_name = $firstname;
                                $customer_country_id = $country;
                                $customer_zone_id = (($zone_id > 0) ? (int)$zone_id : '0');
                                if (isset($_POST['primary']) && ($_POST['primary'] == 'on')) $customer_default_address_id = $new_address_book_id;

                                $sql_data_array = array('customers_firstname' => $firstname);

                                if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['customers_lastname'] = $lastname;
                                if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
                                if (isset($_POST['primary']) && ($_POST['primary'] == 'on')) $sql_data_array['customers_default_address_id'] = $new_address_book_id;

                                tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "'");

                                $customers_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                                $customers_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                                tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_ip = '" . $customers_ip . "', customers_user_agent = '" . $customers_user_agent . "', customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");

                                $messageStack->add_session('addressbook', SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED, 'success');
                            }
                        }
                    }

                    tep_redirect(tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));
                }
            }

            if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {
                $entry_query = tep_db_query("select entry_gender, entry_company, entry_firstname, entry_lastname, entry_street_address, entry_suburb, entry_postcode, entry_city, entry_state, entry_zone_id, entry_country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$_GET['edit'] . "'");

                if (!tep_db_num_rows($entry_query)) {
                    $messageStack->add_session('addressbook', ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY);

                    tep_redirect(tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));
                }

                $entry = tep_db_fetch_array($entry_query);
            } elseif (isset($_GET['delete']) && is_numeric($_GET['delete'])) {
                if ($_GET['delete'] == $customer_default_address_id) {
                    $messageStack->add_session('addressbook', WARNING_PRIMARY_ADDRESS_DELETION, 'warning');

                    tep_redirect(tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));
                } else {
                    $check_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where address_book_id = '" . (int)$_GET['delete'] . "' and customers_id = '" . (int)$customer_id . "'");
                    $check = tep_db_fetch_array($check_query);

                    if ($check['total'] < 1) {
                        $messageStack->add_session('addressbook', ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY);

                        tep_redirect(tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));
                    }
                }
            } else {
                $entry = array();
            }

            if (!isset($_GET['delete']) && !isset($_GET['edit'])) {
                if (tep_count_customer_address_book_entries() >= MAX_ADDRESS_BOOK_ENTRIES) {
                    $messageStack->add_session('addressbook', ERROR_ADDRESS_BOOK_FULL);

                    tep_redirect(tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));
                }
            }

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));

            if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {
                $breadcrumb->add(NAVBAR_TITLE_MODIFY_ENTRY, tep_href_link(ADDRESS_BOOK_PROCESS_URL, 'edit=' . $_GET['edit'], 'SSL'));
            } elseif (isset($_GET['delete']) && is_numeric($_GET['delete'])) {
                $breadcrumb->add(NAVBAR_TITLE_DELETE_ENTRY, tep_href_link(ADDRESS_BOOK_PROCESS_URL, 'delete=' . $_GET['delete'], 'SSL'));
            } else {
                $breadcrumb->add(NAVBAR_TITLE_ADD_ENTRY, tep_href_link(ADDRESS_BOOK_PROCESS_URL, '', 'SSL'));
            }
            break;

        case ADVANCED_SEARCH_RESULT_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            $path = FILENAME_ADVANCED_SEARCH_RESULT;
            require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADVANCED_SEARCH_RESULT);

            $error = false;
            if ( isset($_GET['keywords']) && empty($_GET['keywords'])) {
                $error = true;
                $messageStack->add_session('search', ERROR_AT_LEAST_ONE_INPUT);
            } else {
                $dfrom = '';
                $dto = '';
                $pfrom = '';
                $pto = '';
                $keywords = '';

                if (isset($_GET['keywords'])) {
                    $keywords = tep_db_prepare_input($_GET['keywords']);
                }

                $date_check_error = false;

                if (tep_not_null($keywords)) {
                    if (!tep_parse_search_string($keywords, $search_keywords)) {
                        $error = true;

                        $messageStack->add_session('search', ERROR_INVALID_KEYWORDS);
                    }
                }
            }

            if (empty($keywords)) {
                $error = true;
                $messageStack->add_session('search', ERROR_AT_LEAST_ONE_INPUT);
            }

            if ($error == true) {
                tep_redirect(tep_href_link());
            }

            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ADVANCED_SEARCH_RESULT_URL, '', 'NONSSL', true, false));
            break;

        case AFFILIATES_HISTORY_URL:
            if (!$PARTNER || !tep_session_is_registered('partner_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link()); // TODO : redirect to ...
            }

            require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_HIGHCHARTS);

            $oscTemplate->addBlock('<script type="text/javascript" src="' . HTTP_SERVER_PARTNER . '/ext/daterange.js"></script>', 'header_tags');
            $oscTemplate->addBlock('<script type="text/javascript" src="' . HTTP_SERVER_PARTNER . '/ext/highcharts.js"></script>', 'header_tags');
            $oscTemplate->addBlock(
                '<script type="text/javascript">
                    Highcharts.setOptions({
                        lang : {
                            contextButtonTitle : "' . CHART_CONTEXT_BUTTON_TITLE . '",
                            decimalPoint       : "' . CHART_DECIMAL_POINT . '",
                            downloadJPEG       : "' . CHART_DOWNLOAD_JPEG . '",
                            downloadPDF        : "' . CHART_DOWNLOAD_PDF . '",
                            downloadPNG        : "' . CHART_DOWNLOAD_PNG . '",
                            downloadSVG        : "' . CHART_DOWNLOAD_SVG . '",
                            drillUpText        : "' . CHART_DRILL_UP_TEXT . '",
                            loading            : "' . CHART_LOADING . '",
                            months             :  ' . CHART_MONTHS . ',
                            printChart         : "' . CHART_PRINT_CHART . '",
                            resetZoom          : "' . CHART_RESET_ZOOM . '",
                            resetZoomTitle     : "' . CHART_RESET_ZOOM_TITLE . '",
                            shortMonths        :  ' . CHART_SHORT_MONTHS . ',
                            thousandsSep       : "' . CHART_THOUSANDS_SEP . '",
                            weekdays           :  ' . CHART_WEEKDAYS . '
                        }
                    });
                </script>',
                'header_tags'
            );
            $oscTemplate->addBlock('<script type="text/javascript" src="' . HTTP_SERVER_PARTNER . '/ext/linechart.js"></script>', 'header_tags');

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_AFFILIATES_HISTORY);

            break;

        case CHECKOUT_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            // if the customer is not logged on, redirect them to the login page
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot(array('mode' => 'SSL', 'page' => CHECKOUT_URL));
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }
            // if there is nothing in the customers cart, redirect them to the shopping cart page
            if ($cart->count_contents() < 1) {
                tep_redirect(tep_href_link(SHOPPING_CART_URL));
            }
            // avoid hack attempts during the checkout procedure by checking the internal cartID
            if (isset($cart->cartID) && tep_session_is_registered('cartID')) {
                if ($cart->cartID != $cartID) {
                    tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
                }
            }
            $select_str = 'select ab.address_book_id';
            if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
            if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
            if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
            if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
            if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
            if (ACCOUNT_STATE == 'true') {
                $select_str .= ', ab.entry_state';
                $select_str .= ', ab.entry_zone_id';
            }
            if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
            if (ACCOUNT_TELEPHONE_NUMBER == 'true') $select_str .= ', c.customers_telephone';
            $def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
            $def_addr_book = tep_db_fetch_array($def_addr_book_query);
            if (empty($def_addr_book['entry_state']) && empty($def_addr_book['entry_zone_id'])) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }
            unset($def_addr_book['entry_state']);
            unset($def_addr_book['entry_zone_id']);
            if(in_array(null, $def_addr_book) || in_array('',$def_addr_book) || in_array('0', $def_addr_book)) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            } else {
                tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL));
            }
            break;

        case CHECKOUT_CONFIRMATION_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }

            // if the customer is not logged on, redirect them to the login page
            if (!tep_session_is_registered('customer_id')) {
                if (tep_session_is_registered('tmp_customer_id')) {
                    $customer_id = $tmp_customer_id;
                    $customer_default_address_id = $tmp_customer_default_address_id;
                } else {
                    $navigation->set_snapshot(array('mode' => 'SSL', 'page' => CHECKOUT_CONFIRMATION_URL));
                    tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
                }
            }

// if there is nothing in the customers cart, redirect them to the shopping cart page
            if ($cart->count_contents() < 1) {
                tep_redirect(tep_href_link(SHOPPING_URL));
            }

// avoid hack attempts during the checkout procedure by checking the internal cartID
            if (isset($cart->cartID) && tep_session_is_registered('cartID')) {
                if ($cart->cartID != $cartID) {
                    tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
                }
            }

            $select_str = 'select ab.address_book_id';
            if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
            if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
            if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
            if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
            if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
            if (ACCOUNT_STATE == 'true') {
                $select_str .= ', ab.entry_state';
                $select_str .= ', ab.entry_zone_id';
            }
            if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
            if (ACCOUNT_TELEPHONE_NUMBER == 'true') $select_str .= ', c.customers_telephone';
            $def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
            $def_addr_book = tep_db_fetch_array($def_addr_book_query);
            if (empty($def_addr_book['entry_state']) && empty($def_addr_book['entry_zone_id'])) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }
            unset($def_addr_book['entry_state']);
            unset($def_addr_book['entry_zone_id']);
            if(in_array(null, $def_addr_book) || in_array('',$def_addr_book) || in_array('0', $def_addr_book)) {
                if (tep_session_is_registered('tmp_customer_id') && is_null($def_addr_book['entry_gender'])) {
                    //ok
                } else {
                    if (tep_session_is_registered('tmp_customer_id')) {
                        tep_redirect(tep_href_link(SHOPPING_URL));
                    } else {
                        tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
                    }
                }
            }

// if no shipping method has been selected, redirect the customer to the shipping method selection page
            if (!tep_session_is_registered('shipping')) {
                tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
            }

            if (!tep_session_is_registered('payment')) tep_session_register('payment');
            if (isset($_POST['payment'])) $payment = $_POST['payment'];

            if (!tep_session_is_registered('comments')) tep_session_register('comments');
            if (isset($_POST['comments']) && tep_not_null($_POST['comments'])) {
                $comments = tep_db_prepare_input($_POST['comments']);
            }

// load the selected payment module
            require(DIR_WS_CLASSES . 'payment.php');
            $payment_modules = new payment($payment);

            require(DIR_WS_CLASSES . 'order.php');
            $order = new order;

            $payment_modules->update_status();

            if ( ($payment_modules->selected_module != $payment) || ( is_array($payment_modules->modules) && (sizeof($payment_modules->modules) > 1) && !is_object($$payment) ) || (is_object($$payment) && ($$payment->enabled == false)) ) {
                tep_redirect(tep_href_link(CHECKOUT_PAYMENT_URL, 'error_message=' . urlencode(ERROR_NO_PAYMENT_MODULE_SELECTED), 'SSL'));
            }

            if (is_array($payment_modules->modules)) {
                $payment_modules->pre_confirmation_check();
            }

// load the selected shipping module
            require(DIR_WS_CLASSES . 'shipping.php');
            $shipping_modules = new shipping($shipping);

            require(DIR_WS_CLASSES . 'order_total.php');
            $order_total_modules = new order_total;
            $order_total_modules->process();

// Stock Check
            $any_out_of_stock = false;
            if (STOCK_CHECK == 'true') {
                for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
                    if (tep_check_stock($order->products[$i]['id'], $order->products[$i]['qty'])) {
                        $any_out_of_stock = true;
                    }
                }
                // Out of Stock
                if ( (STOCK_ALLOW_CHECKOUT != 'true') && ($any_out_of_stock == true) ) {
                    tep_redirect(tep_href_link(SHOPPING_URL));
                }
            }

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_CHECKOUT_CONFIRMATION);

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2);
            break;

        case CHECKOUT_INFO_EDIT_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

// needs to be included earlier to set the success message in the messageStack
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_CHECKOUT_INFO_EDIT);

// error checking when updating or adding an entry
            $process = false;
            if (isset($_POST['action']) && (($_POST['action'] == 'process') || ($_POST['action'] == 'update')) && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                $process = true;
                $error = false;

                if (ACCOUNT_GENDER == 'true') $gender = isset($_POST['gender']) ? tep_db_prepare_input($_POST['gender']) : '';
                if (ACCOUNT_COMPANY == 'true') $company = isset($_POST['company']) ? tep_db_prepare_input($_POST['company']) : '';
                $firstname = tep_db_prepare_input($_POST['firstname']);
                if (ACCOUNT_LAST_NAME == 'true') $lastname = isset($_POST['lastname']) ? tep_db_prepare_input($_POST['lastname']) : '';
                if (ACCOUNT_STREET_ADDRESS == 'true') $street_address = isset($_POST['street_address']) ? tep_db_prepare_input($_POST['street_address']) : '';
                if (ACCOUNT_SUBURB == 'true') $suburb = isset($_POST['suburb']) ? tep_db_prepare_input($_POST['suburb']) : '';
                if (ACCOUNT_POST_CODE == 'true') $postcode = isset($_POST['postcode']) ? tep_db_prepare_input($_POST['postcode']) : '';
                if (ACCOUNT_CITY == 'true') $city = isset($_POST['city']) ? tep_db_prepare_input($_POST['city']) : '';
                if (ACCOUNT_COUNTRY == 'true') $country = isset($_POST['country']) ? tep_db_prepare_input($_POST['country']) : '';
                if (ACCOUNT_TELEPHONE_NUMBER == 'true') $telephone = isset($_POST['telephone']) ? tep_db_prepare_input($_POST['telephone']) : '';
                if (ACCOUNT_STATE == 'true') {
                    if (isset($_POST['zone_id'])) {
                        $zone_id = tep_db_prepare_input($_POST['zone_id']);
                    } else {
                        $zone_id = false;
                    }
                    $state = tep_db_prepare_input($_POST['state']);
                }

                if (ACCOUNT_GENDER == 'true') {
                    if ( ($gender != 'm') && ($gender != 'f') ) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_GENDER_ERROR);
                    }
                }

                if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
                    $error = true;

                    $messageStack->add('addressbook', ENTRY_FIRST_NAME_ERROR);
                }

                if (ACCOUNT_LAST_NAME == 'true') {
                    if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_LAST_NAME_ERROR);
                    }
                }

                if (ACCOUNT_STREET_ADDRESS == 'true') {
                    if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_STREET_ADDRESS_ERROR);
                    }
                }

                if (ACCOUNT_POST_CODE == 'true') {
                    if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_POST_CODE_ERROR);
                    }
                }

                if (ACCOUNT_CITY == 'true') {
                    if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_CITY_ERROR);
                    }
                }

                if (ACCOUNT_COUNTRY == 'true') {
                    if (!is_numeric($country)) {
                        $error = true;

                        $messageStack->add('addressbook', ENTRY_COUNTRY_ERROR);
                    }
                }

                if (ACCOUNT_TELEPHONE_NUMBER == 'true'); {
                    if (strlen($telephone) < ENTRY_TELEPHONE_MIN_LENGTH) {
                        $error == true;

                        $messageStack->add('account_edit', ENTRY_TELEPHONE_NUMBER_ERROR);
                    }
                }

                if (ACCOUNT_STATE == 'true') {
                    $zone_id = 0;
                    $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
                    $check = tep_db_fetch_array($check_query);
                    $entry_state_has_zones = ($check['total'] > 0);
                    if ($entry_state_has_zones == true) {
                        $zone_query = tep_db_query("select distinct zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and (zone_name = '" . tep_db_input($state) . "' or zone_code = '" . tep_db_input($state) . "')");
                        if (tep_db_num_rows($zone_query) == 1) {
                            $zone = tep_db_fetch_array($zone_query);
                            $zone_id = $zone['zone_id'];
                        } else {
                            $error = true;

                            $messageStack->add('addressbook', ENTRY_STATE_ERROR_SELECT);
                        }
                    } else {
                        if (strlen($state) < ENTRY_STATE_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('addressbook', ENTRY_STATE_ERROR);
                        }
                    }
                }

                if ($error == false) {
                    $sql_data_array = array('entry_firstname' => $firstname);

                    if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['entry_lastname'] = $lastname;
                    if (ACCOUNT_STREET_ADDRESS == 'true') $sql_data_array['entry_street_address'] = $street_address;
                    if (ACCOUNT_POST_CODE == 'true') $sql_data_array['entry_postcode'] = $postcode;
                    if (ACCOUNT_CITY == 'true') $sql_data_array['entry_city'] = $city;
                    if (ACCOUNT_COUNTRY == 'true') $sql_data_array['entry_country_id'] = (int)$country;
                    if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
                    if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
                    if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb;
                    if (ACCOUNT_STATE == 'true') {
                        if ($zone_id > 0) {
                            $sql_data_array['entry_zone_id'] = (int)$zone_id;
                            $sql_data_array['entry_state'] = '';
                        } else {
                            $sql_data_array['entry_zone_id'] = '0';
                            $sql_data_array['entry_state'] = $state;
                        }
                    }

                    if ($_POST['action'] == 'update') {
                        $check_query = tep_db_query("select address_book_id from " . TABLE_ADDRESS_BOOK . " where address_book_id = '" . (int)$_GET['edit'] . "' and customers_id = '" . (int)$customer_id . "' limit 1");
                        if (tep_db_num_rows($check_query) == 1) {
                            tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "address_book_id = '" . (int)$_GET['edit'] . "' and customers_id ='" . (int)$customer_id . "'");


// reregister session variables
                            if ( (isset($_POST['primary']) && ($_POST['primary'] == 'on')) || ($_GET['edit'] == $customer_default_address_id) ) {
                                $customer_first_name = $firstname;
                                $customer_country_id = $country;
                                $customer_zone_id = (($zone_id > 0) ? (int)$zone_id : '0');
                                $customer_default_address_id = (int)$_GET['edit'];

                                $sql_data_array = array('customers_firstname' => $firstname,
                                    'customers_default_address_id' => (int)$_GET['edit']);

                                if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['customers_lastname'] = $lastname;
                                if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
                            }
                            if (ACCOUNT_TELEPHONE_NUMBER == 'true')
                            {
                                $sql_data_array['customers_telephone'] = $telephone;
                            }
                            tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "'");

                            $customers_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                            $customers_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                            tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_ip = '" . $customers_ip . "', customers_user_agent = '" . $customers_user_agent . "', customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");



                            $messageStack->add_session('addressbook', SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED, 'success');
                        }
                    } else {
                        if (tep_count_customer_address_book_entries() < MAX_ADDRESS_BOOK_ENTRIES) {
                            $sql_data_array['customers_id'] = (int)$customer_id;
                            tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);

                            $new_address_book_id = tep_db_insert_id();

// reregister session variables
                            if (isset($_POST['primary']) && ($_POST['primary'] == 'on')) {
                                $customer_first_name = $firstname;
                                $customer_country_id = $country;
                                $customer_zone_id = (($zone_id > 0) ? (int)$zone_id : '0');
                                if (isset($_POST['primary']) && ($_POST['primary'] == 'on')) $customer_default_address_id = $new_address_book_id;

                                $sql_data_array = array('customers_firstname' => $firstname);

                                if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['customers_lastname'] = $lastname;
                                if (ACCOUNT_GENDER == 'true') $sql_data_array['customers_gender'] = $gender;
                                if (isset($_POST['primary']) && ($_POST['primary'] == 'on')) $sql_data_array['customers_default_address_id'] = $new_address_book_id;

                                tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "'");

                                $customers_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                                $customers_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                                tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_ip = '" . $customers_ip . "', customers_user_agent = '" . $customers_user_agent . "', customers_info_date_account_last_modified = now() where customers_info_id = '" . (int)$customer_id . "'");

                                $messageStack->add_session('addressbook', SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED, 'success');
                            }
                        }
                    }

                    tep_redirect(tep_href_link(CHECKOUT_URL, '', 'SSL'));
                }
            }

            if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {
                $entry_query = tep_db_query("select c.customers_telephone, entry_gender, entry_company, entry_firstname, entry_lastname, entry_street_address, entry_suburb, entry_postcode, entry_city, entry_state, entry_zone_id, entry_country_id from " . TABLE_ADDRESS_BOOK . " join " . TABLE_CUSTOMERS . " as c on(address_book_id = c.customers_default_address_id) where c.customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$_GET['edit'] . "'");

                if (!tep_db_num_rows($entry_query)) {
                    $messageStack->add_session('addressbook', ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY);

                    tep_redirect(tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));
                }

                $entry = tep_db_fetch_array($entry_query);
            } else {
                $entry = array();
            }

            if (!isset($_GET['edit'])) {
                if (tep_count_customer_address_book_entries() >= MAX_ADDRESS_BOOK_ENTRIES) {
                    $messageStack->add_session('addressbook', ERROR_ADDRESS_BOOK_FULL);

                    tep_redirect(tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));
                }
            }

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(ACCOUNT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(ADDRESS_BOOK_URL, '', 'SSL'));

            if (isset($_GET['edit']) && is_numeric($_GET['edit'])) {
                $breadcrumb->add(NAVBAR_TITLE_MODIFY_ENTRY, tep_href_link(ADDRESS_BOOK_PROCESS_URL, 'edit=' . $_GET['edit'], 'SSL'));
            } else {
                $breadcrumb->add(NAVBAR_TITLE_ADD_ENTRY, tep_href_link(ADDRESS_BOOK_PROCESS_URL, '', 'SSL'));
            }
            break;

        case CHECKOUT_PAYMENT_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            // if the customer is not logged on, redirect them to the login page
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

// if there is nothing in the customers cart, redirect them to the shopping cart page
            if ($cart->count_contents() < 1) {
                tep_redirect(tep_href_link(SHOPPING_CART_URL));
            }

// if no shipping method has been selected, redirect the customer to the shipping method selection page
            if (!tep_session_is_registered('shipping')) {
                tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
            }

// avoid hack attempts during the checkout procedure by checking the internal cartID
            if (isset($cart->cartID) && tep_session_is_registered('cartID')) {
                if ($cart->cartID != $cartID) {
                    tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
                }
            }

            $select_str = 'select ab.address_book_id';
            if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
            if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
            if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
            if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
            if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
            if (ACCOUNT_STATE == 'true') {
                $select_str .= ', ab.entry_state';
                $select_str .= ', ab.entry_zone_id';
            }
            if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
            if (ACCOUNT_TELEPHONE_NUMBER == 'true') $select_str .= ', c.customers_telephone';
            $def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
            $def_addr_book = tep_db_fetch_array($def_addr_book_query);
            if (empty($def_addr_book['entry_state']) && empty($def_addr_book['entry_zone_id'])) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }
            unset($def_addr_book['entry_state']);
            unset($def_addr_book['entry_zone_id']);
            if(in_array(null, $def_addr_book) || in_array('',$def_addr_book) || in_array('0', $def_addr_book)) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }

// Stock Check
            if ( (STOCK_CHECK == 'true') && (STOCK_ALLOW_CHECKOUT != 'true') ) {
                $products = $cart->get_products();
                for ($i=0, $n=sizeof($products); $i<$n; $i++) {
                    if (tep_check_stock($products[$i]['id'], $products[$i]['quantity'])) {
                        tep_redirect(tep_href_link(SHOPPING_CART_URL));
                        break;
                    }
                }
            }

// if no billing destination address was selected, use the customers own address as default
            if (!tep_session_is_registered('billto')) {
                tep_session_register('billto');
                $billto = $customer_default_address_id;
            } else {
// verify the selected billing address
                if ( (is_array($billto) && empty($billto)) || is_numeric($billto) ) {
                    $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$billto . "'");
                    $check_address = tep_db_fetch_array($check_address_query);

                    if ($check_address['total'] != '1') {
                        $billto = $customer_default_address_id;
                        if (tep_session_is_registered('payment')) tep_session_unregister('payment');
                    }
                }
            }

            require(DIR_WS_CLASSES . 'order.php');
            $order = new order;

            if (!tep_session_is_registered('comments')) tep_session_register('comments');
            if (isset($_POST['comments']) && tep_not_null($_POST['comments'])) {
                $comments = tep_db_prepare_input($_POST['comments']);
            }

            $total_weight = $cart->show_weight();
            $total_count = $cart->count_contents();

// load all enabled payment modules
            require(DIR_WS_CLASSES . 'payment.php');
            $payment_modules = new payment;

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_CHECKOUT_PAYMENT);

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
            break;

        case CHECKOUT_PAYMENT_ADDRESS_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            // if the customer is not logged on, redirect them to the login page
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

// if there is nothing in the customers cart, redirect them to the shopping cart page
            if ($cart->count_contents() < 1) {
                tep_redirect(tep_href_link(SHOPPING_CART_URL));
            }

            $select_str = 'select ab.address_book_id';
            if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
            if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
            if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
            if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
            if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
            if (ACCOUNT_STATE == 'true') {
                $select_str .= ', ab.entry_state';
                $select_str .= ', ab.entry_zone_id';
            }
            if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
            if (ACCOUNT_TELEPHONE_NUMBER == 'true') $select_str .= ', c.customers_telephone';
            $def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
            $def_addr_book = tep_db_fetch_array($def_addr_book_query);
            if (empty($def_addr_book['entry_state']) && empty($def_addr_book['entry_zone_id'])) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }
            unset($def_addr_book['entry_state']);
            unset($def_addr_book['entry_zone_id']);
            if(in_array(null, $def_addr_book) || in_array('',$def_addr_book) || in_array('0', $def_addr_book)) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }

// needs to be included earlier to set the success message in the messageStack
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_CHECKOUT_PAYMENT_ADDRESS);

            $error = false;
            $process = false;
            if (isset($_POST['action']) && ($_POST['action'] == 'submit') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
// process a new billing address
                if (tep_not_null($_POST['firstname']) && (ACCOUNT_LAST_NAME != 'true' || tep_not_null($_POST['lastname'])) && (ACCOUNT_STREET_ADDRESS != 'true' || tep_not_null($_POST['street_address']))) {
                    $process = true;

                    if (ACCOUNT_GENDER == 'true') $gender = isset($_POST['gender']) ? tep_db_prepare_input($_POST['gender']) : '';
                    if (ACCOUNT_COMPANY == 'true') $company = isset($_POST['company']) ? tep_db_prepare_input($_POST['company']) : '';
                    $firstname = tep_db_prepare_input($_POST['firstname']);
                    if (ACCOUNT_LAST_NAME == 'true') $lastname = isset($_POST['lastname']) ? tep_db_prepare_input($_POST['lastname']) : '';
                    if (ACCOUNT_STREET_ADDRESS == 'true') $street_address = isset($_POST['street_address']) ? tep_db_prepare_input($_POST['street_address']) : '';
                    if (ACCOUNT_SUBURB == 'true') $suburb = isset($_POST['suburb']) ? tep_db_prepare_input($_POST['suburb']) : '';
                    if (ACCOUNT_POST_CODE == 'true') $postcode = isset($_POST['postcode']) ? tep_db_prepare_input($_POST['postcode']) : '';
                    if (ACCOUNT_CITY == 'true') $city = isset($_POST['city']) ? tep_db_prepare_input($_POST['city']) : '';
                    if (ACCOUNT_COUNTRY == 'true') $country = isset($_POST['country']) ? tep_db_prepare_input($_POST['country']) : '';
                    if (ACCOUNT_STATE == 'true') {
                        if (isset($_POST['zone_id'])) {
                            $zone_id = tep_db_prepare_input($_POST['zone_id']);
                        } else {
                            $zone_id = false;
                        }
                        $state = tep_db_prepare_input($_POST['state']);
                    }

                    if (ACCOUNT_GENDER == 'true') {
                        if ( ($gender != 'm') && ($gender != 'f') ) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_GENDER_ERROR);
                        }
                    }

                    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('checkout_address', ENTRY_FIRST_NAME_ERROR);
                    }

                    if (ACCOUNT_LAST_NAME == 'true') {
                        if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_LAST_NAME_ERROR);
                        }
                    }

                    if (ACCOUNT_STREET_ADDRESS == 'true') {
                        if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_STREET_ADDRESS_ERROR);
                        }
                    }

                    if (ACCOUNT_POST_CODE == 'true') {
                        if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_POST_CODE_ERROR);
                        }
                    }

                    if (ACCOUNT_CITY == 'true') {
                        if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_CITY_ERROR);
                        }
                    }

                    if (ACCOUNT_STATE == 'true') {
                        $zone_id = 0;
                        $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
                        $check = tep_db_fetch_array($check_query);
                        $entry_state_has_zones = ($check['total'] > 0);
                        if ($entry_state_has_zones == true) {
                            $zone_query = tep_db_query("select distinct zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and (zone_name = '" . tep_db_input($state) . "' or zone_code = '" . tep_db_input($state) . "')");
                            if (tep_db_num_rows($zone_query) == 1) {
                                $zone = tep_db_fetch_array($zone_query);
                                $zone_id = $zone['zone_id'];
                            } else {
                                $error = true;

                                $messageStack->add('checkout_address', ENTRY_STATE_ERROR_SELECT);
                            }
                        } else {
                            if (strlen($state) < ENTRY_STATE_MIN_LENGTH) {
                                $error = true;

                                $messageStack->add('checkout_address', ENTRY_STATE_ERROR);
                            }
                        }
                    }

                    if (ACCOUNT_COUNTRY == true) {
                        if ( (is_numeric($country) == false) || ($country < 1) ) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_COUNTRY_ERROR);
                        }
                    }

                    if ($error == false) {
                        $sql_data_array = array('customers_id' => $customer_id,
                            'entry_firstname' => $firstname);

                        if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['entry_lastname'] = $lastname;
                        if (ACCOUNT_STREET_ADDRESS == 'true') $sql_data_array['entry_street_address'] = $street_address;
                        if (ACCOUNT_POST_CODE == 'true') $sql_data_array['entry_postcode'] = $postcode;
                        if (ACCOUNT_CITY == 'true') $sql_data_array['entry_city'] = $city;
                        if (ACCOUNT_COUNTRY == 'true') $sql_data_array['entry_country_id'] = $country;
                        if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
                        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
                        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb;
                        if (ACCOUNT_STATE == 'true') {
                            if ($zone_id > 0) {
                                $sql_data_array['entry_zone_id'] = $zone_id;
                                $sql_data_array['entry_state'] = '';
                            } else {
                                $sql_data_array['entry_zone_id'] = '0';
                                $sql_data_array['entry_state'] = $state;
                            }
                        }

                        if (!tep_session_is_registered('billto')) tep_session_register('billto');

                        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);

                        $billto = tep_db_insert_id();

                        if (tep_session_is_registered('payment')) tep_session_unregister('payment');

                        tep_redirect(tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
                    }
// process the selected billing destination
                } elseif (isset($_POST['address'])) {
                    $reset_payment = false;
                    if (tep_session_is_registered('billto')) {
                        if ($billto != $_POST['address']) {
                            if (tep_session_is_registered('payment')) {
                                $reset_payment = true;
                            }
                        }
                    } else {
                        tep_session_register('billto');
                    }

                    $billto = $_POST['address'];

                    $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$billto . "'");
                    $check_address = tep_db_fetch_array($check_address_query);

                    if ($check_address['total'] == '1') {
                        if ($reset_payment == true) tep_session_unregister('payment');
                        tep_redirect(tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
                    } else {
                        tep_session_unregister('billto');
                    }
// no addresses to select from - customer decided to keep the current assigned address
                } else {
                    if (!tep_session_is_registered('billto')) tep_session_register('billto');
                    $billto = $customer_default_address_id;

                    tep_redirect(tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
                }
            }

// if no billing destination address was selected, use their own address as default
            if (!tep_session_is_registered('billto')) {
                $billto = $customer_default_address_id;
            }

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(CHECKOUT_PAYMENT_ADDRESS_URL, '', 'SSL'));

            $addresses_count = tep_count_customer_address_book_entries();
            break;

        case CHECKOUT_PROCESS_URL:
            if (!tep_session_is_registered('customer_id') && !tep_session_is_registered('tmp_customer_id')) {
                tep_redirect(tep_href_link(SHOPPING_URL));
            }

            if (tep_session_is_registered('tmp_customer_id')) {
                $customer_id = $tmp_customer_id;
                $customer_default_address_id = $tmp_customer_default_address_id;
            }

            $select_str = 'select ab.address_book_id';
            if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
            if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
            if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
            if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
            if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
            if (ACCOUNT_STATE == 'true') {
                $select_str .= ', ab.entry_state';
                $select_str .= ', ab.entry_zone_id';
            }
            if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
            if (ACCOUNT_TELEPHONE_NUMBER == 'true') $select_str .= ', c.customers_telephone';
            $def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
            $def_addr_book = tep_db_fetch_array($def_addr_book_query);
            if (empty($def_addr_book['entry_state']) && empty($def_addr_book['entry_zone_id'])) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }
            unset($def_addr_book['entry_state']);
            unset($def_addr_book['entry_zone_id']);
            if(in_array(null, $def_addr_book) || in_array('',$def_addr_book) || in_array('0', $def_addr_book)) {
                if (tep_session_is_registered('tmp_customer_id') && is_null($def_addr_book['entry_gender'])) {
                    //ok
                } else {
                    if (tep_session_is_registered('tmp_customer_id')) {
                        tep_redirect(tep_href_link(SHOPPING_URL));
                    } else {
                        tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
                    }
                }
            }
            require(FILENAME_CHECKOUT_PROCESS);
            break;

        case CHECKOUT_SHIPPING_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            require('includes/classes/http_client.php');


// if the customer is not logged on, redirect them to the login page
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }


// if there is nothing in the customers cart, redirect them to the shopping cart page
            if ($cart->count_contents() < 1) {
                tep_redirect(tep_href_link(SHOPPING_CART_URL));
            }

            $select_str = 'select ab.address_book_id';
            if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
            if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
            if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
            if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
            if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
            if (ACCOUNT_STATE == 'true') {
                $select_str .= ', ab.entry_state';
                $select_str .= ', ab.entry_zone_id';
            }
            if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
            if (ACCOUNT_TELEPHONE_NUMBER == 'true') $select_str .= ', c.customers_telephone';
            $def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
            $def_addr_book = tep_db_fetch_array($def_addr_book_query);
            if (empty($def_addr_book['entry_state']) && empty($def_addr_book['entry_zone_id'])) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }
            unset($def_addr_book['entry_state']);
            unset($def_addr_book['entry_zone_id']);
            if(in_array(null, $def_addr_book) || in_array('',$def_addr_book) || in_array('0', $def_addr_book)) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }

// if no shipping destination address was selected, use the customers own address as default
            if (!tep_session_is_registered('sendto')) {
                tep_session_register('sendto');
                $sendto = $customer_default_address_id;
            } else {
// verify the selected shipping address
                if ( (is_array($sendto) && empty($sendto)) || is_numeric($sendto) ) {
                    $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$sendto . "'");
                    $check_address = tep_db_fetch_array($check_address_query);

                    if ($check_address['total'] != '1') {
                        $sendto = $customer_default_address_id;
                        if (tep_session_is_registered('shipping')) tep_session_unregister('shipping');
                    }
                }
            }

            require(DIR_WS_CLASSES . 'order.php');
            $order = new order;

// register a random ID in the session to check throughout the checkout procedure
// against alterations in the shopping cart contents
            if (!tep_session_is_registered('cartID')) {
                tep_session_register('cartID');
            } elseif (($cartID != $cart->cartID) && tep_session_is_registered('shipping')) {
                tep_session_unregister('shipping');
            }

            $cartID = $cart->cartID = $cart->generate_cart_id();

// if the order contains only virtual products, forward the customer to the billing page as
// a shipping address is not needed
            if ($order->content_type == 'virtual') {
                if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
                $shipping = false;
                $sendto = false;
                tep_redirect(tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
            }

            $total_weight = $cart->show_weight();
            $total_count = $cart->count_contents();

// load all enabled shipping modules
            require(DIR_WS_CLASSES . 'shipping.php');
            $shipping_modules = new shipping;

            if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
                $pass = false;

                switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
                    case 'national':
                        if ($order->delivery['country_id'] == STORE_COUNTRY) {
                            $pass = true;
                        }
                        break;
                    case 'international':
                        if ($order->delivery['country_id'] != STORE_COUNTRY) {
                            $pass = true;
                        }
                        break;
                    case 'both':
                        $pass = true;
                        break;
                }

                $free_shipping = false;
                if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
                    $free_shipping = true;

                    include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
                }
            } else {
                $free_shipping = false;
            }

// process the selected shipping method
            if ( isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken) ) {
                if (!tep_session_is_registered('comments')) tep_session_register('comments');
                if (tep_not_null($_POST['comments'])) {
                    $comments = tep_db_prepare_input($_POST['comments']);
                }

                if (!tep_session_is_registered('shipping')) tep_session_register('shipping');

                if ( (tep_count_shipping_modules() > 0) || ($free_shipping == true) ) {
                    if ( (isset($_POST['shipping'])) && (strpos($_POST['shipping'], '_')) ) {
                        $shipping = $_POST['shipping'];

                        list($module, $method) = explode('_', $shipping);
                        if ( is_object($$module) || ($shipping == 'free_free') ) {
                            if ($shipping == 'free_free') {
                                $quote[0]['methods'][0]['title'] = FREE_SHIPPING_TITLE;
                                $quote[0]['methods'][0]['cost'] = '0';
                            } else {
                                $quote = $shipping_modules->quote($method, $module);
                            }
                            if (isset($quote['error'])) {
                                tep_session_unregister('shipping');
                            } else {
                                if ( (isset($quote[0]['methods'][0]['title'])) && (isset($quote[0]['methods'][0]['cost'])) ) {
                                    $shipping = array('id' => $shipping,
                                        'title' => (($free_shipping == true) ?  $quote[0]['methods'][0]['title'] : $quote[0]['module'] . ' (' . $quote[0]['methods'][0]['title'] . ')'),
                                        'cost' => $quote[0]['methods'][0]['cost']);

                                    tep_redirect(tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
                                }
                            }
                        } else {
                            tep_session_unregister('shipping');
                        }
                    }
                } else {
                    $shipping = false;

                    tep_redirect(tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
                }
            }

// get all available shipping quotes
            $quotes = $shipping_modules->quote();

// if no shipping method has been selected, automatically select the cheapest method.
// if the modules status was changed when none were available, to save on implementing
// a javascript force-selection method, also automatically select the cheapest shipping
// method if more than one module is now enabled
            if ( !tep_session_is_registered('shipping') || ( tep_session_is_registered('shipping') && ($shipping == false) && (tep_count_shipping_modules() > 1) ) ) $shipping = $shipping_modules->cheapest();

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_CHECKOUT_SHIPPING);

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
            break;

        case CHECKOUT_SHIPPING_ADDRESS_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            // if the customer is not logged on, redirect them to the login page
            if (!tep_session_is_registered('customer_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

// if there is nothing in the customers cart, redirect them to the shopping cart page
            if ($cart->count_contents() < 1) {
                tep_redirect(tep_href_link(SHOPPING_CART_URL));
            }

            $select_str = 'select ab.address_book_id';
            if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
            if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
            if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
            if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
            if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
            if (ACCOUNT_STATE == 'true') {
                $select_str .= ', ab.entry_state';
                $select_str .= ', ab.entry_zone_id';
            }
            if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
            if (ACCOUNT_TELEPHONE_NUMBER == 'true') $select_str .= ', c.customers_telephone';
            $def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
            $def_addr_book = tep_db_fetch_array($def_addr_book_query);
            if (empty($def_addr_book['entry_state']) && empty($def_addr_book['entry_zone_id'])) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }
            unset($def_addr_book['entry_state']);
            unset($def_addr_book['entry_zone_id']);
            if(in_array(null, $def_addr_book) || in_array('',$def_addr_book) || in_array('0', $def_addr_book)) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }

            // needs to be included earlier to set the success message in the messageStack
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_CHECKOUT_SHIPPING_ADDRESS);

            require(DIR_WS_CLASSES . 'order.php');
            $order = new order;

// if the order contains only virtual products, forward the customer to the billing page as
// a shipping address is not needed
            if ($order->content_type == 'virtual') {
                if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
                $shipping = false;
                if (!tep_session_is_registered('sendto')) tep_session_register('sendto');
                $sendto = false;
                tep_redirect(tep_href_link(CHECKOUT_PAYMENT_URL, '', 'SSL'));
            }

            $error = false;
            $process = false;
            if (isset($_POST['action']) && ($_POST['action'] == 'submit') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
// process a new shipping address
                if (tep_not_null($_POST['firstname']) && (ACCOUNT_LAST_NAME != 'true' || tep_not_null($_POST['lastname'])) && (ACCOUNT_STREET_ADDRESS != 'true' ||tep_not_null($_POST['street_address']))) {
                    $process = true;

                    if (ACCOUNT_GENDER == 'true') $gender = isset($_POST['gender']) ? tep_db_prepare_input($_POST['gender']) : '';
                    if (ACCOUNT_COMPANY == 'true') $company = isset($_POST['company']) ? tep_db_prepare_input($_POST['company']) : '';
                    $firstname = tep_db_prepare_input($_POST['firstname']);
                    if (ACCOUNT_LAST_NAME == 'true') $lastname = isset($_POST['lastname']) ? tep_db_prepare_input($_POST['lastname']) : '';
                    if (ACCOUNT_STREET_ADDRESS == 'true') $street_address = isset($_POST['street_address']) ? tep_db_prepare_input($_POST['street_address']) : '';
                    if (ACCOUNT_SUBURB == 'true') $suburb = isset($_POST['suburb']) ? tep_db_prepare_input($_POST['suburb']) : '';
                    if (ACCOUNT_POST_CODE == 'true') $postcode = isset($_POST['postcode']) ? tep_db_prepare_input($_POST['postcode']) : '';
                    if (ACCOUNT_CITY == 'true') $city = isset($_POST['city']) ? tep_db_prepare_input($_POST['city']) : '';
                    if (ACCOUNT_COUNTRY == 'true') $country = isset($_POST['country']) ? tep_db_prepare_input($_POST['country']) : '';
                    if (ACCOUNT_STATE == 'true') {
                        if (isset($_POST['zone_id'])) {
                            $zone_id = tep_db_prepare_input($_POST['zone_id']);
                        } else {
                            $zone_id = false;
                        }
                        $state = tep_db_prepare_input($_POST['state']);
                    }

                    if (ACCOUNT_GENDER == 'true') {
                        if ( ($gender != 'm') && ($gender != 'f') ) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_GENDER_ERROR);
                        }
                    }

                    if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
                        $error = true;

                        $messageStack->add('checkout_address', ENTRY_FIRST_NAME_ERROR);
                    }

                    if (ACCOUNT_LAST_NAME == 'true') {
                        if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_LAST_NAME_ERROR);
                        }
                    }

                    if (ACCOUNT_STREET_ADDRESS == 'true') {
                        if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_STREET_ADDRESS_ERROR);
                        }
                    }

                    if (ACCOUNT_POST_CODE == 'true') {
                        if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_POST_CODE_ERROR);
                        }
                    }

                    if (ACCOUNT_CITY == 'true') {
                        if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_CITY_ERROR);
                        }
                    }

                    if (ACCOUNT_STATE == 'true') {
                        $zone_id = 0;
                        $check_query = tep_db_query("select count(*) as total from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "'");
                        $check = tep_db_fetch_array($check_query);
                        $entry_state_has_zones = ($check['total'] > 0);
                        if ($entry_state_has_zones == true) {
                            $zone_query = tep_db_query("select distinct zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and (zone_name = '" . tep_db_input($state) . "' or zone_code = '" . tep_db_input($state) . "')");
                            if (tep_db_num_rows($zone_query) == 1) {
                                $zone = tep_db_fetch_array($zone_query);
                                $zone_id = $zone['zone_id'];
                            } else {
                                $error = true;

                                $messageStack->add('checkout_address', ENTRY_STATE_ERROR_SELECT);
                            }
                        } else {
                            if (strlen($state) < ENTRY_STATE_MIN_LENGTH) {
                                $error = true;

                                $messageStack->add('checkout_address', ENTRY_STATE_ERROR);
                            }
                        }
                    }

                    if (ACCOUNT_COUNTRY == 'true') {
                        if ( (is_numeric($country) == false) || ($country < 1) ) {
                            $error = true;

                            $messageStack->add('checkout_address', ENTRY_COUNTRY_ERROR);
                        }
                    }

                    if ($error == false) {
                        $sql_data_array = array('customers_id' => $customer_id,
                            'entry_firstname' => $firstname);

                        if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['entry_lastname'] = $lastname;
                        if (ACCOUNT_STREET_ADDRESS == 'true') $sql_data_array['entry_street_address'] = $street_address;
                        if (ACCOUNT_POST_CODE == 'true') $sql_data_array['entry_postcode'] = $postcode;
                        if (ACCOUNT_CITY == 'true') $sql_data_array['entry_city'] = $city;
                        if (ACCOUNT_COUNTRY == 'true') $sql_data_array['entry_country_id'] = $country;
                        if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
                        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
                        if (ACCOUNT_SUBURB == 'true' && tep_not_null($suburb)) $sql_data_array['entry_suburb'] = $suburb;
                        if (ACCOUNT_STATE == 'true') {
                            if ($zone_id > 0) {
                                $sql_data_array['entry_zone_id'] = $zone_id;
                                $sql_data_array['entry_state'] = '';
                            } else {
                                $sql_data_array['entry_zone_id'] = '0';
                                $sql_data_array['entry_state'] = $state;
                            }
                        }

                        if (!tep_session_is_registered('sendto')) tep_session_register('sendto');

                        tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);

                        $sendto = tep_db_insert_id();

                        if (tep_session_is_registered('shipping')) tep_session_unregister('shipping');

                        tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
                    }
// process the selected shipping destination
                } elseif (isset($_POST['address'])) {
                    $reset_shipping = false;
                    if (tep_session_is_registered('sendto')) {
                        if ($sendto != $_POST['address']) {
                            if (tep_session_is_registered('shipping')) {
                                $reset_shipping = true;
                            }
                        }
                    } else {
                        tep_session_register('sendto');
                    }

                    $sendto = $_POST['address'];

                    $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$sendto . "'");
                    $check_address = tep_db_fetch_array($check_address_query);

                    if ($check_address['total'] == '1') {
                        if ($reset_shipping == true) tep_session_unregister('shipping');
                        tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
                    } else {
                        tep_session_unregister('sendto');
                    }
                } else {
                    if (!tep_session_is_registered('sendto')) tep_session_register('sendto');
                    $sendto = $customer_default_address_id;

                    tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
                }
            }

// if no shipping destination address was selected, use their own address as default
            if (!tep_session_is_registered('sendto')) {
                $sendto = $customer_default_address_id;
            }

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(CHECKOUT_SHIPPING_ADDRESS_URL, '', 'SSL'));

            $addresses_count = tep_count_customer_address_book_entries();
            break;

        case CHECKOUT_SUCCESS_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            // if the customer is not logged on, redirect them to the shopping cart page
            if (!tep_session_is_registered('customer_id') && !tep_session_is_registered('tmp_customer_id')) {
                tep_redirect(tep_href_link(SHOPPING_URL));
            }

            if (tep_session_is_registered('tmp_customer_id')) {
                $customer_id = $tmp_customer_id;
            }

            $select_str = 'select ab.address_book_id';
            if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
            if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
            if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
            if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
            if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
            if (ACCOUNT_STATE == 'true') $select_str .= ', ab.entry_state';
            if (ACCOUNT_STATE == 'true') {
                $select_str .= ', ab.entry_state';
                $select_str .= ', ab.entry_zone_id';
            }
            if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
            if (ACCOUNT_TELEPHONE_NUMBER == 'true') $select_str .= ', c.customers_telephone';
            $def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
            $def_addr_book = tep_db_fetch_array($def_addr_book_query);
            if (empty($def_addr_book['entry_state']) && empty($def_addr_book['entry_zone_id'])) {
                tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
            }
            unset($def_addr_book['entry_state']);
            unset($def_addr_book['entry_zone_id']);
            if(in_array(null, $def_addr_book) || in_array('',$def_addr_book) || in_array('0', $def_addr_book)) {
                if (tep_session_is_registered('tmp_customer_id') && is_null($def_addr_book['entry_gender'])) {
                    //ok
                } else {
                    if (tep_session_is_registered('tmp_customer_id')) {
                        tep_redirect(tep_href_link(SHOPPING_URL));
                    } else {
                        tep_redirect(tep_href_link(CHECKOUT_INFO_EDIT_URL, 'edit=' . $def_addr_book['address_book_id']));
                    }
                }
            }

            if (isset($_GET['action']) && ($_GET['action'] == 'update')) {
                $notify_string = '';

                if (isset($_POST['notify']) && !empty($_POST['notify'])) {
                    $notify = $_POST['notify'];

                    if (!is_array($notify)) {
                        $notify = array($notify);
                    }

                    for ($i=0, $n=sizeof($notify); $i<$n; $i++) {
                        if (is_numeric($notify[$i])) {
                            $notify_string .= 'notify[]=' . $notify[$i] . '&';
                        }
                    }

                    if (!empty($notify_string)) {
                        $notify_string = 'action=notify&' . substr($notify_string, 0, -1);
                    }
                }

                tep_redirect(tep_href_link('', $notify_string));
            }

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_CHECKOUT_SUCCESS);

            $breadcrumb->add(NAVBAR_TITLE_1);
            $breadcrumb->add(NAVBAR_TITLE_2);

            $global_query = tep_db_query("select global_product_notifications from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customer_id . "'");
            $global = tep_db_fetch_array($global_query);

            if ($global['global_product_notifications'] != '1') {
                $orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where customers_id = '" . (int)$customer_id . "' order by date_purchased desc limit 1");
                $orders = tep_db_fetch_array($orders_query);

                $products_array = array();
                $products_query = tep_db_query("select products_id, products_name from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$orders['orders_id'] . "' order by products_name");
                while ($products = tep_db_fetch_array($products_query)) {
                    $products_array[] = array('id' => $products['products_id'],
                        'text' => $products['products_name']);
                }
            }
            break;

        case CONTACT_US_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_CONTACT_US);

            if (isset($_GET['action']) && ($_GET['action'] == 'send') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                $error = false;

                $name = tep_db_prepare_input($_POST['name']);
                $email_address = tep_db_prepare_input($_POST['email']);
                $enquiry = tep_db_prepare_input($_POST['enquiry']);

                if (!tep_validate_email($email_address)) {
                    $error = true;

                    $messageStack->add('contact', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
                }

                $actionRecorder = new actionRecorder('ar_contact_us', (tep_session_is_registered('customer_id') ? $customer_id : null), $name);
                if (!$actionRecorder->canPerform()) {
                    $error = true;

                    $actionRecorder->record(false);

                    $messageStack->add('contact', sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES') ? (int)MODULE_ACTION_RECORDER_CONTACT_US_EMAIL_MINUTES : 15)));
                }

                if ($error == false) {
                    tep_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, EMAIL_SUBJECT, $enquiry, $name, $email_address);

                    $actionRecorder->record();

                    tep_redirect(tep_href_link(CONTACT_US_URL, 'action=success'));
                }
            }

            $breadcrumb->add(NAVBAR_TITLE, tep_href_link(CONTACT_US_URL));
            break;

        case COOKIE_USAGE_URL:
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_COOKIE_USAGE);
            $breadcrumb->add(NAVBAR_TITLE, tep_href_link(COOKIE_USAGE_URL));
            break;

        case CREATE_ACCOUNT_URL:
            if ((tep_session_is_registered('customer_id') && !$PARTNER) || (tep_session_is_registered('partner_id') && $PARTNER)) {
                //$navigation->set_snapshot();
                die(json_encode(array('url' => tep_href_link(ACCOUNT_URL, '', 'SSL'))));
            }
            if (tep_session_is_registered('filter_data'))
                $_POST = array_merge($_POST,$_SESSION['filter_data']);

            $process = false;
            if ((isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken))) {
                $account_data = $_POST;
                $account_data['soc_type_id'] = 0;
                tep_create_account($account_data, $PARTNER, true);
            }
            exit;

        case CREATE_ACCOUNT_SUCCESS_URL:
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_CREATE_ACCOUNT_SUCCESS);
            $breadcrumb->add(NAVBAR_TITLE_1);
            $breadcrumb->add(NAVBAR_TITLE_2);

            if (sizeof($navigation->snapshot) > 0) {
                $origin_href = tep_href_link($navigation->snapshot['page'], tep_array_to_string($navigation->snapshot['get'], array(tep_session_name())), $navigation->snapshot['mode']);
                $navigation->clear_snapshot();
            } else {
                $origin_href = tep_href_link();
            }
            break;

        case DOWNLOAD_URL:
            require(FILENAME_DOWNLOAD);
            break;

        case FACEBOOK_LOGIN_URL:
            require 'facebook/facebook.php';
            $facebook = new Facebook(array(
                'appId'  => FB_APP_ID,
                'secret' => FB_APP_SECRET,
            ));

// See if there is a user from a cookie
            $user = $facebook->getUser();
            if ($user) {
                try {
                    // Proceed knowing you have a logged in user who's authenticated.
                    $user_profile = $facebook->api('/me');
                } catch (FacebookApiException $e) {
                    tep_redirect(tep_href_link(LOGIN_URL));
                }

                if (!isset($user_profile['email'])) {
                    tep_redirect(tep_href_link(LOGIN_URL, 'errormsg=email'));
                }

                $account_data = array(
                    'soc_id' => 0,
                    'soc_type_id' => 1,
                    'firstname' => '',
                    'lastname' => '',
                    'city' => '',
                    'state' => '',
                    'company' => '',
                    'gender' => 'm',
                    'country' => 0,
                    'dob' => null
                );

                $account_data['email_address'] = $user_profile['email'];

                if (isset($user_profile['first_name'])) {
                    $account_data['firstname'] = $user_profile['first_name'];
                }
                if (isset($user_profile['last_name'])) {
                    $account_data['lastname'] = $user_profile['last_name'];
                }

                if (isset($user_profile['id'])) {
                    $account_data['soc_id'] = $user_profile['id'];
                }


                if (isset($user_profile['gender'])) {
                    $account_data['gender'] = $user_profile['gender'][0];
                }

                if (isset($user_profile['birthday'])) {
                    $account_data['dob'] = date('Y-m-d H:i:s',strtotime($user_profile['birthday']));
                }

                if ((isset($user_profile['location']) && $addr = $user_profile['location']) || (isset($user_profile['hometown']) && $addr = $user_profile['hometown'])) {
                    $addr = explode(', ',$addr['name']);
                    if (is_array($addr)) $addr = array_pad($addr, -3, '');
                    list($account_data['city'], $account_data['state'],) = list($filter_data['city'], $filter_data['state'], $filter_data['country']) = tep_db_prepare_input($addr);
                    $country_query = tep_db_query('select countries_id from ' . TABLE_COUNTRIES . ' where countries_name = "' . tep_db_input($filter_data['country']) . '" limit 1');
                    if (tep_db_num_rows($country_query) > 0) {
                        $country = tep_db_fetch_array($country_query);
                        $account_data['country'] = (int)$country['countries_id'];
                    } else{
                        $account_data['country'] = 0;
                        $account_data['state'] = $filter_data['country'];
                        $account_data['city'] = $filter_data['city'] . $filter_data['state'];
                    }
                }

                if (isset($user_profile['work']) && tep_not_null($user_profile['work'])) {
                    foreach($user_profile['work'] as $current_work) {
                        if (!isset($current_work['end_date'])) {
                            $account_data['company'] = $current_work['employer']['name'];
                            break;
                        }
                    }
                }

                tep_soc_sign_in_up($account_data, $PARTNER);
            }
            tep_redirect(tep_href_link(LOGIN_URL));
            break;

        case GOOGLE_SIGN_IN_URL:
            require (DIR_WS_MODULES . 'socials/google_plus/google-api-php-client/src/Google_Client.php');
            require (DIR_WS_MODULES . 'socials/google_plus/google-api-php-client/src/contrib/Google_Oauth2Service.php');

            $client = new Google_Client();
            $oauth2 = new Google_Oauth2Service($client);

            if (isset($_GET['error'])) {
                tep_redirect(tep_href_link(LOGIN_URL));
            }
            if (isset($_GET['code'])) {
                $client->authenticate($_GET['code']);
                $token = $client->getAccessToken();
                tep_session_register('token');
                tep_redirect(tep_href_link(GOOGLE_SIGN_IN_URL));
                return;
            }

            if (isset($_SESSION['token'])) {
                $client->setAccessToken($_SESSION['token']);
            }

            if (isset($_REQUEST['logout'])) {
                tep_session_unregister('token');
                $client->revokeToken();
            }

            if ($client->getAccessToken()) {
                $user = $oauth2->userinfo->get();

                $token = $client->getAccessToken();
                tep_session_register('token');

                if (!isset($user['email'])) {
                    tep_redirect(tep_href_link(LOGIN_URL, 'errormsg=email'));
                }

                $account_data = array(
                    'soc_id' => 0,
                    'soc_type_id' => 2,
                    'firstname' => '',
                    'lastname' => '',
                    'city' => '',
                    'state' => '',
                    'company' => '',
                    'gender' => 'm',
                    'country' => 0,
                    'dob' => null
                );

                $account_data['email_address'] = filter_var($user['email'], FILTER_SANITIZE_EMAIL);

                if (isset($user['gender'])) {
                    $account_data['gender'] = $user['gender'][0];
                }
                if (isset($user['id'])) {
                    $account_data['soc_id'] = $user['id'];
                }
                if (isset($user['given_name'])) {
                    $account_data['firstname'] = $user['given_name'];
                }
                if (isset($user['family_name'])) {
                    $account_data['lastname'] = $user['family_name'];
                }

                tep_soc_sign_in_up($account_data, $PARTNER);
                tep_redirect(tep_href_link(ACCOUNT_URL));
            } else tep_redirect(tep_href_link(LOGIN_URL));

            break;

        case LOGIN_URL:
            if ($session_started == false) {
                if (isset($_GET['ajax'])) {
                    die(json_encode(array('url' => tep_href_link(COOKIE_USAGE_URL))));
                } else {
                    tep_redirect(tep_href_link(COOKIE_USAGE_URL));
                }
            }
            if (tep_session_is_registered('customer_id') && !$PARTNER) {
                if (isset($_GET['ajax'])) {
                    die(json_encode(array('url' => tep_href_link(ACCOUNT_URL, '', 'SSL'))));
                } else {
                    tep_redirect(tep_href_link(ACCOUNT_URL, '', 'SSL'));
                }
            }
            if (tep_session_is_registered('partner_id') && $PARTNER) {
                if (isset($_GET['ajax'])) {
                    die(json_encode(array('url' => tep_href_link(ACCOUNT_URL, '', 'SSL'))));
                } else {
                    tep_redirect(tep_href_link(ACCOUNT_URL, '', 'SSL'));
                }
            }
            if (isset($_GET['errormsg']) && $_GET['errormsg'] == 'email') {
                $messageStack->add('login', TEXT_LOGIN_EMAIL_ERROR);
            }


//------------google -------------------

            require_once DIR_WS_MODULES . 'socials/google_plus/google-api-php-client/src/Google_Client.php';
            require_once DIR_WS_MODULES . 'socials/google_plus/google-api-php-client/src/contrib/Google_Oauth2Service.php';

            $client = new Google_Client();

            $oauth2 = new Google_Oauth2Service($client);

            $authUrl = $client->createAuthUrl();


//-----google end------------------

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_LOGIN);  // See if there is a user from a cookie

            if (isset($_GET['action']) && ($_GET['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                $email_address = tep_db_prepare_input($_POST['email_address']);
                $password = tep_db_prepare_input($_POST['password']);

                tep_login(array('email_address' => $email_address, 'password' => $password), $PARTNER, true);
            }
            if (isset($_GET['ajax'])) {
                require(DIR_FS_CATALOG . FILENAME_LOGIN);
                die();
            }
            break;

        case LOGOFF_URL:

            tep_logoff();
            $cart->reset();

            tep_redirect(tep_href_link());
            break;

        case MANUFACTURERS_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            $main = '';
            $item_get = tep_strtolower_int($item_get);
            if(tep_not_null($item_get))
            {
                $manufacturers_query = tep_db_query("select manufacturers_name, manufacturers_id, manufacturers_image from " . TABLE_MANUFACTURERS);
                while ($res = tep_db_fetch_array($manufacturers_query)) {
                    if (tep_clean_text_int($res['manufacturers_name']) === $item_get) {
                        $main = $main_get;
                        $item = $res['manufacturers_id'];
                        $breadcrumb->add($res['manufacturers_name'], tep_href_link(MANUFACTURERS_URL . '/' . $item_get));
                        $og_title = $res['manufacturers_name'] . ' products';
                        $og_desc = $res['manufacturers_name'];
                        $og_pic = DIR_WS_IMAGES . $res['manufacturers_image'];
                        $og_url = tep_href_link(MANUFACTURERS_URL . '/' . tep_clean_text_int($res['manufacturers_name']));
                        require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);
                        break;
                    }
                }
            }
            if (!tep_not_null($main)) {
                $not_found = true;
            }
            break;

        case PARTNERS_ANALYTIC_URL:
            if (!$PARTNER || !tep_session_is_registered('partner_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link()); // TODO : redirect to ...
            }

            if(!empty($item_get) && in_array($item_get, array(METHODS_ANALYTIC_URL, PRODUCTS_ANALYTIC_URL, METHOD_ANALYTIC_URL))) {
                $item = $item_get;
                $details = $item;
                if(in_array($item_get, array(METHOD_ANALYTIC_URL))) {
                    $extra = $extra_get;
                }
            } else {
                $details = '';
            }

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_PARTNERS_ANALYTIC);
            require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_HIGHCHARTS);

            $oscTemplate->addBlock('<script type="text/javascript" src="' . HTTP_SERVER_PARTNER . '/ext/highcharts.js"></script>', 'header_tags');
            $oscTemplate->addBlock(
                '<script type="text/javascript">
                    Highcharts.setOptions({
                        lang : {
                            contextButtonTitle : "' . CHART_CONTEXT_BUTTON_TITLE . '",
                            decimalPoint       : "' . CHART_DECIMAL_POINT . '",
                            downloadJPEG       : "' . CHART_DOWNLOAD_JPEG . '",
                            downloadPDF        : "' . CHART_DOWNLOAD_PDF . '",
                            downloadPNG        : "' . CHART_DOWNLOAD_PNG . '",
                            downloadSVG        : "' . CHART_DOWNLOAD_SVG . '",
                            drillUpText        : "' . CHART_DRILL_UP_TEXT . '",
                            loading            : "' . CHART_LOADING . '",
                            months             :  ' . CHART_MONTHS . ',
                            printChart         : "' . CHART_PRINT_CHART . '",
                            resetZoom          : "' . CHART_RESET_ZOOM . '",
                            resetZoomTitle     : "' . CHART_RESET_ZOOM_TITLE . '",
                            shortMonths        :  ' . CHART_SHORT_MONTHS . ',
                            thousandsSep       : "' . CHART_THOUSANDS_SEP . '",
                            weekdays           :  ' . CHART_WEEKDAYS . '
                        }
                    });
                </script>',
                'header_tags'
            );
            $oscTemplate->addBlock('<script type="text/javascript" src="' . HTTP_SERVER_PARTNER . '/ext/daterange.js"></script>', 'header_tags');
            $oscTemplate->addBlock('<script type="text/javascript" src="' . HTTP_SERVER_PARTNER . '/ext/linechart.js"></script>', 'header_tags');
            $oscTemplate->addBlock('<script type="text/javascript" src="' . HTTP_SERVER_PARTNER . '/ext/barchart.js"></script>', 'header_tags');

            break;

        case PARTNERS_MARKETING_URL:
            if (!$PARTNER || !tep_session_is_registered('partner_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link()); // TODO : redirect to ...
            }

            if (empty($item_get) || empty($extra_get) || !is_numeric($extra_get)) {
                $not_found = true;
                break;
            }

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_PARTNERS_MARKETING);

            $method_query = tep_db_query('select m.method_id, mi.method_name from ' . TABLE_MARKETING_METHODS . ' as m join ' . TABLE_MARKETING_METHODS_INFO . ' as mi on(m.method_id = mi.method_id and m.method_status = b\'1\' and mi.language_id = ' . (int)$languages_id . ')');

            $not_found = true;
            while ($method_info = tep_db_fetch_array($method_query)) {
                if (tep_clean_text_int($method_info['method_name']) == $item_get) {
                    $not_found = false;
                    break;
                }
            }

            if ($not_found) {
                break;
            }
            $item = $item_get;

            switch($method_info['method_id']) {
                case 2: //banners
                    $TABLE_MATERIAL = TABLE_MARKETING_BANNERS;
                    $TABLE_MATERIAL_TAGS = TABLE_MARKETING_BANNERS_TAGS;
                    $oscTemplate->addBlock('<script type="text/javascript" src="ext/swfobject.js"></script>', 'header_tags');
                    break;
                case 3: //pics
                    $TABLE_MATERIAL = TABLE_MARKETING_PICS;
                    $TABLE_MATERIAL_TAGS = TABLE_MARKETING_PICS_TAGS;
                    break;
                case 4: //mails
                    $TABLE_MATERIAL = TABLE_MARKETING_MAILS;
                    $TABLE_MATERIAL_TAGS = TABLE_MARKETING_MAILS_TAGS;
                    break;
                case 5: //statuses
                    $TABLE_MATERIAL = TABLE_MARKETING_STATUSES;
                    $TABLE_MATERIAL_TAGS = TABLE_MARKETING_STATUSES_TAGS;
                    break;
                case 6: //shares
                    $TABLE_MATERIAL = TABLE_MARKETING_SHARES;
                    $TABLE_MATERIAL_TAGS = TABLE_MARKETING_SHARES_TAGS;
                    break;
                default: // posts
                    $TABLE_MATERIAL = TABLE_MARKETING_POSTS;
                    $TABLE_MATERIAL_TAGS = TABLE_MARKETING_POSTS_TAGS;
                    break;
            }

            $material_query = tep_db_query('select * from ' . $TABLE_MATERIAL . ' where material_id = ' . (int)$extra_get . ' and material_status = 1');

            if (tep_db_num_rows($material_query) != 1) {
                $not_found = true;
                break;
            }

            $material = tep_db_fetch_array($material_query);

            $extra = (int)$extra_get;

            $og_title = $material['material_title'];
            break;

        case PARTNERS_MARKETING_DOWNLOADS_URL:
            if (!$PARTNER || !tep_session_is_registered('partner_id')) {
                tep_redirect(tep_href_link()); // TODO : redirect to ...
            }
            empty($item_get) && tep_exit();

            tep_db_num_rows($check_image = tep_db_query('select material_pic, material_title from ' . TABLE_MARKETING_PICS . ' where material_id = ' . (int)$item_get . ' and material_status = 1')) != 1 && tep_exit();
            $item = (int)$item_get;
            $image = tep_db_fetch_array($check_image);

            require(DIR_FS_CATALOG . FILENAME_PARTNERS_MARKETING_DOWNLOADS);
            tep_exit();

            break;

        case PARTNERS_MARKETING_MATERIALS_URL:
            if (!$PARTNER || !tep_session_is_registered('partner_id')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link()); // TODO : redirect to ...
            }

            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_PARTNERS_MARKETING_MATERIALS);

            $oscTemplate->addBlock('<script type="text/javascript" src="ext/swfobject.js"></script>', 'header_tags');
	        $oscTemplate->addBlock('<link rel="stylesheet" href="ext/tinyscrollbar/tinyscrollbar.css"/>', 'header_tags');
	        $oscTemplate->addBlock('<script src="ext/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>', 'header_tags');

            //methods block

            $methods_query = tep_db_query('select m.method_id, mi.method_name from ' . TABLE_MARKETING_METHODS . ' as m join ' . TABLE_MARKETING_METHODS_INFO . ' mi on(m.method_id = mi.method_id and mi.language_id = ' . (int)$languages_id . ' and m.method_status = b\'1\') order by sort_order');

            $methods_block = '<div class="marketing-block">';
            $methods_block .= '<div class="marketing-block-head">' . TEXT_MARKETING_METHOD . '</div>';
            $methods_block .= '<div class="marketing-block-body">';
            $check = true;
            while ($method = tep_db_fetch_array($methods_query)) {
                $methods_block .= '<label>' . tep_draw_radio_field('method', (int)$method['method_id']) . '<span>' . $method['method_name'] . '</span>' . '</label>';
                $check = false;
            }
            $methods_block .= '</div>';
            $methods_block .= '</div>';

            //products block

            $product_block = '<div class="marketing-block" id="products-block">';
            $product_block .= '<div class="marketing-block-head">' . TEXT_MARKETING_PRODUCT . '</div>';
            $product_block .= '<div class="marketing-block-body">';
            $products_query = tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " as p join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on(p.products_id = pd.products_id and p.products_status = 1 and pd.language_id = " . (int)$languages_id . ") order by p.products_ordered");
            $product_block .= '<label>' . tep_draw_checkbox_field('products[]', 0) . '<span>' . TEXT_PRODUCT_GLOBAL . '</span>' . '</label>';
            while($product = tep_db_fetch_array($products_query)){
                $product_block .= '<label>' . tep_draw_checkbox_field('products[]', (int)$product['products_id']) . '<span>' . $product['products_name'] . '</span>' . '</label>';
            }
            $product_block .= '</div>';
            $product_block .= '</div>';

            //languages

            $language_block = '<div class="marketing-block" id="languages-block">';
            $language_block .= '<div class="marketing-block-head">' . TEXT_MARKETING_LANGUAGE . '</div>';
            $language_block .= '<div class="marketing-block-body">';
            $langs_query = tep_db_query("select languages_id, name from " . TABLE_LANGUAGES . " order by sort_order");
            while($lang = tep_db_fetch_array($langs_query)) {
                $language_block .= '<label>' . tep_draw_checkbox_field('languages[]', (int)$lang['languages_id']) . '<span>' . $lang['name'] . '</span>' . '</label>';
            }
            $language_block .= '</div>';
            $language_block .= '</div>';


            //tags block

            $tags_block = '<div class="marketing-block" id="tags-block"><div class="marketing-block-head">' . TEXT_MARKETING_TAGS . '</div><div class="marketing-block-body"></div></div>';

			//filter button

			//$filter_button = '<div id="marketing-filter-cont"><button id="marketing-do-filter" type="button">' . TEXT_MARKETING_FILTER . '</button></div>';

            $marketing_block = '<div id="marketing-filter">' . $methods_block . $product_block . $language_block . $tags_block . /*$filter_button . */'</div>';

            $oscTemplate->addBlock($marketing_block, 'boxes_column_left');
            break;

        case PASSWORD_FORGOTTEN_URL:
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_PASSWORD_FORGOTTEN);

            $password_reset_initiated = false;

            if (isset($_GET['action']) && ($_GET['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                $email_address = tep_db_prepare_input($_POST['email_address']);

                $check_customer_query = tep_db_query("select customers_firstname, customers_lastname, customers_id from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "'");
                if (tep_db_num_rows($check_customer_query)) {
                    $check_customer = tep_db_fetch_array($check_customer_query);

                    $actionRecorder = new actionRecorder('ar_reset_password', $check_customer['customers_id'], $email_address);

                    if ($actionRecorder->canPerform()) {
                        $actionRecorder->record();

                        $reset_key = tep_create_random_value(40);

                        $customers_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                        $customers_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                        tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_ip = '" . $customers_ip . "', customers_user_agent = '" . $customers_user_agent . "', password_reset_key = '" . tep_db_input($reset_key) . "', password_reset_date = now() where customers_info_id = '" . (int)$check_customer['customers_id'] . "'");

                        $reset_key_url = tep_href_link(PASSWORD_RESET_URL, 'account=' . urlencode($email_address) . '&key=' . $reset_key, 'SSL', false);

                        if ( strpos($reset_key_url, '&amp;') !== false ) {
                            $reset_key_url = str_replace('&amp;', '&', $reset_key_url);
                        }

                        tep_mail($check_customer['customers_firstname'] . ' ' . $check_customer['customers_lastname'], $email_address, EMAIL_PASSWORD_RESET_SUBJECT, sprintf(EMAIL_PASSWORD_RESET_BODY, $reset_key_url), STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);

                        $password_reset_initiated = true;
                        die('{"password_reset_initiated":true}');
                    } else {
                        $actionRecorder->record(false);

                        $messageStack->add('password_forgotten', sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_RESET_PASSWORD_MINUTES') ? (int)MODULE_ACTION_RECORDER_RESET_PASSWORD_MINUTES : 5)));
                    }
                } else {
                    $messageStack->add('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
                }
                if ($messageStack->size('password_forgotten') > 0) {
                    die(json_encode(array('error' => $messageStack->output('password_forgotten'))));
                }
            }
            exit();

        case PASSWORD_RESET_URL:
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_PASSWORD_RESET);

            $error = false;

            if ( !isset($_GET['account']) || !isset($_GET['key']) ) {
                $error = true;

                $messageStack->add_session('password_forgotten', TEXT_NO_RESET_LINK_FOUND);
            }

            if ($error == false) {
                $email_address = tep_db_prepare_input($_GET['account']);
                $password_key = tep_db_prepare_input($_GET['key']);

                if ( (strlen($email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) || (tep_validate_email($email_address) == false) ) {
                    $error = true;

                    $messageStack->add_session('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
                } elseif (strlen($password_key) != 40) {
                    $error = true;

                    $messageStack->add_session('password_forgotten', TEXT_NO_RESET_LINK_FOUND);
                } else {
                    $check_customer_query = tep_db_query("select c.customers_id, c.customers_email_address, ci.password_reset_key, ci.password_reset_date from " . TABLE_CUSTOMERS . " c left join " . TABLE_CUSTOMERS_INFO . " ci on(c.customers_id = ci.customers_info_id) where c.customers_email_address = '" . tep_db_input($email_address) . "'");
                    if (tep_db_num_rows($check_customer_query)) {
                        $check_customer = tep_db_fetch_array($check_customer_query);

                        if ( empty($check_customer['password_reset_key']) || ($check_customer['password_reset_key'] != $password_key) || (strtotime($check_customer['password_reset_date'] . ' +1 day') <= time()) ) {
                            $error = true;

                            $messageStack->add_session('password_forgotten', TEXT_NO_RESET_LINK_FOUND);
                        }
                    } else {
                        $error = true;

                        $messageStack->add_session('password_forgotten', TEXT_NO_EMAIL_ADDRESS_FOUND);
                    }
                }
            }

            if ($error == true) {
                tep_redirect(tep_href_link(PASSWORD_FORGOTTEN_URL));
            }

            if (isset($_GET['action']) && ($_GET['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                $password_new = tep_db_prepare_input($_POST['password']);
                $password_confirmation = tep_db_prepare_input($_POST['confirmation']);

                if (strlen($password_new) < ENTRY_PASSWORD_MIN_LENGTH) {
                    $error = true;

                    $messageStack->add('password_reset', ENTRY_PASSWORD_NEW_ERROR);
                } elseif ($password_new != $password_confirmation) {
                    $error = true;

                    $messageStack->add('password_reset', ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING);
                }

                if ($error == false) {
                    tep_db_query("update " . TABLE_CUSTOMERS . " set customers_password = '" . tep_encrypt_password($password_new) . "' where customers_id = '" . (int)$check_customer['customers_id'] . "'");

                    $customers_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                    $customers_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                    tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set customers_ip = '" . $customers_ip . "', customers_user_agent = '" . $customers_user_agent . "', customers_info_date_account_last_modified = now(), password_reset_key = null, password_reset_date = null where customers_info_id = '" . (int)$check_customer['customers_id'] . "'");

                    $messageStack->add_session('login', SUCCESS_PASSWORD_RESET, 'success');

                    tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
                }
            }

            $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(LOGIN_URL, '', 'SSL'));
            $breadcrumb->add(NAVBAR_TITLE_2);
            break;

        case PAYPAL_EXPRESS_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }

            require(DIR_WS_LANGUAGES . $language . '/modules/payment/paypal_express.php');
            require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CREATE_ACCOUNT);

// initialize variables if the customer is not logged in
            if (!tep_session_is_registered('customer_id')) {
                $customer_id = 0;
                $customer_default_address_id = 0;
            }

            require('includes/modules/payment/paypal_express.php');
            $paypal_express = new paypal_express();

            if (!$paypal_express->check() || !$paypal_express->enabled) {
                tep_redirect(tep_href_link(SHOPPING_URL));
            }

            if (!tep_session_is_registered('sendto')) {
                tep_session_register('sendto');
                $sendto = $customer_default_address_id;
            }

            if (!tep_session_is_registered('billto')) {
                tep_session_register('billto');
                $billto = $customer_default_address_id;
            }

// register a random ID in the session to check throughout the checkout procedure
// against alterations in the shopping cart contents
            if (!tep_session_is_registered('cartID')) tep_session_register('cartID');
            $cartID = $cart->cartID;

            switch (empty($_GET['osC_Action']) ? '' : $_GET['osC_Action']) {
                case 'cancel':
                    tep_session_unregister('ppe_token');

                    tep_redirect(tep_href_link(SHOPPING_URL, '', 'SSL'));

                    break;
                case 'callbackSet':
                    if (MODULE_PAYMENT_PAYPAL_EXPRESS_INSTANT_UPDATE == 'True') {
                        $counter = 0;

                        while (true) {
                            if (isset($_POST['L_NUMBER' . $counter])) {
                                $cart->add_cart($_POST['L_NUMBER' . $counter], $_POST['L_QTY' . $counter]);
                            } else {
                                break;
                            }

                            $counter++;
                        }

// exit if there is nothing in the shopping cart
                        if ($cart->count_contents() < 1) {
                            exit;
                        }

                        $sendto = array('firstname' => '',
                            'lastname' => '',
                            'company' => '',
                            'street_address' => '',
                            'suburb' => '',
                            'postcode' => $_POST['SHIPTOZIP'],
                            'city' => $_POST['SHIPTOCITY'],
                            'zone_id' => '',
                            'zone_name' => $_POST['SHIPTOSTATE'],
                            'country_id' => '',
                            'country_name' => $_POST['SHIPTOCOUNTRY'],
                            'country_iso_code_2' => '',
                            'country_iso_code_3' => '',
                            'address_format_id' => '');

                        $country_query = tep_db_query("select * from " . TABLE_COUNTRIES . " where countries_iso_code_2 = '" . tep_db_input($sendto['country_name']) . "' limit 1");
                        if (tep_db_num_rows($country_query)) {
                            $country = tep_db_fetch_array($country_query);

                            $sendto['country_id'] = $country['countries_id'];
                            $sendto['country_name'] = $country['countries_name'];
                            $sendto['country_iso_code_2'] = $country['countries_iso_code_2'];
                            $sendto['country_iso_code_3'] = $country['countries_iso_code_3'];
                            $sendto['address_format_id'] = $country['address_format_id'];
                        }

                        if ($sendto['country_id'] > 0) {
                            $zone_query = tep_db_query("select * from " . TABLE_ZONES . " where zone_country_id = '" . (int)$sendto['country_id'] . "' and (zone_name = '" . tep_db_input($sendto['zone_name']) . "' or zone_code = '" . tep_db_input($sendto['zone_name']) . "') limit 1");
                            if (tep_db_num_rows($zone_query)) {
                                $zone = tep_db_fetch_array($zone_query);

                                $sendto['zone_id'] = $zone['zone_id'];
                                $sendto['zone_name'] = $zone['zone_name'];
                            }
                        }

                        $billto = $sendto;

                        $quotes_array = array();

                        include(DIR_WS_CLASSES . 'order.php');

                        if ($cart->get_content_type() != 'virtual') {
                            $order = new order;

                            $total_weight = $cart->show_weight();
                            $total_count = $cart->count_contents();

// load all enabled shipping modules
                            include(DIR_WS_CLASSES . 'shipping.php');
                            $shipping_modules = new shipping;

                            $free_shipping = false;

                            if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
                                $pass = false;

                                switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
                                    case 'national':
                                        if ($order->delivery['country_id'] == STORE_COUNTRY) {
                                            $pass = true;
                                        }
                                        break;

                                    case 'international':
                                        if ($order->delivery['country_id'] != STORE_COUNTRY) {
                                            $pass = true;
                                        }
                                        break;

                                    case 'both':
                                        $pass = true;
                                        break;
                                }

                                if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
                                    $free_shipping = true;

                                    include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
                                }
                            }

                            if ( (tep_count_shipping_modules() > 0) || ($free_shipping == true) ) {
                                if ($free_shipping == true) {
                                    $quotes_array[] = array('id' => 'free_free',
                                        'name' => FREE_SHIPPING_TITLE,
                                        'label' => FREE_SHIPPING_TITLE,
                                        'cost' => '0',
                                        'tax' => '0');
                                } else {
// get all available shipping quotes
                                    $quotes = $shipping_modules->quote();

                                    foreach ($quotes as $quote) {
                                        if (!isset($quote['error'])) {
                                            foreach ($quote['methods'] as $rate) {
                                                $quotes_array[] = array('id' => $quote['id'] . '_' . $rate['id'],
                                                    'name' => $quote['module'],
                                                    'label' => $rate['title'],
                                                    'cost' => $rate['cost'],
                                                    'tax' => isset($quote['tax']) ? $quote['tax'] : '0');
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $quotes_array[] = array('id' => 'null',
                                'name' => 'No Shipping',
                                'label' => 'No Shipping',
                                'cost' => '0',
                                'tax' => '0');
                        }

                        $params = array('METHOD' => 'CallbackResponse',
                            'OFFERINSURANCEOPTION' => 'false');

                        $counter = 0;
                        $cheapest_rate = null;
                        $cheapest_counter = $counter;

                        foreach ($quotes_array as $quote) {
                            $shipping_rate = $paypal_express->format_raw($quote['cost'] + tep_calculate_tax($quote['cost'], $quote['tax']));

                            $params['L_SHIPPINGOPTIONNAME' . $counter] = $quote['name'] . ' (' . $quote['label'] . ')';
                            $params['L_SHIPINGPOPTIONLABEL' . $counter] = $quote['name'] . ' (' . $quote['label'] . ')';
                            $params['L_SHIPPINGOPTIONAMOUNT' . $counter] = $paypal_express->format_raw($quote['cost']);
                            $params['L_SHIPPINGOPTIONISDEFAULT' . $counter] = 'false';
                            $params['L_TAXAMT' . $counter] = $paypal_express->format_raw($order->info['tax'] + tep_calculate_tax($quote['cost'], $quote['tax']));

                            if (is_null($cheapest_rate) || ($shipping_rate < $cheapest_rate)) {
                                $cheapest_rate = $shipping_rate;
                                $cheapest_counter = $counter;
                            }

                            $counter++;
                        }

                        $params['L_SHIPPINGOPTIONISDEFAULT' . $cheapest_counter] = 'true';

                        $post_string = '';

                        foreach ($params as $key => $value) {
                            $post_string .= $key . '=' . urlencode(utf8_encode(trim($value))) . '&';
                        }

                        $post_string = substr($post_string, 0, -1);

                        echo $post_string;
                    }

                    exit;

                    break;
                case 'retrieve':
// if there is nothing in the customers cart, redirect them to the shopping cart page
                    if ($cart->count_contents() < 1) {
                        tep_redirect(tep_href_link(SHOPPING_URL));
                    }

                    $response_array = $paypal_express->getExpressCheckoutDetails($_GET['token']);

                    if (($response_array['ACK'] == 'Success') || ($response_array['ACK'] == 'SuccessWithWarning')) {
                        $force_login = false;

// check if e-mail address exists in database and login or create customer account
                        if (!tep_session_is_registered('customer_id')) {
                            $force_login = true;

                            $email_address = tep_db_prepare_input($response_array['EMAIL']);

                            $check_query = tep_db_query("select * from " . TABLE_CUSTOMERS . " where customers_email_address = '" . tep_db_input($email_address) . "' limit 1");
                            if (tep_db_num_rows($check_query)) {
                                $check = tep_db_fetch_array($check_query);

                                $customer_id = $check['customers_id'];
                                $customers_firstname = $check['customers_firstname'];
                                $customer_default_address_id = $check['customers_default_address_id'];
                            } else {
                                $customers_firstname = tep_db_prepare_input($response_array['FIRSTNAME']);
                                $customers_lastname = tep_db_prepare_input($response_array['LASTNAME']);

                                $customer_password = tep_create_random_value(max(ENTRY_PASSWORD_MIN_LENGTH, 8));

                                $sql_data_array = array('customers_firstname' => $customers_firstname,
                                    'customers_lastname' => $customers_lastname,
                                    'customers_email_address' => $email_address,
                                    'customers_telephone' => '',
                                    'customers_fax' => '',
                                    'customers_newsletter' => '0',
                                    'customers_password' => tep_encrypt_password($customer_password));

                                if (isset($response_array['PHONENUM']) && tep_not_null($response_array['PHONENUM'])) {
                                    $customers_telephone = tep_db_prepare_input($response_array['PHONENUM']);

                                    $sql_data_array['customers_telephone'] = $customers_telephone;
                                }

                                tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);

                                $customer_id = tep_db_insert_id();

                                tep_db_query("insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id, customers_info_number_of_logons, customers_info_date_account_created) values ('" . (int)$customer_id . "', '0', now())");

// build the message content
                                $name = $customers_firstname . ' ' . $customers_lastname;
                                $email_text = sprintf(EMAIL_GREET_NONE, $customers_firstname) . EMAIL_WELCOME . sprintf(MODULE_PAYMENT_PAYPAL_EXPRESS_EMAIL_PASSWORD, $email_address, $customer_password) . EMAIL_TEXT . EMAIL_CONTACT . EMAIL_WARNING;
                                tep_mail($name, $email_address, EMAIL_SUBJECT, $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
                            }

                            if (SESSION_RECREATE == 'True') {
                                tep_session_recreate();
                            }

                            $customer_first_name = $customers_firstname;

                            tep_session_unregister('partner_id');
                            tep_session_unregister('partner_first_name');
                            tep_session_unregister('partner_key');

                            tep_session_register('customer_id');
                            tep_session_register('customer_first_name');

// reset session token
                            $sessiontoken = md5(tep_rand() . tep_rand() . tep_rand() . tep_rand());
                        }

// check if paypal shipping address exists in the address book
                        $ship_firstname = tep_db_prepare_input(substr($response_array['SHIPTONAME'], 0, strpos($response_array['SHIPTONAME'], ' ')));
                        $ship_lastname = tep_db_prepare_input(substr($response_array['SHIPTONAME'], strpos($response_array['SHIPTONAME'], ' ')+1));
                        $ship_address = tep_db_prepare_input($response_array['SHIPTOSTREET']);
                        $ship_city = tep_db_prepare_input($response_array['SHIPTOCITY']);
                        $ship_zone = tep_db_prepare_input($response_array['SHIPTOSTATE']);
                        $ship_zone_id = 0;
                        $ship_postcode = tep_db_prepare_input($response_array['SHIPTOZIP']);
                        $ship_country = tep_db_prepare_input($response_array['SHIPTOCOUNTRYCODE']);
                        $ship_country_id = 0;
                        $ship_address_format_id = 1;

                        $country_query = tep_db_query("select countries_id, address_format_id from " . TABLE_COUNTRIES . " where countries_iso_code_2 = '" . tep_db_input($ship_country) . "' limit 1");
                        if (tep_db_num_rows($country_query)) {
                            $country = tep_db_fetch_array($country_query);

                            $ship_country_id = $country['countries_id'];
                            $ship_address_format_id = $country['address_format_id'];
                        }

                        if ($ship_country_id > 0) {
                            $zone_query = tep_db_query("select zone_id from " . TABLE_ZONES . " where zone_country_id = '" . (int)$ship_country_id . "' and (zone_name = '" . tep_db_input($ship_zone) . "' or zone_code = '" . tep_db_input($ship_zone) . "') limit 1");
                            if (tep_db_num_rows($zone_query)) {
                                $zone = tep_db_fetch_array($zone_query);

                                $ship_zone_id = $zone['zone_id'];
                            }
                        }

                        $check_query = tep_db_query("select address_book_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and entry_firstname = '" . tep_db_input($ship_firstname) . "' and entry_lastname = '" . tep_db_input($ship_lastname) . "' and entry_street_address = '" . tep_db_input($ship_address) . "' and entry_postcode = '" . tep_db_input($ship_postcode) . "' and entry_city = '" . tep_db_input($ship_city) . "' and (entry_state = '" . tep_db_input($ship_zone) . "' or entry_zone_id = '" . (int)$ship_zone_id . "') and entry_country_id = '" . (int)$ship_country_id . "' limit 1");
                        if (tep_db_num_rows($check_query)) {
                            $check = tep_db_fetch_array($check_query);

                            $sendto = $check['address_book_id'];
                        } else {
                            $sql_data_array = array('customers_id' => $customer_id,
                                'entry_firstname' => $ship_firstname,
                                'entry_lastname' => $ship_lastname,
                                'entry_street_address' => $ship_address,
                                'entry_postcode' => $ship_postcode,
                                'entry_city' => $ship_city,
                                'entry_country_id' => $ship_country_id);

                            if (ACCOUNT_STATE == 'true') {
                                if ($ship_zone_id > 0) {
                                    $sql_data_array['entry_zone_id'] = $ship_zone_id;
                                    $sql_data_array['entry_state'] = '';
                                } else {
                                    $sql_data_array['entry_zone_id'] = '0';
                                    $sql_data_array['entry_state'] = $ship_zone;
                                }
                            }

                            tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);

                            $address_id = tep_db_insert_id();

                            $sendto = $address_id;

                            if ($customer_default_address_id < 1) {
                                tep_db_query("update " . TABLE_CUSTOMERS . " set customers_default_address_id = '" . (int)$address_id . "' where customers_id = '" . (int)$customer_id . "'");
                                $customer_default_address_id = $address_id;
                            }
                        }

                        if ($force_login == true) {
                            $customer_country_id = $ship_country_id;
                            $customer_zone_id = $ship_zone_id;
                            tep_session_register('customer_default_address_id');
                            tep_session_register('customer_country_id');
                            tep_session_register('customer_zone_id');

                            $billto = $sendto;
                        }

                        include(DIR_WS_CLASSES . 'order.php');

                        if ($cart->get_content_type() != 'virtual') {
                            $order = new order;

                            $total_weight = $cart->show_weight();
                            $total_count = $cart->count_contents();

// load all enabled shipping modules
                            include(DIR_WS_CLASSES . 'shipping.php');
                            $shipping_modules = new shipping;

                            $free_shipping = false;

                            if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
                                $pass = false;

                                switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
                                    case 'national':
                                        if ($order->delivery['country_id'] == STORE_COUNTRY) {
                                            $pass = true;
                                        }
                                        break;

                                    case 'international':
                                        if ($order->delivery['country_id'] != STORE_COUNTRY) {
                                            $pass = true;
                                        }
                                        break;

                                    case 'both':
                                        $pass = true;
                                        break;
                                }

                                if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
                                    $free_shipping = true;

                                    include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
                                }
                            }

                            if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
                            $shipping = false;

                            if ( (tep_count_shipping_modules() > 0) || ($free_shipping == true) ) {
                                if ($free_shipping == true) {
                                    $shipping = 'free_free';
                                } else {
// get all available shipping quotes
                                    $quotes = $shipping_modules->quote();

                                    $shipping_set = false;

// if available, set the selected shipping rate from PayPals order review page
                                    if (isset($response_array['SHIPPINGOPTIONNAME']) && isset($response_array['SHIPPINGOPTIONAMOUNT'])) {
                                        foreach ($quotes as $quote) {
                                            if (!isset($quote['error'])) {
                                                foreach ($quote['methods'] as $rate) {
                                                    if ($response_array['SHIPPINGOPTIONNAME'] == $quote['module'] . ' (' . $rate['title'] . ')') {
                                                        if ($response_array['SHIPPINGOPTIONAMOUNT'] == $paypal_express->format_raw($rate['cost'] + tep_calculate_tax($rate['cost'], $quote['tax']))) {
                                                            $shipping = $quote['id'] . '_' . $rate['id'];
                                                            $shipping_set = true;
                                                            break 2;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ($shipping_set == false) {
// select cheapest shipping method
                                        $shipping = $shipping_modules->cheapest();
                                        $shipping = $shipping['id'];
                                    }
                                }
                            }

                            if (strpos($shipping, '_')) {
                                list($module, $method) = explode('_', $shipping);

                                if ( is_object($$module) || ($shipping == 'free_free') ) {
                                    if ($shipping == 'free_free') {
                                        $quote[0]['methods'][0]['title'] = FREE_SHIPPING_TITLE;
                                        $quote[0]['methods'][0]['cost'] = '0';
                                    } else {
                                        $quote = $shipping_modules->quote($method, $module);
                                    }

                                    if (isset($quote['error'])) {
                                        tep_session_unregister('shipping');

                                        tep_redirect(tep_href_link(CHECKOUT_SHIPPING_URL, '', 'SSL'));
                                    } else {
                                        if ( (isset($quote[0]['methods'][0]['title'])) && (isset($quote[0]['methods'][0]['cost'])) ) {
                                            $shipping = array('id' => $shipping,
                                                'title' => (($free_shipping == true) ?  $quote[0]['methods'][0]['title'] : $quote[0]['module'] . ' (' . $quote[0]['methods'][0]['title'] . ')'),
                                                'cost' => $quote[0]['methods'][0]['cost']);
                                        }
                                    }
                                }
                            }
                        } else {
                            if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
                            $shipping = false;

                            $sendto = false;
                        }

                        if (!tep_session_is_registered('payment')) tep_session_register('payment');
                        $payment = $paypal_express->code;

                        if (!tep_session_is_registered('ppe_token')) tep_session_register('ppe_token');
                        $ppe_token = $response_array['TOKEN'];

                        if (!tep_session_is_registered('ppe_payerid')) tep_session_register('ppe_payerid');
                        $ppe_payerid = $response_array['PAYERID'];

                        if (!tep_session_is_registered('ppe_payerstatus')) tep_session_register('ppe_payerstatus');
                        $ppe_payerstatus = $response_array['PAYERSTATUS'];

                        if (!tep_session_is_registered('ppe_addressstatus')) tep_session_register('ppe_addressstatus');
                        $ppe_addressstatus = $response_array['ADDRESSSTATUS'];

                        tep_redirect(tep_href_link(CHECKOUT_CONFIRMATION_URL, '', 'SSL'));
                    } else {
                        tep_redirect(tep_href_link(SHOPPING_URL, 'error_message=' . stripslashes($response_array['L_LONGMESSAGE0']), 'SSL'));
                    }

                    break;

                default:
// if there is nothing in the customers cart, redirect them to the shopping cart page
                    if ($cart->count_contents() < 1) {
                        tep_redirect(tep_href_link(SHOPPING_URL));
                    }

                    if (MODULE_PAYMENT_PAYPAL_EXPRESS_TRANSACTION_SERVER == 'Live') {
                        $paypal_url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
                    } else {
                        $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout';
                    }

                    include(DIR_WS_CLASSES . 'order.php');
                    $order = new order;

                    $params = array('CURRENCYCODE' => $order->info['currency']);

// A billing address is required for digital orders so we use the shipping address PayPal provides
//      if ($order->content_type == 'virtual') {
//        $params['NOSHIPPING'] = '1';
//      }

                    $line_item_no = 0;
                    $items_total = 0;
                    $tax_total = 0;

                    foreach ($order->products as $product) {
                        $params['L_NAME' . $line_item_no] = $product['name'];
                        $params['L_AMT' . $line_item_no] = $paypal_express->format_raw($product['final_price']);
                        $params['L_NUMBER' . $line_item_no] = $product['id'];
                        $params['L_QTY' . $line_item_no] = $product['qty'];

                        $product_tax = tep_calculate_tax($product['final_price'], $product['tax']);

                        $params['L_TAXAMT' . $line_item_no] = $paypal_express->format_raw($product_tax);
                        $tax_total += $paypal_express->format_raw($product_tax) * $product['qty'];

                        $items_total += $paypal_express->format_raw($product['final_price']) * $product['qty'];

                        $line_item_no++;
                    }

                    $params['ITEMAMT'] = $items_total;
                    $params['TAXAMT'] = $tax_total;

                    if (tep_not_null($order->delivery['firstname'])) {
                        $params['ADDROVERRIDE'] = '1';
                        $params['SHIPTONAME'] = $order->delivery['firstname'] . ' ' . $order->delivery['lastname'];
                        $params['SHIPTOSTREET'] = $order->delivery['street_address'];
                        $params['SHIPTOCITY'] = $order->delivery['city'];
                        $params['SHIPTOSTATE'] = tep_get_zone_code($order->delivery['country']['id'], $order->delivery['zone_id'], $order->delivery['state']);
                        $params['SHIPTOCOUNTRYCODE'] = $order->delivery['country']['iso_code_2'];
                        $params['SHIPTOZIP'] = $order->delivery['postcode'];
                    }

                    $quotes_array = array();

                    if ($cart->get_content_type() != 'virtual') {
                        $total_weight = $cart->show_weight();
                        $total_count = $cart->count_contents();

// load all enabled shipping modules
                        include(DIR_WS_CLASSES . 'shipping.php');
                        $shipping_modules = new shipping;

                        $free_shipping = false;

                        if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
                            $pass = false;

                            switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
                                case 'national':
                                    if ($order->delivery['country_id'] == STORE_COUNTRY) {
                                        $pass = true;
                                    }
                                    break;

                                case 'international':
                                    if ($order->delivery['country_id'] != STORE_COUNTRY) {
                                        $pass = true;
                                    }
                                    break;

                                case 'both':
                                    $pass = true;
                                    break;
                            }

                            if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
                                $free_shipping = true;

                                include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
                            }
                        }

                        if ( (tep_count_shipping_modules() > 0) || ($free_shipping == true) ) {
                            if ($free_shipping == true) {
                                $quotes_array[] = array('id' => 'free_free',
                                    'name' => FREE_SHIPPING_TITLE,
                                    'label' => FREE_SHIPPING_TITLE,
                                    'cost' => '0.00',
                                    'tax' => '0');
                            } else {
// get all available shipping quotes
                                $quotes = $shipping_modules->quote();

                                foreach ($quotes as $quote) {
                                    if (!isset($quote['error'])) {
                                        foreach ($quote['methods'] as $rate) {
                                            $quotes_array[] = array('id' => $quote['id'] . '_' . $rate['id'],
                                                'name' => $quote['module'],
                                                'label' => $rate['title'],
                                                'cost' => $rate['cost'],
                                                'tax' => isset($quote['tax']) ? $quote['tax'] : '0');
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $counter = 0;
                    $cheapest_rate = null;
                    $expensive_rate = 0;
                    $cheapest_counter = $counter;
                    $default_shipping = null;

                    foreach ($quotes_array as $quote) {
                        $shipping_rate = $paypal_express->format_raw($quote['cost'] + tep_calculate_tax($quote['cost'], $quote['tax']));

                        $params['L_SHIPPINGOPTIONNAME' . $counter] = $quote['name'] . ' (' . $quote['label'] . ')';
                        $params['L_SHIPINGPOPTIONLABEL' . $counter] = $quote['name'] . ' (' . $quote['label'] . ')';
                        $params['L_SHIPPINGOPTIONAMOUNT' . $counter] = $shipping_rate;
                        $params['L_SHIPPINGOPTIONISDEFAULT' . $counter] = 'false';

                        if (is_null($cheapest_rate) || ($shipping_rate < $cheapest_rate)) {
                            $cheapest_rate = $shipping_rate;
                            $cheapest_counter = $counter;
                        }

                        if ($shipping_rate > $expensive_rate) {
                            $expensive_rate = $shipping_rate;
                        }

                        if (tep_not_null($shipping) && ($shipping['id'] == $quote['id'])) {
                            $default_shipping = $counter;
                        }

                        $counter++;
                    }

                    if (!is_null($default_shipping)) {
                        $cheapest_rate = $params['L_SHIPPINGOPTIONAMOUNT' . $default_shipping];
                        $cheapest_counter = $default_shipping;
                    }

                    if (!is_null($cheapest_rate)) {
                        if ( (MODULE_PAYMENT_PAYPAL_EXPRESS_INSTANT_UPDATE == 'True') && ((MODULE_PAYMENT_PAYPAL_EXPRESS_TRANSACTION_SERVER != 'Live') || ((MODULE_PAYMENT_PAYPAL_EXPRESS_TRANSACTION_SERVER == 'Live') && (ENABLE_SSL == true))) ) { // Live server requires SSL to be enabled
                            $params['CALLBACK'] = tep_href_link(PAYPAL_EXPRESS_URL, 'osC_Action=callbackSet', 'SSL', false, false);
                            $params['CALLBACKTIMEOUT'] = '5';
                        }

                        $params['INSURANCEOPTIONSOFFERED'] = 'false';
                        $params['L_SHIPPINGOPTIONISDEFAULT' . $cheapest_counter] = 'true';
                    }

// don't recalculate currency values as they have already been calculated
                    $params['SHIPPINGAMT'] = $paypal_express->format_raw($cheapest_rate, '', 1);
                    $params['AMT'] = $paypal_express->format_raw($params['ITEMAMT'] + $params['TAXAMT'] + $params['SHIPPINGAMT'], '', 1);
                    $params['MAXAMT'] = $paypal_express->format_raw($params['AMT'] + $expensive_rate + 100, '', 1); // safely pad higher for dynamic shipping rates (eg, USPS express)

                    $response_array = $paypal_express->setExpressCheckout($params);

                    if (($response_array['ACK'] == 'Success') || ($response_array['ACK'] == 'SuccessWithWarning')) {
                        tep_redirect($paypal_url . '&token=' . $response_array['TOKEN'] . '&useraction=commit');
                    } else {
                        tep_redirect(tep_href_link(SHOPPING_URL, 'error_message=' . stripslashes($response_array['L_LONGMESSAGE0']), 'SSL'));
                    }

                    break;
            }

            tep_redirect(tep_href_link(SHOPPING_URL, '', 'SSL'));
            break;

        case PRODUCT_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            $main = '';
            if(tep_not_null($item_get)) {
                $all_prods_query = tep_db_query('select p.products_id, pd.products_name from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd on(pd.products_id = p.products_id and pd.language_id = ' . (int)$languages_id . ')');
                $products_id = false;
                while($cur_ans_prod = tep_db_fetch_array($all_prods_query)) {
                    if (tep_clean_text_int($cur_ans_prod['products_name']) == $item_get) {
                        $products_id = (int)$cur_ans_prod['products_id'];
                        break;
                    }
                }
                if ($products_id) {
                    $product_info_query = tep_db_query("select p.products_id, p.shopify_id, pd.products_name, pd.meta_keyword, pd.meta_title, pd.meta_description, pd.products_short_desc, pd.products_description, p.products_model, p.products_quantity, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available from " . TABLE_PRODUCTS . " p join " . TABLE_PRODUCTS_DESCRIPTION . " pd on(p.products_id = pd.products_id and p.products_id = '" . $products_id . "' and pd.language_id = '" . (int)$languages_id . "') where p.products_status = '1'");
                    $categories_query = tep_db_query('select categories_id from ' . TABLE_PRODUCTS_TO_CATEGORIES . ' where products_id = ' . (int)$products_id);
                    $product_info = tep_db_fetch_array($product_info_query);
                    while ($curr_cat = tep_db_fetch_array($categories_query)) {
                        $cPath_array[] = $curr_cat['categories_id'];
                    }
                    $main = $main_get;
                    $item = $item_get;
                    $cPath = '';
                    require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_PRODUCT_INFO);
                    require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_REVIEWS);
                    $og_pic = DIR_WS_IMAGES .'products/'. $product_info['products_image'] . '-1' . IMAGE_EXTENSION;
                    $og_title = $product_info['meta_title'];
                    //$og_desc = $product_info['products_description'];
                    $og_desc =  $product_info['meta_description'];
                    $og_text =  $product_info['meta_keyword'];
                    $og_url = tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product_info['products_name']));

                    if (!empty($_GET['mtd']) && !empty($_GET['mtr']) && is_numeric($_GET['mtd']) && is_numeric($_GET['mtr'])) {
                        switch ($_GET['mtd']) {
                            case 2: //banner
                            case 6: //share
                                if(!tep_db_num_rows($material_query = tep_db_query('select * from ' . TABLE_MARKETING_SHARES . ' where material_id = ' . (int)$_GET['mtr'] . ' and material_status = 1'))) break;
                                $material_info = tep_db_fetch_array($material_query);
                                if ($material_info['products_id'] != $products_id) break;
                                $og_pic = HTTP_SERVER . '/' . DIR_WS_MARKETING_SHARES . $material_info['material_pic'];
                                $og_title = $material_info['material_title'];
                                $og_desc = $material_info['material_desc'];
                                $og_url = tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product_info['products_name']), '');
                                if (!$IS_BOT && !$visit_id) {
                                    $visit_info['method_id'] = (int)$_GET['mtd'];
                                    $visit_info['material_id'] = (int)$material_info['material_id'];
                                    $visit_info['products_id'] = $material_info['products_id'] ? (int)$material_info['products_id'] : 'null';
                                }
                                $js_redirect = $og_url;
                                break;
                        }
                    }

                    $og_type = 'article';
                    $do_breadcrumb = '$breadcrumb->add($product_info["products_name"], tep_href_link(PRODUCT_URL . "/" . tep_clean_text_int($product_info["products_name"])));';

                    $products_name = tep_db_input($product_info['products_name']);

                    if (tep_session_is_registered('customer_id')) {
                        $customer_query = tep_db_query("select customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
                        $customer = tep_db_fetch_array($customer_query);
                        $customers_name = tep_db_input($customer['customers_firstname']) . ' ' . tep_db_input($customer['customers_lastname']);
                        $customers_email = '';
                        $customers_id = (int)$customer_id;
                    }
                    require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_PRODUCT_REVIEWS_WRITE);

                    if (isset($_GET['action']) && ($_GET['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                        $error = false;
                        if (empty($_POST['rating']) || (($rating = (int)$_POST['rating']) < 1) || ($rating > 5)) {
                            $error = true;

                            $messageStack->add('review', JS_REVIEW_RATING);
                        }
                        if (empty($_POST['review']) || (mb_strlen($review = tep_db_prepare_input($_POST['review'])) < REVIEW_TEXT_MIN_LENGTH)) {
                            $error = true;

                            $messageStack->add('review', JS_REVIEW_TEXT);
                        }
                        if (!tep_session_is_registered('customer_id')) {
                            if (empty($_POST['customers_name']) || !tep_not_null($customers_name = tep_db_prepare_input($_POST['customers_name']))) {
                                $error = true;

                                $messageStack->add('review', JS_REVIEW_CUSTOMERS_NAME);
                            }
                            if (empty($_POST['customers_email']) || !tep_validate_email($customers_email = tep_db_prepare_input($_POST['customers_email']))) {
                                $error = true;

                                $messageStack->add('review', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
                            }
                            $customers_id = 0;
                        }

                        if ($error == false) {

                            $customers_ip = tep_db_input(substr(tep_get_ip_address(), 0, 45));
                            $customers_user_agent = empty($_SERVER['HTTP_USER_AGENT']) ? '' : tep_db_input(substr($_SERVER['HTTP_USER_AGENT'], 0 , 255));

                            tep_db_query("insert into " . TABLE_REVIEWS . " (products_id, customers_id, customers_name, customers_email, customers_ip, reviews_text, reviews_rating, date_added) values (" . (int)$products_id . ", " . (int)$customers_id . ", '" . $customers_name . "', '" . $customers_email . "', '" . $customers_ip . "', '" . tep_db_input($review) . "', " . $rating . ", now())");
                            $insert_id = tep_db_insert_id();

                            $messageStack->add('review', TEXT_REVIEW_RECEIVED, 'success');
                            die(json_encode(array('success' => $messageStack->output('review'))));
                        } else {
                            die(json_encode(array('error' => $messageStack->output('review'))));
                        }
                    }
                } else {
                    $not_found = true;
                }
            } else {
                $not_found = true;
            }
            break;

	    case SHOPPING_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }

// if there is nothing in the customers cart, redirect them to the shopping cart page

            $oscTemplate->addBlock('<script type="text/javascript" src="' . HTTP_SERVER . '/ext/jg-counter.js"></script>', 'header_tags');
            $oscTemplate->addBlock('<script type="text/javascript" src="' . HTTP_SERVER . '/ext/jg-basket.js"></script>', 'header_tags');
            $oscTemplate->addBlock('
                <script>
                    var JgBasketSettings = {
                        addURL : "ajax/cart?act=add",
                        updateURL : "ajax/cart?act=update",
                        removeURL : "ajax/cart?act=remove",
                        loadURL : "ajax/cart?act=load",
                        numberFormat : {
                            decimals : 0,
                            decPoint : ".",
                            thousSep : " "
                        },
                        jgCounter : {
                            min : 1,
                            max: 99
                        },
                        events : {
                            beforeProductAdding : function(infoObj) {
                                infoObj.$adder && infoObj.$adder.addClass("loading");
                            },
                            afterProductAdding : function(infoObj, success) {
                                infoObj.$adder && infoObj.$adder.removeClass("loading");
                                //$("#flyiteminfo").nanoScroller();
                            },
                            beforeProductUpdating : function(infoObj) {
                                infoObj.$updater.parent().addClass("loading");
                            },
                            afterProductUpdating : function(infoObj, success, resp) {
                                infoObj.$updater.parent().removeClass("loading");
                                if  (success) {
                                    for (var prop in resp) {
                                        if (resp.hasOwnProperty(prop)) {
                                            $(prop).html(resp[prop]);
                                        }
                                    }
                                }
                            },
                            beforeProductLoading : function() {
                                $(".jg-basket-products").addClass("loading");
                            },
                            afterProductLoading : function(success) {
                                $(".jg-basket-products").removeClass("loading");
                                //$("#flyiteminfo").nanoScroller();
                            },
                            afterProductRemoving : function(totalQty) {
                                totalQty == 0 && $("#full-cart").fadeOut("slow", function() {$("#full-cart").remove();$("#empty-cart").fadeIn("slow");});
                                // $("#flyiteminfo").nanoScroller();
                            },
                            beforeProductRemoving : function(infoObj) {
                                if (infoObj.id) {
                                    var $prod = $(".jg-basket-prod-row[data-jg-basket-prod-id=" + infoObj.id + "]");
                                    if ($prod.length) {
                                        var cont = true;
                                        infoObj.$remover.addClass("loading");
                                        $prod.fadeOut("slow", function() {cont = false;});
                                    }
                                }
                            }
                        }
                    };
                </script>
            ', 'header_tags');

            require(DIR_WS_CLASSES . 'order.php');
            $order = new order;

            //-- shipping ----
            if (!tep_session_is_registered('cartID')) {
                tep_session_register('cartID');
            } elseif (($cartID != $cart->cartID) && tep_session_is_registered('shipping')) {
                tep_session_unregister('shipping');
            }

            $cartID = $cart->cartID = $cart->generate_cart_id();

// if the order contains only virtual products, forward the customer to the billing page as
// a shipping address is not needed
            if ($order->content_type == 'virtual') {
                if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
                $shipping = false;
                $sendto = false;
                tep_redirect(tep_href_link(SHOPPING_URL, '', 'SSL'));
            }

            $total_weight = $cart->show_weight();
            $total_count = $cart->count_contents();

// load all enabled shipping modules
            require(DIR_WS_CLASSES . 'shipping.php');
            $shipping_modules = new shipping;

            if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
                $pass = false;

                switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
                    case 'national':
                        if ($order->delivery['country_id'] == STORE_COUNTRY) {
                            $pass = true;
                        }
                        break;
                    case 'international':
                        if ($order->delivery['country_id'] != STORE_COUNTRY) {
                            $pass = true;
                        }
                        break;
                    case 'both':
                        $pass = true;
                        break;
                }

                $free_shipping = false;
                if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
                    $free_shipping = true;

                    include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
                }
            } else {
                $free_shipping = false;
            }


            // process
            if (isset($_POST['action']) && ($_POST['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                if ($cart->count_contents() < 1) {
                    tep_redirect(tep_href_link(SHOPPING_URL));
                }

                // Stock Check
                if ( (STOCK_CHECK == 'true') && (STOCK_ALLOW_CHECKOUT != 'true') ) {
                    $products = $cart->get_products();
                    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
                        if (tep_check_stock($products[$i]['id'], $products[$i]['quantity'])) {
                            tep_redirect(tep_href_link(SHOPPING_URL));
                            break;
                        }
                    }
                }

                if (tep_session_is_registered('customer_id') || tep_create_account($_POST, false, false, false)) {
                    if ($customer_id) {
                        $tmp_customer_id = $customer_id;
                        $tmp_customer_default_address_id = $customer_default_address_id;
                    }

                    // :|  $sendto = $billto = $tmp_customer_default_address_id;
                    $sendto = $billto = $_POST['address_id'];

                    tep_session_register('sendto');
                    tep_session_register('billto');

                    $order = new order;

                    if (!tep_session_is_registered('shipping')) tep_session_register('shipping');

                    $error = false;

                    if ( (tep_count_shipping_modules() > 0) || ($free_shipping == true) ) {
                        if ( (isset($_POST['shipping'])) && (strpos($_POST['shipping'], '_')) ) {
                            $shipping = $_POST['shipping'];

                            list($module, $method) = explode('_', $shipping);
                            if ( is_object($$module) || ($shipping == 'free_free') ) {
                                if ($shipping == 'free_free') {
                                    $quote[0]['methods'][0]['title'] = FREE_SHIPPING_TITLE;
                                    $quote[0]['methods'][0]['cost'] = '0';
                                } else {
                                    $quote = $shipping_modules->quote($method, $module);
                                }
                                if (isset($quote['error'])) {
                                    $error = true;
                                    tep_session_unregister('shipping');
                                    $messageStack->add('shipping', TEXT_CHOOSE_SHIPPING_METHOD);
                                } else {
                                    if ( (isset($quote[0]['methods'][0]['title'])) && (isset($quote[0]['methods'][0]['cost'])) ) {
                                        $shipping = array('id' => $shipping,
                                            'title' => (($free_shipping == true) ?  $quote[0]['methods'][0]['title'] : $quote[0]['module'] . ' (' . $quote[0]['methods'][0]['title'] . ')'),
                                            'cost' => $quote[0]['methods'][0]['cost']);

                                        //ok
                                    }
                                }
                            } else {
                                $error = true;
                                tep_session_unregister('shipping');
                                $messageStack->add('shipping', TEXT_CHOOSE_SHIPPING_METHOD);
                            }
                        }
                    } else {
                        $shipping = false;
                        //ok
                    }

                    if (!$error) {
                        if (!tep_session_is_registered('payment')) tep_session_register('payment');
                        if (isset($_POST['payment'])) $payment = $_POST['payment'];

                        require(DIR_WS_CLASSES . 'payment.php');
                        $payment_modules = new payment($payment);

                        $payment_modules->update_status();

                        if ( ($payment_modules->selected_module != $payment) || ( is_array($payment_modules->modules) && (sizeof($payment_modules->modules) > 1) && !is_object($$payment) ) || (is_object($$payment) && ($$payment->enabled == false)) ) {
                            $messageStack->add('payment', ERROR_NO_PAYMENT_MODULE_SELECTED);
                        } else {
                            tep_redirect(tep_href_link(CHECKOUT_CONFIRMATION_URL));
                        }
                    }
                }
            }

            $quotes = $shipping_modules->quote();


// load all enabled payment modules
            require_once(DIR_WS_CLASSES . 'payment.php');
            $payment_modules = new payment;


		    require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_SHOPPING);

		    $breadcrumb->add(NAVBAR_TITLE, tep_href_link(SHOPPING_URL));
		    break;

        case SHOPPING_CART_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            if ($cart->count_contents() > 0) {
                include(DIR_WS_CLASSES . 'payment.php');
                $payment_modules = new payment;
            }
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_SHOPPING_CART);

            $breadcrumb->add(NAVBAR_TITLE, tep_href_link(SHOPPING_CART_URL));
            break;

        case SPECIALS_URL:
            if ($PARTNER) {
                $not_found = true;
                break;
            }
            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_SPECIALS);

            $breadcrumb->add(NAVBAR_TITLE, tep_href_link(SPECIALS_URL));
            break;

        case SSL_CHECK_URL:
            require(DIR_WS_LANGUAGES . $language . '/' . $payh = FILENAME_SSL_CHECK);
            $breadcrumb->add(NAVBAR_TITLE, tep_href_link(SSL_CHECK_URL));
            break;

        case TELL_A_FRIEND_URL:
            if (!tep_session_is_registered('customer_id') && (ALLOW_GUEST_TO_TELL_A_FRIEND == 'false')) {
                $navigation->set_snapshot();
                tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
            }

            $valid_product = false;
            if (isset($_GET['products_id'])) {
                $product_info_query = tep_db_query("select pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd using(products_id) where p.products_status = '1' and p.products_id = '" . (int)$_GET['products_id'] . "' and pd.language_id = '" . (int)$languages_id . "'");
                if (tep_db_num_rows($product_info_query)) {
                    $valid_product = true;

                    $product_info = tep_db_fetch_array($product_info_query);
                }
            }

            if ($valid_product == false) {
                tep_redirect(tep_href_link(PRODUCT_INFO_URL, 'products_id=' . (int)$_GET['products_id']));
            }

            require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_TELL_A_FRIEND);

            if (isset($_GET['action']) && ($_GET['action'] == 'process') && isset($_POST['formid']) && ($_POST['formid'] == $sessiontoken)) {
                $error = false;

                $to_email_address = tep_db_prepare_input($_POST['to_email_address']);
                $to_name = tep_db_prepare_input($_POST['to_name']);
                $from_email_address = tep_db_prepare_input($_POST['from_email_address']);
                $from_name = tep_db_prepare_input($_POST['from_name']);
                $message = tep_db_prepare_input($_POST['message']);

                if (empty($from_name)) {
                    $error = true;

                    $messageStack->add('friend', ERROR_FROM_NAME);
                }

                if (!tep_validate_email($from_email_address)) {
                    $error = true;

                    $messageStack->add('friend', ERROR_FROM_ADDRESS);
                }

                if (empty($to_name)) {
                    $error = true;

                    $messageStack->add('friend', ERROR_TO_NAME);
                }

                if (!tep_validate_email($to_email_address)) {
                    $error = true;

                    $messageStack->add('friend', ERROR_TO_ADDRESS);
                }

                $actionRecorder = new actionRecorder('ar_tell_a_friend', (tep_session_is_registered('customer_id') ? $customer_id : null), $from_name);
                if (!$actionRecorder->canPerform()) {
                    $error = true;

                    $actionRecorder->record(false);

                    $messageStack->add('friend', sprintf(ERROR_ACTION_RECORDER, (defined('MODULE_ACTION_RECORDER_TELL_A_FRIEND_EMAIL_MINUTES') ? (int)MODULE_ACTION_RECORDER_TELL_A_FRIEND_EMAIL_MINUTES : 15)));
                }

                if ($error == false) {
                    $email_subject = sprintf(TEXT_EMAIL_SUBJECT, $from_name, STORE_NAME);
                    $email_body = sprintf(TEXT_EMAIL_INTRO, $to_name, $from_name, $product_info['products_name'], STORE_NAME) . "\n\n";

                    if (tep_not_null($message)) {
                        $email_body .= $message . "\n\n";
                    }

                    $email_body .= sprintf(TEXT_EMAIL_LINK, tep_href_link(PRODUCT_INFO_URL, 'products_id=' . (int)$_GET['products_id'], 'NONSSL', false)) . "\n\n" .
                        sprintf(TEXT_EMAIL_SIGNATURE, STORE_NAME . "\n" . HTTP_SERVER . DIR_WS_CATALOG . "\n");

                    tep_mail($to_name, $to_email_address, $email_subject, $email_body, $from_name, $from_email_address);

                    $actionRecorder->record();

                    $messageStack->add_session('header', sprintf(TEXT_EMAIL_SUCCESSFUL_SENT, $product_info['products_name'], tep_output_string_protected($to_name)), 'success');

                    tep_redirect(tep_href_link(PRODUCT_INFO_URL, 'products_id=' . (int)$_GET['products_id']));
                }
            } elseif (tep_session_is_registered('customer_id')) {
                $account_query = tep_db_query("select customers_firstname, customers_lastname, customers_email_address from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
                $account = tep_db_fetch_array($account_query);

                $from_name = $account['customers_firstname'] . ' ' . $account['customers_lastname'];
                $from_email_address = $account['customers_email_address'];
            }

            $breadcrumb->add(NAVBAR_TITLE, tep_href_link(TELL_A_FRIEND_URL, 'products_id=' . (int)$_GET['products_id']));
            break;
        case CART_URL:

            $path="cart.php";

            break;
	    case DASHBOARD_URL:

		    $path="dashboard.php";

		    break;
        /*case TWITTER_LOGIN_URL:
            require(DIR_WS_CLASSES . 'TwitterAuth.php');
            $connection = new TwitterOAuth('7M7bVxE1UUdGhDd8ih1jjw', 'Z1NAHea6SQDEJclEJ3JrhdWZrr82J0gcKpuI4UiP4Gw', 'http://oscommerce.com/en/twitter-login?from=twitter');
            if (!empty($_GET['from']) && $_GET['from'] === 'twitter') {
                $connection->access_token();
            }
            $connection->request_token();
            break;*/
        default:
            $main = '';
            if (!$PARTNER) {
                $categories_tree = new bm_tree();

                $all_categories_query = tep_db_query('select c.categories_id, c.parent_id, cd.categories_name from ' . TABLE_CATEGORIES . ' as c join ' . TABLE_CATEGORIES_DESCRIPTION . ' as cd using(categories_id) where cd.language_id = "' . (int)$languages_id . '" order by parent_id, sort_order, categories_name');

                while ($current_category = tep_db_fetch_array($all_categories_query)) {
                    $new_category = new bm_category(
                        $current_category['categories_id'],
                        $current_category['categories_name']
                    );
                    $categories_tree->add($new_category, $current_category['parent_id']);
                }

                if ($current_category_obj = $categories_tree->get_category_by_name($main_get)) {
                    $cPath = $current_category_obj->id;
                    $main = $main_get;
                } elseif (tep_not_null($main_get)) {
	                $main_page = false;
	                $item_page = false;
	                $pages_query = tep_db_query("select p.pages_id, pd.pages_title, pd.meta_keyword, pd.meta_description, p.parents_id, p.pages_status, p.pages_sort_order, (pd.pages_description is not null and pd.pages_description <> '') as pages_has_content from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) where p.pages_status = 1 and pd.language_id = '" . (int)$languages_id . "' order by p.pages_sort_order");
					$page_index = -1;
	                while($page = tep_db_fetch_array($pages_query)) {
						if ($page['parents_id'] == 0) {
							++$page_index;
						}
		                if ($page['parents_id'] == 0 && tep_clean_text_int($page['pages_title']) === $main_get) {
			                $main_page = $page;
							$main_page['index'] = $page_index;

                            $og_desc = $page['meta_description'];
                            $og_text = $page['meta_keyword'];
                            $og_title = $page['pages_title'];
			                if ($item_get) {
				                if ($item_page) {
					                if ($item_page['parents_id'] == $main_page['pages_id']) {
						                $main = $main_get;
						                $item = $item_get;
					                } else {
						                $main_page = $item_page = null;
					                }
					                break;
				                }
			                } else {
				                if ($page['pages_has_content']) {
					                $main = $main_get;
				                }
				                break;
			                }
		                }
		                if ($item_get && $page['parents_id'] != 0 && tep_clean_text_int($page['pages_title']) === $item_get) {
			                $item_page = $page;
			                if ($main_page) {
				                if ($item_page['parents_id'] == $main_page['pages_id']) {
					                $main = $main_get;
					                $item = $item_get;
				                } else {
									continue;
				                }
				                break;
			                }
		                }
	                }

					if (!$main) {
						$main_page = $item_page = null;
					}
                }
            }

            if (tep_not_null($main)) {
	            if (empty($main_page)) {
		            require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);
	            } else {
		            require(DIR_WS_LANGUAGES . $language . '/' . $path = FILENAME_PAGES);
	            }
            } else {
                $not_found = true;
            }

    }
} else {
    if ($PARTNER && tep_session_is_registered('partner_id')) {
        $PARTNER = false;
        tep_redirect(tep_href_link());
    } elseif ($PARTNER && tep_session_is_registered('customer_id')) {
        $PARTNER = false;
        tep_redirect(tep_href_link());
    }

    if (!$PARTNER && !empty($_GET['mtd']) && !empty($_GET['mtr']) && is_numeric($_GET['mtd']) && is_numeric($_GET['mtr'])) {
        $can_continue = true;
        switch($method_info['method_id']) {
            /*case 1: // posts
                $TABLE_MATERIAL = TABLE_MARKETING_POSTS;
                $TABLE_MATERIAL_TAGS = TABLE_MARKETING_POSTS_TAGS;
                break;*/
            case 2: //banners
                $TABLE_MATERIAL = TABLE_MARKETING_BANNERS;
                $TABLE_MATERIAL_TAGS = TABLE_MARKETING_BANNERS_TAGS;
                break;
            /*case 3: //pics
                $TABLE_MATERIAL = TABLE_MARKETING_PICS;
                $TABLE_MATERIAL_TAGS = TABLE_MARKETING_PICS_TAGS;
                break;
            case 4: //mails
                $TABLE_MATERIAL = TABLE_MARKETING_MAILS;
                $TABLE_MATERIAL_TAGS = TABLE_MARKETING_MAILS_TAGS;
                break;
            case 5: //statuses
                $TABLE_MATERIAL = TABLE_MARKETING_STATUSES;
                $TABLE_MATERIAL_TAGS = TABLE_MARKETING_STATUSES_TAGS;
                break;*/
            case 6: //shares
                $TABLE_MATERIAL = TABLE_MARKETING_SHARES;
                $TABLE_MATERIAL_TAGS = TABLE_MARKETING_SHARES_TAGS;
                break;
            default:
                $can_continue = false;
                break;
        }

        if ($can_continue && tep_db_num_rows($material_query = tep_db_query('select * from ' . $TABLE_MATERIAL . ' where material_id = ' . (int)$_GET['mtr'] . ' and material_status = 1'))) {
            $material_info = tep_db_fetch_array($material_query);
            switch ($_GET['mtd']) {
                case 2: //banners
                    if ($material_info['products_id']) break;
                    $visit_array['method_id'] = (int)$_GET['mtd'];
                    $visit_array['material_id'] = (int)$_GET['mtr'];
                    break;
                case 6: //shares
                    if ($material_info['products_id']) break;
                    $og_pic = HTTP_SERVER . '/' . DIR_WS_MARKETING_SHARES . $material_info['material_pic'];
                    $og_title = $material_info['material_title'];
                    $og_desc = $material_info['material_desc'];
                    $js_redirect = $og_url;
                    if (!$IS_BOT && $AFFILIATE && !$visit_id) {
                        $visit_array['method_id'] = (int)$_GET['mtd'];
                        $visit_array['material_id'] = (int)$_GET['mtr'];
                    }
                    break;
            }
        }
    }

    require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);
    $main = $main_get;

    // default meta keys and title
    $og_title ='Vitaminized: E-shop for nutrition supplements and vitamins';
    $og_desc ='Vitaminized e-shop for healthy lifestyle offers nutrition supplements and vitamins made in USA for connective tissue, eye care, digestive system, and more.';

}

$og_title = empty($og_title) ? (defined('NAVBAR_TITLE_3') ? NAVBAR_TITLE_3 :
            (defined('NAVBAR_TITLE_2') ? NAVBAR_TITLE_2 :
                (defined('NAVBAR_TITLE_1') ? NAVBAR_TITLE_1 :
                    (defined('NAVBAR_TITLE') ? NAVBAR_TITLE : '')))) : $og_title;
$og_title = empty($og_title) ? '' : $og_title . '-';
if(tep_not_null($og_desc)){
    $og_desc = strip_tags(tep_break_text_int($og_desc, MAX_SHORT_DESCRIPTION_LENGTH)) . ' - ' . STORE_NAME;
} else
    $og_desc = STORE_NAME;
$og_text = tep_draw_meta_keywords(tep_prepare_html($og_title . ' ' . $og_desc));

if (!$IS_BOT && $AFFILIATE && $visit_id) {
    tep_db_query('update ' . TABLE_VISITS . ' set last_url = "' . tep_db_input(tep_get_current_url()) . '", last_online = now(), pageviews = pageviews + 1 where visit_id = ' . (int)$visit_id);
}