<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

$listing_split = new splitPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');
?>

<div class="contentText">

<?php
if ( ($listing_split->number_of_rows > 0) && ( (PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3') ) ) {
    ?>

    <div>
        <span style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></span>

        <span><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></span>
    </div>

    <br />

<?php
}

if (isset($search_keywords)) {
    $search_keywords = array_diff($search_keywords, array('(', ')', 'and', 'or'));
    $search_string_delimiter = ' ... ';

    if (isset($search_keywords) && (sizeof($search_keywords) > 0)) {
        $filtered_search_result = array();
        $unfiltered_search_result_query = tep_db_query($listing_split->sql_query);
        while($search_result_row = tep_db_fetch_array($unfiltered_search_result_query)) {
            $current_row_pID = $search_result_row['products_id'];
            $current_row_lID = $search_result_row['language_id'];
            if(!isset($filtered_search_result[$current_row_pID])) {
                $filtered_search_result[$current_row_pID] = array(
                    'products_id' => $current_row_pID,
                    'specials_new_products_price' => $search_result_row['specials_new_products_price'],
                    'manufacturers_id' => $search_result_row['manufacturers_id'],
                    'final_price' => $search_result_row['final_price'],
                    'products_tax_class_id' => $search_result_row['products_tax_class_id']
                );
                if (isset($search_result_row['products_quantity'])) $filtered_search_result[$current_row_pID]['products_quantity'] = $search_result_row['products_quantity'];
                if (isset($search_result_row['products_image'])) $filtered_search_result[$current_row_pID]['products_image'] = $search_result_row['products_image'];
                if (isset($search_result_row['products_price'])) $filtered_search_result[$current_row_pID]['products_price'] = $search_result_row['products_price'];
                if (isset($search_result_row['products_weight'])) $filtered_search_result[$current_row_pID]['products_weight'] = $search_result_row['products_weight'];
                $diff_results = array_diff_key($search_result_row, $filtered_search_result[$current_row_pID]);
                if (isset($search_result_row['products'])) $filtered_search_result[$current_row_pID]['tax_rate'] = $search_result_row['tax_rate'];

                $filtered_search_result[$current_row_pID]['products_model'] = $search_result_row['products_model'];
                $filtered_search_result[$current_row_pID]['products_misspellings'] = $search_result_row['products_misspellings'];
                $filtered_search_result[$current_row_pID]['products_description'] = '';
                $filtered_search_result[$current_row_pID]['products_info_text'] = '';
                $filtered_search_result[$current_row_pID]['manufacturers_name'] = $search_result_row['manufacturers_name'];
                $filtered_search_result[$current_row_pID]['flag'] = false;
            } else {
                $diff_results = array('products_description' => $search_result_row['products_description'],
                    'products_name' => $search_result_row['products_name'],
                    'products_info_text' => $search_result_row['products_info_text']
                );
            }

            foreach($diff_results as $k => $v) {
                switch($k) {
                    case 'products_misspellings':
                        foreach ($search_keywords as $keyword) {
                            if (mb_stripos($v, $keyword) !== false) {
                                $filtered_search_result[$current_row_pID]['flag'] = true;
                                break;
                            }
                        }
                        break;
                    case 'manufacturers_name':
                    case 'products_model':
                        $filtered_val = tep_mark_search_string(strip_tags($v), $search_keywords);
                        if ($filtered_val !== strip_tags($v)) {
                            $filtered_search_result[$current_row_pID][$k] = $filtered_val;
                            $filtered_search_result[$current_row_pID]['flag'] = true;
                        }

                        break;
                    case 'products_name':
                    case 'products_description':
                    case 'products_info_text':
                        $filtered_val = tep_mark_search_string(html_entity_decode(strip_tags($v)), $search_keywords);
                        if (!isset($filtered_search_result[$current_row_pID][$current_row_lID][$k])) {
                            $open_tag = '<span class="search-mark">';
                            $right_mark_start = mb_strpos($filtered_val, $open_tag);
                            $clipped_str = '';
                            if ($right_mark_start !== false) {
                                $close_tag = '</span>';
                                $open_tag_length  = mb_strlen($open_tag);
                                $close_tag_length = mb_strlen($close_tag);

                                $key_both_length = 20;

                                $right_mark_end = mb_stripos($filtered_val, $close_tag, $right_mark_start) + $close_tag_length;


                                $keywords_stack = array();
                                $current_key = mb_substr($filtered_val, $right_mark_start, $right_mark_end - $right_mark_start);
                                array_push($keywords_stack, tep_strtolower_int($current_key));
                                if ($right_mark_start > 0) {
                                    if ($right_mark_start - $key_both_length <= 0) {
                                        $current_str = mb_substr($filtered_val, 0, $right_mark_start);
                                    } else {
                                        $current_str = mb_strstr(mb_substr($filtered_val, $right_mark_start - $key_both_length, $key_both_length), ' ');
                                    }
                                    if ($current_str !== false) $clipped_str .= $current_str;
                                }


                                $clipped_str .= $current_key;

                                $prev_cut_mark = $current_cut_mark = $current_cut_from = $left_mark_end = $right_mark_end;

                                $current_cut_to = $current_cut_mark + $key_both_length;

                                $str_length = mb_strlen($filtered_val);

                                while (true) {
                                    $right_mark_start = mb_stripos($filtered_val, $open_tag, $left_mark_end);

                                    if ($right_mark_start === false) {
                                        if ($current_cut_to >= $str_length && $current_cut_from < $str_length) {
                                            $clipped_str .= mb_substr($filtered_val, $current_cut_from);
                                            break;
                                        }
                                        if ($current_cut_mark > $current_cut_from)
                                            $clipped_str .= mb_substr($filtered_val, $current_cut_from, $current_cut_mark - $current_cut_from);
                                        $current_str = mb_strrchr(mb_substr($filtered_val, $current_cut_mark, $current_cut_to - $current_cut_mark), ' ', true);
                                        if ($current_str !== false) $clipped_str .= $current_str;
                                        break;
                                    }

                                    $right_mark_end = mb_strpos($filtered_val, $close_tag, $right_mark_start) + $close_tag_length;
                                    $current_key = mb_substr($filtered_val, $right_mark_start, $right_mark_end - $right_mark_start);

                                    if (in_array(tep_strtolower_int($current_key), $keywords_stack)) {
                                        if ($current_cut_to >= $right_mark_start) {
                                            $current_cut_to += mb_strlen($current_key);
                                            $current_cut_mark = $right_mark_end;
                                        } else {

                                        }
                                    } else {
                                        if ($current_cut_to >= $right_mark_start - $key_both_length) {
                                            $current_cut_to = $right_mark_end + $key_both_length;
                                            $current_cut_mark = $right_mark_end;

                                        } else {
                                            if ($current_cut_mark > $current_cut_from) {
                                                $clipped_str .= mb_substr($filtered_val, $current_cut_from, $current_cut_mark - $current_cut_from);
                                            }
                                            $current_str = mb_strrchr(mb_substr($filtered_val, $current_cut_mark, $current_cut_to - $current_cut_mark), ' ', true);
                                            if ($current_str !== false) $clipped_str .= $current_str;
                                            $clipped_str .= $search_string_delimiter;

                                            $cut_index = $right_mark_start - $key_both_length;
                                            $right_open_pos = mb_strpos($filtered_val, $open_tag, $right_mark_start - $key_both_length);
                                            $right_close_pos = mb_strpos($filtered_val, $close_tag, $right_mark_start - $key_both_length) + $close_tag_length;
                                            if ($right_open_pos > $right_close_pos) {
                                                $cut_index = $right_close_pos;
                                            } else{
                                                $left_open_pos = mb_strrpos($filtered_val, $open_tag, $right_mark_start - $key_both_length - $str_length);
                                                $left_close_pos = mb_strrpos($filtered_val, $open_tag, $right_mark_start - $key_both_length - $str_length);

                                                if ($left_open_pos !== false && $left_open_pos > $left_close_pos) {
                                                    $cut_index = $right_close_pos;
                                                }
                                            }

                                            $current_str = mb_strstr(mb_substr($filtered_val, $cut_index, $right_mark_start - $cut_index), ' ');
                                            if ($current_str !== false) $clipped_str .= $current_str;
                                            $clipped_str .= $current_key;
                                            $current_cut_mark = $current_cut_from = $right_mark_end;
                                            $current_cut_to = $right_mark_end + $key_both_length;
                                        }
                                        array_push($keywords_stack, tep_strtolower_int($current_key));
                                    }
                                    $left_mark_end = $right_mark_end;
                                }


                                $filtered_search_result[$current_row_pID][$current_row_lID][$k] = $clipped_str;
                                $filtered_search_result[$current_row_pID]['flag'] = true;

                            } elseif ($k == 'products_name' && $current_row_lID == $languages_id) {
                                $filtered_search_result[$current_row_pID][$k] = $filtered_val;
                            }
                        }
                        break;
                }
            }
        }

        foreach($filtered_search_result as $pid => $parr) {
            if (!$parr['flag']) {
                unset($filtered_search_result[$pid]);
            } else {
                foreach($parr as $lang => $val) {
                    if (is_array($val)) {
                        foreach($val as $key => $value) {
                            switch($key) {
                                case 'products_name':
                                    if ($lang != $languages_id) {
                                        $filtered_search_result[$pid]['products_description'] .= $value . $search_string_delimiter;
                                    } else {
                                        $filtered_search_result[$pid]['products_name'] = $value;
                                    }
                                    break;
                                case 'products_description':
                                case 'products_info_text':
                                    $filtered_search_result[$pid][$key] .= $value . $search_string_delimiter;
                            }
                        }
                    }
                }
                if (empty($filtered_search_result[$pid]['products_name'])) {
                    $prod_name_query = tep_db_query('select pd.products_name from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION .' as pd on(p.products_id = pd.products_id and p.products_id = ' . (int)$pid . ' and pd.language_id = ' . (int)$languages_id . ')');
                    $prod_name_arr = tep_db_fetch_array($prod_name_query);
                    $filtered_search_result[$pid]['products_name'] = $prod_name_arr['products_name'];
                }
            }
        }
    }
}

$prod_list_contents = '<div class="ui-widget infoBoxContainer">' .
    '  <div class="ui-widget-header ui-corner-top infoBoxHeading">' .
    '    <table border="0" width="100%" cellspacing="0" cellpadding="2" class="productListingHeader">' .
    '      <tr>';

for ($col=0, $n=sizeof($column_list); $col<$n; $col++) {
    $lc_align = '';

    switch ($column_list[$col]) {
        case 'PRODUCT_LIST_MODEL':
            $lc_text = TABLE_HEADING_MODEL;
            $lc_align = '';
            break;
        case 'PRODUCT_LIST_NAME':
            $lc_text = TABLE_HEADING_PRODUCTS;
            $lc_align = '';
            break;
        case 'PRODUCT_LIST_MANUFACTURER':
            $lc_text = TABLE_HEADING_MANUFACTURER;
            $lc_align = '';
            break;
        case 'PRODUCT_LIST_PRICE':
            $lc_text = TABLE_HEADING_PRICE;
            $lc_align = 'right';
            break;
        case 'PRODUCT_LIST_QUANTITY':
            $lc_text = TABLE_HEADING_QUANTITY;
            $lc_align = 'right';
            break;
        case 'PRODUCT_LIST_WEIGHT':
            $lc_text = TABLE_HEADING_WEIGHT;
            $lc_align = 'right';
            break;
        case 'PRODUCT_LIST_IMAGE':
            $lc_text = TABLE_HEADING_IMAGE;
            $lc_align = 'center';
            break;
        case 'PRODUCT_LIST_BUY_NOW':
            $lc_text = TABLE_HEADING_BUY_NOW;
            $lc_align = 'center';
            break;
    }

    if ( ($column_list[$col] != 'PRODUCT_LIST_BUY_NOW') && ($column_list[$col] != 'PRODUCT_LIST_IMAGE') ) {
        $lc_text = tep_create_sort_heading($_GET['sort'], $col+1, $lc_text);
    }

    $prod_list_contents .= '        <td' . (tep_not_null($lc_align) ? ' align="' . $lc_align . '"' : '') . '>' . $lc_text . '</td>';
}

$prod_list_contents .= '      </tr>' .
    '    </table>' .
    '  </div>';

if ($listing_split->number_of_rows > 0) {
    $rows = 0;
    if (!isset($filtered_search_result)) {
        $listing_query = tep_db_query($listing_split->sql_query);
        while($filtered_search_result[] = tep_db_fetch_array($listing_query));
        array_pop($filtered_search_result);
    }


    $prod_list_contents .= '  <div class="ui-widget-content ui-corner-bottom productListTable">' .
        '    <table border="0" width="100%" cellspacing="0" cellpadding="2" class="productListingData">';

    foreach ($filtered_search_result as $listing) {
        $rows++;

        $prod_list_contents .= '      <tr>';

        $count_cols = 0;

        for ($col=0, $n=sizeof($column_list); $col<$n; $col++) {
            switch ($column_list[$col]) {
                case 'PRODUCT_LIST_MODEL':
                    $prod_list_contents .= '        <td>' . $listing['products_model'] . '</td>';
                    break;
                case 'PRODUCT_LIST_NAME':
                    $prod_list_contents .= '        <td><a href="' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($listing['products_name'])) . '">' . $listing['products_name'] . '</a></td>';
                    break;
                case 'PRODUCT_LIST_MANUFACTURER':
                    $prod_list_contents .= '        <td><a href="' . tep_href_link(MANUFACTURERS_URL . '/' . tep_clean_text_int($listing['manufacturers_name'])) . '">' . $listing['manufacturers_name'] . '</a></td>';
                    break;
                case 'PRODUCT_LIST_PRICE':
                    if (tep_not_null($listing['specials_new_products_price'])) {
                        $prod_list_contents .= '        <td align="right"><del>' .  $currencies->display_price($listing['products_price'], tep_get_tax_rate($listing['products_tax_class_id'])) . '</del>&nbsp;&nbsp;<span class="productSpecialPrice">' . $currencies->display_price($listing['specials_new_products_price'], tep_get_tax_rate($listing['products_tax_class_id'])) . '</span></td>';
                    } else {
                        $prod_list_contents .= '        <td align="right">' . $currencies->display_price($listing['products_price'], tep_get_tax_rate($listing['products_tax_class_id'])) . '</td>';
                    }
                    break;
                case 'PRODUCT_LIST_QUANTITY':
                    $prod_list_contents .= '        <td align="right">' . $listing['products_quantity'] . '</td>';
                    break;
                case 'PRODUCT_LIST_WEIGHT':
                    $prod_list_contents .= '        <td align="right">' . $listing['products_weight'] . '</td>';
                    break;
                case 'PRODUCT_LIST_IMAGE':
                    $prod_list_contents .= '        <td align="center"><a href="' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int(strip_tags($listing['products_name']))) . '">' . tep_image(DIR_WS_IMAGES_PRODUCTS . $listing['products_image']  . '-2' . IMAGE_EXTENSION, strip_tags($listing['products_name']), SMALL_IMAGE_WIDTH, '', 'id="image-' . $listing['products_id'] . '"') . '</a></td>';
                    break;
                case 'PRODUCT_LIST_BUY_NOW':
                    $prod_list_contents .= '        <td align="center"><span id="ajax-submit-' . $listing['products_id']  . '">' . tep_draw_button(IMAGE_BUTTON_BUY_NOW, 'cart', tep_href_link('', 'action=buy_now&products_id=' . $listing['products_id'])) . '</span>';
                    if (WITH_SHOPPING_CART) {
                        $prod_list_contents .= '<script>$("#ajax-submit-' . $listing['products_id']  . '").click(function () {return myCart.addProduct(' . $listing['products_id'] . ', "' . $listing['products_name'] . '", ' . $listing['products_price'] . ', 1, "' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($listing['products_name'])) . '");});</script>';
                    }
                    $prod_list_contents .= '</td>';
                    break;
            }
        }
        $prod_list_contents .= '      </tr>';
        if(!empty($listing['products_info_text']) || !empty($listing['products_description'])) {
            $prod_list_contents .= '<tr class="alt" ><td colspan="' . $n . '" >' . (empty($listing['products_description']) ? '' : $listing['products_description']) . (empty($listing['products_info_text']) ? '' : $listing['products_info_text']) . '</td></tr>';
        }
    }

    $prod_list_contents .= '    </table>' .
        '  </div>' .
        '</div>';

    echo $prod_list_contents;
} else {
    ?>

    <p><?php echo TEXT_NO_PRODUCTS; ?></p>

<?php
}

if ( ($listing_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3')) ): ?>

    <br />

    <div>
        <span style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info', 'x', 'y'))); ?></span>

        <span><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></span>
    </div>

<?php endif; ?>

</div>
