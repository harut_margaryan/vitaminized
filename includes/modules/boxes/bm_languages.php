<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

class bm_languages {
    var $code = 'bm_languages';
    var $group = 'boxes';
    var $title;
    var $description;
    var $sort_order;
    var $enabled = false;

    function bm_languages() {
        $this->title = MODULE_BOXES_LANGUAGES_TITLE;
        $this->description = MODULE_BOXES_LANGUAGES_DESCRIPTION;

        if ( defined('MODULE_BOXES_LANGUAGES_STATUS') ) {
            $this->sort_order = MODULE_BOXES_LANGUAGES_SORT_ORDER;
            $this->enabled = (MODULE_BOXES_LANGUAGES_STATUS == 'True');

            $this->group = ((MODULE_BOXES_LANGUAGES_CONTENT_PLACEMENT == 'Left Column') ? 'boxes_column_left' : 'boxes_column_right');
        }
    }

    function execute() {
        global $PHP_SELF, $lng, $request_type, $oscTemplate, $current_language, $og_url, $current_category_id, $main, $item, $cPath, $products_id, $languages_id, $main_page, $item_page;

        if (substr(($PHP_SELF), 0, 8) != 'checkout') {
            if (!isset($lng) || (isset($lng) && !is_object($lng))) {
                include(DIR_WS_CLASSES . 'language.php');
                $lng = new language;
            }

            if (count($lng->catalog_languages) > 1) {
                $languages_string = '<select id="language_list">';

                $flag = '';


                if($main === PRODUCT_URL) {
                    $products_query = tep_db_query('select pd.products_name, pd.language_id from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd using(products_id) where p.products_id = ' . (int)$products_id);
                    if(tep_db_num_rows($products_query)) {
                        $flag = 'product';
                    }
                } elseif ($main_page) {
					$pages_lngs_query = tep_db_query('select p.parents_id, pd.pages_title, pd.language_id from ' . TABLE_PAGES . ' as p join ' . TABLE_PAGES_DESCRIPTION . ' as pd on(p.pages_id = pd.pages_id) where p.pages_status = 1 and (p.pages_id = ' . intval($main_page['pages_id']) . ($item_page ? ' or p.pages_id = ' . intval($item_page['pages_id']) : '') . ') order by p.parents_id asc');
                    if (tep_db_num_rows($pages_lngs_query)) {
                        $flag = 'page';
                    }
                } elseif ($current_category_id) {
                    $cats_query = tep_db_query('select c.categories_id, cd.categories_name, cd.language_id from ' . TABLE_CATEGORIES . ' as c join ' . TABLE_CATEGORIES_DESCRIPTION . ' as cd using(categories_id) where categories_id = ' . (int)$current_category_id . ' or categories_id = ' . (int)strtok($cPath, '_'));
                    if (tep_db_num_rows($cats_query)) {
                        $flag = 'category';
                    }
                }

                reset($lng->catalog_languages);
                $cur_lang_save = $current_language;
                while (list($key, $value) = each($lng->catalog_languages)) {
                    $current_language = $key;
                    //$languages_string .= ' <a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('language', 'currency')) . 'language=' . $key, $request_type) . '">' . tep_image(DIR_WS_LANGUAGES .  $value['directory'] . '/images/' . $value['image'], $value['name']) . '</a> ';
					$url = '';
                    switch($flag) {
                        case 'product' :
                            tep_db_data_seek($products_query, 0);
                            while($cur_prod = tep_db_fetch_array($products_query)) {
                                if ($cur_prod['language_id'] == $value['id']) {
                                    $languages_string .= ' <option value="' . htmlentities(tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($cur_prod['products_name']))) . '"' . ($value['id'] == $languages_id ? ' selected' : '') . '>' . $value['name'] . '</option> ';
                                    break;
                                }
                            }
                            break;
						case 'page' :
							tep_db_data_seek($pages_lngs_query, 0);
							while($page = tep_db_fetch_array($pages_lngs_query)) {
								if ($page['language_id'] == $value['id']) {
									$url .= tep_clean_text_int($page['pages_title']) . '/';
								}
							}

							$languages_string .= ' <option value="' . htmlentities(tep_href_link($url)) . '"' . ($value['id'] == $languages_id ? ' selected' : '') . '>' . $value['name'] . '</option> ';
							break;
                        case 'category' :
                            $url_cat = '';
                            $url_subcat = '';
                            tep_db_data_seek($cats_query, 0);
                            while($cur_cat = tep_db_fetch_array($cats_query)) {
                                if ($cur_cat['language_id'] == $value['id']) {
                                    if ($cur_cat['categories_id'] == strtok($cPath, '_')) {
                                        $url_cat = tep_clean_text_int($cur_cat['categories_name']);
                                    } else {
                                        $url_subcat = tep_clean_text_int($cur_cat['categories_name']);
                                    }
                                }
                            }

                            if ($url_cat) {
                                if ($url_subcat) {
                                    //$languages_string .= ' <a href="' . tep_href_link($url_cat . '/' . $url_subcat) . '">' . tep_image(DIR_WS_LANGUAGES .  $value['directory'] . '/images/' . $value['image'], $value['name']) . '</a> ';
                                    $languages_string .= ' <option value="' . htmlentities(tep_href_link($url_cat . '/' . $url_subcat)) . '"' . ($value['id'] == $languages_id ? ' selected' : '') . '>' . $value['name'] . '</option> ';
                                } else {
                                    $languages_string .= ' <option value="' . htmlentities(tep_href_link($url_cat)) . '"' . ($value['id'] == $languages_id ? ' selected' : '') . '>' . $value['name'] . '</option> ';
                                }
                            }
                            break;
                        default:
                            $url_array = parse_url($og_url);
                            if (!empty($url_array['path'])) {
                                $url_array['path'] = substr_replace($url_array['path'], $key, 1, 2);
                                $new_url = tep_build_url($url_array);
                            } else {
                                $new_url = $og_url . '/' . $key;
                            }
                            $languages_string .= ' <option value="' . htmlentities($new_url) . '"' . ($value['id'] == $languages_id ? ' selected' : '') . '>' . $value['name'] . '</option> ';

                            break;
                    }

                }
                $current_language = $cur_lang_save;
                /*$data = '<div class="ui-widget infoBoxContainer">' .
                        '  <div class="ui-widget-header infoBoxHeading">' . MODULE_BOXES_LANGUAGES_BOX_TITLE . '</div>' .
                        '  <div class="ui-widget-content infoBoxContents" style="text-align: center;">' . $languages_string . '</div>' .
                        '</div>';*/
				$languages_string .= '</select>';
				$languages_string .= '<script>$("#language_list").change(function() {window.location = $(this).val();});</script>';
                $data = $languages_string;

                //$oscTemplate->addBlock($data, $this->group);
                return $data;
            }
        }
    }

    function isEnabled() {
        return $this->enabled;
    }

    function check() {
        return defined('MODULE_BOXES_LANGUAGES_STATUS');
    }

    function install() {
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Languages Module', 'MODULE_BOXES_LANGUAGES_STATUS', 'True', 'Do you want to add the module to your shop?', '6', '1', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Content Placement', 'MODULE_BOXES_LANGUAGES_CONTENT_PLACEMENT', 'Right Column', 'Should the module be loaded in the left or right column?', '6', '1', 'tep_cfg_select_option(array(\'Left Column\', \'Right Column\'), ', now())");
        tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_BOXES_LANGUAGES_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
    }

    function remove() {
        tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
        return array('MODULE_BOXES_LANGUAGES_STATUS', 'MODULE_BOXES_LANGUAGES_CONTENT_PLACEMENT', 'MODULE_BOXES_LANGUAGES_SORT_ORDER');
    }
}
?>
