<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
class bm_category {
    var $name;
    var $id;
    var $parent;
    var $childes = array();

    function __construct($m_id = 0, $m_name = '', $m_parent = null, $m_childes = array()) {
        $this->name = (string)$m_name;
        $this->id = (int)$m_id;
        $this->parent = $m_parent;
        $this->childes = (array)$m_childes;
    }
}

class bm_tree {
    var $root;

    function __construct() {
        $this->root = new bm_category();
    }

    function get_category_by_id($m_id) {
        $m_id = (int)$m_id;
        $parent = $this->root;
        if($m_id === 0) return $parent;
        if (array_key_exists($m_id, $parent->childes)) {
            return $parent->childes[$m_id];
        }
        foreach($parent->childes as $childes) {
            if (array_key_exists($m_id, $childes->childes)) {
                return $childes->childes[$m_id];
            }
        }
        return false;
    }

    function get_category_by_name($m_name) {
        $parent = $this->root;
        if(empty($m_name)) return $parent;
        foreach($parent->childes as $childes) {
            if (tep_clean_text_int($childes->name) === $m_name) {
                return $childes;
            }
        }
        return false;
    }

    function add($m_category, $m_parent_id) {
        $parent = $this->get_category_by_id($m_parent_id);
        if ($parent) {
            $parent->childes[$m_category->id] = $m_category;
            $m_category->parent = $parent;
        }
    }

    function reverse_tree ($f, &$out) {
        foreach($this->root->childes as $cat) {
            $f($cat, $out);
            foreach($cat->childes as $sub) {
                $f($sub, $out);
            }
        }
    }
}

class bm_categories {
    var $code = 'bm_categories';
    var $group = 'boxes';
    var $title;
    var $description;
    var $cats_out;
    var $sort_order;
    var $enabled = false;

    function bm_categories() {
        $this->cats_out = '';
      $this->title = MODULE_BOXES_CATEGORIES_TITLE;
      $this->description = MODULE_BOXES_CATEGORIES_DESCRIPTION;

      if ( defined('MODULE_BOXES_CATEGORIES_STATUS') ) {
        $this->sort_order = MODULE_BOXES_CATEGORIES_SORT_ORDER;
        $this->enabled = (MODULE_BOXES_CATEGORIES_STATUS == 'True');

        $this->group = ((MODULE_BOXES_CATEGORIES_CONTENT_PLACEMENT == 'Left Column') ? 'boxes_column_left' : 'boxes_column_right');
      }
    }
    function getData() {
        global $languages_id, $categories_tree;

        if (empty($categories_tree)) {
            $categories_tree = new bm_tree();

            $all_categories_query = tep_db_query('select c.categories_id, c.parent_id, cd.categories_name from ' . TABLE_CATEGORIES . ' as c join ' . TABLE_CATEGORIES_DESCRIPTION . ' as cd using(categories_id) where cd.language_id = "' . (int)$languages_id .'" order by parent_id, sort_order, categories_name');

            while($current_category = tep_db_fetch_array($all_categories_query)) {
                $new_category = new bm_category(
                    $current_category['categories_id'],
                    $current_category['categories_name']
                );
                $categories_tree->add($new_category, $current_category['parent_id']);
            }
        }

        $categories_tree->reverse_tree('out_categories', $this->cats_out);

        $data = '<div class="ui-widget infoBoxContainer">' .
            '  <div class="ui-widget-header infoBoxHeading">' . MODULE_BOXES_CATEGORIES_BOX_TITLE . '</div>' .
            '  <div class="ui-widget-content infoBoxContents">' . $this->cats_out . '</div>' .
            '</div>';

        return $data;
    }

    function execute() {
      global $SID, $oscTemplate;

      /*if ((USE_CACHE == 'true') && empty($SID)) {
        $output = tep_cache_categories_box();
      } else {*/
        $output = $this->getData();
      //}

      $oscTemplate->addBlock($output, $this->group);
    }

    function isEnabled() {
      return $this->enabled;
    }

    function check() {
      return defined('MODULE_BOXES_CATEGORIES_STATUS');
    }

    function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Categories Module', 'MODULE_BOXES_CATEGORIES_STATUS', 'True', 'Do you want to add the module to your shop?', '6', '1', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Content Placement', 'MODULE_BOXES_CATEGORIES_CONTENT_PLACEMENT', 'Left Column', 'Should the module be loaded in the left or right column?', '6', '1', 'tep_cfg_select_option(array(\'Left Column\', \'Right Column\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_BOXES_CATEGORIES_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
    }

    function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    function keys() {
      return array('MODULE_BOXES_CATEGORIES_STATUS', 'MODULE_BOXES_CATEGORIES_CONTENT_PLACEMENT', 'MODULE_BOXES_CATEGORIES_SORT_ORDER');
    }
  }