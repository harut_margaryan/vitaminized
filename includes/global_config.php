<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

$num_queries = 0;
$time = explode(' ', microtime());
$start = $time[1] + $time[0];
$with_partners_key = true;

error_reporting(E_ALL);
ini_set("display_errors", 1);


// start the timer for the page parse time log
define('PAGE_PARSE_START_TIME', microtime());
define('MYSQL_FORMAT_DATETIME', 'Y-m-d H:i:s');

mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");

// set the level of error reporting
//  error_reporting(E_ALL & ~E_NOTICE);

// check support for register_globals
if (function_exists('ini_get') && (ini_get('register_globals') == false) && (PHP_VERSION < 4.3) ) {
    exit('Server Requirement Error: register_globals is disabled in your PHP configuration. This can be enabled in your php.ini configuration file or in the .htaccess file in your catalog directory. Please use PHP 4.3+ if register_globals cannot be enabled on the server.');
}

// load server configuration parameters
if (file_exists('includes/local/configure.php')) { // for developers
    include('includes/local/configure.php');
} else {
    include('includes/configure.php');
}

$PARTNER = $_SERVER['HTTP_HOST'] == HTTP_HOST_PARTNER ? true : false;

if (strlen(DB_SERVER) < 1) {
    if (is_dir('install')) {
        header('Location: install/index.php');
    }
}

// define the project version --- obsolete, now retrieved with tep_get_version()
define('PROJECT_VERSION', 'osCommerce Online Merchant v2.3');

// some code to solve compatibility issues
require(DIR_WS_FUNCTIONS . 'compatibility.php');

// set the type of request (secure or not)
$request_type = (getenv('HTTPS') == 'on') ? 'SSL' : 'NONSSL';

// set php_self in the local scope
$PHP_SELF = '';
if(isset($_GET['main'])) {
    $PHP_SELF = $_GET['main'];
    if(isset($_GET['item']))
        $PHP_SELF .= '/' . $_GET['item'];
}
//$PHP_SELF = (((strlen(ini_get('cgi.fix_pathinfo')) > 0) && ((bool)ini_get('cgi.fix_pathinfo') == false)) || !isset($_SERVER['SCRIPT_NAME'])) ? basename($_SERVER['PHP_SELF']) : basename($_SERVER['SCRIPT_NAME']);

if ($request_type == 'NONSSL') {
    define('DIR_WS_CATALOG', DIR_WS_HTTP_CATALOG);
} else {
    define('DIR_WS_CATALOG', DIR_WS_HTTPS_CATALOG);
}

$SOC_TYPES_ROWS = array(
    'partner' => array(
        0 => false,
        1 => 'partners_soc_id_fb',
        2 => 'partners_soc_id_g',
        3 => 'partners_soc_id_tw',
        4 => 'partners_soc_id_vk'
    ),
    'customer' => array(
        0 => false,
        1 => 'customers_soc_id_fb',
        2 => 'customers_soc_id_g',
        3 => 'customers_soc_id_tw',
        4 => 'customers_soc_id_vk'
    )
);

// include the database functions
require(DIR_WS_FUNCTIONS . 'database.php');

// make a connection to the database... now
tep_db_connect() or die('Unable to connect to database server!');

// set the application parameters
$configuration_query = tep_db_query('select configuration_key as cfgKey, configuration_value as cfgValue from ' . TABLE_CONFIGURATION);
while ($configuration = tep_db_fetch_array($configuration_query)) {
    define($configuration['cfgKey'], $configuration['cfgValue']);
}

// define general functions used application-wide
require(DIR_WS_FUNCTIONS . 'general.php');
require(DIR_WS_FUNCTIONS . 'html_output.php');

require(DIR_WS_FUNCTIONS . 'html_output_our.php');////////////////////////////

// set the cookie domain
$cookie_domain = (($request_type == 'NONSSL') ? HTTP_COOKIE_DOMAIN : HTTPS_COOKIE_DOMAIN);
$cookie_path = (($request_type == 'NONSSL') ? HTTP_COOKIE_PATH : HTTPS_COOKIE_PATH);

// include shopping cart class
require(DIR_WS_CLASSES . 'shopping_cart.php');

// define how the session functions will be used
require(DIR_WS_FUNCTIONS . 'sessions.php');

// set the session name and save path
//tep_session_name('osCsid');
tep_session_save_path(SESSION_WRITE_DIRECTORY);

// set the session cookie parameters
if (function_exists('session_set_cookie_params')) {
    session_set_cookie_params(0, $cookie_path, $cookie_domain);
} elseif (function_exists('ini_set')) {
    ini_set('session.cookie_lifetime', '0');
    ini_set('session.cookie_path', $cookie_path);
    ini_set('session.cookie_domain', $cookie_domain);
}

@ini_set('session.use_only_cookies', (SESSION_FORCE_COOKIE_USE == 'True') ? 1 : 0);

// set the session ID if it exists
if (isset($_POST[tep_session_name()])) {
    tep_session_id($_POST[tep_session_name()]);
} elseif ( ($request_type == 'SSL') && isset($_GET[tep_session_name()]) ) {
    tep_session_id($_GET[tep_session_name()]);
}

// start the session
$session_started = false;
if (SESSION_FORCE_COOKIE_USE == 'True') {
    tep_setcookie('cookie_test', 'please_accept_for_session', time()+60*60*24*30, $cookie_path, $cookie_domain);

    if (isset($_COOKIE['cookie_test'])) {
        tep_session_start();
        $session_started = true;
    }
} elseif (SESSION_BLOCK_SPIDERS == 'True') {
    $user_agent = strtolower(getenv('HTTP_USER_AGENT'));
    $spider_flag = false;

    if (tep_not_null($user_agent)) {
        $spiders = file(DIR_WS_INCLUDES . 'spiders.txt');

        for ($i=0, $n=sizeof($spiders); $i<$n; $i++) {
            if (tep_not_null($spiders[$i])) {
                if (is_integer(strpos($user_agent, trim($spiders[$i])))) {
                    $spider_flag = true;
                    break;
                }
            }
        }
    }

    if ($spider_flag == false) {
        tep_session_start();
        $session_started = true;
    }
} else {
    tep_session_start();
    $session_started = true;
}

if ( ($session_started == true) && (PHP_VERSION >= 4.3) && function_exists('ini_get') && (ini_get('register_globals') == false) ) {
    extract($_SESSION, EXTR_OVERWRITE+EXTR_REFS);
}

// initialize a session token
if (!tep_session_is_registered('sessiontoken')) {
    $sessiontoken = md5(tep_rand() . tep_rand() . tep_rand() . tep_rand());
    tep_session_register('sessiontoken');
}

// set SID once, even if empty
$SID = (defined('SID') ? SID : '');

// verify the ssl_session_id if the feature is enabled
if ( ($request_type == 'SSL') && (SESSION_CHECK_SSL_SESSION_ID == 'True') && (ENABLE_SSL == true) && ($session_started == true) ) {
    $ssl_session_id = getenv('SSL_SESSION_ID');
    if (!tep_session_is_registered('SSL_SESSION_ID')) {
        $SESSION_SSL_ID = $ssl_session_id;
        tep_session_register('SESSION_SSL_ID');
    }

    if ($SESSION_SSL_ID != $ssl_session_id) {
        tep_session_destroy();
        tep_redirect(tep_href_link(SSL_CHECK_URL));
    }
}

// verify the browser user agent if the feature is enabled
if (SESSION_CHECK_USER_AGENT == 'True') {
    $http_user_agent = getenv('HTTP_USER_AGENT');
    if (!tep_session_is_registered('SESSION_USER_AGENT')) {
        $SESSION_USER_AGENT = $http_user_agent;
        tep_session_register('SESSION_USER_AGENT');
    }

    if ($SESSION_USER_AGENT != $http_user_agent) {
        tep_session_destroy();
        tep_redirect(tep_href_link(LOGIN_URL));
    }
}

// verify the IP address if the feature is enabled
if (SESSION_CHECK_IP_ADDRESS == 'True') {
    $ip_address = tep_get_ip_address();
    if (!tep_session_is_registered('SESSION_IP_ADDRESS')) {
        $SESSION_IP_ADDRESS = $ip_address;
        tep_session_register('SESSION_IP_ADDRESS');
    }

    if ($SESSION_IP_ADDRESS != $ip_address) {
        tep_session_destroy();
        tep_redirect(tep_href_link(LOGIN_URL));
    }
}



// create the shopping cart
if (!tep_session_is_registered('cart') || !is_object($cart)) {
	tep_session_register('cart');
	$cart = new shoppingCart;
}



// include currencies class and create an instance
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();


// set the language
//  if (/*!tep_session_is_registered('language') ||*/ isset($_GET['language'])) {
/*if (!tep_session_is_registered('language')) {
  tep_session_register('language');
  tep_session_register('languages_id');
}*/

include(DIR_WS_CLASSES . 'language.php');
$lng = new language();
$current_language = '';
if (isset($_GET['language']) && tep_not_null($_GET['language'])) {
    $lng->set_language(strtolower($_GET['language']));
} else {
    $lng->set_language(DEFAULT_LANGUAGE);
}
/*else {
     $lng->get_browser_language();
   }*/

$language = $lng->language['directory'];
$languages_id = $lng->language['id'];
//  }

// include the language translations
require(DIR_WS_LANGUAGES . $language . '.php');


// GLOBALS
$IS_BOT = tep_is_bots_agent(tep_get_user_agent());
$AFFILIATE = false;

if (isset($_COOKIE['affiliate']['partners_id']) && isset($_COOKIE['affiliate']['partners_key']) && isset($_COOKIE['affiliate']['from_url'])) {
    if ($partners_id_cookie = tep_decrypt_cookie($_COOKIE['affiliate']['partners_id']) && $partners_key_cookie = tep_decrypt_cookie($_COOKIE['affiliate']['partners_key']) && ($from_url_cookie = tep_decrypt_cookie($_COOKIE['affiliate']['from_url'])) !== false) {
        $AFFILIATE = array(
            'partners_id' => $partners_id_cookie,
            'partners_key' => $partners_key_cookie,
            'from_url' => $from_url_cookie
        );
    }
}

if (!$IS_BOT && isset($_GET['p']) && is_numeric($_GET['p']) && (!tep_session_is_registered('partner_key') || $partner_key != (int)$_GET['p'])) {
    $partners_key_get = (int)$_GET['p'];
    if ($AFFILIATE && $AFFILIATE['partners_key'] == $partners_key_get) {
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $new_referer = $_SERVER['HTTP_REFERER'];
            $new_referers_host = parse_url($new_referer , PHP_URL_HOST);
            if ($new_referers_host && $new_referers_host != HTTP_HOST && $new_referers_host != HTTP_HOST_PARTNER) {
                tep_setcookie('affiliate[from_url]', $new_referer, time() + 60 * 60 * 24 * 365 * 10);
                $AFFILIATE['from_url'] = $new_referer;
            }
        }
    } else {
        $check_partner_query = tep_db_query('select partners_id from ' . TABLE_PARTNERS . ' where partners_key = ' . $partners_key_get);
        if (tep_db_num_rows($check_partner_query) == 1) {
            $check_partner = tep_db_fetch_array($check_partner_query);
            $referer = empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
            tep_setcookie('affiliate[partners_id]', tep_encrypt_cookie($check_partner['partners_id']), time() + 60 * 60 * 24 * 365 * 10);
            tep_setcookie('affiliate[partners_key]', tep_encrypt_cookie($partners_key_get), time() + 60 * 60 * 24 * 365 * 10);
            tep_setcookie('affiliate[from_url]', tep_encrypt_cookie($referer), time() + 60 * 60 * 24 * 365 * 10);
            $AFFILIATE = array();
            $AFFILIATE['partners_key'] = $partners_key_get;
            $AFFILIATE['partners_id'] = $check_partner['partners_id'];
            $AFFILIATE['from_url'] = $check_partner['from_url'];
        }
    }
}