<?php

class TwitterOAuth {    
    const SIGNATURE_METHOD = 'HMAC-SHA1';
    const OAUTH_VERSION = '1.0';

    const URL_REQUEST_TOKEN      = 'https://api.twitter.com/oauth/request_token';
    const URL_AUTHENTICATE       = 'https://api.twitter.com/oauth/authenticate';
    const URL_ACCESS_TOKEN       = 'https://api.twitter.com/oauth/access_token';
    const URL_VERIFY_CREDENTIALS = 'https://api.twitter.com/1.1/account/verify_credentials.json';

    private $_consumer_key;
    private $_consumer_secret;
    private $_url_callback;
    
    private $_http_method;

    private $_oauth_token_secret = '';
    private $_oauth_parameters = array();



    public  function __construct($consumer_key, $consumer_secret, $url_callback) {
        $this->_consumer_key = $consumer_key;
        $this->_consumer_secret = $consumer_secret;
        $this->_url_callback = $url_callback;
    }

    public function access_token () {
        $this->_http_method = 'POST';
        if (empty($_GET['oauth_token']) || $_GET['oauth_token'] !== $_SESSION['oauth_token']) return false;
        $oauth_verifier = $_GET['oauth_verifier'];

        $this->_oauth_parameters['oauth_consumer_key'] = $this->_consumer_key;
        $this->_oauth_parameters['oauth_callback'] = $this->_url_callback;
        $this->_oauth_parameters['oauth_signature_method'] = self::SIGNATURE_METHOD;
        $this->_oauth_parameters['oauth_version'] = self::OAUTH_VERSION;
        $this->_oauth_parameters['oauth_nonce'] = base64_encode(openssl_random_pseudo_bytes(32));
        $this->_oauth_parameters['oauth_timestamp'] = time();
        $this->_oauth_parameters['oauth_token'] = $_SESSION['oauth_token'];
        $this->_oauth_parameters['oauth_signature'] = $this->create_signature();


        $DST = 'OAuth ';
        foreach ($this->_oauth_parameters as $key => $value) {
            $DST .= rawurlencode($key) . '="' . rawurlencode($value) . '", ';
        }
        $DST = substr($DST, 0, -2);

        $post_body = 'oauth_verifier=' . $oauth_verifier;
        $content_length = strlen($post_body);
        $ch = curl_init();
        $curl_opts = array(
            CURLOPT_URL => self::URL_ACCESS_TOKEN,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => array(
                'Host: api.twitter.com',
                'Accept: */*',
                'Authorization: ' . $DST,
                'Content-Length: ' . $content_length,
                'Expect:',
                'Content-Type: application/x-www-form-urlencoded'
            ),
            CURLOPT_POSTFIELDS => $post_body,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        );
        curl_setopt_array($ch, $curl_opts);
        $response = curl_exec($ch);
        $error = curl_error($ch);

    }

    public function  request_token () {
        $this->_http_method = 'GET';
        $this->authorizing_request();
    }


    private  function authorizing_request () {
        $this->_oauth_parameters['oauth_consumer_key'] = $this->_consumer_key;
        $this->_oauth_parameters['oauth_callback'] = $this->_url_callback;
        $this->_oauth_parameters['oauth_signature_method'] = self::SIGNATURE_METHOD;
        $this->_oauth_parameters['oauth_version'] = self::OAUTH_VERSION;
        $this->_oauth_parameters['oauth_nonce'] = base64_encode(openssl_random_pseudo_bytes(32));
        $this->_oauth_parameters['oauth_timestamp'] = time();
        $this->_oauth_parameters['oauth_signature'] = $this->create_signature();

        $url = self::URL_REQUEST_TOKEN . '?';
        foreach ($this->_oauth_parameters as $key => $value) {
            $url .= rawurlencode($key) . '=' . rawurlencode($value) . '&';
        }
        $url = substr($url, 0, -1);

        /*$DST = 'OAuth ';
        $DST_arr = array();
        foreach ($this->_oauth_parameters as $key => $value) {
            $DST_arr[] = rawurlencode($key) . '="' . rawurlencode($value) . '"';
        }
        $DST .= join(', ', $DST_arr);

        $ch = curl_init();
        $curl_opts = array(
            CURLOPT_URL => self::URL_ACCESS_TOKEN,
            CURLOPT_HEADER => false,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . $DST,
                'Expect:'
            ),
            CURLOPT_POSTFIELDS => array(),
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        );

        curl_setopt_array($ch, $curl_opts);

        $response = curl_exec($ch);
        */

        $response = file_get_contents($url);
        if (!$response) return false;
        parse_str($response, $result);
        if (empty($result['oauth_callback_confirmed']) || $result['oauth_callback_confirmed'] != true) return false;
        $_SESSION['oauth_token'] = $result['oauth_token'];
        $_SESSION['oauth_token_secret'] = $result['oauth_token_secret'];
        header('Location: ' . self::URL_AUTHENTICATE . '?oauth_token=' . $result['oauth_token']);
        exit();
    }

    private function create_signature () {
        $parameter_string = '';
        $encoded_pairs = array();

        foreach($this->_oauth_parameters as $k => $v) {
            $encoded_pairs[rawurlencode($k)] = rawurlencode($v);
        }
        ksort($encoded_pairs);
        foreach($encoded_pairs as $k => $v) {
            $parameter_string .= $k . '=' . $v . '&';
        }
        $parameter_string = substr($parameter_string, 0, -1);

        $signature_base_string = $this->_http_method . '&'
                               . rawurlencode(self::URL_REQUEST_TOKEN) . '&'
                               . rawurlencode($parameter_string);

        $signing_key = rawurlencode($this->_consumer_secret) . '&' . rawurlencode($this->_oauth_token_secret);

        return base64_encode(hash_hmac('sha1', $signature_base_string, $signing_key, true));
    }
}

class TwitterAuth {

  const URL_REQUEST_TOKEN = 'https://api.twitter.com/oauth/request_token';
  const URL_AUTHORIZE = 'https://api.twitter.com/oauth/authorize';
  const URL_ACCESS_TOKEN = 'https://api.twitter.com/oauth/access_token';
  const URL_ACCOUNT_DATA = 'http://twitter.com/users/show';

  // secret key and redirection url
  private $_consumer_key = '';
  private $_consumer_secret = '';
  private $_url_callback = '';
  // oauth array
  private $_oauth = array();
  // twitter user id
  private $_user_id = 0;

  public function __construct($consumerkey, $consumersecret, $urlcallback) {
            $this->_consumer_key = $consumerkey;
            $this->_consumer_secret = $consumersecret;
            $this->_url_callback = $urlcallback;
          }

  /**
       * First step
       *
       */
  public function request_token() {
            $this->_init_oauth();

            // The first must be oauth_callback -> oauth_consumer_key -> ... -> oauth_version.
            $oauth_base_text = "GET&";
            $oauth_base_text .= urlencode(self::URL_REQUEST_TOKEN) . "&";
            $oauth_base_text .= urlencode("oauth_callback=" . urlencode($this->_url_callback) . "&");
            $oauth_base_text .= urlencode("oauth_consumer_key=" . $this->_consumer_key . "&");
            $oauth_base_text .= urlencode("oauth_nonce=" . $this->_oauth['nonce'] . "&");
            $oauth_base_text .= urlencode("oauth_signature_method=HMAC-SHA1&");
            $oauth_base_text .= urlencode("oauth_timestamp=" . $this->_oauth['timestamp'] . "&");
            $oauth_base_text .= urlencode("oauth_version=1.0");

            // Generete key
            $key = $this->_consumer_secret . "&";

            // generate oauth_signature
            $this->_oauth['signature'] = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));

            // Generate get request
            $url = self::URL_REQUEST_TOKEN;
            $url .= '?oauth_callback=' . urlencode($this->_url_callback);
            $url .= '&oauth_consumer_key=' . $this->_consumer_key;
            $url .= '&oauth_nonce=' . $this->_oauth['nonce'];
            $url .= '&oauth_signature=' . urlencode($this->_oauth['signature']);
            $url .= '&oauth_signature_method=HMAC-SHA1';
            $url .= '&oauth_timestamp=' . $this->_oauth['timestamp'];
            $url .= '&oauth_version=1.0';

            // Execute a query
            $response = file_get_contents($url);

            // Parse the response string
            parse_str($response, $result);

            // Save session
            $_SESSION['oauth_token'] = $this->_oauth['token'] = $result['oauth_token'];
            $_SESSION['oauth_token_secret'] = $this->_oauth['token_secret'] = $result['oauth_token_secret'];
          }

  /**
       * The second step
       *
       */
  public function authorize() {
            // с
            $url = self::URL_AUTHORIZE;
            $url .= '?oauth_token=' . $this->_oauth['token'];
            header('Location: ' . $url . '');
          }

  /**
       * The third step
       */
  public function access_token($token, $verifier) {
            $this->_init_oauth();

            // Token from Get query
            $this->_oauth['token'] = $token;

            $this->_oauth['token_secret'] = $_SESSION['oauth_token_secret'];

            // Token from Get query
            $this->_oauth['verifier'] = $verifier;

            // The first must be oauth_callback -> oauth_consumer_key -> ... -> oauth_version.
            $oauth_base_text = "GET&";
            $oauth_base_text .= urlencode(self::URL_ACCESS_TOKEN) . "&";
            $oauth_base_text .= urlencode("oauth_consumer_key=" . $this->_consumer_key . "&");
            $oauth_base_text .= urlencode("oauth_nonce=" . $this->_oauth['nonce'] . "&");
            $oauth_base_text .= urlencode("oauth_signature_method=HMAC-SHA1&");
            $oauth_base_text .= urlencode("oauth_token=" . $this->_oauth['token'] . "&");
            $oauth_base_text .= urlencode("oauth_timestamp=" . $this->_oauth['timestamp'] . "&");
            $oauth_base_text .= urlencode("oauth_verifier=" . $this->_oauth['verifier'] . "&");
            $oauth_base_text .= urlencode("oauth_version=1.0");

            // Generete keys(Consumer secret + '&' + oauth_token_secret)
            $key = $this->_consumer_secret . "&" . $this->_oauth['token_secret'];

            // Forming auth_signature
            $this->_oauth['signature'] = base64_encode(hash_hmac("sha1", $oauth_base_text, $key, true));

            // Forming GET request
            $url = self::URL_ACCESS_TOKEN;
            $url .= '?oauth_nonce=' . $this->_oauth['nonce'];
            $url .= '&oauth_signature_method=HMAC-SHA1';
            $url .= '&oauth_timestamp=' . $this->_oauth['timestamp'];
            $url .= '&oauth_consumer_key=' . $this->_consumer_key;
            $url .= '&oauth_token=' . urlencode($this->_oauth['token']);
            $url .= '&oauth_verifier=' . urlencode($this->_oauth['verifier']);
            $url .= '&oauth_signature=' . urlencode($this->_oauth['signature']);
            $url .= '&oauth_version=1.0';

            $response = file_get_contents($url);

            // Parse the response string
            parse_str($response, $result);

            // Get user id from twitter
            $this->_user_id = $result['user_id'];
          }

  /**
       * The fourth step. Getting the user's data
       *
       */
  public function user_data($format = 'json') {
            switch ($format) {
              case 'xml':
                      case 'json':
                        $url = self::URL_ACCOUNT_DATA . '.' . $format;
                        break;

      default:
                        return false;
        break;
    }

    $url .= '?user_id=' . $this->_user_id;
    $response = file_get_contents($url);
    // Return result
    return $response;
  }

  /**
       * Get oauth_nonce and oauth_timestamp
       *
       */
  private function _init_oauth() {
            // Формируем oauth_nonce
            $this->_oauth['nonce'] = md5(uniqid(rand(), true));

            // Получаем текущее время в секундах
            $this->_oauth['timestamp'] = time();
          }

}