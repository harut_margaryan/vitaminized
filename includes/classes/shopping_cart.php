<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/

class shoppingCart {
    var $contents, $total, $weight, $cartID, $content_type;

    function shoppingCart() {
        $this->reset();
    }

    function restore_contents() {
        global $customer_id;

        if (!tep_session_is_registered('customer_id')) return false;

// insert current cart contents in database
        if (is_array($this->contents)) {
            reset($this->contents);
            while (list($products_id, ) = each($this->contents)) {
                $qty = $this->contents[$products_id]['qty'];
                $product_query = tep_db_query("select products_id from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
                if (!tep_db_num_rows($product_query)) {
                    tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET . " (customers_id, products_id, customers_basket_quantity, customers_basket_date_added) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id) . "', '" . tep_db_input($qty) . "', '" . date('Ymd') . "')");
                    if (isset($this->contents[$products_id]['attributes'])) {
                        reset($this->contents[$products_id]['attributes']);
                        while (list($option, $value) = each($this->contents[$products_id]['attributes'])) {
                            tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " (customers_id, products_id, products_options_id, products_options_value_id) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id) . "', '" . (int)$option . "', '" . (int)$value . "')");
                        }
                    }
                } else {
                    tep_db_query("update " . TABLE_CUSTOMERS_BASKET . " set customers_basket_quantity = '" . tep_db_input($qty) . "' where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
                }
            }
        }

// reset per-session cart contents, but not the database contents
        $this->reset(false);

        $products_query = tep_db_query("select products_id, customers_basket_quantity from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "'");
        while ($products = tep_db_fetch_array($products_query)) {
            $this->contents[$products['products_id']] = array('qty' => $products['customers_basket_quantity']);
// attributes
            $attributes_query = tep_db_query("select products_options_id, products_options_value_id from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products['products_id']) . "'");
            while ($attributes = tep_db_fetch_array($attributes_query)) {
                $this->contents[$products['products_id']]['attributes'][$attributes['products_options_id']] = $attributes['products_options_value_id'];
            }
        }

        $this->cleanup();

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
        $this->cartID = $this->generate_cart_id();
    }

    function reset($reset_database = false) {
        global $customer_id;

        $this->contents = array();
        $this->total = 0;
        $this->weight = 0;
        $this->content_type = false;

        if (tep_session_is_registered('customer_id') && ($reset_database == true)) {
            tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "'");
            tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "'");
        }

        unset($this->cartID);
        if (tep_session_is_registered('cartID')) tep_session_unregister('cartID');
    }

    function add_cart($products_id, $qty = '1', $attributes = '', $notify = true) {
        global $new_products_id_in_cart, $customer_id;

        $products_id_string = tep_get_uprid($products_id, $attributes);
        $products_id = tep_get_prid($products_id_string);

        if (defined('MAX_QTY_IN_CART') && (MAX_QTY_IN_CART > 0) && ((int)$qty > MAX_QTY_IN_CART)) {
            $qty = MAX_QTY_IN_CART;
        }

        $attributes_pass_check = true;

        /*if (is_array($attributes) && !empty($attributes)) {
          reset($attributes);
          while (list($option, $value) = each($attributes)) {
            if (!is_numeric($option) || !is_numeric($value)) {
              $attributes_pass_check = false;
              break;
            } else {
              $check_query = tep_db_query("select products_attributes_id from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$products_id . "' and options_id = '" . (int)$option . "' and options_values_id = '" . (int)$value . "' limit 1");
              if (tep_db_num_rows($check_query) < 1) {
                $attributes_pass_check = false;
                break;
              }
            }
          }
        } elseif (tep_has_product_attributes($products_id)) {
          $attributes_pass_check = false;
        }*/

        if (is_numeric($products_id) && is_numeric($qty) && ($attributes_pass_check == true)) {
            $check_product_query = tep_db_query("select products_status from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
            $check_product = tep_db_fetch_array($check_product_query);

            if (($check_product !== false) && ($check_product['products_status'] == '1')) {
                if ($notify == true) {
                    $new_products_id_in_cart = $products_id;
                    tep_session_register('new_products_id_in_cart');
                }

                if ($this->in_cart($products_id_string)) {
                    $this->update_quantity($products_id_string, $qty, $attributes);
                } else {
                    $this->contents[$products_id_string] = array('qty' => (int)$qty);
// insert into database
                    if (tep_session_is_registered('customer_id')) tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET . " (customers_id, products_id, customers_basket_quantity, customers_basket_date_added) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id_string) . "', '" . (int)$qty . "', '" . date('Ymd') . "')");

                    if (is_array($attributes)) {
                        reset($attributes);
                        while (list($option, $value) = each($attributes)) {
                            $this->contents[$products_id_string]['attributes'][$option] = $value;
// insert into database
                            if (tep_session_is_registered('customer_id'))
                                tep_db_query("insert into " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " (customers_id, products_id, products_options_id, products_options_value_id) values ('" . (int)$customer_id . "', '" . tep_db_input($products_id_string) . "', '" . (int)$option . "', '" . (int)$value . "')");
                        }
                    }
                }

                $this->cleanup();

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
                $this->cartID = $this->generate_cart_id();
            }
        }
    }

    function update_quantity($products_id, $quantity = '', $attributes = '') {
        global $customer_id;

        $products_id_string = tep_get_uprid($products_id, $attributes);
        $products_id = tep_get_prid($products_id_string);

        if (defined('MAX_QTY_IN_CART') && (MAX_QTY_IN_CART > 0) && ((int)$quantity > MAX_QTY_IN_CART)) {
            $quantity = MAX_QTY_IN_CART;
        }

        $attributes_pass_check = true;

        if (is_array($attributes)) {
            reset($attributes);
            while (list($option, $value) = each($attributes)) {
                if (!is_numeric($option) || !is_numeric($value)) {
                    $attributes_pass_check = false;
                    break;
                }
            }
        }

        if (is_numeric($products_id) && isset($this->contents[$products_id_string]) && is_numeric($quantity) && ($attributes_pass_check == true)) {
            $this->contents[$products_id_string] = array('qty' => (int)$quantity);
// update database
            if (tep_session_is_registered('customer_id')) tep_db_query("update " . TABLE_CUSTOMERS_BASKET . " set customers_basket_quantity = '" . (int)$quantity . "' where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id_string) . "'");

            if (is_array($attributes)) {
                reset($attributes);
                while (list($option, $value) = each($attributes)) {
                    $this->contents[$products_id_string]['attributes'][$option] = $value;
// update database
                    if (tep_session_is_registered('customer_id')) tep_db_query("update " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " set products_options_value_id = '" . (int)$value . "' where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id_string) . "' and products_options_id = '" . (int)$option . "'");
                }
            }

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
            $this->cartID = $this->generate_cart_id();
        }
    }

    function cleanup() {
        global $customer_id;

        reset($this->contents);
        while (list($key,) = each($this->contents)) {
            if ($this->contents[$key]['qty'] < 1) {
                unset($this->contents[$key]);
// remove from database
                if (tep_session_is_registered('customer_id')) {
                    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($key) . "'");
                    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($key) . "'");
                }
            }
        }
    }

    function count_contents() {  // get total number of items in cart
        $total_items = 0;
        if (is_array($this->contents)) {
            reset($this->contents);
            while (list($products_id, ) = each($this->contents)) {
                $total_items += $this->get_quantity($products_id);
            }
        }

        return $total_items;
    }

    function get_quantity($products_id) {
        if (isset($this->contents[$products_id])) {
            return $this->contents[$products_id]['qty'];
        } else {
            return 0;
        }
    }

    function in_cart($products_id) {
        if (isset($this->contents[$products_id])) {
            return true;
        } else {
            return false;
        }
    }

    function remove($products_id) {
        global $customer_id;

        unset($this->contents[$products_id]);
// remove from database
        if (tep_session_is_registered('customer_id')) {
            tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
            tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "' and products_id = '" . tep_db_input($products_id) . "'");
        }

// assign a temporary unique ID to the order contents to prevent hack attempts during the checkout procedure
        $this->cartID = $this->generate_cart_id();
    }

    function remove_all() {
        $this->reset();
    }

    function get_product_id_list() {
        $product_id_list = '';
        if (is_array($this->contents)) {
            reset($this->contents);
            while (list($products_id, ) = each($this->contents)) {
                $product_id_list .= ', ' . $products_id;
            }
        }

        return substr($product_id_list, 2);
    }

    function calculate() {
        global $currencies;

        $this->total = 0;
        $this->weight = 0;
        if (!is_array($this->contents)) return 0;

        reset($this->contents);
        while (list($products_id, ) = each($this->contents)) {
            $qty = $this->contents[$products_id]['qty'];

// products price
            $product_query = tep_db_query("select products_id, products_price, products_tax_class_id, products_weight from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
            if ($product = tep_db_fetch_array($product_query)) {
                $prid = $product['products_id'];
                $products_tax = tep_get_tax_rate($product['products_tax_class_id']);
                $products_price = $product['products_price'];
                $products_weight = $product['products_weight'];

                $specials_query = tep_db_query("select specials_new_products_price from " . TABLE_SPECIALS . " where products_id = '" . (int)$prid . "' and status = '1'");
                if (tep_db_num_rows ($specials_query)) {
                    $specials = tep_db_fetch_array($specials_query);
                    $products_price = $specials['specials_new_products_price'];
                }

                $this->total += $currencies->calculate_price($products_price, $products_tax, $qty);
                $this->weight += ($qty * $products_weight);
            }

// attributes price
            /*if (isset($this->contents[$products_id]['attributes'])) {
                reset($this->contents[$products_id]['attributes']);
                while (list($option, $value) = each($this->contents[$products_id]['attributes'])) {
                    $attribute_price_query = tep_db_query("select options_values_price, price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$prid . "' and options_id = '" . (int)$option . "' and options_values_id = '" . (int)$value . "'");
                    $attribute_price = tep_db_fetch_array($attribute_price_query);
                    if ($attribute_price['price_prefix'] == '+') {
                        $this->total += $currencies->calculate_price($attribute_price['options_values_price'], $products_tax, $qty);
                    } else {
                        $this->total -= $currencies->calculate_price($attribute_price['options_values_price'], $products_tax, $qty);
                    }
                }
            }*/
        }
    }

    function attributes_price($products_id) {
        $attributes_price = 0;

        if (isset($this->contents[$products_id]['attributes'])) {
            reset($this->contents[$products_id]['attributes']);
            while (list($option, $value) = each($this->contents[$products_id]['attributes'])) {
                $attribute_price_query = tep_db_query("select options_values_price, price_prefix from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$products_id . "' and options_id = '" . (int)$option . "' and options_values_id = '" . (int)$value . "'");
                $attribute_price = tep_db_fetch_array($attribute_price_query);
                if ($attribute_price['price_prefix'] == '+') {
                    $attributes_price += $attribute_price['options_values_price'];
                } else {
                    $attributes_price -= $attribute_price['options_values_price'];
                }
            }
        }

        return $attributes_price;
    }

    function get_products() {
        global $languages_id, $currencies;

        if (!is_array($this->contents) || sizeof($this->contents) == 0) return false;
        $this->total = 0;
        $this->weight = 0;

        $products_array = array();
        reset($this->contents);
        $products_query_str = "select p.products_id, pd.products_name,p.products_quantity as products_stock, p.products_model, p.products_image, p.products_price, p.products_weight, p.products_tax_class_id, s.specials_new_products_price from " . TABLE_PRODUCTS . " p join " . TABLE_PRODUCTS_DESCRIPTION . " pd on(p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and (";
        while (list($products_id, ) = each($this->contents)) {
            $products_query_str .= "p.products_id = '" . (int)$products_id . "' or ";
        }
        $products_query_str = substr($products_query_str, 0, -4) . '))  left join ' . TABLE_SPECIALS . ' s on(p.products_id = s.products_id and s.status = "1")';
        $products_query = tep_db_query($products_query_str);
        while ($products = tep_db_fetch_array($products_query)) {
            $prid = $products['products_id'];
            $products_price = $products['products_price'];

            if (tep_not_null($products['specials_new_products_price'])) {
                $products_price = $products['specials_new_products_price'];
            }

            $this->total += $currencies->calculate_price($products_price, tep_get_tax_rate($products['products_tax_class_id']), $this->contents[$prid]['qty']);
            $this->weight += ($this->contents[$prid]['qty'] * $products['products_weight']);

            $products_array[] = array('id' => $prid,
                'name' => $products['products_name'],
                'model' => $products['products_model'],
                'stock' => $products['products_stock'],
                'image' => $products['products_image'],
                'price' => $products_price,
                'quantity' => $this->contents[$prid]['qty'],
                'weight' => $products['products_weight'],
                'final_price' => ($products_price + $this->attributes_price($prid)),
                'tax_class_id' => $products['products_tax_class_id'],
                'link' => tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($products['products_name'])),
                'attributes' => (isset($this->contents[$prid]['attributes']) ? $this->contents[$prid]['attributes'] : ''));



        }

        return $products_array;
    }

    function show_total() {
        //$this->calculate();

        return $this->total;
    }

    function show_weight() {
        //$this->calculate();

        return $this->weight;
    }

    function generate_cart_id($length = 5) {
        return tep_create_random_value($length, 'digits');
    }

    function get_content_type() {
        $this->content_type = false;

        if ( (DOWNLOAD_ENABLED == 'true') && ($this->count_contents() > 0) ) {
            reset($this->contents);
            while (list($products_id, ) = each($this->contents)) {
                if (isset($this->contents[$products_id]['attributes'])) {
                    reset($this->contents[$products_id]['attributes']);
                    while (list(, $value) = each($this->contents[$products_id]['attributes'])) {
                        $virtual_check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_ATTRIBUTES . " pa left join " . TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD . " pad using(products_attributes_id) where pa.products_id = '" . (int)$products_id . "' and pa.options_values_id = '" . (int)$value . "'");
                        $virtual_check = tep_db_fetch_array($virtual_check_query);

                        if ($virtual_check['total'] > 0) {
                            switch ($this->content_type) {
                                case 'physical':
                                    $this->content_type = 'mixed';

                                    return $this->content_type;
                                    break;
                                default:
                                    $this->content_type = 'virtual';
                                    break;
                            }
                        } else {
                            switch ($this->content_type) {
                                case 'virtual':
                                    $this->content_type = 'mixed';

                                    return $this->content_type;
                                    break;
                                default:
                                    $this->content_type = 'physical';
                                    break;
                            }
                        }
                    }
                } else {
                    switch ($this->content_type) {
                        case 'virtual':
                            $this->content_type = 'mixed';

                            return $this->content_type;
                            break;
                        default:
                            $this->content_type = 'physical';
                            break;
                    }
                }
            }
        } else {
            $this->content_type = 'physical';
        }

        return $this->content_type;
    }

    function unserialize($broken) {
        for(reset($broken);$kv=each($broken);) {
            $key=$kv['key'];
            if (gettype($this->$key)!="user function")
                $this->$key=$kv['value'];
        }
    }

}
?>
