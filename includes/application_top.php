<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

$num_queries = 0;
$time = explode(' ', microtime());
$start = $time[1] + $time[0];
$with_partners_key = true;

error_reporting(E_ALL);
ini_set("display_errors", 1);


// start the timer for the page parse time log
define('PAGE_PARSE_START_TIME', microtime());
define('MYSQL_FORMAT_DATETIME', 'Y-m-d H:i:s');
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");

// set the level of error reporting
//  error_reporting(E_ALL & ~E_NOTICE);

// check support for register_globals
/*if (function_exists('ini_get') && (ini_get('register_globals') == false) && (PHP_VERSION < 4.3) ) {
    exit('Server Requirement Error: register_globals is disabled in your PHP configuration. This can be enabled in your php.ini configuration file or in the .htaccess file in your catalog directory. Please use PHP 4.3+ if register_globals cannot be enabled on the server.');
}*/

// load server configuration parameters
if (file_exists('includes/local/configure.php')) { // for developers
    include('includes/local/configure.php');
} else {
    include('includes/configure.php');
}

$PARTNER = $_SERVER['HTTP_HOST'] == HTTP_HOST_PARTNER ? true : false;

if (strlen(DB_SERVER) < 1) {
    if (is_dir('install')) {
        header('Location: install/index.php');
    }
}

// define the project version --- obsolete, now retrieved with tep_get_version()
define('PROJECT_VERSION', 'osCommerce Online Merchant v2.3');

// some code to solve compatibility issues
require(DIR_WS_FUNCTIONS . 'compatibility.php');

// set the type of request (secure or not)
$request_type = (getenv('HTTPS') == 'on') ? 'SSL' : 'NONSSL';

// set php_self in the local scope
$PHP_SELF = '';
if(isset($_GET['main'])) {
	if(intval ($_GET['main']) > 0) {
		$_GET['p'] = $_GET['main'];
		unset($_GET['main']);
	}
	else {
		$PHP_SELF = $_GET['main'];
		if(isset($_GET['item'])){
			if(intval ($_GET['item']) > 0) {
				$_GET['p'] = $_GET['item'];
				unset($_GET['item']);
			}
			else {
				$PHP_SELF .= '/' . $_GET['item'];
			}
		}
	}
}
//$PHP_SELF = (((strlen(ini_get('cgi.fix_pathinfo')) > 0) && ((bool)ini_get('cgi.fix_pathinfo') == false)) || !isset($_SERVER['SCRIPT_NAME'])) ? basename($_SERVER['PHP_SELF']) : basename($_SERVER['SCRIPT_NAME']);

if ($request_type == 'NONSSL') {
    define('DIR_WS_CATALOG', DIR_WS_HTTP_CATALOG);
} else {
    define('DIR_WS_CATALOG', DIR_WS_HTTPS_CATALOG);
}

$SOC_TYPES_ROWS = array(
    'partner' => array(
    0 => false,
    1 => 'partners_soc_id_fb',
    2 => 'partners_soc_id_g',
    3 => 'partners_soc_id_tw',
    4 => 'partners_soc_id_vk'
    ),
    'customer' => array(
        0 => false,
        1 => 'customers_soc_id_fb',
        2 => 'customers_soc_id_g',
        3 => 'customers_soc_id_tw',
        4 => 'customers_soc_id_vk'
    )
);

// include the database functions
require(DIR_WS_FUNCTIONS . 'database.php');

// make a connection to the database... now
tep_db_connect() or die('Unable to connect to database server!');

// set the application parameters
$configuration_query = tep_db_query('select configuration_key as cfgKey, configuration_value as cfgValue from ' . TABLE_CONFIGURATION);
while ($configuration = tep_db_fetch_array($configuration_query)) {
    define($configuration['cfgKey'], $configuration['cfgValue']);
}

// if gzip_compression is enabled, start to buffer the output
if ( (GZIP_COMPRESSION == 'true') && ($ext_zlib_loaded = extension_loaded('zlib')) && !headers_sent() ) {
    if (($ini_zlib_output_compression = (int)ini_get('zlib.output_compression')) < 1) {
        if (PHP_VERSION < '5.4' || PHP_VERSION > '5.4.5') { // see PHP bug 55544
            if (PHP_VERSION >= '4.0.4') {
                ob_start('ob_gzhandler');
            } elseif (PHP_VERSION >= '4.0.1') {
                include(DIR_WS_FUNCTIONS . 'gzip_compression.php');
                ob_start();
                ob_implicit_flush();
            }
        }
    } elseif (function_exists('ini_set')) {
        ini_set('zlib.output_compression_level', GZIP_LEVEL);
    }
}

// set the HTTP GET parameters manually if search_engine_friendly_urls is enabled
if (SEARCH_ENGINE_FRIENDLY_URLS == 'true') {
    if (strlen(getenv('PATH_INFO')) > 1) {
        $GET_array = array();
        $PHP_SELF = str_replace(getenv('PATH_INFO'), '', $PHP_SELF);
        $vars = explode('/', substr(getenv('PATH_INFO'), 1));
        do_magic_quotes_gpc($vars);
        for ($i=0, $n=sizeof($vars); $i<$n; $i++) {
            if (strpos($vars[$i], '[]')) {
                $GET_array[substr($vars[$i], 0, -2)][] = $vars[$i+1];
            } else {
                $_GET[$vars[$i]] = $vars[$i+1];
            }
            $i++;
        }

        if (sizeof($GET_array) > 0) {
            while (list($key, $value) = each($GET_array)) {
                $_GET[$key] = $value;
            }
        }
    }
}

// define general functions used application-wide
require(DIR_WS_FUNCTIONS . 'general.php');
require(DIR_WS_FUNCTIONS . 'html_output.php');
require(DIR_WS_FUNCTIONS . 'html_output_our.php');

// set the cookie domain
$cookie_domain = (($request_type == 'NONSSL') ? HTTP_COOKIE_DOMAIN : HTTPS_COOKIE_DOMAIN);
$cookie_path = (($request_type == 'NONSSL') ? HTTP_COOKIE_PATH : HTTPS_COOKIE_PATH);

if($_SERVER['SERVER_NAME'] == 'localhost') {
    $cookie_domain = null;
}

// include cache functions if enabled
if (USE_CACHE == 'true') include(DIR_WS_FUNCTIONS . 'cache.php');

// include shopping cart class
require(DIR_WS_CLASSES . 'shopping_cart.php');

// include navigation history class
require(DIR_WS_CLASSES . 'navigation_history.php');

// define how the session functions will be used
require(DIR_WS_FUNCTIONS . 'sessions.php');

// set the session name and save path
//tep_session_name('osCsid');
tep_session_save_path(SESSION_WRITE_DIRECTORY);

// set the session cookie parameters
if (function_exists('session_set_cookie_params')) {
    session_set_cookie_params(0, $cookie_path, $cookie_domain);
} elseif (function_exists('ini_set')) {
    ini_set('session.cookie_lifetime', '0');
    ini_set('session.cookie_path', $cookie_path);
    ini_set('session.cookie_domain', $cookie_domain);
}

ini_set('session.use_only_cookies', (SESSION_FORCE_COOKIE_USE == 'True') ? 1 : 0);

// set the session ID if it exists
if (isset($_POST[tep_session_name()])) {
    tep_session_id($_POST[tep_session_name()]);
} elseif ( ($request_type == 'SSL') && isset($_GET[tep_session_name()]) ) {
    tep_session_id($_GET[tep_session_name()]);
}
// start the session
$session_started = false;
if (SESSION_FORCE_COOKIE_USE == 'True') {
    tep_setcookie('cookie_test', 'please_accept_for_session', time()+60*60*24*30, $cookie_path, $cookie_domain);

    if (isset($_COOKIE['cookie_test'])) {
        tep_session_start();
        $session_started = true;
    }
} elseif (SESSION_BLOCK_SPIDERS == 'True') {
    $user_agent = strtolower(getenv('HTTP_USER_AGENT'));
    $spider_flag = false;

    if (tep_not_null($user_agent)) {
        $spiders = file(DIR_WS_INCLUDES . 'spiders.txt');

        for ($i=0, $n=sizeof($spiders); $i<$n; $i++) {
            if (tep_not_null($spiders[$i])) {
                if (is_integer(strpos($user_agent, trim($spiders[$i])))) {
                    $spider_flag = true;
                    break;
                }
            }
        }
    }

    if ($spider_flag == false) {
        tep_session_start();
        $session_started = true;
    }
} else {
    tep_session_start();
    $session_started = true;
}

if ( ($session_started == true) && function_exists('ini_get') && (ini_get('register_globals') == false) ) {
    extract($_SESSION, EXTR_OVERWRITE+EXTR_REFS);
}

// initialize a session token
if (!tep_session_is_registered('sessiontoken')) {
    $sessiontoken = md5(tep_rand() . tep_rand() . tep_rand() . tep_rand());
    tep_session_register('sessiontoken');
}

// set SID once, even if empty
$SID = (defined('SID') ? SID : '');

// verify the ssl_session_id if the feature is enabled
if ( ($request_type == 'SSL') && (SESSION_CHECK_SSL_SESSION_ID == 'True') && (ENABLE_SSL == true) && ($session_started == true) ) {
    $ssl_session_id = getenv('SSL_SESSION_ID');
    if (!tep_session_is_registered('SSL_SESSION_ID')) {
        $SESSION_SSL_ID = $ssl_session_id;
        tep_session_register('SESSION_SSL_ID');
    }

    if ($SESSION_SSL_ID != $ssl_session_id) {
        tep_session_destroy();
        tep_redirect(tep_href_link(SSL_CHECK_URL));
    }
}

// verify the browser user agent if the feature is enabled
if (SESSION_CHECK_USER_AGENT == 'True') {
    $http_user_agent = getenv('HTTP_USER_AGENT');
    if (!tep_session_is_registered('SESSION_USER_AGENT')) {
        $SESSION_USER_AGENT = $http_user_agent;
        tep_session_register('SESSION_USER_AGENT');
    }

    if ($SESSION_USER_AGENT != $http_user_agent) {
        tep_session_destroy();
        tep_redirect(tep_href_link(LOGIN_URL));
    }
}

// verify the IP address if the feature is enabled
if (SESSION_CHECK_IP_ADDRESS == 'True') {
    $ip_address = tep_get_ip_address();
    if (!tep_session_is_registered('SESSION_IP_ADDRESS')) {
        $SESSION_IP_ADDRESS = $ip_address;
        tep_session_register('SESSION_IP_ADDRESS');
    }

    if ($SESSION_IP_ADDRESS != $ip_address) {
        tep_session_destroy();
        tep_redirect(tep_href_link(LOGIN_URL));
    }
}

// create the shopping cart
if (!tep_session_is_registered('cart') || !is_object($cart)) {
    tep_session_register('cart');
    $cart = new shoppingCart;
}

// include currencies class and create an instance
require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

// include the mail classes
require(DIR_WS_CLASSES . 'mime.php');
require(DIR_WS_CLASSES . 'email.php');

// set the language
//  if (/*!tep_session_is_registered('language') ||*/ isset($_GET['language'])) {
/*if (!tep_session_is_registered('language')) {
  tep_session_register('language');
  tep_session_register('languages_id');
}*/

include(DIR_WS_CLASSES . 'language.php');
$lng = new language();
$current_language = '';
if (isset($_GET['language']) && tep_not_null($_GET['language'])) {
    $lng->set_language(strtolower($_GET['language']));
} else {
    $lng->set_language(DEFAULT_LANGUAGE);
}
/*else {
     $lng->get_browser_language();
   }*/

$language = $lng->language['directory'];
$languages_id = $lng->language['id'];
//  }

// include the language translations
require(DIR_WS_LANGUAGES . $language . '.php');

// currency
if (!tep_session_is_registered('currency') || isset($_GET['currency']) || ( (USE_DEFAULT_LANGUAGE_CURRENCY == 'true') && (LANGUAGE_CURRENCY != $currency) ) ) {
    if (!tep_session_is_registered('currency')) tep_session_register('currency');

    if (isset($_GET['currency']) && $currencies->is_set($_GET['currency'])) {
        $currency = $_GET['currency'];
    } else {
        $currency = ((USE_DEFAULT_LANGUAGE_CURRENCY == 'true') && $currencies->is_set(LANGUAGE_CURRENCY)) ? LANGUAGE_CURRENCY : DEFAULT_CURRENCY;
    }
}

// navigation history
if (!tep_session_is_registered('navigation') || !is_object($navigation)) {
    tep_session_register('navigation');
    $navigation = new navigationHistory;
}
$navigation->add_current_page();

// action recorder
include('includes/classes/action_recorder.php');

// Shopping cart actions
if (isset($_GET['action'])) {
// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled
    if ($session_started == false) {
        tep_redirect(tep_href_link(COOKIE_USAGE_URL));
    }

    if (DISPLAY_CART == 'true') {
        $goto =  SHOPPING_URL;
        $parameters = array('action', 'cPath', 'products_id', 'pid');
    } else {
        $goto = $PHP_SELF;
        if ($_GET['action'] == 'buy_now') {
            $parameters = array('action', 'pid', 'products_id');
        } else {
            $parameters = array('action', 'pid');
        }
    }
    switch ($_GET['action']) {
        case 'give_products' :
            $cart_products_arr = $cart->get_products();
            die(json_encode($cart_products_arr));

        // customer wants to update the product quantity in their shopping cart
        case 'update_product' :
            for ($i=0, $n=sizeof($_POST['products_id']); $i<$n; $i++) {
                /* if (in_array($_POST['products_id'][$i], (is_array($_POST['cart_delete']) ? $_POST['cart_delete'] : array()))) {
                   $cart->remove($_POST['products_id'][$i]);
                 } else {*/
                $attributes = !empty($_POST['id'][$_POST['products_id'][$i]]) ? $_POST['id'][$_POST['products_id'][$i]] : '';
                $cart->add_cart($_POST['products_id'][$i], $_POST['cart_quantity'][$i], $attributes, false);
                // }
            }
            if (isset($_POST['ajax'])) {
                exit;
            }
            tep_redirect(tep_href_link($goto));
            break;
        // customer adds a product from the products page
        case 'add_product' :
            $ans = array('ok' => false);
            if (isset($_POST['products_id']) && is_numeric($_POST['products_id'])) {
                $attributes = isset($_POST['id']) ? $_POST['id'] : '';
                $qty = 1;
                if (!empty($_POST['products_quantity']) && (int)$_POST['products_quantity'] > 0) {
                    $qty = (int)$_POST['products_quantity'];
                }
                $cart->add_cart((int)$_POST['products_id'], $qty, $attributes);
                $ans['ok'] = true;
            }
            if (isset($_POST['ajax'])) exit(json_encode($ans));
            tep_redirect(tep_href_link($goto));
            break;
        // customer removes a product from their shopping cart
        case 'remove_product' :
            $ans = array('ok' => false);
            if (isset($_GET['products_id'])) {
                $cart->remove($_GET['products_id']);
                $ans['ok'] = true;
            }
            if (isset($_POST['ajax'])) {
                exit(json_encode($ans));
            }
            tep_redirect(tep_href_link($goto));
            break;
        // performed by the 'buy now' button in product listings and review page
        /*case 'buy_now' :
            if (isset($_GET['products_id'])) {
                if (tep_has_product_attributes($_GET['products_id'])) {
                    tep_redirect(tep_href_link(PRODUCT_INFO_URL, 'products_id=' . $_GET['products_id']));
                } else {
                    $cart->add_cart($_GET['products_id'], $cart->get_quantity($_GET['products_id'])+1);
                }
            }
            tep_redirect(tep_href_link($goto, tep_get_all_get_params($parameters)));
            break;*/
        case 'buy_now' :
            if (isset($_REQUEST['products_id'])) {
                $count = isset($_REQUEST['products_count']) ? (int)$_REQUEST['products_count'] : 1;
                //$count = $count > 0 ? $count : 1;
                $cart->add_cart($_REQUEST['products_id'], $cart->get_quantity($_REQUEST['products_id']) + $count);
            }
            tep_redirect(tep_href_link(SHOPPING_URL));
            break;
        case 'notify' :
            if (tep_session_is_registered('customer_id')) {
                if (isset($_GET['products_id'])) {
                    $notify = $_GET['products_id'];
                } elseif (isset($_GET['notify'])) {
                    $notify = $_GET['notify'];
                } elseif (isset($_POST['notify'])) {
                    $notify = $_POST['notify'];
                } else {
                    tep_redirect(tep_href_link($PHP_SELF, tep_get_all_get_params(array('action', 'notify'))));
                }
                if (!is_array($notify)) $notify = array($notify);
            for ($i=0, $n=sizeof($notify); $i<$n; $i++) {
                $check_query = tep_db_query("select count(*) as count from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . (int)$notify[$i] . "' and customers_id = '" . (int)$customer_id . "'");
                $check = tep_db_fetch_array($check_query);
                if ($check['count'] < 1) {
                    tep_db_query("insert into " . TABLE_PRODUCTS_NOTIFICATIONS . " (products_id, customers_id, date_added) values ('" . (int)$notify[$i] . "', '" . (int)$customer_id . "', now())");
                }
            }
            tep_redirect(tep_href_link($PHP_SELF, tep_get_all_get_params(array('action', 'notify'))));
        } else {
            $navigation->set_snapshot();
            tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
        }
            break;
        case 'notify_remove' :
            if (tep_session_is_registered('customer_id') && isset($_GET['products_id'])) {
                $check_query = tep_db_query("select count(*) as count from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . (int)$_GET['products_id'] . "' and customers_id = '" . (int)$customer_id . "'");
                $check = tep_db_fetch_array($check_query);
                if ($check['count'] > 0) {
                    tep_db_query("delete from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . (int)$_GET['products_id'] . "' and customers_id = '" . (int)$customer_id . "'");
                }
                tep_redirect(tep_href_link($PHP_SELF, tep_get_all_get_params(array('action'))));
        } else {
            $navigation->set_snapshot();
            tep_redirect(tep_href_link(LOGIN_URL, '', 'SSL'));
        }
            break;
        case 'cust_order' :
            if (tep_session_is_registered('customer_id') && isset($_GET['pid'])) {
                if (tep_has_product_attributes($_GET['pid'])) {
                    tep_redirect(tep_href_link(PRODUCT_INFO_URL, 'products_id=' . $_GET['pid']));
                } else {
                    $cart->add_cart($_GET['pid'], $cart->get_quantity($_GET['pid']) + 1);
                }
            }
            tep_redirect(tep_href_link($goto, tep_get_all_get_params($parameters)));
            break;
    }
}

// include the who's online functions
//require(DIR_WS_FUNCTIONS . 'whos_online.php');
//tep_update_whos_online();

// include the password crypto functions
require(DIR_WS_FUNCTIONS . 'password_funcs.php');

// include validation functions (right now only email address)
require(DIR_WS_FUNCTIONS . 'validations.php');

// split-page-results
require(DIR_WS_CLASSES . 'split_page_results.php');

// infobox
require(DIR_WS_CLASSES . 'boxes.php');

// auto activate and expire banners
//require(DIR_WS_FUNCTIONS . 'banner.php');
//tep_activate_banners();
//tep_expire_banners();

// auto expire special products
//require(DIR_WS_FUNCTIONS . 'specials.php');
//tep_expire_specials();

require(DIR_WS_CLASSES . 'osc_template.php');
$oscTemplate = new oscTemplate();

// calculate category path
/*if (isset($_GET['cPath'])) {
  $cPath = $_GET['cPath'];
} elseif (isset($_GET['products_id']) && !isset($_GET['manufacturers_id'])) {
  $cPath = tep_get_product_path($_GET['products_id']);
} else {
  $cPath = '';
}*/

// include the breadcrumb class and start the breadcrumb trail
require(DIR_WS_CLASSES . 'breadcrumb.php');
$breadcrumb = new breadcrumb;
$breadcrumb->add(HEADER_TITLE_TOP, HTTP_SERVER . '/' . $current_language);
//$breadcrumb->add(HEADER_TITLE_CATALOG, tep_href_link());

require(DIR_WS_CLASSES . 'message_stack.php');
$messageStack = new messageStack;


// GLOBALS
$IS_BOT = tep_is_bots_agent(tep_get_user_agent());
$AFFILIATE = false;

if (isset($_COOKIE['affiliate']['partners_id']) && isset($_COOKIE['affiliate']['partners_key']) && isset($_COOKIE['affiliate']['from_url'])) {
    if (($partners_id_cookie = tep_decrypt_cookie($_COOKIE['affiliate']['partners_id'])) && ($partners_key_cookie = tep_decrypt_cookie($_COOKIE['affiliate']['partners_key'])) && ($from_url_cookie = tep_decrypt_cookie($_COOKIE['affiliate']['from_url'])) !== false) {
        $AFFILIATE = array(
            'partners_id' => $partners_id_cookie,
            'partners_key' => $partners_key_cookie,
            'from_url' => $from_url_cookie
        );
    }
}

if (!$IS_BOT && isset($_GET['p']) && is_numeric($_GET['p']) && (!tep_session_is_registered('partner_key') || $partner_key != (int)$_GET['p'])) {
    $partners_key_get = (int)$_GET['p'];
    if ($AFFILIATE && $AFFILIATE['partners_key'] == $partners_key_get) {
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $new_referer = $_SERVER['HTTP_REFERER'];
            $new_referers_host = parse_url($new_referer , PHP_URL_HOST);
            if ($new_referers_host && $new_referers_host != HTTP_HOST && $new_referers_host != HTTP_HOST_PARTNER) {
                tep_setcookie('affiliate[from_url]', $new_referer, time() + 60 * 60 * 24 * 365 * 10);
                $AFFILIATE['from_url'] = $new_referer;
            }
        }
    } elseif (!$AFFILIATE || !tep_session_is_registered('visit_id')) {
        $check_partner_query = tep_db_query('select partners_id from ' . TABLE_PARTNERS . ' where partners_key = ' . $partners_key_get);
        if (tep_db_num_rows($check_partner_query) == 1) {
            !$AFFILIATE && tep_session_is_registered('visit_id') && tep_session_unregister('visit_id');
            $check_partner = tep_db_fetch_array($check_partner_query);
            $referer = empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
            tep_setcookie('affiliate[partners_id]', tep_encrypt_cookie($check_partner['partners_id']), time() + 60 * 60 * 24 * 365 * 10);
            tep_setcookie('affiliate[partners_key]', tep_encrypt_cookie($partners_key_get), time() + 60 * 60 * 24 * 365 * 10);
            tep_setcookie('affiliate[from_url]', tep_encrypt_cookie($referer), time() + 60 * 60 * 24 * 365 * 10);
            $AFFILIATE = array();
            $AFFILIATE['partners_key'] = $partners_key_get;
            $AFFILIATE['partners_id'] = $check_partner['partners_id'];
            $AFFILIATE['from_url'] = $referer;
        }
    }
}

//--------------------------------------begin-------------------------------------------

require(DIR_WS_INCLUDES . 'application_top+.php');

//--------------------------------------end-------------------------------------------

if (tep_not_null($cPath)) {
    $cPath_array = tep_parse_category_path($cPath);
    $cPath = implode('_', $cPath_array);
    $current_category_id = $cPath_array[(sizeof($cPath_array)-1)];
    $categories_url = '';
    /*for ($i=0, $n=sizeof($cPath_array); $i<$n; $i++) {
        $categories_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$cPath_array[$i] . "' and language_id = '" . (int)$languages_id . "'");
        if (tep_db_num_rows($categories_query) > 0) {
            $categories = tep_db_fetch_array($categories_query);
            $categories_url .= tep_clean_text_int($categories['categories_name']) . '/';
            $breadcrumb->add($categories['categories_name'], tep_href_link($categories_url));
        } else {
            break;
        }
    }*/
    eval($do_breadcrumb);
} else {
    $current_category_id = 0;
}