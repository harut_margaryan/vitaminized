<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

if ($messageStack->size('header') > 0) {
    echo '<div class="grid_24">' . $messageStack->output('header') . '</div>';
} ?>

    <header id="site-header">
		<?php
		$all_prods_query = tep_db_query('select p.products_id, p.shopify_id, p.products_image, pd.products_short_desc, p.products_price, pd.products_name, pd.products_description from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd using(products_id) where p.products_status = 1 and pd.language_id = ' . intval($languages_id) . ' order by rand()');
		$all_products = tep_db_fetch_all($all_prods_query);
		?>
        <!--<div class="head-row"><a href="<?/*= tep_href_link()*/?>"><?/*= tep_image('store_logo.png', STORE_NAME, '', '', 'style="width:100%;"')*/?></a></div>-->
		<div id="header-slider-container" class="my-slider">
            <ul>
			<?php foreach ($all_products as $prod_info):?>
			<li>
			    <div class="item" style="background-image: url('<?= tep_image_path(DIR_WS_IMAGES_PRODUCTS_BACKGROUNDS . $prod_info['products_id'] . '.jpg');?>')">
					<div class="head-prod">
					    <div class="capsul">
                            <a href="<?= tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($prod_info['products_name']))?>" class="product-link">
                                <?= tep_image(DIR_WS_IMAGES_PRODUCTS . $prod_info['products_id'] . '.png', $prod_info['products_short_desc'], '', '', 'class="product-header-image"')?>
                            </a>
                        </div>

						<div class="head-prod-cont">
							<a href="#" class="head-prod-name"><?= $prod_info['products_name']?><sup>&reg;</sup></a>
							<span><?= $prod_info['products_short_desc']?></span>

							<div class="head-prod-btns">
								<?/*= tep_draw_form('cart_quantity', tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($prod_info['products_name'])), 'get', 'id="form-' . $prod_info['products_id'] . '"'); */?>
								    <button class="dena-btn dena-btn-white add-to-cart" onclick="buyNowQuick(<?=$prod_info['shopify_id']?>)" style="margin-left: 17px;">
								    <!--<i class="dena-btn-icon"></i>-->BUY NOW</button>
								   <a class="show-more" href="<?= tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($prod_info['products_name']))?>">
								     <button class="dena-btn dena-btn-white add-to-cart">LEARN MORE</button>
                                 </a>
								<!--</form>-->
                            </div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
				</li>
			<?php endforeach; ?>
			</ul>
		</div>
    </header>

<div style="display: none" class="grid_24"<?= ($PARTNER ? ' style="background-color:#eee;"' : '') ?>>
<?php
$flag = false;
if ($PARTNER) { $PARTNER = false; $flag = true;} ?>
    <div id="storeLogo"><?php echo '<a href="' . tep_href_link() . '">' . tep_image('store_logo.png', STORE_NAME) . '</a>'; ?></div>
<?php $PARTNER = $flag; ?>
    <div id="headerShortcuts">
        <?php
        if (!tep_session_is_registered('partner_id') && !$PARTNER && $main != SHOPPING_URL) {?>
        <div id="shopping-cart-container">
            <span id="myCart">
            <?php
            echo tep_draw_button(HEADER_TITLE_CART_CONTENTS . ($cart->count_contents() > 0 ? ' (' . $cart->count_contents() . ')' : ''), 'cart', tep_href_link(SHOPPING_CART_URL))
                . '</span>';
            echo '<div class="cart-menu"><div id="cart-products-container"></div>';
            echo tep_session_is_registered('customer_id') ? tep_draw_button(HEADER_TITLE_CHECKOUT, 'triangle-1-e', tep_href_link(CHECKOUT_URL, '', 'SSL')) : '<a href="' .tep_href_link(LOGIN_URL) . '#signInTab" onclick="return showLoginPopup(\'signInTab\',\'' . tep_href_link(CHECKOUT_URL) . '\');">' . tep_draw_button(HEADER_TITLE_CHECKOUT, 'triangle-1-e') . '</a>';
            echo tep_draw_button('MY CART', 'cart', tep_href_link(SHOPPING_CART_URL));
            echo '</div></div>';

            }

            if (tep_session_is_registered('partner_id')) {
                echo tep_draw_button(HEADER_TITLE_LOGOFF, null, tep_href_link(LOGOFF_URL, '', 'SSL')) .
                    tep_draw_button($partner_first_name, 'person', tep_href_link(ACCOUNT_URL, '', 'SSL'));
            } elseif (tep_session_is_registered('customer_id')) {
                echo tep_draw_button(HEADER_TITLE_LOGOFF, null, tep_href_link(LOGOFF_URL, '', 'SSL')) .
                    tep_draw_button($customer_first_name, 'person', tep_href_link(ACCOUNT_URL, '', 'SSL'));
            } else {
                if (!$PARTNER && !tep_session_is_registered('partner_id')) {
                    $PARTNER = true;
                    echo '<a href="' .  tep_href_link() . '">' . tep_draw_button(HEADER_TITLE_BECOME_PARTNER, 'person') . '</a>';
                    $PARTNER = false;
                }
                if ($main != LOGIN_URL) {
                    ?>
                    <a href="<?php echo tep_href_link(LOGIN_URL) . '#signInTab'?>" onclick="return showLoginPopup('signInTab','<?php echo $og_url;?>');"><?php echo tep_draw_button(HEADER_TITLE_SIGN_IN, 'person')?></a>
                    <a href="<?php echo tep_href_link(LOGIN_URL) . '#signUpTab'?>" onclick="return showLoginPopup('signUpTab','<?php echo $og_url;?>');"><?php echo tep_draw_button(HEADER_TITLE_SIGN_UP, 'person')?></a>
                <?php
                }
            }
            ?>
        </div>
    </div>

    <div class="grid_24 ui-widget infoBoxContainer" style="display: none;margin-top: 64px; float: none;">
        <div class="ui-widget-header infoBoxHeading"><?php echo '&nbsp;&nbsp;' . $breadcrumb->trail(' &raquo; '); ?></div>
    </div>
<?php

if ($PARTNER && tep_session_is_registered('partner_id')) {
    echo '<div class="head-menu">';
    echo '<ul>';
    echo '<li><a href="' . tep_href_link(PARTNERS_ANALYTIC_URL, '', 'SSL') . '">' . TEXT_PARTNER_ANALYTIC . '</a></li>';
    echo '<li><a href="' . tep_href_link(PARTNERS_MARKETING_MATERIALS_URL) . '">' . TEXT_PARTNER_MARKETING_MATERIALS . '</a></li>';
    echo '<li><a href="' . tep_href_link(AFFILIATES_HISTORY_URL) . '">' . TEXT_AFFILIATE_HISTORY . '</a></li>';
    echo '</ul>';
    echo '</div>';
}


if (isset($_GET['error_message']) && tep_not_null($_GET['error_message'])) {
    ?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr class="headerError">
            <td class="headerError"><?php echo htmlspecialchars(stripslashes(urldecode($_GET['error_message']))); ?></td>
        </tr>
    </table>
<?php
}

if (isset($_GET['info_message']) && tep_not_null($_GET['info_message'])) {
    ?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr class="headerInfo">
            <td class="headerInfo"><?php echo htmlspecialchars(stripslashes(urldecode($_GET['info_message']))); ?></td>
        </tr>
    </table>
<?php
}
