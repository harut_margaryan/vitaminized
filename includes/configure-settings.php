<?php

// define the urls used in the project
define('ACCOUNT_URL', 'account');
define('ACCOUNT_EDIT_URL', 'account-edit');
define('ACCOUNT_HISTORY_URL', 'account-history');
define('ACCOUNT_HISTORY_INFO_URL', 'account-history-info');
define('ACCOUNT_NEWSLETTERS_URL', 'account-newsletters');
define('ACCOUNT_NOTIFICATIONS_URL', 'account-notifications');
define('ACCOUNT_PASSWORD_URL', 'account-password');
define('ADDRESS_BOOK_URL', 'address-book');
define('ADDRESS_BOOK_PROCESS_URL', 'address-book-process');
define('ADVANCED_SEARCH_RESULT_URL', 'advanced-search-result');
define('AFFILIATES_HISTORY_URL', 'history');
define('ALSO_PURCHASED_PRODUCTS_URL', 'also-purchased-products');
define('CHECKOUT_URL', 'checkout');
define('CHECKOUT_CONFIRMATION_URL', 'checkout-confirmation');
define('CHECKOUT_INFO_EDIT_URL', 'checkout-info-edit');
define('CHECKOUT_PAYMENT_URL', 'checkout-payment');
define('CHECKOUT_PAYMENT_ADDRESS_URL', 'checkout-payment-address');
define('CHECKOUT_PROCESS_URL', 'checkout-process');
define('CHECKOUT_SHIPPING_URL', 'checkout-shipping');
define('CHECKOUT_SHIPPING_ADDRESS_URL', 'checkout-shipping-address');
define('CHECKOUT_SUCCESS_URL', 'checkout-success');
define('CONTACT_US_URL', 'contact-us');
define('CONDITIONS_URL', 'conditions');
define('COOKIE_USAGE_URL', 'cookie-usage');
define('CREATE_ACCOUNT_URL', 'create-account');
define('CREATE_ACCOUNT_SUCCESS_URL', 'create-account-success');
define('DOWNLOAD_URL', 'download');
define('FACEBOOK_LOGIN_URL', 'facebook_login');
define('GOOGLE_SIGN_IN_URL', 'google-sign-in');
define('INFO_SHOPPING_CART_URL', 'info-shopping-cart');
define('LOGIN_URL', 'login');
define('LOGOFF_URL', 'logoff');
define('MANUFACTURERS_URL', 'manufacturers');
define('PARTNERS_ANALYTIC_URL', 'analytic');
    define('METHODS_ANALYTIC_URL', 'methods');
        define('METHOD_ANALYTIC_URL', 'method');
    define('PRODUCTS_ANALYTIC_URL', 'products');
define('PARTNERS_MARKETING_URL', 'marketing');
define('PARTNERS_MARKETING_DOWNLOADS_URL', 'marketing-downloads');
define('PARTNERS_MARKETING_MATERIALS_URL', 'marketing-materials');
define('PASSWORD_FORGOTTEN_URL', 'password-forgotten');
define('PASSWORD_RESET_URL', 'password-reset');
define('PAYPAL_EXPRESS_URL', 'paypal-express');
define('POPUP_IMAGE_URL', 'popup-image');
define('POPUP_SEARCH_HELP_URL', 'popup-search-help');
define('PRODUCT_INFO_URL', 'product-info');
define('PRODUCT_LISTING_URL', 'product-listing');
define('PRODUCT_REVIEWS_URL', 'reviews');
define('PRODUCT_URL', 'product');
//define('PRODUCTS_NEW_URL', 'products-new');
define('REDIRECT_URL', 'redirect');
define('REVIEWS_URL', 'reviews');
define('SIGNIN_URL', 'sign-in');
define('SIGNUP_URL', 'sign-up');
define('SHIPPING_URL', 'shipping');
define('SHOPPING_CART_URL', 'shopping-cart');
define('SHOPPING_URL', 'shopping');
define('SPECIALS_URL', 'specials');
define('SSL_CHECK_URL', 'ssl-check');
define('TELL_A_FRIEND_URL', 'tell-a-friend');
define('TWITTER_LOGIN_URL', 'twitter-login');
define('UPCOMING_PRODUCTS_URL', 'upcoming-products');

define('CART_URL', 'cart');
define('DASHBOARD_URL', 'dashboard');



// define the filenames used in the project
define('FILENAME_ACCOUNT', 'account.php');
define('FILENAME_ACCOUNT_EDIT', 'account_edit.php');
define('FILENAME_ACCOUNT_HISTORY', 'account_history.php');
define('FILENAME_ACCOUNT_HISTORY_INFO', 'account_history_info.php');
define('FILENAME_ACCOUNT_NEWSLETTERS', 'account_newsletters.php');
define('FILENAME_ACCOUNT_NOTIFICATIONS', 'account_notifications.php');
define('FILENAME_ACCOUNT_PASSWORD', 'account_password.php');
define('FILENAME_ADDRESS_BOOK', 'address_book.php');
define('FILENAME_ADDRESS_BOOK_PROCESS', 'address_book_process.php');
define('FILENAME_ADVANCED_SEARCH_RESULT', 'advanced_search_result.php');
define('FILENAME_AFFILIATES_HISTORY', 'affiliates_history.php');
define('FILENAME_ALSO_PURCHASED_PRODUCTS', 'also_purchased_products.php');
define('FILENAME_CHECKOUT', 'checkout.php');
define('FILENAME_CHECKOUT_CONFIRMATION', 'checkout_confirmation.php');
define('FILENAME_CHECKOUT_INFO_EDIT', 'checkout_info_edit.php');
define('FILENAME_CHECKOUT_PAYMENT', 'checkout_payment.php');
define('FILENAME_CHECKOUT_PAYMENT_ADDRESS', 'checkout_payment_address.php');
define('FILENAME_CHECKOUT_PROCESS', 'checkout_process.php');
define('FILENAME_CHECKOUT_SHIPPING', 'checkout_shipping.php');
define('FILENAME_CHECKOUT_SHIPPING_ADDRESS', 'checkout_shipping_address.php');
define('FILENAME_CHECKOUT_SUCCESS', 'checkout_success.php');
define('FILENAME_CONTACT_US', 'contact_us.php');
define('FILENAME_CONDITIONS', 'conditions.php');
define('FILENAME_COOKIE_USAGE', 'cookie_usage.php');
define('FILENAME_CREATE_ACCOUNT', 'create_account.php');
define('FILENAME_CREATE_ACCOUNT_SUCCESS', 'create_account_success.php');
define('FILENAME_DEFAULT', 'index.php');
define('FILENAME_DOWNLOAD', 'download.php');
define('FILENAME_GET_ANALYTICS', 'get_analytics.php');
define('FILENAME_INFO_SHOPPING_CART', 'info_shopping_cart.php');
define('FILENAME_LOGIN', 'login.php');
define('FILENAME_LOGOFF', 'logoff.php');
define('FILENAME_MM_GETTER', 'mm_getter.php');
define('FILENAME_NEW_PRODUCTS', 'new_products.php');
define('FILENAME_PARTNERS_ANALYTIC', 'partners_analytic.php');
define('FILENAME_HIGHCHARTS', 'highcharts.php');
define('FILENAME_PAGES', 'pages.php');
define('FILENAME_PARTNERS_MARKETING', 'partners_marketing.php');
define('FILENAME_PARTNERS_MARKETING_DOWNLOADS', 'partners_marketing_downloads.php');
define('FILENAME_PARTNERS_MARKETING_MATERIALS', 'partners_marketing_materials.php');
define('FILENAME_PASSWORD_FORGOTTEN', 'password_forgotten.php');
define('FILENAME_PASSWORD_RESET', 'password_reset.php');
define('FILENAME_PAYPAL_EXPRESS', 'ext/modules/payment/paypal/express.php');
define('FILENAME_POPUP_IMAGE', 'popup_image.php');
define('FILENAME_PRIVACY', 'privacy.php');
define('FILENAME_PRODUCT_INFO', 'product_info.php');
define('FILENAME_PRODUCT_LISTING', 'product_listing.php');
define('FILENAME_PRODUCT_REVIEWS', 'product_reviews.php');
define('FILENAME_PRODUCT_REVIEWS_WRITE', 'product_reviews_write.php');
define('FILENAME_PRODUCTS_NEW', 'products_new.php');
define('FILENAME_REDIRECT', 'redirect.php');
define('FILENAME_REVIEWS', 'reviews.php');
define('FILENAME_SHIPPING', 'shipping.php');
define('FILENAME_SHOPPING', 'shopping.php');
define('FILENAME_SHOPPING_CART', 'shopping_cart.php');
define('FILENAME_SHOPPING_CHECKOUT_PROCESS', 'shopping_checkout_process.php');
define('FILENAME_SPECIALS', 'specials.php');
define('FILENAME_SSL_CHECK', 'ssl_check.php');
define('FILENAME_TELL_A_FRIEND', 'tell_a_friend.php');
define('FILENAME_UPCOMING_PRODUCTS', 'upcoming_products.php');

// define the database table names used in the project
define('TABLE_ACTION_RECORDER', 'action_recorder');
define('TABLE_ADDRESS_BOOK', 'address_book');
define('TABLE_ADDRESS_FORMAT', 'address_format');
define('TABLE_ADMINISTRATORS', 'administrators');
define('TABLE_BANNERS', 'banners');
define('TABLE_BANNERS_HISTORY', 'banners_history');
define('TABLE_CATEGORIES', 'categories');
define('TABLE_CATEGORIES_DESCRIPTION', 'categories_description');
define('TABLE_CONFIGURATION', 'configuration');
define('TABLE_CONFIGURATION_GROUP', 'configuration_group');
define('TABLE_COUNTER', 'counter');
define('TABLE_COUNTER_HISTORY', 'counter_history');
define('TABLE_COUNTRIES', 'countries');
define('TABLE_CURRENCIES', 'currencies');
define('TABLE_CUSTOMERS', 'customers');
define('TABLE_CUSTOMERS_TO_PARTNERS', 'customers_to_partners');
define('TABLE_PARTNERS', 'partners');
define('TABLE_CUSTOMERS_BASKET', 'customers_basket');
define('TABLE_CUSTOMERS_BASKET_ATTRIBUTES', 'customers_basket_attributes');
define('TABLE_CUSTOMERS_INFO', 'customers_info');
define('TABLE_PARTNERS_INFO', 'partners_info');
define('TABLE_LANGUAGES', 'languages');
define('TABLE_MANUFACTURERS', 'manufacturers');
define('TABLE_MANUFACTURERS_INFO', 'manufacturers_info');
define('TABLE_MARKETING_METHODS', 'marketing_methods');
define('TABLE_MARKETING_METHODS_INFO', 'marketing_methods_info');
define('TABLE_MARKETING_POSTS', 'marketing_posts');
define('TABLE_MARKETING_POSTS_TAGS', 'marketing_posts_tags');
define('TABLE_MARKETING_BANNERS', 'marketing_banners');
define('TABLE_MARKETING_BANNERS_TAGS', 'marketing_banners_tags');
define('TABLE_MARKETING_PICS', 'marketing_pics');
define('TABLE_MARKETING_PICS_TAGS', 'marketing_pics_tags');
define('TABLE_MARKETING_MAILS', 'marketing_mails');
define('TABLE_MARKETING_MAILS_TAGS', 'marketing_mails_tags');
define('TABLE_MARKETING_SHARES', 'marketing_shares');
define('TABLE_MARKETING_SHARES_TAGS', 'marketing_shares_tags');
define('TABLE_MARKETING_STATUSES', 'marketing_statuses');
define('TABLE_MARKETING_STATUSES_TAGS', 'marketing_statuses_tags');
define('TABLE_MARKETING_TAGS', 'marketing_tags');
define('TABLE_MARKETING_TAGS_INFO', 'marketing_tags_info');
define('TABLE_ORDERS', 'orders');
define('TABLE_ORDERS_PRODUCTS', 'orders_products');
define('TABLE_ORDERS_PRODUCTS_ATTRIBUTES', 'orders_products_attributes');
define('TABLE_ORDERS_PRODUCTS_DOWNLOAD', 'orders_products_download');
define('TABLE_ORDERS_STATUS', 'orders_status');
define('TABLE_ORDERS_STATUS_HISTORY', 'orders_status_history');
define('TABLE_ORDERS_TOTAL', 'orders_total');
define('TABLE_PARTNERS_HISTORY', 'partners_history');
define('TABLE_PAGES', 'pages');
define('TABLE_PAGES_DESCRIPTION', 'pages_description');
define('TABLE_PRODUCTS', 'products');
define('TABLE_PRODUCTS_ATTRIBUTES', 'products_attributes');
define('TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD', 'products_attributes_download');
define('TABLE_PRODUCTS_DESCRIPTION', 'products_description');
define('TABLE_PRODUCTS_IMAGES', 'products_images');
define('TABLE_PRODUCTS_INFO', 'products_info');
define('TABLE_PRODUCTS_INFO_DESCRIPTION', 'products_info_description');
define('TABLE_PRODUCTS_NOTIFICATIONS', 'products_notifications');
define('TABLE_PRODUCTS_OPTIONS', 'products_options');
define('TABLE_PRODUCTS_OPTIONS_VALUES', 'products_options_values');
define('TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS', 'products_options_values_to_products_options');
define('TABLE_PRODUCTS_TO_CATEGORIES', 'products_to_categories');
define('TABLE_PRODUCTS_TO_PRODUCTS_INFO', 'products_to_products_info');
define('TABLE_REVIEWS', 'reviews');
define('TABLE_SESSIONS', 'sessions');
define('TABLE_SPECIALS', 'specials');
define('TABLE_TABS_INFO', 'tabs_info');
define('TABLE_TABS_INFO_DESC', 'tabs_info_desc');
define('TABLE_TAX_RATES', 'tax_rates');
define('TABLE_GEO_ZONES', 'geo_zones');
define('TABLE_ZONES_TO_GEO_ZONES', 'zones_to_geo_zones');
define('TABLE_VISITS', 'visits');
define('TABLE_WHOS_ONLINE', 'whos_online');
define('TABLE_ZONES', 'zones');

//---
define('HTTP_SERVER', 'http://' . DOMAIN_NAME);
define('HTTP_SERVER_PARTNER', 'http://partner.' . DOMAIN_NAME);
define('HTTPS_SERVER', 'http://' . DOMAIN_NAME);
define('HTTPS_SERVER_PARTNER', 'http://partner.' . DOMAIN_NAME);
define('HTTP_HOST_PARTNER', 'partner.' . DOMAIN_NAME);
define('HTTP_HOST', DOMAIN_NAME);
define('ENABLE_SSL', true);
define('HTTP_COOKIE_DOMAIN', '.' . DOMAIN_NAME);
define('HTTPS_COOKIE_DOMAIN', '.' . DOMAIN_NAME);
define('HTTP_COOKIE_PATH', '/');
define('HTTPS_COOKIE_PATH', '/');
define('DIR_WS_HTTP_CATALOG', '/');
define('DIR_WS_HTTPS_CATALOG', '/');
define('DIR_WS_FACEBOOK','facebook/');
define('DIR_WS_IMAGES', HTTP_SERVER . '/images/');
define('DIR_WS_IMAGES_PAGES', 'pages/');
define('DIR_WS_IMAGES_PRODUCTS', 'products/');
define('DIR_WS_IMAGES_PRODUCTS_BACKGROUNDS', DIR_WS_IMAGES_PRODUCTS . 'backgrounds/');
define('DIR_WS_IMAGES_TABS_INFO', 'tabs_info/');
define('DIR_WS_ICONS', 'icons/');
define('DIR_WS_INCLUDES', 'includes/');
define('DIR_WS_FUNCTIONS', DIR_WS_INCLUDES . 'functions/');
define('DIR_WS_CLASSES', DIR_WS_INCLUDES . 'classes/');
define('DIR_WS_MODULES', DIR_WS_INCLUDES . 'modules/');
define('DIR_WS_LANGUAGES', DIR_WS_INCLUDES . 'languages/');
define('DIR_WS_DOWNLOAD_PUBLIC', 'pub/');
define('DIR_WS_MARKETING_BANNERS', DIR_WS_IMAGES . 'marketing/banners/');
define('DIR_WS_MARKETING_PICS', DIR_WS_IMAGES . 'marketing/pics/');
define('DIR_WS_MARKETING_MAILS', DIR_WS_IMAGES . 'marketing/mails/');
define('DIR_WS_MARKETING_SHARES', DIR_WS_IMAGES . 'marketing/shares/');

define('DIR_FS_PHOTOS', DIR_FS_CATALOG . 'images/photos/');
define('DIR_FS_DOWNLOAD', DIR_FS_CATALOG . 'download/');
define('DIR_FS_DOWNLOAD_PUBLIC', DIR_FS_CATALOG . 'pub/');
define('DIR_FS_CATALOG_IMAGES', DIR_FS_CATALOG . 'images/');
define('DIR_FS_CATALOG_IMAGES_PRODUCTS', DIR_FS_CATALOG_IMAGES . 'products/');
define('DIR_FS_MARKETING_BANNERS', DIR_FS_CATALOG_IMAGES . 'marketing/banners/');
define('DIR_FS_MARKETING_PICS', DIR_FS_CATALOG_IMAGES . 'marketing/pics/');
define('DIR_FS_MARKETING_MAILS', DIR_FS_CATALOG_IMAGES . 'marketing/mails/');
define('DIR_FS_MARKETING_SHARES', DIR_FS_CATALOG_IMAGES . 'marketing/shares/');
define('DIR_FS_CACHE', DIR_FS_CATALOG . 'includes/work/');

define('SESSION_WRITE_DIRECTORY', DIR_FS_CATALOG . 'includes/work');