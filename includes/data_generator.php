<?php
$start_timer = time();
$start_date = '2012-06-00 00:00:00';
$start_time = strtotime($start_date);
$end_time = time();
$user_agents = array(
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36',
    'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:25.0) Gecko/20100101 Firefox/25.0',
    'Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25',
);

$urls = array(
    'http://random-name-generator.info/random/?n=10&g=1&st=2',
    'http://dena.am/en?p=9169156',
    'en.wikipedia.org/wiki/Atrial_septal_defect',
    'asd-electro.ru/products/',
    'www.asd-electro.ru/products/ofisnye_svetilniki/',
    'svetinn.ru/catalog/asd4/asdsvet54/asdsvet54_94.html&nc=1',
    'xmusic.me/q/LgwCEQ4yW1MkNT0xFA/',
    'www.snob.ru/profile/20736/blog',
    'www.youtube.com/user/NevzorovTV'
);

$last_urls = array(
    'http://partner.dena.am/en/marketing-materials#method=1&products[]=0&products[]=4&products[]=5&products[]=3&products[]=2&products[]=1&languages[]=1&languages[]=2&languages[]=4&languages[]=6&tags=all',
    'http://dena.am/en',
    'http://dena.am/en/product/zealut-dena?p=9169156',
    'http://dena.am/en/shopping-cart?p=9169156',
    'http://dena.am/en/dysbiosis?p=9169156',
    'http://dena.am/en/advanced-search-result?keywords=asd&x=0&y=0'
);

$totals_codes = array('ot_subtotal', 'ot_shipping', 'ot_tax', 'ot_total');
$totals_titles = array('Sub-Total:', 'Flat Rate (Best Way):', 'FL TAX 0.0%:', 'Total:');

$first_day_visits_count = 30;
$visits_count_diff = 0;

$products = array(
    array(
        'products_id' => 1,
        'products_name' => 'Cavsor',
        'products_price' => 52,
        'final_price' => 52,
        'products_tax' => 0
    ),
    array(
        'products_id' => 2,
        'products_name' => 'MegaEl Dena',
        'products_price' => 45.3,
        'final_price' => 45.3,
        'products_tax' => 0
    ),
    array(
        'products_id' => 3,
        'products_name' => 'LactoEl Dena',
        'products_price' => 35.8,
        'final_price' => 35.8,
        'products_tax' => 0
    ),
    array(
        'products_id' => 4,
        'products_name' => 'Zealut-Dena',
        'products_price' => 53,
        'final_price' => 53,
        'products_tax' => 0
    ),
    array(
        'products_id' => 5,
        'products_name' => 'Elgenoflex',
        'products_price' => 45,
        'final_price' => 45,
        'products_tax' => 0
    )
);
$products_length = sizeof($products);

$customers = array(
    array(
        'customers_id' => 36,
        'customers_name' => 'Artavazd Amirkhanyan',
        'customers_company' => '',
        'customers_street_address' => 'Andranik',
        'customers_suburb' => '',
        'customers_city' => 'Yerevan',
        'customers_postcode' => '32217',
        'customers_state' => 'Yerevan',
        'customers_country' => 'Armenia',
        'customers_telephone' => '+37455544155',
        'customers_email_address' => 'artavazd.amirkhanyan@gmail.com',
        'customers_address_format_id' => 1,
        'delivery_name' => 'Artavazd Amirkhanyan',
        'delivery_company' => '',
        'delivery_street_address' => 'Andranik',
        'delivery_suburb' => '',
        'delivery_city' => 'Yerevan',
        'delivery_postcode' => '32217',
        'delivery_state' => 'Yerevan',
        'delivery_country' => 'Armenia',
        'delivery_address_format_id' => 1,
        'billing_name' => 'Artavazd Amirkhanyan',
        'billing_company' => '',
        'billing_street_address' => 'Andranik',
        'billing_suburb' => '',
        'billing_city' => 'Yerevan',
        'billing_postcode' => '32217',
        'billing_state' => 'Yerevan',
        'billing_country' => 'Armenia',
        'billing_address_format_id' => 1,
        'payment_method' => 'Cash on Delivery',
        'cc_type' => '',
        'cc_owner' => '',
        'cc_number' => '',
        'cc_expires' => '',
        'currency' => 'USD',
        'currency_value' => 1
    ),
    array(
        'customers_id' => 37,
        'customers_name' => 'Jackob Jaguar',
        'customers_company' => '',
        'customers_street_address' => 'North Laura',
        'customers_suburb' => '',
        'customers_city' => 'Jacksonville',
        'customers_postcode' => '89546',
        'customers_state' => 'Florida',
        'customers_country' => 'United States',
        'customers_telephone' => '+37455544155',
        'customers_email_address' => 'artavazd@jaguar.am',
        'customers_address_format_id' => 1,
        'delivery_name' => 'Jackob Jaguar',
        'delivery_company' => '',
        'delivery_street_address' => 'North Laura',
        'delivery_suburb' => '',
        'delivery_city' => 'Jacksonville',
        'delivery_postcode' => '89546',
        'delivery_state' => 'Florida',
        'delivery_country' => 'United States',
        'delivery_address_format_id' => 1,
        'billing_name' => 'Jackob Jaguar',
        'billing_company' => '',
        'billing_street_address' => 'asdasd',
        'billing_suburb' => '',
        'billing_city' => 'Jacksonville',
        'billing_postcode' => '89546',
        'billing_state' => 'Florida',
        'billing_country' => 'United States',
        'billing_address_format_id' => 1,
        'payment_method' => 'Cash on Delivery',
        'cc_type' => '',
        'cc_owner' => '',
        'cc_number' => '',
        'cc_expires' => '',
        'currency' => 'USD',
        'currency_value' => 1
    ),
    array(
        'customers_id' => 38,
        'customers_name' => 'Valentin Dampha',
        'customers_company' => '',
        'customers_street_address' => 'Rue Paul Bert',
        'customers_suburb' => '',
        'customers_city' => 'Lyon',
        'customers_postcode' => '69005',
        'customers_state' => 'Lyon',
        'customers_country' => 'Albania',
        'customers_telephone' => '+509(444)-8554833',
        'customers_email_address' => 'arto.93@mail.ru',
        'customers_address_format_id' => 1,
        'delivery_name' => 'Valentin Dampha',
        'delivery_company' => '',
        'delivery_street_address' => 'Rue Paul Bert',
        'delivery_suburb' => '',
        'delivery_city' => 'Lyon',
        'delivery_postcode' => '69005',
        'delivery_state' => 'Lyon',
        'delivery_country' => 'Albania',
        'delivery_address_format_id' => 1,
        'billing_name' => 'Valentin Dampha',
        'billing_company' => '',
        'billing_street_address' => 'Rue Paul Bert',
        'billing_suburb' => '',
        'billing_city' => 'Lyon',
        'billing_postcode' => '69005',
        'billing_state' => 'Lyon',
        'billing_country' => 'Albania',
        'billing_address_format_id' => 1,
        'payment_method' => 'Cash on Delivery',
        'cc_type' => '',
        'cc_owner' => '',
        'cc_number' => '',
        'cc_expires' => '',
        'currency' => 'USD',
        'currency_value' => 1
    ),
    array(
        'customers_id' => 42,
        'customers_name' => 'Aria Fox',
        'customers_company' => '',
        'customers_street_address' => 'Rue Paul Bert',
        'customers_suburb' => '',
        'customers_city' => 'Lyon',
        'customers_postcode' => '69003',
        'customers_state' => 'Lyon',
        'customers_country' => 'France',
        'customers_telephone' => '+501(656)-6331004',
        'customers_email_address' => 'ariafox@gmail.com',
        'customers_address_format_id' => 1,
        'delivery_name' => 'Aria Fox',
        'delivery_company' => '',
        'delivery_street_address' => 'Rue Paul Bert',
        'delivery_suburb' => '',
        'delivery_city' => 'Lyon',
        'delivery_postcode' => '69003',
        'delivery_state' => 'Lyon',
        'delivery_country' => 'France',
        'delivery_address_format_id' => 1,
        'billing_name' => 'Aria Fox',
        'billing_company' => '',
        'billing_street_address' => 'Rue Paul Bert',
        'billing_suburb' => '',
        'billing_city' => 'Lyon',
        'billing_postcode' => '69003',
        'billing_state' => 'Lyon',
        'billing_country' => 'France',
        'billing_address_format_id' => 1,
        'payment_method' => 'Cash on Delivery',
        'cc_type' => '',
        'cc_owner' => '',
        'cc_number' => '',
        'cc_expires' => '',
        'currency' => 'USD',
        'currency_value' => 1
    ),
    array(
        'customers_id' => 43,
        'customers_name' => 'Nathan Janda',
        'customers_company' => '',
        'customers_street_address' => '249 Florence Drive',
        'customers_suburb' => '',
        'customers_city' => 'Florence',
        'customers_postcode' => '33226',
        'customers_state' => 'Florence',
        'customers_country' => 'United States',
        'customers_telephone' => '+34(333)-3005411',
        'customers_email_address' => 'omargrifin@gmail.com',
        'customers_address_format_id' => 1,
        'delivery_name' => 'Nathan Janda',
        'delivery_company' => '',
        'delivery_street_address' => '249 Florence Drive',
        'delivery_suburb' => '',
        'delivery_city' => 'Florence',
        'delivery_postcode' => '33226',
        'delivery_state' => 'Florence',
        'delivery_country' => 'United States',
        'delivery_address_format_id' => 1,
        'billing_name' => 'Nathan Janda',
        'billing_company' => '',
        'billing_street_address' => '249 Florence Drive',
        'billing_suburb' => '',
        'billing_city' => 'Florence',
        'billing_postcode' => '33226',
        'billing_state' => 'Florence',
        'billing_country' => 'United States',
        'billing_address_format_id' => 1,
        'payment_method' => 'Cash on Delivery',
        'cc_type' => '',
        'cc_owner' => '',
        'cc_number' => '',
        'cc_expires' => '',
        'currency' => 'USD',
        'currency_value' => 1
    ),
    array(
        'customers_id' => 44,
        'customers_name' => 'Arturo Halford',
        'customers_company' => '',
        'customers_street_address' => '574 Bates',
        'customers_suburb' => '',
        'customers_city' => 'Ashwood',
        'customers_postcode' => '72717',
        'customers_state' => 'Middlesbrough-England',
        'customers_country' => 'United Kingdom',
        'customers_telephone' => '+44(535)-6001912',
        'customers_email_address' => 'crarturo17@yopmail.com',
        'customers_address_format_id' => 1,
        'delivery_name' => 'Arturo Halford',
        'delivery_company' => '',
        'delivery_street_address' => '574 Bates',
        'delivery_suburb' => '',
        'delivery_city' => 'Ashwood',
        'delivery_postcode' => '72717',
        'delivery_state' => 'Middlesbrough-England',
        'delivery_country' => 'United Kingdom',
        'delivery_address_format_id' => 1,
        'billing_name' => 'Arturo Halford',
        'billing_company' => '',
        'billing_street_address' => '574 Bates',
        'billing_suburb' => '',
        'billing_city' => 'Ashwood',
        'billing_postcode' => '72717',
        'billing_state' => 'Middlesbrough-England',
        'billing_country' => 'United Kingdom',
        'billing_address_format_id' => 1,
        'payment_method' => 'Cash on Delivery',
        'cc_type' => '',
        'cc_owner' => '',
        'cc_number' => '',
        'cc_expires' => '',
        'currency' => 'USD',
        'currency_value' => 1
    ),
    array(
        'customers_id' => 45,
        'customers_name' => 'Melina Hugo',
        'customers_company' => '',
        'customers_street_address' => '574 Bates',
        'customers_suburb' => '',
        'customers_city' => 'Murcia',
        'customers_postcode' => '88413',
        'customers_state' => 'Murcia',
        'customers_country' => 'Spain',
        'customers_telephone' => '+34(535)-5889904',
        'customers_email_address' => 'axmelina23@yopmail.com',
        'customers_address_format_id' => 1,
        'delivery_name' => 'Melina Hugo',
        'delivery_company' => '',
        'delivery_street_address' => '574 Bates',
        'delivery_suburb' => '',
        'delivery_city' => 'Murcia',
        'delivery_postcode' => '88413',
        'delivery_state' => 'Murcia',
        'delivery_country' => 'Spain',
        'delivery_address_format_id' => 1,
        'billing_name' => 'Melina Hugo',
        'billing_company' => '',
        'billing_street_address' => '574 Bates',
        'billing_suburb' => 'asd',
        'billing_city' => 'Murcia',
        'billing_postcode' => '88413',
        'billing_state' => 'Murcia',
        'billing_country' => 'Spain',
        'billing_address_format_id' => 1,
        'payment_method' => 'Cash on Delivery',
        'cc_type' => '',
        'cc_owner' => '',
        'cc_number' => '',
        'cc_expires' => '',
        'currency' => 'USD',
        'currency_value' => 1
    )
);

function get_rand_ip() {
    return tep_rand(1, 254) . '.' . tep_rand(1, 254) . '.' . tep_rand(1, 254) . '.' . tep_rand(1, 254);
}

function get_rand_url() {
    global $urls;
    return $urls[array_rand($urls)];
}

function get_rand_last_url() {
    global $last_urls;
    return $last_urls[array_rand($last_urls)];
}

function get_rand_user_agent() {
    global $user_agents;
    return $user_agents[array_rand($user_agents)];
}

function get_dif($n) {
    for($i = 1; true; ++$i) {
        if ($i * ($i + 1) / 2 >= $n) return $i;
    }
}

$is_visits_count_inc = tep_rand(1, 2) === 1;
$min_visits = 0;
$max_visits = 60;
$upper_limit = ($max_visits - $min_visits) / 4 * 3 + $min_visits;
$lower_limit = ($max_visits - $min_visits) / 4 + $min_visits;
$place = ($max_visits - $min_visits) / 4;

for ($current_time = $start_time + 24*3600, $visits_count = $first_day_visits_count; $current_time < $end_time; $current_time += 24 * 3600) {
    if ($is_visits_count_inc) {
        if ($visits_count > $upper_limit) {
            $is_visits_count_inc = tep_rand($visits_count - $upper_limit - $place, $visits_count - $upper_limit) < $min_visits;
        } elseif ($visits_count < $upper_limit) {
            $is_visits_count_inc = tep_rand($visits_count - $upper_limit * 2, $visits_count) < $min_visits;
        } else {
            $is_visits_count_inc = !tep_rand(0, 1);
        }
    } else {
        if ($visits_count < $lower_limit) {
            $is_visits_count_inc = tep_rand($visits_count - $lower_limit, $visits_count) <= $min_visits;
        } elseif ($visits_count > $lower_limit) {
            $is_visits_count_inc = tep_rand($visits_count - $lower_limit * 2, $visits_count) <= $min_visits;
        }
    }

    $n = $is_visits_count_inc ? $max_visits - $visits_count : $visits_count - $min_visits;
    $visits_count_diff = $n == 0 ? 0 : get_dif(tep_rand(0, $n * ($n + 1) / 2));

    $visits_count_diff *= $is_visits_count_inc ? 1 : -1;

    $visits_count += $visits_count_diff;

    if ($visits_count <= 0) continue;

    for($vi = 0; $vi < $visits_count; ++$vi) {
        $ip_address = get_rand_ip();
        $user_agent = get_rand_user_agent();
        $first_online = $current_time + tep_rand(0, 24*3600 - 1);
        $visit_duration = tep_rand(1, 10000);
        $method_id = tep_rand(0, 6);
        $material_id = tep_rand(1, 10);
        !$method_id && $method_id = 'null';
        !$method_id && $material_id = 'null';
        shuffle($customers);
        $customer = $customers[0];
        $from_url = get_rand_url();

        $last_online = $first_online + $visit_duration;
        $partners_id = tep_rand(1, 10) > 6 ? 28 : 'null';
        $orders_count = tep_rand(0, 9) ? 0 : 5 - intval(sqrt(tep_rand(1, 24)));

        $sql_data_array = array(
            'partners_id' => $partners_id,
            'method_id' => $method_id,
            'material_id' => $material_id,
            'products_id' => 'null',
            'from_url' => $from_url,
            'to_url' => get_rand_url(),
            'last_url' => get_rand_last_url(),
            'first_online' => date(MYSQL_FORMAT_DATETIME, $first_online),
            'last_online' => date(MYSQL_FORMAT_DATETIME, $last_online),
            'pageviews' => tep_rand(1, 500),
            'user_agent' => $user_agent,
            'ip_address' => $ip_address
        );

        tep_db_perform(TABLE_VISITS, $sql_data_array);
        $visits_id = tep_db_insert_id();



        if (!$orders_count) continue;
        for ($oi = 0; $oi < $orders_count; ++$oi) {
            $orders_date = $first_online + tep_rand(0, $visit_duration);
            $orders_status_id = tep_rand(1, 4);
            $order_data = array_merge(
                $customer,
                array(
                    'date_purchased' => date(MYSQL_FORMAT_DATETIME, $orders_date),
                    'orders_status' => $orders_status_id,
                    'customers_ip' => $ip_address,
                    'customers_user_agent' => $user_agent,
                    'visit_id' => $visits_id
                )
            );
            tep_db_perform(TABLE_ORDERS, $order_data);
            $orders_id = tep_db_insert_id();

            $sql_data = array(
                'orders_id' => $orders_id,
                'orders_status_id' => $orders_status_id,
                'date_added' => date(MYSQL_FORMAT_DATETIME, $orders_date),
                'customer_notified' => 1,
                'comments' => ''
            );
            tep_db_perform(TABLE_ORDERS_STATUS_HISTORY, $sql_data);

            $products_count = $products_length - intval(sqrt(tep_rand(1, $products_length * $products_length - 1)));
            shuffle($products);
            $total_price = 0;

            for ($pi = 0; $pi < $products_count; ++$pi) {
                $products_quantity = 6 - intval(sqrt(tep_rand(1, 35)));

                $sql_data = array_merge(
                    $products[$pi],
                    array(
                        'orders_id' => $orders_id,
                        'products_quantity' => $products_quantity
                    )
                );

                tep_db_perform(TABLE_ORDERS_PRODUCTS, $sql_data);

                $total_price += $sql_data['products_price'];
            }


            $val = round($total_price, 2);
            $total = round(5 + $val, 2);

            $totals_texts = array('$' . number_format($val, 2), '$5.00', '$0', '<strong>$' . number_format($total, 2) . '</strong>');
            $totals_vals = array($val, 5.0000, 0, $total);
            for ($k = 0; $k < 4; $k++) {
                if ($totals_codes[$k] == 'ot_total') {
                    $total_price = $totals_vals[$k];
                }
                $sql_data_array = array('orders_id' => $orders_id,
                    'title' => $totals_titles[$k],
                    'text' => $totals_texts[$k],
                    'value' => $totals_vals[$k],
                    'class' => $totals_codes[$k],
                    'sort_order' => $k + 1);
                tep_db_perform(TABLE_ORDERS_TOTAL, $sql_data_array);
            }

	        if ($partners_id !== 'null') {

		        $sql_data_ph = array(
			        'partners_id' => $partners_id,
			        'customers_id' => $customer['customers_id'],
			        'orders_id' => $orders_id,
			        'visit_id' => $visits_id,
			        'affiliates_commission' => AFFILIATES_COMMISSION,
			        'affiliates_share' => $total_price * AFFILIATES_COMMISSION / 100,
			        'customers_from_url' => $from_url,
			        'commission_status' => $orders_status_id == 4 ? 1 : 0, //waiting
			        'date_added' => date(MYSQL_FORMAT_DATETIME, $orders_date),
		        );
		        tep_db_perform(TABLE_PARTNERS_HISTORY, $sql_data_ph);
	        }
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
            echo '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------';
        }

        $now_timer = time();

        $seconds_remaining = ($now_timer - $start_timer) * ($end_time - $current_time) / ($current_time - $start_time);
        $hours = intval($seconds_remaining / 3600);
        $minutes = intval(($seconds_remaining - $hours * 3600) / 60);
        $seconds = intval($seconds_remaining - $minutes * 60);
        echo '<br/>' . $seconds_remaining . 's - ' . $hours . ':' . $minutes . ':' . $seconds . '<br/>';
    }
}

tep_db_query('update ' . TABLE_VISITS . ' set products_id = 1 where method_id = 1 and material_id = 3');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 1 where method_id = 1 and material_id = 4');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 1 where method_id = 1 and material_id = 5');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 2 where method_id = 1 and material_id = 6');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 2 where method_id = 1 and material_id = 7');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 0 where method_id = 1 and material_id = 8');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 0 where method_id = 1 and material_id = 9');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 0 where method_id = 1 and material_id = 10');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 0 where method_id = 1 and material_id = 11');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 2 where method_id = 2 and material_id = 1');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 0 where method_id = 2 and material_id = 2');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 3 where method_id = 2 and material_id = 3');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 1 where method_id = 3 and material_id = 1');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 0 where method_id = 3 and material_id = 3');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 3 where method_id = 4 and material_id = 1');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 0 where method_id = 5 and material_id = 1');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 1 where method_id = 5 and material_id = 2');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 2 where method_id = 5 and material_id = 3');
tep_db_query('update ' . TABLE_VISITS . ' set products_id = 2 where method_id = 6 and material_id = 2');

exit('FINISH');