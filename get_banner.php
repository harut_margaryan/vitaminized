<?php
empty($_GET['m']) || !is_numeric($_GET['m']) && exit;
require('includes/global_config.php');
$banner_query = tep_db_query('select * from ' . TABLE_MARKETING_BANNERS . ' where material_id = ' . (int)$_GET['m'] . ' and material_status = 1');
tep_db_num_rows($banner_query) != 1 && exit;
$banner = tep_db_fetch_array($banner_query);

if (tep_db_num_rows($language_query = tep_db_query('select code, languages_id as id from ' . TABLE_LANGUAGES . ' where languages_id = ' . (int)$banner['languages_id'] . ' and interface_status = 1')) == 1) {
    $share_language = tep_db_fetch_array($language_query);
} else {
    $share_language = array('id' => (int)DEFAULT_LANGUAGE, 'code' => $current_language);
}

$save_language = array('id' => $languages_id, 'code' => $current_language);
$languages_id = $share_language['id'];
$current_language = $share_language['code'];
$PARTNER = false;

$with_partners_key = false;

$share_url = tep_href_link('', (empty($_GET['p']) ? '' : 'p=' . urlencode($_GET['p']) . '&') . 'method=6&material=' . (int)$banner['material_id']);
if ($banner['products_id'] != 0) {
    $products_query = tep_db_query('select products_name from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd on(p.products_id = ' . (int)$banner['products_id'] . ' and p.products_id = pd.products_id and p.products_status = 1 and pd.language_id = ' . (int)$share_language . ')');
    if (tep_db_num_rows($products_query) == 1) {
        $product = tep_db_fetch_array($products_query);
        $share_url = tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product['products_name']), (empty($_GET['p']) ? '' : 'p=' . urlencode($_GET['p']) . '&') . 'method=6&material=' . (int)$banner['material_id']);
    }
}

$PARTNER = true;
$languages_id = $save_language['id'];
$current_language = $save_language['code'];
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $banner['material_title'];?></title>
</head>
<body style="margin: 0;overflow: hidden;">
<?php
switch(pathinfo($banner['material_file'], PATHINFO_EXTENSION)) {
    case 'jpg':
    case 'png':
    case 'gif':
        echo '<a style="display:block;width:' . $banner['material_width'] . 'px;height:' . $banner['material_height'] . 'px;" href="' . $share_url . '" target="_blank"><img style="display:block;" src="' . HTTP_SERVER . '/' . DIR_WS_MARKETING_BANNERS . $banner['material_file'] . '" alt="' . $banner['material_title'] . '" title="' . $banner['material_title'] . '" width="' . $banner['material_width'] . '" height="' . $banner['material_height'] . '"/></a>';
        break;
    case 'swf':
        echo '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>';
        echo '<div id="banner"></div>';
        echo '<script type="text/javascript">window.swfobject.embedSWF("' . HTTP_SERVER . '/' . DIR_WS_MARKETING_BANNERS . $banner['material_file'] . '", "banner", "' . $banner['material_width'] . '", "' . $banner['material_height'] . '", "9.0.0");</script>';
        break;
}?>
</body>
</html>