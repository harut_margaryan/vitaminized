<h1><?php echo HEADING_TITLE; ?></h1>

<div class="contentContainer">
    <div class="contentText">
        <?php echo TEXT_INFORMATION; ?>
    </div>

    <div class="buttonSet">
        <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link()); ?></span>
    </div>
</div>
