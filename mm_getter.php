<?php
require('includes/global_config.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_MM_GETTER);

// split-page-results
require(DIR_WS_CLASSES . 'split_page_results.php');

$exit = false;

$method = empty($_GET['method']) ? 1 : $_GET['method'];
$page = empty($_GET['page']) ? 1 : $_GET['page'];
$products = empty($_GET['products']) ? array() : $_GET['products'];
$languages = empty($_GET['languages']) ? array() : $_GET['languages'];
$tags = empty($_GET['tags']) ? array() : $_GET['tags'];

$defaults = array(
    'html' => '<p class="mm-no-data">' . TEXT_NO_DATA . '</p>',
    'method' => $method,
    'products' => $products,
    'languages' => $languages,
    'pages' => 1,
    'current' => 1
);


if (empty($products) || ($method != 3 && empty($languages))) {
    exit(json_encode($defaults));
}

$TABLE_MATERIAL = '';
$TABLE_MATERIAL_TAGS = '';

switch($method) {
    case 1: // posts
        $TABLE_MATERIAL = TABLE_MARKETING_POSTS;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_POSTS_TAGS;
        break;
    case 2: //banners
        $TABLE_MATERIAL = TABLE_MARKETING_BANNERS;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_BANNERS_TAGS;
        break;
    case 3: //pics
        $TABLE_MATERIAL = TABLE_MARKETING_PICS;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_PICS_TAGS;
        break;
    case 4: //mails
        $TABLE_MATERIAL = TABLE_MARKETING_MAILS;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_MAILS_TAGS;
        break;
    case 5: //statuses
        $TABLE_MATERIAL = TABLE_MARKETING_STATUSES;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_STATUSES_TAGS;
        break;
    case 6: //shares
        $TABLE_MATERIAL = TABLE_MARKETING_SHARES;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_SHARES_TAGS;
        break;
    default:
        exit(json_encode($defaults));
        break;
}

if (is_array($products)) {
    $products_filter = ' and (';
    for ($i = 0, $n = sizeof($products); $i < $n; ++$i) {
        $products_filter .= 'm.products_id = ' . (int)$products[$i] . ' or ';
    }
    $products_filter = substr($products_filter, 0, -4) . ')';
} elseif ($products == 'all') {
    $products_filter = '';
} else {
    $defaults['products'] = array();
    exit(json_encode($defaults));
}

if (is_array($tags) && !empty($tags)) {
    $tags_filter = ' and (';
    for ($i = 0, $n = sizeof($tags); $i < $n; ++$i) {
        $tags_filter .= 'mt.tag_id = ' . (int)$tags[$i] . ' or ';
    }
    $tags_filter = substr($tags_filter, 0, -4) . ')';
} elseif ($tags == 'all') {
    $tags_filter = '';
} else {
    $tags_filter = '';
    $exit = true;
}

$languages_filter = '';
if ($method != 3) {
    if (is_array($languages) && !empty($languages)) {
        $languages_filter = ' and (';
        for ($i = 0, $n = sizeof($languages); $i < $n; ++$i) {
            $languages_filter .= 'm.languages_id = ' . (int)$languages[$i] . ' or ';
        }
        $languages_filter = substr($languages_filter, 0, -4) . ')';
    } elseif ($languages == 'all') {
        $languages_filter = '';
    } else {
        $defaults['languages'] = array();
        exit(json_encode($defaults));
    }
}

$tags_query_raw = 'select distinct t.tag_id, ti.tag_name from ' . $TABLE_MATERIAL . ' as m join ' . TABLE_PRODUCTS . ' as p on(((p.products_id = m.products_id and p.products_status = 1) or m.products_id = 0) and m.material_status = 1' . $products_filter . $languages_filter . ') join ' . $TABLE_MATERIAL_TAGS . ' as mt on(m.material_id = mt.material_id) join ' . TABLE_MARKETING_TAGS . ' as t on (t.tag_id = mt.tag_id and t.tag_status = 1) join ' . TABLE_MARKETING_TAGS_INFO . ' as ti on (t.tag_id = ti.tag_id and ti.language_id = ' . (int)$languages_id . ') order by t.sort_order';

if (!$tags_filter) {
    $material_query_raw = 'select distinct m.* from ' . $TABLE_MATERIAL . ' as m join ' . TABLE_PRODUCTS . ' as p on(((p.products_id = m.products_id and p.products_status = 1) or m.products_id = 0) and m.material_status = 1' . $products_filter . $languages_filter . ') order by m.material_sort asc, m.material_id desc';
} else {
    $material_query_raw = 'select distinct m.* from ' . $TABLE_MATERIAL . ' as m join ' . $TABLE_MATERIAL_TAGS . ' as mt on(m.material_id = mt.material_id and m.material_status = 1 ' . $products_filter . $languages_filter . $tags_filter . ') join ' . TABLE_MARKETING_TAGS . ' as t on(t.tag_id = mt.tag_id and t.tag_status = 1) join ' . TABLE_PRODUCTS . ' as p on((p.products_id = m.products_id and p.products_status = 1) or m.products_id = 0) order by m.material_sort asc, m.material_id desc';
}

$res_tags = array();
$tags_query = tep_db_query($tags_query_raw);
while ($cur_tag = tep_db_fetch_array($tags_query)) {
    $res_tags[] = array(
        'id' => $cur_tag['tag_id'],
        'name' => $cur_tag['tag_name'],
        'checked' => ($tags == 'all' || in_array($cur_tag['tag_id'], $tags)) ? true : false
    );
}
$defaults['tags'] = $res_tags;

if ($exit) {
    exit(json_encode($defaults));
}

$method_query = tep_db_query('select m.method_id, mi.method_name from ' . TABLE_MARKETING_METHODS . ' as m join ' . TABLE_MARKETING_METHODS_INFO . ' as mi on(m.method_id = ' . (int)$method . ' and m.method_id = mi.method_id and m.method_status = b\'1\' and mi.language_id = ' . (int)$languages_id . ')');

if (tep_db_num_rows($method_query) == 0) {
    exit(json_encode($defaults));
}

$method_info = tep_db_fetch_array($method_query);

$material_split = new splitPageResults($material_query_raw, 5 , 'm.material_id');
$material_query = tep_db_query($material_split->sql_query);

if (tep_db_num_rows($material_query) == 0) {
    exit(json_encode($defaults));
}

$content = '';

switch($method) {
	case 1: //posts
		break;
	case 2: //banners
		break;
	case 3: //pics
		break;
	case 4: //mails
		break;
	case 5: //statuses
		break;
	case 6: //shares
		break;
}

$defaults['pages'] = $material_split->number_of_pages;
$defaults['current'] = $material_split->current_page_number;
while($material = tep_db_fetch_array($material_query)) {
    $material_link = tep_href_link(PARTNERS_MARKETING_URL . '/' . tep_clean_text_int($method_info['method_name']) . '/' . (int)$material['material_id']);
    switch($method) {
        case 1: //posts
            $content .= '<div class="marketing-post">';
            $content .= '<h3><a href="' . $material_link . '">' . $material['material_title'] . '</a></h3>';
            $content .= '<p>' . tep_break_text_int(strip_tags($material['material_text']), 200) . '</p>';
            $content .= '</div>';
            break;
        case 2: //banners
            $content .= '<div class="marketing-banner">';
            $content .= '<h3>' . $material['material_title'] . '</h3>';
            $content .= '<div class="banner-cont">';
            $content .= '<div class="banner-iframe nano">';
            $banner_code = '<iframe src="' . HTTP_SERVER . '/banner/' . $material['material_id'] . '?p=' . $partner_key . '" width="' . $material['material_width'] . 'px" height="' . $material['material_height'] . 'px" frameBorder="0"></iframe>';
            $content .= $banner_code;
            $content .= '</div>';
            $content .= '<div class="banner-code">';
            $content .= '<label for="banner-code-' . $material['material_id'] . '">' . TEXT_BANNER_CODE . '</label>';
            $content .=  tep_draw_checkbox_field('banner-code', 0, false, 'id="banner-code-' . $material['material_id'] . '"');
            $content .= '<textarea readonly="readonly" onclick="this.focus(); this.select()" rows="6">';
            $content .= $banner_code;
            $content .= '</textarea>';
            $content .= '</div>';
            $content .= '</div>';
            $content .= '</div>';
            break;
        case 3: //pics
            $content .= '<div class="marketing-pic">';
            $content .= '<h3>' . $material['material_title'] . '</h3>';
            $content .= '<img src="' . DIR_WS_MARKETING_PICS . $material['material_pic'] . '" title="' . $material['material_title'] . '" alt="' . $material['material_title'] . '"/>';
            $content .= '<a href="' . tep_href_link(PARTNERS_MARKETING_DOWNLOADS_URL . '/' . $material['material_id']) . '">download</a>';
            $content .= '</div>';
            break;
        case 4: //mails
            $content .= '<div class="marketing-mail">';
            $content .= '<a href="' . $material_link . '"><h3>' . $material['material_title'] . '</h3>';
            $content .= '<img src="' . DIR_WS_MARKETING_MAILS . $material['material_pic'] . '" title="' . $material['material_title'] . '" alt="' . $material['material_title'] . '"/></a>';
            $content .= '</div>';
            break;
        case 5: //statuses
            $content .= '<div class="marketing-status">';
            $content .= '<p>' . $material['material_title'] . '</p>';
            $content .= '</div>';
            break;
        case 6: //shares
            $content .= '<div class="marketing-share">';
            $content .= tep_image(DIR_WS_MARKETING_SHARES . $material['material_pic'], $material['material_title'], 40);
            $content .= '<h3>' . $material['material_title'] . '</h3>';
            $content .= '<p>' . $material['material_desc'] . '</p>';

            if (tep_db_num_rows($language_query = tep_db_query('select code, languages_id as id from ' . TABLE_LANGUAGES . ' where languages_id = ' . (int)$material['languages_id'] . ' and interface_status = 1')) == 1) {
                $share_language = tep_db_fetch_array($language_query);
            } else {
                $share_language = array('id' => (int)DEFAULT_LANGUAGE, 'code' => $current_language);
            }

            $save_language = array('id' => $languages_id, 'code' => $current_language);
            $languages_id = $share_language['id'];
            $current_language = $share_language['code'];
            $PARTNER = false;

            $share_url = tep_href_link('', 'mtd=6&mtr=' . (int)$material['material_id']);
            if ($material['products_id'] != 0) {
                $products_query = tep_db_query('select products_name from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd on(p.products_id = ' . (int)$material['products_id'] . ' and p.products_id = pd.products_id and p.products_status = 1 and pd.language_id = ' . (int)$share_language . ')');
                if (tep_db_num_rows($products_query) == 1) {
                    $product = tep_db_fetch_array($products_query);
                    $share_url = tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product['products_name']), 'mtd=6&mtr=' . (int)$material['material_id']);
                }
            }

            $PARTNER = true;
            $languages_id = $save_language['id'];
            $current_language = $save_language['code'];

            $content .= '<div class="share-buttons">';
            //$content .= '<span class="fb-share-button" onclick="javascript:window.open(\'http://www.facebook.com/sharer.php?u=' . urlencode($share_url) . '\',\'displayWindow\',\'width=670,height=340,status=no,toolbar=no,menubar=no\');">share</span>';
            $content .= '<div class="addthis_toolbox addthis_default_style" addthis:url="' . $share_url . '" addthis:title="' . $material['material_title'] . '" style="padding-top:14px; height:20px; overflow:hidden;">
	<a class="addthis_button_google_plusone_share"  style="padding-top:2px;"></a>
    <a class="addthis_button_linkedin" style="padding-top:2px;"></a>
    <a class="addthis_button_facebook"  style="padding-top:2px;"></a>
    <a class="addthis_button_twitter"  style="padding-top:2px;"></a>
    <span class="addthis_separator" style="color:#ccc; margin:0 2px;">|</span>

    <a class="addthis_counter addthis_pill_style" style="padding-left:10px;"></a></div>';
            //$content .= '<script>addthis.toolbox(".addthis_toolbox");addthis.counter(".addthis_counter");</script>';
            $content .= '</div>';
            $content .= '</div>';
            break;
    }
}

switch($method) {
    case 1: //posts
        break;
    case 2: //banners
        break;
    case 3: //pics
        break;
    case 4: //mails
        break;
    case 5: //statuses
        break;
    case 6: //shares
        $content .= '<script>addthis.toolbox(".addthis_toolbox");addthis.counter(".addthis_counter");</script>';
        break;
}

$defaults['html'] = $content;
exit(json_encode($defaults));