<h1><?php echo HEADING_TITLE; ?></h1>
<div class="contentContainer">
<div class="contentText">
<?php
switch($details) {
    case '':
        $check_visits = tep_db_query('select visit_id from ' . TABLE_VISITS . ' where partners_id = ' . (int)$partner_id . ' limit 1');
        if (tep_db_num_rows($check_visits) === 1) {
            ?>
            <div id="visits_cont"></div>
            <div id="methods_cont"></div>
            <a href="<?php echo tep_href_link(PARTNERS_ANALYTIC_URL . '/' . METHODS_ANALYTIC_URL); ?>" class="analytic-details"><?php echo TEXT_IN_DETAIL; ?></a>
            <div class="claer"></div>
            <script>
                var visits_analytic_url = '<?php echo HTTP_SERVER_PARTNER . '/analytic/visits';?>';
                var methods_analytic_url = '<?php echo HTTP_SERVER_PARTNER . '/analytic/methods';?>';
                var products_analytic_url = '<?php echo HTTP_SERVER_PARTNER . '/analytic/products';?>';

                $('#visits_cont').lineChart({url: visits_analytic_url});
                $('#methods_cont').barChart({url: methods_analytic_url});
            </script>
            <?php
            $check_products = tep_db_query(
                'SELECT ' .
                    'v.products_id, ' .
                    'v.products_name, ' .
                    'v.visits, ' .
                    'b.buys ' .
                'FROM (' .
                    'SELECT ' .
                        'pd.products_id, ' .
                        'pd.products_name, ' .
                        'COUNT(DISTINCT v.visit_id) AS visits ' .
                    'FROM ' . TABLE_PRODUCTS_DESCRIPTION . ' AS pd ' .
                    'LEFT JOIN ' . TABLE_VISITS . ' AS v ' .
                        'ON(' .
                            'v.partners_id = ' . intval($partner_id) . ' ' .
                            'AND pd.products_id = v.products_id' .
                        ') ' .
                    'WHERE ' .
                        'pd.language_id = ' . intval($languages_id) . ' ' .
                    'GROUP BY(pd.products_id)' .
                ') AS v ' .
                'LEFT JOIN (' .
                    'SELECT ' .
                        'op.products_id, ' .
                        'SUM(op.products_quantity) AS buys ' .
                    'FROM ' . TABLE_PARTNERS_HISTORY . ' AS ph ' .
                    'JOIN ' . TABLE_ORDERS_PRODUCTS . ' AS op ' .
                        'ON(' .
                            'ph.orders_id = op.orders_id ' .
                            'AND ph.partners_id = ' . intval($partner_id) . '' .
                        ') ' .
                    'GROUP BY(op.products_id) ' .
                ') AS b ' .
                    'ON(v.products_id = b.products_id) ' .
                'WHERE (' .
                    'v.visits > 0 ' .
                    'OR b.buys > 0' .
                ') ' .
                'LIMIT 1'
            );
            if (tep_db_num_rows($check_products) === 1) {
            ?>
                <div id="products_cont"></div>
                <a href="<?php echo tep_href_link(PARTNERS_ANALYTIC_URL . '/' . PRODUCTS_ANALYTIC_URL); ?>"
                   class="analytic-details"><?php echo TEXT_IN_DETAIL; ?></a>
                <div class="clear"></div>
                <script>
                    var products_analytic_url = '<?php echo HTTP_SERVER_PARTNER . '/analytic/products';?>';
                    $('#products_cont').barChart({url: products_analytic_url});
                </script>
            <?php
            }
        } else {
            echo TEXT_NO_ANALYTIC;
        }
        break;

    case METHODS_ANALYTIC_URL:
        $methods_query = tep_db_query('select distinct m.method_id, mi.method_name from ' . TABLE_VISITS . ' as v join ' . TABLE_MARKETING_METHODS . ' as m on(v.partners_id = ' . (int)$partner_id . ' and m.method_id = v.method_id) join ' . TABLE_MARKETING_METHODS_INFO . ' as mi on(m.method_id = mi.method_id and mi.language_id = ' . (int)$languages_id . ') order by m.sort_order');
        if (tep_db_num_rows($methods_query) > 0) {
            while ($method = tep_db_fetch_array($methods_query)) {
                $id = 'method_' . $method['method_id'] . '_cont';
                echo '<div id="' . $id . '"></div>';
                echo '<a href="' . tep_href_link(PARTNERS_ANALYTIC_URL . '/' . METHOD_ANALYTIC_URL . '/' . $method['method_id']) . '" class="analytic-details">' . TEXT_IN_DETAIL . '</a>';
                echo '<div class="clear"></div>';
                echo '<script>$("#' . $id . '").lineChart({title:"' . $method['method_name'] . '",url:"' . HTTP_SERVER_PARTNER . '/analytic/method?m=' . $method['method_id'] . '"});</script>';
            }
            $id = 'method_0_cont';
            echo '<div id="' . $id . '"></div>';
            echo '<script>$("#' . $id . '").lineChart({title:"' . METHOD_NAME_OTHER . '",url:"' . HTTP_SERVER_PARTNER . '/analytic/method?m=0"});</script>';
        } else {
            echo TEXT_NO_ANALYTIC;
        }
        break;

    case METHOD_ANALYTIC_URL:
        $method_id = (int)$extra;

        $method_query = tep_db_query('select m.method_id, mi.method_name from ' . TABLE_MARKETING_METHODS . ' as m join ' . TABLE_MARKETING_METHODS_INFO . ' as mi on(m.method_id = ' . (int)$method_id . ' and m.method_id = mi.method_id and mi.language_id = ' . (int)$languages_id . ' and m.method_status = b\'1\')');
        if (tep_db_num_rows($method_query) === 1) {
            $method = tep_db_fetch_array($method_query);

            switch($method_id) {
                case 2: //banners
                    $TABLE_MATERIAL = TABLE_MARKETING_BANNERS;
                    break;
                case 3: //pics
                    $TABLE_MATERIAL = TABLE_MARKETING_PICS;
                    break;
                case 4: //mails
                    $TABLE_MATERIAL = TABLE_MARKETING_MAILS;
                    break;
                case 5: //statuses
                    $TABLE_MATERIAL = TABLE_MARKETING_STATUSES;
                    break;
                case 6: //shares
                    $TABLE_MATERIAL = TABLE_MARKETING_SHARES;
                    break;
                default: // posts
                    $TABLE_MATERIAL = TABLE_MARKETING_POSTS;
                    break;
            }

            $materials_query = tep_db_query('SELECT v.material_id, m.material_title, COUNT(DISTINCT v.visit_id) AS visits, COUNT(DISTINCT ph.visit_id) AS buys FROM ' . $TABLE_MATERIAL . ' AS m JOIN ' . TABLE_VISITS . ' AS v ON(v.partners_id = ' . (int)$partner_id . ' AND v.method_id = ' . (int)$method_id . ' AND v.material_id = m.material_id AND m.material_status = 1) LEFT JOIN ' . TABLE_PARTNERS_HISTORY . ' AS ph ON(ph.visit_id = v.visit_id) GROUP BY v.material_id ORDER BY m.material_sort ASC');
            if (tep_db_num_rows($materials_query) > 0) {
                echo '<table class="materials-table">';
                echo '<tr>';
                echo '<th>' . MATERIAL_HEAD_TITLE . '</th>';
                echo '<th>' . MATERIAL_HEAD_VISITS . '</th>';
                echo '<th>' . MATERIAL_HEAD_PURCHASES . '</th>';
                echo '</tr>';
                while ($material = tep_db_fetch_array($materials_query)) {
                    echo '<tr class="material-head-row' . ($material['visits'] > 0 ? ' opening' : '') . '">';
                    echo '<td>' . $material['material_title'] . '</td>';
                    echo '<td>' . $material['visits'] . '</td>';
                    echo '<td>' . $material['buys'] . '</td>';
                    echo '</tr>';
                    if ($material['visits'] > 0) {
                        echo '<tr class="material-body-row">';
                        echo '<td colspan="10">';
                        echo '<div class="material-body-content" data-title="' . $material['material_title'] . '" data-url="' . HTTP_SERVER_PARTNER . '/analytic/material?m=' . $material['material_id'] . '&t=method&n=' . $method_id . '">';
                        echo '</div>';
                        echo '</td>';
                        echo '</tr>';
                    }
                }
                echo '</table>';
                echo '<script>$(".material-head-row.opening").click(function(){$this = $(this);$cont = $this.next().find(".material-body-content"); if(!$cont.data("loaded")){$cont.data("loaded", "1");$cont.lineChart({title:$cont.data("title"),url:$cont.data("url")});} else {$cont.slideToggle();}});</script>';
            } else {
                echo TEXT_NO_MATERIALS;
            }
        } else {
            echo TEXT_NO_MATERIALS;
        }
        break;

    case PRODUCTS_ANALYTIC_URL:
        $products_query = tep_db_query(
            'SELECT ' .
                'v.products_id, ' .
                'v.products_name, ' .
                'v.visits, ' .
                'b.buys ' .
            'FROM (' .
                'SELECT ' .
                    'pd.products_id, ' .
                    'pd.products_name, ' .
                    'COUNT(DISTINCT v.visit_id) AS visits ' .
                'FROM ' . TABLE_PRODUCTS_DESCRIPTION . ' AS pd ' .
                'LEFT JOIN ' . TABLE_VISITS . ' AS v ' .
                    'ON(' .
                        'v.partners_id = ' . (int)$partner_id . ' ' .
                        'AND pd.products_id = v.products_id' .
                    ') ' .
                'WHERE ' .
                    'pd.language_id = 1 ' .
                'GROUP BY(pd.products_id)' .
            ') AS v ' .
            'LEFT JOIN (' .
                'SELECT ' .
                    'op.products_id, ' .
                    'SUM(op.products_quantity) AS buys ' .
                'FROM ' . TABLE_PARTNERS_HISTORY . ' AS ph ' .
                'JOIN ' . TABLE_ORDERS_PRODUCTS . ' AS op ' .
                    'ON(' .
                        'ph.orders_id = op.orders_id ' .
                        'AND ph.partners_id = ' . (int)$partner_id .
                    ') ' .
                'GROUP BY(op.products_id) ' .
            ') AS b ' .
                'ON(v.products_id = b.products_id) ' .
            'WHERE (' .
                'v.visits > 0 ' .
                'OR b.buys > 0' .
            ') '
        );
        if (tep_db_num_rows($products_query) > 0) {
            while ($product = tep_db_fetch_array($products_query)) {
                $id = 'product_' . $product['products_id'] . '_cont';
                echo '<div id="' . $id . '"></div>';
                echo '<script>$("#' . $id . '").lineChart({title:"' . $product['products_name'] . '",url:"' . HTTP_SERVER_PARTNER . '/analytic/product?p=' . $product['products_id'] . '"});</script>';
            }
        } else {
            echo TEXT_NO_ANALYTIC;
        }
        break;
}
?>
</div>
</div>