	<?php

	$page_id = empty($item_page) ? $main_page['pages_id'] : $item_page['pages_id'];

	$page_query = tep_db_query('select p.*, pd.* from ' . TABLE_PAGES . ' as p join ' . TABLE_PAGES_DESCRIPTION . ' as pd on(p.pages_id = pd.pages_id) where p.pages_id = ' . intval($page_id) . ' and p.pages_status = 1 and pd.language_id = ' . intval($languages_id));
	$page = tep_db_fetch_array($page_query);
	echo '<h1 class="page-main-h1">' . $page['pages_title'] . '</h1>';
	if (!empty($page['pages_image'])) {
		echo '<div class="page-main-image-cont">' . tep_image(DIR_WS_IMAGES_PAGES . $page['pages_image'], $page['pages_title'], '', '', 'class="page-main-image"') . '</div>';
	}

	if ($page_id == 36) {
        echo '<div class="page-content-cont"><div class="page-content-cont-inner">';
        echo '<div class="sitemap-wrapper">';
		$nav_links = '<div>';
		$prods_query = tep_db_query('select p.products_id, p.products_image, p.products_price, pd.products_name, pd.products_short_desc, pd.products_description 
                      from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd using(products_id) where p.products_status = 1 and pd.language_id = ' . intval($languages_id));
		echo '<h2>Products</h2>';
		echo '<ul class="sitemap-ul">';

		while ($prod = tep_db_fetch_array($prods_query)) :?>
          <li>
            <a href="<?= tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($prod['products_name'])) ?>">
				<?= $prod['products_name']; ?>
            </a>
          </li>
		<?php endwhile;
		echo '</ul>';

		$pages_query = tep_db_query("select p.pages_id, pd.pages_title, (pd.pages_description is not null and pd.pages_description <> '') as page_has_content from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) WHERE p.parents_id = 0 and p.pages_status = 1 and pd.language_id = '" . (int)$languages_id . "' order by p.pages_sort_order asc");
		if (tep_db_num_rows($pages_query)) {
			while ($page = tep_db_fetch_array($pages_query)) {
				$subpages_query = tep_db_query("select p.pages_id, p.parents_id, pd.pages_title from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) where p.parents_id = " . intval($page['pages_id']) . " and p.pages_status = 1 and pd.language_id = '" . (int)$languages_id . "' order by p.pages_sort_order, pd.pages_title asc");
				if (tep_db_num_rows($subpages_query)) {
					if ($page['pages_id'] != 26) {
						echo '<h2>' . $page['pages_title'] . '</h2>';
						echo '<ul>';
					} else {
						$nav_links .= '<a href="' . tep_href_link(tep_clean_text_int($page['pages_title'])) . '">' .
							$page['pages_title'] . '</a><br>';
					}
					while ($subpage = tep_db_fetch_array($subpages_query)) {
						if ($subpage['parents_id'] == 26) {
							$nav_links .= '<a
                href="' . tep_href_link(tep_clean_text_int($page['pages_title']) . '/' . tep_clean_text_int($subpage['pages_title'])) . '">'
								. $subpage['pages_title'] . '</a><br>';
						} else {
							echo '<li>';
							echo '<a href="' . tep_href_link(tep_clean_text_int($page['pages_title']) . '/' . tep_clean_text_int($subpage['pages_title'])) . '">';
							echo $subpage['pages_title'];
							echo '</a>';
							echo '</li>';
						}

					}
					echo '
      </ul>';

				} else {
					$nav_links .= '<a href="' . tep_href_link(tep_clean_text_int($page['pages_title'])) . '">' .
						$page['pages_title'] . '</a><br>';
				}
			}
		}
    echo $nav_links . '</div>';
		echo '</div><div class="clear"></div></div>';
		echo "</div>";

	} else {

		echo '<div class="page-content-cont"><div class="page-content-cont-inner">' . $page['pages_description'] . '</div><div class="clear"></div></div>';

		if ($page['parents_id'] != 0) {
			$neighbors_query = tep_db_query('select pd.pages_title from ' . TABLE_PAGES . ' as p join ' . TABLE_PAGES_DESCRIPTION . ' as pd on(p.pages_id = pd.pages_id and pd.language_id = ' . intval($languages_id) . ') where p.parents_id = ' . intval($page['parents_id']) . ' and p.pages_status = 1 and p.pages_id <> ' . intval($page['pages_id']) . ' order by p.pages_sort_order');
			if (tep_db_num_rows($neighbors_query)) {
				echo '<ul class="page-main-ul">';
				while ($neighbor_page = tep_db_fetch_array($neighbors_query)) {
					echo '<li><a href="' . tep_href_link(tep_clean_text_int($main_page['pages_title']) . '/' . tep_clean_text_int($neighbor_page['pages_title'])) . '">' . $neighbor_page['pages_title'] . '</a></li>';
				}
				echo '</ul>';
			}
		}
	}



