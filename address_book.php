<h1><?php echo HEADING_TITLE; ?></h1>

<?php
  if ($messageStack->size('addressbook') > 0) {
    echo $messageStack->output('addressbook');
  }
?>

<div class="contentContainer">
  <h2><?php echo PRIMARY_ADDRESS_TITLE; ?></h2>

  <div class="contentText">
    <div class="ui-widget infoBoxContainer" style="float: right;">
      <div class="ui-widget-header infoBoxHeading"><?php echo PRIMARY_ADDRESS_TITLE; ?></div>

      <div class="ui-widget-content infoBoxContents">
        <?php echo tep_address_label($customer_id, $customer_default_address_id, true, ' ', '<br />'); ?>
      </div>
    </div>

    <?php echo PRIMARY_ADDRESS_DESCRIPTION; ?>
  </div>

  <div style="clear: both;"></div>

  <h2><?php echo ADDRESS_BOOK_TITLE; ?></h2>

  <div class="contentText">

<?php
if(empty($addresses_array)) {
    $addresses_query = tep_db_query("select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' order by firstname, lastname");
} else {
    $addresses = $addresses_query;
    $addresses->data_seek(0);
}
while ($addresses = tep_db_fetch_array($addresses_query)) {
    $format_id = tep_get_address_format_id($addresses['country_id']);
    ?>

    <div>
        <span style="float: right;"><?php echo tep_draw_button(SMALL_IMAGE_BUTTON_EDIT, 'document', tep_href_link(ADDRESS_BOOK_PROCESS_URL, 'edit=' . $addresses['address_book_id'], 'SSL')) . ' ' . tep_draw_button(SMALL_IMAGE_BUTTON_DELETE, 'trash', tep_href_link(ADDRESS_BOOK_PROCESS_URL, 'delete=' . $addresses['address_book_id'], 'SSL')); ?></span>
        <p><strong><?php echo tep_output_string_protected($addresses['firstname']); if (ACCOUNT_LAST_NAME == 'true') echo tep_output_string_protected(' ' . $addresses['lastname']); ?></strong><?php if ($addresses['address_book_id'] == $customer_default_address_id) echo '&nbsp;<small><i>' . PRIMARY_ADDRESS . '</i></small>'; ?></p>
        <p style="padding-left: 20px;"><?php echo tep_address_format($format_id, $addresses, true, ' ', '<br />'); ?></p>
    </div>

<?php
}
?>

  </div>

  <div class="buttonSet">

<?php
  if (tep_count_customer_address_book_entries() < MAX_ADDRESS_BOOK_ENTRIES) {
      $select_str = 'select ab.address_book_id';
      if (ACCOUNT_GENDER == 'true') $select_str .= ', ab.entry_gender';
      if (ACCOUNT_LAST_NAME == 'true') $select_str .= ', ab.entry_lastname';
      if (ACCOUNT_STREET_ADDRESS == 'true') $select_str .= ', ab.entry_street_address';
      if (ACCOUNT_POST_CODE == 'true') $select_str .= ', ab.entry_postcode';
      if (ACCOUNT_CITY == 'true') $select_str .= ', ab.entry_city';
      if (ACCOUNT_STATE == 'true') $select_str .= ', ab.entry_state';
      if (ACCOUNT_COUNTRY == 'true') $select_str .= ', ab.entry_country_id';
      $def_addr_book_query = tep_db_query($select_str . ' from ' . TABLE_ADDRESS_BOOK . ' as ab join ' . TABLE_CUSTOMERS . ' as c on (c.customers_default_address_id = ab.address_book_id) where c.customers_id = ' . (int)$customer_id . '');
      $def_addr_book = tep_db_fetch_array($def_addr_book_query);
      if(!in_array(null, $def_addr_book) && !in_array('',$def_addr_book) && !in_array('0', $def_addr_book)) {
          ?>
          <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_ADD_ADDRESS, 'home', tep_href_link(ADDRESS_BOOK_PROCESS_URL, '', 'SSL'), 'primary'); ?></span>
<?php
      }
  }
?>

    <?php echo tep_draw_button(IMAGE_BUTTON_BACK, 'triangle-1-w', tep_href_link(ACCOUNT_URL, '', 'SSL')); ?>
  </div>

  <p><?php echo sprintf(TEXT_MAXIMUM_ENTRIES, MAX_ADDRESS_BOOK_ENTRIES); ?></p>
</div>