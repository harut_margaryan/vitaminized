
<style>

    .cart-container {
        width:100%;
    }

    .cart-header {
        border: 1px; solid #ccc;
        width:100%;
        border-right:1px solid #ccc;
        border-top:1px solid #ccc;
        /*border-bottom:1px solid #ccc;*/
    }

    .cart-items {
        border: 1px; solid #ccc;
        width:100%;
        border-right:1px solid #ccc;
        border-bottom:1px solid #ccc;
        /*border-bottom:1px solid #ccc;*/
    }

    .cart-container .cart {
        float: left;
        padding: 5px 10px 5px 10px;
        border-left: 1px solid #ccc;
        font-size: 16px;
    }

    .cart.cart-item-1 {
        width:50px;
        text-align: center;
    }
    .cart.cart-item-2 {
        width:calc(60% - 90px);
    }
    .cart.cart-item-3 {
        width:15%;
    }
    .cart.cart-item-4 {
        width:calc(25% - 44px);
        text-align: right;
    }

    .cart-header .cart {
        background: #b1eaff;
        color: #fff;

    }

    .cart-icon {
        background: url(images/btn-basket-icon.png) no-repeat;
        width: 24px;
        height: 24px;
        margin: 0 auto;
    }

    .cart-items .cart {
        min-height: 80px;
    }

    .cart-container input {
        padding: 6px 10px 6px 10px;
        max-width: 60%;
        margin-bottom: 5px;
    }

    .cart-items .cart img {
        max-width: 30px;
        max-height: 75px;
        margin: 0 auto;
    }

    .subtotal {
        text-align: right;
        padding: 8px 0 5px 0;
        font-size: 17px;
        text-transform: uppercase;
    }
    
    .vitamines-btn-2 {
        cursor: pointer;
        float: right;
    }

    .cart .cart-item-3  input{
        float: left;
    }

    .remove-cart-item {
        float: right;
        background: #b1eaff;
        padding: 0px 10px 4px 9px;
        border-radius: 5px;
        text-transform: lowercase;
        color: #fff;
        cursor: pointer;
    }

    .remove-cart-item:hover {
        background: #a71818;;
    }

    @media (max-width: 760px) {
        #main-content {
            padding: 25px 10px 30px;
        }

        .page-content-cont {
            padding: 30px 0;
        }
    }

</style>


<h1 class="page-main-h1">WHAT's IN MY CART?</h1>

<div class="page-content-cont">

<div class="cart-container">
    <div class="cart-header">

        <div class="cart cart-item-1"><div class="cart-icon"></div></div>
        <div class="cart cart-item-2">Product</div>
        <div class="cart cart-item-3">Quantity</div>
        <div class="cart cart-item-4">Price</div>

        <div class="clear"></div>
    </div>
    <div class="cart-items-container">
        <!--<div class="cart-items">

            <div class="cart cart-item-1">
                <img src="http://localhost/vitaminized/images/products/zealut-dena-p4-1.png"/>
            </div>
            <div class="cart cart-item-2">Product</div>
            <div class="cart cart-item-3">
                <label>
                    <input type="number" value="15" min="0" max="99"/>
                </label>
            </div>
            <div class="cart cart-item-4">29</div>

            <div class="clear"></div>
        </div>-->
    </div>

    <div class="subtotal">
        Subtotal price: <span class="subtotal-price">29$</span>
    </div>

    <div class="cart-checkout">
        <div class="vitamines-btn-2 dena-btn dena-btn-blue" onclick="onCardClick()">
            CHECKOUT
        </div>
        <div class="clear"></div>
    </div>

</div>

</div>

<script>

    $(document).ready(function () {
      renderLinetItems();
    });

    /* Update Total Cart Pricing
     ============================================================ */
    function updateTotalCartPricing() {
      $('.subtotal-price').text(formatAsMoney(cart.subtotal));
      refreshCard();
    }

    /* Update details for item already in cart. Remove if necessary
     ============================================================ */
    function updateVariantInCart(cartLineItem, quantity) {
      var variantId = cartLineItem.variant_id;
      var cartLength = cart.lineItems.length;
      cart.updateLineItem(cartLineItem.id, quantity).then(function(updatedCart) {

        if (updatedCart.lineItems.length != cartLength) {
          renderLinetItems();
        } else {

        }

        updateTotalCartPricing();
      }).catch(function (errors) {
        console.log('Fail');
        console.error(errors);
      });
    }


    /* Format amount as currency
     ============================================================ */
    function formatAsMoney(amount, currency, thousandSeparator, decimalSeparator, localeDecimalSeparator) {
      currency = currency || '$';
      thousandSeparator = thousandSeparator || ',';
      decimalSeparator = decimalSeparator || '.';
      localeDecimalSeparator = localeDecimalSeparator || '.';
      var regex = new RegExp('(\\d)(?=(\\d{3})+\\.)', 'g');

      return currency + parseFloat(amount, 10).toFixed(2)
          .replace(localeDecimalSeparator, decimalSeparator)
          .replace(regex, '$1' + thousandSeparator)
          .toString();
    }

    function renderLinetItems() {

      if(cart.lineItems.length == 0) {
        $('.cart-container').html("Cart is empty");
        return false;
      }

      var str ="";

      for(var i=0; i < cart.lineItems.length; i++) {
        var lineItem = cart.lineItems[i];

        str += '<div class="cart-items">\
            <div class="cart cart-item-1">\
                <img src="'+ lineItem.image.src +'"/>\
            </div>\
            <div class="cart cart-item-2">'+ lineItem.title +'</div>\
            <div class="cart cart-item-3">\
                <label>\
                    <input type="number" value="'+ lineItem.quantity +'" onchange="onQuantityChange(this, '+lineItem.variant_id+')" ' +
                        'min="0" max="99"/>\
                        <div class="remove-cart-item" onclick="onRemoveItem('+lineItem.variant_id+')" >X</div>\
                </label>\
            </div>\
            <div class="cart cart-item-4">'+ lineItem.price +'</div>\
            <div class="clear"></div>\
            </div>';

      }

      $('.cart-items-container').html(str);
      updateTotalCartPricing();

    }

    function onQuantityChange(el, variant_id) {
      var cartLineItem = findCartItemByVariantId(variant_id);

      updateVariantInCart(cartLineItem, $(el).val());

    }

    function onRemoveItem(variant_id) {
      var cartLineItem = findCartItemByVariantId(variant_id);
      updateVariantInCart(cartLineItem, 0);
    }

    function onCardClick() {
        window.location.href = cart.checkoutUrl;
    }

</script>