<?php
require('includes/application_top.php');
if ($not_found)
    die("not found");

$oscTemplate->buildBlocks();

if (!$oscTemplate->hasBlocks('boxes_column_left'))
    $oscTemplate->setGridContentWidth($oscTemplate->getGridContentWidth() + $oscTemplate->getGridColumnWidth());

if (!$oscTemplate->hasBlocks('boxes_column_right'))
    $oscTemplate->setGridContentWidth($oscTemplate->getGridContentWidth() + $oscTemplate->getGridColumnWidth());

?>
    <!DOCTYPE html>
    <html <?= HTML_PARAMS ?>>
    <head>
        <meta charset="<?php echo CHARSET; ?>"/>
        <title><?= $og_title . STORE_NAME; ?></title>
        <base href="<?= (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG ?>"/>
        <meta name="robots" content="all"/>
        <meta name="robots" content="index,follow"/>
        <meta name="googlebot" content="all"/>
        <meta name="googlebot" content="index,follow"/>
        <meta name="application-name" content="<?= STORE_NAME; ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="title" content="<?= $og_title; ?>"/>
        <meta name="description" content="<?= $og_desc; ?>"/>
        <meta name="keywords" content="<?= $og_text; ?>"/>
        <meta property="og:title" content="<?= $og_title; ?>"/>
        <meta property="og:description" content="<?= $og_desc; ?>"/>
        <meta property="og:image" content="<?= $og_pic; ?>"/>
        <meta property="og:url" content="<?= $og_url; ?>"/>
        <meta property="og:site_name" content="<?= STORE_NAME; ?>"/>
        <meta itemprop="name" content="<?= $og_title ?>"/>
        <meta itemprop="description" content="<?= $og_desc; ?>"/>
        <meta itemprop="image" content="<?= $og_pic; ?>"/>
        <meta itemprop="url" content="<?= $og_url; ?>"/>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="ext/jquery/ui/smoothness/jquery-ui-1.10.4.min.css"/>
        <link rel="stylesheet" type="text/css" href="ext/fontello/css/fontello.css"/>
        <link rel="stylesheet" type="text/css" href="ext/fontello/css/animation.css"/>

        <script type="text/javascript" src="ext/jquery/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="ext/jquery/ui/jquery-ui-1.10.4.min.js"></script>

        <script type="text/javascript" src="ext/vue/node_modules/vue/dist/vue.js"></script>
        <script type="text/javascript" src="ext/vue/bundle.js"></script>
        <script type="text/javascript" src="ext/vue/app.js"></script>

        <?php if (tep_session_is_registered('visit_info')): ?>
            <script type="text/javascript">
                $.ajax('ajax/ck');
            </script>
        <?php endif; ?>

        <?php if (tep_not_null(JQUERY_DATEPICKER_I18N_CODE)) : ?>
            <script type="text/javascript"
                    src="ext/jquery/ui/i18n/jquery.ui.datepicker-<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>.js"></script>
            <script type="text/javascript">
                $.datepicker.setDefaults($.datepicker.regional['<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>']);
            </script>
        <?php endif; ?>

        <script>
            $(function () {
                $("#site-nav").accordion({
                    collapsible: true,
                    heightStyle: "content",
                    active: <?= !empty($main_page) ? $main_page['index'] + 1 : ($main_get == PRODUCT_URL ? 0 : 'false') ?>
                });
            });
        </script>
        <?php if (!$path): ?>
            <link rel="stylesheet" href="ext/unslider-master/dist/css/unslider.css"/>
            <link rel="stylesheet" href="ext/unslider-master/dist/css/unslider-dots.css"/>
            <script src="ext/unslider-master/dist/js/unslider-min.js"></script>
            <script>
                $(document).ready(function () {
                    var slider = $(".my-slider").unslider({
                        autoplay: false,
                        delay: 10000,
                        arrows: false
                        /*arrows: {
                         prev: '<div class="unslider-arrow prev"></div>',
                         next: '<div class="unslider-arrow next"></div>'
                         }*/
                    });
                    var secondSlider = $('#middle-line').unslider({
                        autoplay: false,
                        arrows: false
                    });
                    slider.on('unslider.change', function (event, index, slide) {
                        secondSlider.unslider('animate:' + index);
                    });
                });
            </script>
        <?php endif; ?>
        <script src="node_modules/shopify-buy/dist/shopify-buy.umd.polyfilled.js"></script>
        <script src="ext/jquery/jquery.form.min.js"></script>
        <script src="ext/my.js"></script>

        <link rel="stylesheet" type="text/css"
              href="ext/960gs/<?php echo((stripos(HTML_PARAMS, 'dir="rtl"') !== false) ? 'rtl_' : ''); ?>960_24_col.css"/>
        <link rel="stylesheet" type="text/css" href="stylesheet.css"/>
        <link rel="stylesheet" type="text/css" href="mobile.css"/>
        <?php if ($current_language == 'ru') { ?>
            <link
                href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&subset=latin,cyrillic,cyrillic-ext,latin-ext'
                rel='stylesheet' type='text/css'>
            <style> body, .ui-widget, .page-main-ul li a, .jg-counter-container input, .jg-tabs-head, button {
                    font-family: 'Roboto Condensed', sans-serif !important;
                } </style>
        <?php } else { ?>
            <link
                href='http://fonts.googleapis.com/css?family=Oswald:300,400,700&subset=latin,cyrillic,cyrillic-ext,latin-ext'
                rel='stylesheet' type='text/css'>
            <style> body, .ui-widget, .page-main-ul li a, .jg-counter-container input, .jg-tabs-head, button {
                    font-family: 'Oswald', sans-serif !important;
                } </style>
        <?php } ?>
        <link rel="stylesheet"
              href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700&subset=latin,cyrillic,cyrillic-ext,latin-ext"/>
        <?php echo $oscTemplate->getBlocks('header_tags'); ?>
        <script>
            var global_settings = {};
            global_settings.dir = '<?php $save_with_partners_key = $with_partners_key; $with_partners_key = false; echo tep_href_link(); $with_partners_key = $save_with_partners_key;?>';
            global_settings.ajax_url = '<?php echo ($PARTNER ? HTTP_SERVER_PARTNER : HTTP_SERVER) . '/ajax';?>';
            global_settings.language = '<?php echo $current_language;?>';
            <?php
            switch ($current_language) {
                case 'ru':
                    $sdk_lng = 'ru_RU';
                    break;
                case 'es':
                    $sdk_lng = 'es_ES';
                    break;
                default:
                    $sdk_lng = 'en_US';
                    break;
            } ?>
            global_settings.fb_sdk_language = '<?= $sdk_lng ?>';
            global_settings.fb_app_id = '<?= FB_APP_ID ?>';
        </script>
    </head>
    <body>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-90628350-1', 'auto');
      ga('send', 'pageview');
    </script>

    <script src="https://apis.google.com/js/platform.js" async defer>
        {
            lang: global_settings.language
        }
    </script>
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1264857350260263',
          xfbml      : true,
          version    : 'v2.8'
        });
        FB.AppEvents.logPageView();
      };
      (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>

    <div>
        <div class="header-logo">
            <a href="<?= tep_href_link()?>">
                <?= tep_image('vitaminized_logo.jpg', STORE_NAME, '', '', 'style="width:260px;"')?>
            </a>
        </div>
        <div class="v-cart-place">
            <a class="cart-link" href="<?= tep_href_link('cart')?>">
                <div class="cart">
                    <div class="cart-menu-icon"></div>
                    CART
                    <span class="cart-quantity"></span>
                  <div class="clear"></div>
                </div>
            </a>
            <!--<div class="clear-card" onclick="onCardClear()">CLEAR</div>-->
        </div>

    </div>

    <div id="content">
        <div id="main">
            <?php
            //--------------------------------------begin-------------------------------------------
            if (tep_not_null($path)) { ?>
                <div id="header-back">
                    <div class="head-bg"
                         style="background-image: url('<?php echo tep_image_path(DIR_WS_IMAGES_PRODUCTS_BACKGROUNDS . ($main == PRODUCT_URL ? $product_info['products_id'] : '5') . '.jpg'); ?>');"></div>
                </div>
                <div id="main-content">
                    <?php require($path); ?>
                </div>
                <?php
            } elseif ($PARTNER) { ?>
                <?= TEXT_PARTNER_WELCOME ?>
                <br/>
                <a href="<?php echo tep_href_link(LOGIN_URL) . '#signInTab' ?>"
                   onclick="return showLoginPopup('signInTab','<?php echo $og_url; ?>');"><?php echo tep_draw_button(HEADER_TITLE_SIGN_IN, 'person') ?></a>
                <a href="<?php echo tep_href_link(LOGIN_URL) . '#signUpTab' ?>"
                   onclick="return showLoginPopup('signUpTab','<?php echo $og_url; ?>');"><?php echo tep_draw_button(HEADER_TITLE_SIGN_UP, 'person') ?></a>
                <?php
            } else {
                $category_depth = 'top';
                if (isset($cPath) && tep_not_null($cPath)) {
                    $category_depth = 'products';
                }

                if ($category_depth == 'products' || ($main === MANUFACTURERS_URL && tep_not_null($item))) {

                } else { // default page
                    require(DIR_WS_INCLUDES . 'header.php'); ?>

                    <section class="vitamines-menu">
                        <div class="vitamines-menu-main">
                            <div class="vitamines-menu-head">
                                <h1 class="page-main-h1-main">
                                    <?= tep_image('logo_icon_only.png', '', 45) ?> Nutrition Supplements and Vitamins for a Healthy Lifestyle
                                </h1>
                            </div>
                            <div class="vitamines-menu-cont">
                                <ul>
                                    <?php
                                    $prods_query = tep_db_query('select p.products_id, p.shopify_id, p.products_image, p.products_price, pd.products_name, pd.products_short_desc, pd.products_description from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd using(products_id) where p.products_status = 1 and pd.language_id = ' . intval($languages_id) . ' order by rand() limit 4');
                                    while ($prod = tep_db_fetch_array($prods_query)) {
                                        echo '<li>';
                                        echo '<div class="vitamines-product-main">' . $prod['products_name'] . '<sup>®</sup></div>';
                                        echo '<a href="' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($prod['products_name'])) . '">';
                                        echo tep_image(DIR_WS_IMAGES_PRODUCTS . $prod['products_image'] . '-1' . IMAGE_EXTENSION);
                                        echo '</a>';
                                        echo '<span>$ ' . round($prod['products_price'], 2) . '</span>';
                                        //echo tep_draw_form('cart_quantity', tep_href_link('', 'action=buy_now&'), 'post', 'id="form-' . $prod_info['products_id'] . '"');
                                        ?>
                                        <?= tep_draw_hidden_field('products_id', $prod['products_id']); ?>
                                        <button class="vitamines-btn-2 dena-btn dena-btn-blue" onclick="buyNowQuick(<?=$prod['shopify_id']?>)"><?= TEXT_BUY ?></button>
                                        <button class="vitamines-btn-2 dena-btn dena-btn-blue" onclick="addToCart(<?=$prod['shopify_id']?>, this)">Add to CART</button>
                                        <!--</form>-->
                                        <?php
                                        echo '<p>' . strip_tags($prod['products_short_desc']) . '</p></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </section>
                    <div id="middle-line" class="elgeno-headline">
                        <ul>
                            <?php foreach ($all_products as $prod) : ?>
                                <li>
                                    <div class="item"
                                         style="background-image: url('<?= tep_image_path(DIR_WS_IMAGES_PRODUCTS_BACKGROUNDS . $prod['products_id'] . '.jpg'); ?>')">
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php
                    $pages_arr = [];
                    $i = 0;
                    //$pages_query = tep_db_query("select p.pages_id, pd.pages_title, (pd.pages_description is not null and pd.pages_description <> '') as page_has_content from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) where p.parents_id = 0 and p.pages_status = 1 and pd.language_id = '" . (int)$languages_id . "' order by p.pages_sort_order asc limit 4");
                    $pages_query = tep_db_query("select p.pages_id, pd.pages_title, (pd.pages_description is not null and pd.pages_description <> '') as page_has_content from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) where p.pages_id in (26, 35, 8, 16) AND pd.language_id = '" . (int)$languages_id . "' ");
                    if (tep_db_num_rows($pages_query)) {
                        while ($page = tep_db_fetch_array($pages_query)) {
                            $pages_arr[$i] = array('title' => $page['pages_title'], 'link' => tep_href_link(tep_clean_text_int($page['pages_title'])));
                            $subpages_query = tep_db_query("select p.pages_id, pd.pages_title from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) where p.parents_id = " . intval($page['pages_id']) . " and p.pages_status = 1 and pd.language_id = '" . (int)$languages_id . "' order by p.pages_sort_order, pd.pages_title asc");
                            if (tep_db_num_rows($subpages_query)) {
                                while ($subpage = tep_db_fetch_array($subpages_query)) {
                                    $pages_arr[$i][] = array('title' => $subpage['pages_title'], 'link' => tep_href_link(tep_clean_text_int($page['pages_title']) . '/' . tep_clean_text_int($subpage['pages_title'])));
                                }
                            }
                            ++$i;
                        }
                    }
                    ?>

                    <section class="vitamines-menu">
                        <div class="vitamines-menu-main">
                            <div class="vitamines-menu-cont-2">
                                <ul>
                                    <li><a href="<?= $pages_arr[2]['link'] ?>" ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="block-header orange-1">
                                                        <?= tep_image('icon-affiliate-program.png') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 orange-1">
                                                    <div class="block-header-text">
                                                        <?= $pages_arr[2]['title'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= $pages_arr[3]['link'] ?>" ?>

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="block-header blue-1">
                                                        <?= tep_image('icon-quality.png') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 blue-1">
                                                    <div class="block-header-text">
                                                        <?= $pages_arr[3]['title'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= $pages_arr[0]['link'] ?>" ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="block-header green-1">
                                                        <?= tep_image('icon-shipping-payment.png') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 green-1">
                                                    <div class="block-header-text">
                                                        <?= $pages_arr[0]['title'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= $pages_arr[1]['link'] ?>" ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="block-header grey-1">
                                                        <?= tep_image('icon-export-distribution.png') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12 grey-1">
                                                    <div class="block-header-text">
                                                        <?= $pages_arr[1]['title'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>

                <?php }
            }
            ?>

        </div><!-- main //-->
        <div class="clear"></div>
    </div>
    <?php require(DIR_WS_INCLUDES . 'aside.php'); ?>
    <?php require(DIR_WS_INCLUDES . 'footer.php'); ?>

    <?= $oscTemplate->getBlocks('footer_scripts') ?>

    </body>
    </html>

<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>