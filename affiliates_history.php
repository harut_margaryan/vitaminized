<h1><?php echo HEADING_TITLE; ?></h1>

<div class="contentContainer">
    <div class="contentText">
        <div id="aff-hist-chart"></div>
        <div id="aff-hist-datepicker"></div>
        <div class="aff-hist-table-cont"></div>
        <script>
            var text_no_hist = '<?php echo TEXT_NO_DATA;?>';
            var hist_anal_url = '<?php echo HTTP_SERVER_PARTNER . '/analytic/aff_hist?language=' . $current_language;?>';
            function formatURL(from, to) {
                var res = global_settings.ajax_url + '/get_aff_hist?language=' + global_settings.language;
                from && (res += '&from=' + from);
                to && (res += '&to=' + to);
                return res;
            }


            function loadContent(from, to) {
                if (from) {
                    from.setHours(0,0,0,0);
                    from = from.getTime();
                } else {
                    from = '';
                }

                if (to) {
                    to.setHours(0,0,0,0);
                    to = to.getTime() + 3600 * 24 * 1000 - 1;
                } else {
                    to = '';
                }

                $table_cont.addClass('loading');
                $.getJSON(formatURL(from, to), null, function(response) {
                    $table_cont.removeClass('loading');
                    if (response.html) $table_cont.html(response.html);
                });
            }

            function initContent() {
                $.getJSON(global_settings.ajax_url + '/get_aff_hist_bounds', null, function(response) {
                    if (response.minDate && response.maxDate) {
                        var minDate = new Date(parseInt(response.minDate)), maxDate = new Date(parseInt(response.maxDate));
                        minDate.setHours(0,0,0,0);
                        maxDate.setHours(0,0,0,0);
                        var date_from = maxDate.getTime() - 24 * 3600000 * 7; //1 week
                        date_from = date_from < minDate.getTime() ? minDate : new Date(date_from);
                        var date_ranger = new DateRange({
                            dateFrom : date_from,
                            minDate : minDate,
                            dateTo : maxDate,
                            maxDate : maxDate,
                            dateFormat : 'dd.mm.yy',
                            numberOfMonths : 2,
                            changeMonth : true,
                            minRange : 1,
                            container : '#aff-hist-datepicker'
                        });
                        date_ranger.onChange(loadContent);
                        loadContent(date_from, maxDate);

                        $chart_cont.lineChart({url: hist_anal_url});
                    } else {
                        $table_cont.html(text_no_hist);
                    }
                });
            }

            var $table_cont = $('.aff-hist-table-cont');
            var $chart_cont = $('#aff-hist-chart');
            initContent();
        </script>
    </div>    
</div>