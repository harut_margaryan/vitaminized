<script type="text/javascript">
    function checkForm() {
        var error = 0;
        //var error_message = "<?php echo JS_ERROR; ?>";
        var error_message = '<div style="background: red; border: 1px double blue">';

        var review = document.product_reviews_write.review.value;

        if (review.length < <?php echo REVIEW_TEXT_MIN_LENGTH; ?>) {
            error_message = error_message + "<?php echo JS_REVIEW_TEXT; ?><br>";
            error = 1;
        }

        if  (!document.product_reviews_write.rating || document.product_reviews_write.rating.value < 1) {
            error_message = error_message + "<?php echo JS_REVIEW_RATING; ?><br>";
            error = 1;
        }

        if (document.product_reviews_write.customers_name && $.trim(document.product_reviews_write.customers_name.value) == '') {
            error_message = error_message + "<?php echo JS_REVIEW_CUSTOMERS_NAME; ?><br>";
            error = 1;
        }

        if (error == 1) {
            $('#writeReviewMessages').html(error_message  + '</div>');
            return false;
        } else {
            return true;
        }
    }
</script>
<?php

if (tep_session_is_registered('customer_id')) {
     $customer_query = tep_db_query("select customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
    $customer = tep_db_fetch_array($customer_query);
    $customers_name = tep_db_input($customer['customers_firstname']) . ' ' . tep_db_input($customer['customers_lastname']);
    $customers_email = '';
    $customers_id = (int)$customer_id;
}
?>

<?php echo tep_draw_form('product_reviews_write', tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product_info['products_name']), 'action=process') . '#reviews', 'post', 'id="write_review_form"', true); ?>

<table border="0" cellspacing="0" cellpadding="2">
    <tr>
        <td colspan="2" id="writeReviewMessages"></td>
    </tr>
    <?php
    if (!tep_session_is_registered('customer_id')) {
    ?>

    <tr>
        <td class="fieldKey"><?php echo SUB_TITLE_NAME; ?></td>
        <td class="fieldValue"><?php echo tep_draw_input_field('customers_name', '', 'required');?></td>
    </tr>
    <?php
    }
    ?>
    <tr>
        <td class="fieldKey" valign="top"><?php echo SUB_TITLE_REVIEW; ?></td>
        <td class="fieldValue"><?php echo tep_draw_textarea_field('review', 'soft', 60, 15) . '<br /><span style="float: right;">' . TEXT_NO_HTML . '</span>'; ?></td>
    </tr>
    <?php
    if (!tep_session_is_registered('customer_id')) {
        echo '<tr>';
        echo '    <td class="fieldKey" valign="top">' . SUB_TITLE_EMAIL . '</td>';
        echo '    <td  class="fieldValue">' . tep_draw_input_field('customers_email', '', 'required') . '</td>';
        echo '</tr>';
    }
    ?>
    <tr>
        <td class="fieldKey"><?php echo SUB_TITLE_RATING; ?></td>
        <td class="fieldValue"><span class="ratingStars"></span></td>
    </tr>
</table>

<div class="buttonSet">
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_WRITE_REVIEW, 'triangle-1-e', null, 'primary'); ?></span>
</div>
</form>
<script>
    var ratingStar = $('.ratingStars').eq(0);
    $(function () {ratingStar = new JgRatingRanger(ratingStar);});

    var writeReviewMessageStack = $('#writeReviewMessages');
    function writeReviewSuccess (data) {
        if (data.error) {
            writeReviewMessageStack.html(data.error);
        } else if (data.success) {
            writeReviewMessageStack.html(data.success);
            $('#write_review_form').resetForm();
            ratingStar.rating = 0;
            ratingStar.reset();
        }
    }
    var writeReviewFormOptions = {
        success: writeReviewSuccess,
        beforeSubmit : checkForm,
        dataType: 'json'
    };
    $('#write_review_form').ajaxForm(writeReviewFormOptions);
</script>