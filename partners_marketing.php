<h1><?php /** @var $method_info string */
    echo $method_info['method_name']; ?></h1>
<div class="contentContainer">
    <div class="contentText">
        <script>
            var nowSending = false;
            function sendMail(m) {
                if(nowSending) return;
                var button = $('#sender-' + m);
                var resp_cont = $('#resp-cont-' + m);
                nowSending = true;
                button.addClass('sending');
                button.attr('disabled', true);
                resp_cont.fadeOut();
                $.ajax({
                    url : '<?php echo HTTP_SERVER_PARTNER;?>/ajax/send_mail?m=' + m,
                    success : function (resp) {
                        nowSending = false;
                        button.attr('disabled', false);
                        button.removeClass('sending');
                        if (resp == 'ok') {
                            resp_cont.html('<?php echo TEXT_MAIL_SENT;?>');
                            resp_cont.removeClass('no');
                            resp_cont.addClass('ok');
                        } else {
                            resp_cont.html('<?php echo TEXT_MAIL_NOT_SENT;?>');
                            resp_cont.removeClass('ok');
                            resp_cont.addClass('no');
                        }
                        resp_cont.fadeIn();
                    },
                    error : function () {
                        nowSending = false;
                        button.attr('disabled', false);
                        button.removeClass('sending');
                        resp_cont.html('<?php echo TEXT_MAIL_NOT_SENT;?>');
                        resp_cont.removeClass('ok');
                        resp_cont.addClass('no');
                        resp_cont.fadeIn();
                    }
                });
            }
        </script>
        <?php
        $content = '';
            switch($method_info['method_id']) {
                case 1: //posts
                    $content .= '<div class="material-post">';
                    $content .= '<h3>' . $material['material_title'] . '</h3>';
                    $content .= '<p>' . $material['material_text'] . '</p>';
                    $content .= '</div>';
                    break;
                case 2: //banners
                    $content .= '<div class="material-banner">';
                    $content .= '<h3>' . $material['material_title'] . '</h3>';
                    $content .= '<div id="marketing-banner-' . $material['material_id'] . '"></div>';
                    $content .= '<script type="text/javascript">swfobject.embedSWF("' . DIR_WS_MARKETING_BANNERS . $material['material_file'] . '", "marketing-banner-' . $material['material_id'] . '", "500", "' . 500 * $material['material_height'] / $material['material_width'] . '", "9.0.0");</script>';
                    $content .= '</div>';
                    break;
                case 3: //pics
                    $content .= '<div class="material-pic">';
                    $content .= '<h3>' . $material['material_title'] . '</h3>';
                    $content .= tep_image(DIR_WS_MARKETING_PICS . $material['material_pic'], $material['material_title'], 500);
                    $content .= '</div>';
                    break;
                case 4: //mails
                    $content .= '<div class="material-mail">';
                    $content .= '<script language="JavaScript">function autoResize(id){var newheight;var newwidth;if(document.getElementById){newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;newwidth=document.getElementById(id).contentWindow.document .body.scrollWidth;}document.getElementById(id).height= (newheight) + "px";document.getElementById(id).width= (newwidth) + "px";}</script>';
                    $content .= '<h3>' . $material['material_title'] . '</h3>';
                    $content .= '<div style="overflow: hidden;height:24px;"><button onclick="sendMail(' . $material['material_id'] . ');" id="sender-' . $material['material_id'] . '"><span class="text">' . TEXT_SEND_MAIL . '</span></button><span class="resp-cont" id="resp-cont-' . $material['material_id'] . '"></span></div>';
                    $content .= '<div>' . $material['material_html'] . '</div>';
                    $content .= '</div>';
                    break;
                case 5: //statuses
                    $content .= '<div class="material-status">';
                    $content .= '<h3>' . $material['material_title'] . '</h3>';
                    $content .= '</div>';
                    break;
                case 6: //shares
                    $content .= '<div class="material-share">';
                    $content .= tep_image(DIR_WS_MARKETING_SHARES . $material['material_pic'], $material['material_title'], 40);
                    $content .= '<h3>' . $material['material_title'] . '</h3>';
                    $content .= '<p>' . $material['material_desc'] . '</p>';
                    $content .= '</div>';
                    break;
            }

        echo $content;
        ?>
    </div>
</div>