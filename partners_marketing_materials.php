<h1><?php echo HEADING_TITLE;?></h1>
<div class="contentContainer">
    <div class="contentText">
        <div id="marketing-container"></div>
        <script>
        "use strict";
        $(document).ready(function () {
            function MarketingFilters(contentObj) {
                this.content = contentObj;
                this.filters_container = $('#marketing-filter');
                this.filters_container.append('<div class="curtain"></div>');
                var self_this = this;
                this.methods = {};
                this.products = {};
                this.languages = {};
                this.tags = {};
                this.filter_button = {};

                this.methods.inputs = this.filters_container.find('input[type="radio"][name="method"]');
                this.products.inputs = this.filters_container.find('input[type="checkbox"][name="products[]"]');
                this.languages.inputs = this.filters_container.find('input[type="checkbox"][name="languages[]"]');

                this.methods.inputs.change(function () {
                    self_this.onChange();
                });

                this.products.inputs.change(function () {
                    self_this.onChange();
                });

                this.languages.inputs.change(function () {
                    self_this.onChange();
                });


                this.tags.container = $('#tags-block');
                this.languages.container = $('#language-block');

	            /*this.filter_button.button = $('#marketing-do-filter');
	            this.filter_button.container = $('#marketing-filter-cont');

	            this.filter_button.button.click(this.content.update.bind(this.content));*/

                this.tags.content = this.tags.container.find('.marketing-block-body');
            }

            MarketingFilters.prototype.update = function (filters) {
                var i, n;
                if (filters.method) {
                    this.methods.inputs.filter('[value="' + filters.method + '"]').prop('checked', true);
                } else {
                    this.methods.inputs.map(function () {
                        this.checked = false;
                    });
                }

                if (filters.products) {
                    if (filters.products instanceof Array) {
                        for (i = 0, n = filters.products.length; i < n; ++i) {
                            this.products.inputs.filter('[value="' + filters.products[i] + '"]').prop('checked', true);
                        }
                    } else if (filters.products == 'all') {
                        this.products.inputs.map(function () {
                            this.checked = true;
                        });
                    } else {
                        this.products.inputs.map(function () {
                            this.checked = false;
                        });
                    }
                } else {
                    this.products.inputs.map(function () {
                        this.checked = false;
                    });
                }

                if (filters.languages) {
                    if (filters.languages instanceof Array) {
                        for (i = 0, n = filters.languages.length; i < n; ++i) {
                            this.languages.inputs.filter('[value="' + filters.languages[i] + '"]').prop('checked', true);
                        }
                    } else if (filters.languages == 'all') {
                        this.languages.inputs.map(function () {
                            this.checked = true;
                        });
                    } else {
                        this.languages.inputs.map(function () {
                            this.checked = false;
                        });
                    }
                } else {
                    this.languages.inputs.map(function () {
                        this.checked = false;
                    });
                }

                this.tags.content.empty();
                if (filters.tags) {
                    if (filters.tags instanceof Array) {
                        for (i = 0, n = filters.tags.length; i < n; ++i) {
                            this.tags.content.append('<label><input type="checkbox" name="tags[]" value="' + filters.tags[i].id + '"' + (filters.tags[i].checked ? ' checked' : '') + '/><span>' + filters.tags[i].name + '</span></label>');
                        }
                        if (i) {
                            this.tags.container.fadeIn();
                            this.tags.inputs = this.filters_container.find('input[type="checkbox"][name="tags[]"]');
                            var self_this = this;
                            this.tags.inputs.change(function () {
                                self_this.onChange(false);
                            });
                        } else {
                            this.tags.container.fadeOut();
                        }
                    } else {
                        this.tags.container.fadeOut();
                    }
                }
            };

            MarketingFilters.prototype.onChange = function (clear_tags) {
                clear_tags = clear_tags == undefined ? true : clear_tags;
                var filters = {};
                filters.method = decodeURIComponent(this.methods.inputs.filter(":checked").serialize());
                filters.products = decodeURIComponent(this.products.inputs.filter(":checked").serialize());
                filters.languages = decodeURIComponent(this.languages.inputs.filter(":checked").serialize());
                if (!clear_tags) {
                    filters.tags = decodeURIComponent(this.tags.inputs.filter(":checked").serialize());
                } else {
                    filters.tags = 'tags=all';
                }

                var newHash = filters.method;
                if (filters.products) {
                    newHash += '&' + filters.products;
                }
                if (filters.languages) {
                    newHash += '&' + filters.languages;
                }
                if (filters.tags) {
                    newHash += '&' + filters.tags;
                }
                window.location.hash = newHash;
            };

            function JgPages(cont, contentObj) {
                this.container = $(cont);
                this.content = contentObj;
                this.show = 10;

                this.list = $('<ul>');
                this.list.addClass('jg-pages-list');
                this.container.append(this.list);

                this.list.hide();
                this.list.empty();

                var self_this = this;
                this.list.click(function (ev) {
                    var $target = $(ev.target);
                    if ($target.hasClass('jg-page')) {
                        self_this.to($target.data('page'));
                    }
                });
            }

            JgPages.prototype.update = function(newOpts) {
                this.total = newOpts.total;
                this.current = newOpts.current || 1;
                this.show = newOpts.show || this.show;

                this.reDraw();
            };

            JgPages.prototype.to = function (newpage) {
                switch (newpage) {
                    case -1 :
                        newpage = 1;
                        break;
                    case -2 :
                        newpage = this.current*1 - 1;
                        break;
                    case -3 :
                        newpage = this.current*1 + 1;
                        break;
                    case -4 :
                        newpage = this.total;
                        break;
                }

                this.current = newpage;

                var hashInfo = this.content.getHashInfo();
                hashInfo.page = this.current;
                this.content.setHashInfo(hashInfo);

                this.reDraw();
            };

            JgPages.prototype.current = function () {
                return this.current;
            };

            JgPages.prototype.reDraw = function () {
                this.list.hide();
                this.list.empty();

                if (this.total == 1) return;

                var leftSide = this.show / 2, rightSide;

                var start, end;
                if (this.current - leftSide < 1) {
                    start = 1;
                    leftSide = this.current - 1;
                    rightSide = this.show - leftSide - 1;
                    if (this.current + rightSide >= this.total) {
                        end = this.total;
                    } else {
                        end = this.current + rightSide;
                    }
                } else {
                    rightSide = this.show - leftSide - 1;
                    if (this.current + rightSide > this.total) {
                        end = this.total;
                        rightSide = this.total - this.current;
                        leftSide = this.show - rightSide - 1;
                        if (this.current - leftSide > 0) {
                            start = this.current - leftSide;
                        } else {
                            start = 1;
                        }
                    } else {
                        start = this.current - leftSide;
                        end = this.current + rightSide;
                    }
                }

                if (this.current > 2) {
                    this.list.append('<li class="jg-page" data-page="-1"><<</li>');
                }

                if (this.current > 1) {
                    this.list.append('<li class="jg-page" data-page="-2"><</li>');
                }

                for (var i = start; i <= end; ++i) {
                    this.list.append('<li class="jg-page' + (this.current == i ? ' current' : '') + '" data-page="' + i + '">' + i + '</li>');
                }

                if (this.current < this.total) {
                    this.list.append('<li class="jg-page" data-page="-3">></li>');
                }

                if (this.current < this.total - 1) {
                    this.list.append('<li class="jg-page" data-page="-4">>></li>');
                }
                this.list.show();
            };

            function ContentManager(container) {
                this.container = $(container);
                this.content = $('<div id="marketing-content">');
                this.pages_container = $('<div id="marketing-pages">');
                this.curtain = $('<div class="curtain">');
                this.loader = $('<?php echo tep_image(DIR_WS_IMAGES . 'loader.gif');?>');
                this.loader.addClass('marketing-loader');
                this.container.append(this.content);
                this.container.append(this.pages_container);
                this.container.append(this.curtain);
                this.curtain.append(this.loader);
                this.pages = new JgPages(this.pages_container, this);

                $(window).on('hashchange', this.update.bind(this));

                this.filters = new MarketingFilters(this);
                this.loadTimer = 0;

                this.update();
                return this;
            }

            ContentManager.prototype.getHashInfo = function () {
                var filterObj = {};
                filterObj.method = '';
                filterObj.page = '';
                filterObj.products = [];
                filterObj.languages = [];
                filterObj.tags = [];
                if (window.location.hash) {
                    var filterArr = window.location.hash.substr(1).split('&');
                    for (var i = 0, n = filterArr.length; i < n; ++i) {
                        if (filterArr[i]) {
                            var filter_val = filterArr[i].split('=');
                            if (filter_val.length == 2) {
                                switch (filter_val[0]) {
                                    case 'method':
                                        filterObj.method = filter_val[1];
                                        break;
                                    case 'page':
                                        filterObj.page = filter_val[1];
                                        break;
                                    case 'products[]':
                                        filterObj.products.push(filter_val[1]);
                                        break;
                                    case 'products':
                                        filterObj.products = filter_val[1];
                                        break;
                                    case 'languages[]':
                                        filterObj.languages.push(filter_val[1]);
                                        break;
                                    case 'languages':
                                        filterObj.languages = filter_val[1];
                                        break;
                                    case 'tags[]':
                                        filterObj.tags.push(filter_val[1]);
                                        break;
                                    case 'tags':
                                        filterObj.tags = filter_val[1];
                                        break;
                                }
                            }
                        }
                    }
                }
                return filterObj;
            };

            ContentManager.prototype.setHashInfo = function (hashObj) {
                hashObj.method = hashObj.method || 1;
                hashObj.page = hashObj.page || 1;
                hashObj.products = hashObj.products || [];
                hashObj.languages = hashObj.languages || [];
                hashObj.tags = hashObj.tags || [];

                var hashArray = [];
                hashArray.push('method=' + (hashObj.method));
                hashArray.push('page=' + (hashObj.page));
                var i, n;
                if (hashObj.products instanceof Array) {
                    for (i = 0, n = hashObj.products.length; i < n; ++i) {
                        hashArray.push('products[]=' + hashObj.products[i]);
                    }
                } else {
                    hashArray.push('products=' + (hashObj.products));
                }

                if (hashObj.languages instanceof Array) {
                    for (i = 0, n = hashObj.languages.length; i < n; ++i) {
                        hashArray.push('languages[]=' + hashObj.languages[i]);
                    }
                } else {
                    hashArray.push('languages=' + (hashObj.languages));
                }

                if (hashObj.tags instanceof Array) {
                    for (i = 0, n = hashObj.tags.length; i < n; ++i) {
                        hashArray.push('tags[]=' + hashObj.tags[i]);
                    }
                } else {
                    hashArray.push('tags=' + (hashObj.tags));
                }

                window.location.hash = hashArray.join('&');
            };

            ContentManager.prototype.update = function () {
                var hashInfo = this.getHashInfo();
                if (hashInfo.method == '') {
                    this.setHashInfo({
                        method: 1,
                        page: 1,
                        products: 'all',
                        languages: 'all',
                        tags: 'all'
                    });
                    return;
                }

                var url = window.location.protocol + "//" + window.location.hostname + '/mm_getter.php?';

                url += 'method=' + hashInfo.method;
                url += '&page=' + hashInfo.page;
                var i, n;
                if (hashInfo.languages instanceof Array) {
                    for (i = 0, n = hashInfo.languages.length; i < n; ++i) {
                        url += '&languages[]=' + hashInfo.languages[i];
                    }
                } else {
                    url += '&languages=' + hashInfo.languages;
                }

                if (hashInfo.products instanceof Array) {
                    for (i = 0, n = hashInfo.products.length; i < n; ++i) {
                        url += '&products[]=' + hashInfo.products[i];
                    }
                } else {
                    url += '&products=' + hashInfo.products;
                }

                if (hashInfo.tags instanceof Array) {
                    for (i = 0, n = hashInfo.tags.length; i < n; ++i) {
                        url += '&tags[]=' + hashInfo.tags[i];
                    }
                } else {
                    url += '&tags=' + hashInfo.tags;
                }

                url += '&language=<?php echo $current_language?>';

                var self_this = this;
                self_this.container.addClass('load');
                self_this.filters.filters_container.addClass('load');

                clearTimeout(this.loadTimer);
                this.loadTimer = setTimeout(function () {
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        success: function (data) {
                            if (data.html) {
                                self_this.content.html(data.html);
                            }

                            var filters = {};
                            filters.method = data.method || '';
                            filters.products = data.products || '';
                            filters.languages = data.languages || '';
                            filters.tags = data.tags || '';

                            if (data.pages && data.current) {
                                self_this.pages.update({total: data.pages, current: data.current});
                            }

                            self_this.filters.update(filters);
                            self_this.container.removeClass('load');
                            self_this.filters.filters_container.removeClass('load');
                        }
                    });
                } , 1);
            };

            new ContentManager('#marketing-container', '#material-pages');
        });
        </script>
    </div>
</div>