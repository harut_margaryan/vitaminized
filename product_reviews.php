<?php
/*if ($messageStack->size('product_reviews') > 0) {
echo $messageStack->output('product_reviews');
}*/
require(DIR_FS_CATALOG . FILENAME_PRODUCT_REVIEWS_WRITE);
?>

<div>
    <?php

    $reviews_query_raw = "select reviews_id, reviews_text, reviews_rating, date_added, customers_name from " . TABLE_REVIEWS . " where products_id = '" . (int)$product_info['products_id'] . "' and reviews_status = 1 order by reviews_id desc";
    $reviews_split = new splitPageResults($reviews_query_raw, MAX_DISPLAY_NEW_REVIEWS);

    if ($reviews_split->number_of_rows > 0) {
        if ((PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3')) {
            ?>

            <div>
                <p style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $reviews_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info'))); ?></p>

                <p><?php echo $reviews_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></p>
            </div>

            <br />

        <?php
        }

        $reviews_query = tep_db_query($reviews_split->sql_query);
        while ($reviews = tep_db_fetch_array($reviews_query)) {
            ?>

            <div>
                <span style="float: right;"><?php echo sprintf(TEXT_REVIEW_DATE_ADDED, tep_date_long($reviews['date_added'])); ?></span>
                <h2><?php echo '<a href="' . tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($product_info['products_name'])) . '#reviews">' . sprintf(TEXT_REVIEW_BY, tep_output_string_protected($reviews['customers_name'])) . '</a>'; ?></h2>
            </div>

            <div class="contentText">
                <?php echo tep_break_string(tep_output_string_protected($reviews['reviews_text']), 60, '-<br />') . ((strlen($reviews['reviews_text']) >= 100) ? '..' : '') . '<br /><br /><i>' . sprintf(TEXT_REVIEW_RATING, tep_image(DIR_WS_IMAGES . 'stars_' . $reviews['reviews_rating'] . '.gif', sprintf(TEXT_OF_5_STARS, $reviews['reviews_rating'])), sprintf(TEXT_OF_5_STARS, $reviews['reviews_rating'])) . '</i>'; ?>
            </div>

        <?php
        }
    } else {
        ?>

        <div class="contentText">
            <?php echo TEXT_NO_REVIEWS; ?>
        </div>

    <?php
    }

    if (($reviews_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
        ?>

        <div class="contentText">
            <p style="float: right;"><?php echo TEXT_RESULT_PAGE . ' ' . $reviews_split->display_links(MAX_DISPLAY_PAGE_LINKS, tep_get_all_get_params(array('page', 'info'))); ?></p>

            <p><?php echo $reviews_split->display_count(TEXT_DISPLAY_NUMBER_OF_REVIEWS); ?></p>
        </div>

    <?php
    }
    ?>

    <br />
</div>