<h1><?php echo HEADING_TITLE; ?></h1>

<?php
if ($messageStack->size('account') > 0) {
    echo $messageStack->output('account');
}

if (tep_session_is_registered('partner_id')) {
    $check_account_password_query = tep_db_query('select p.partners_password, pi.partners_soc_type_id from ' . TABLE_PARTNERS . ' as p join ' . TABLE_PARTNERS_INFO . ' as pi on(p.partners_id = pi.partners_info_id and p.partners_id = ' . (int)$partner_id . ')');
    $check_account_password = tep_db_fetch_array($check_account_password_query);
    if (empty($check_account_password['partners_password'])) {
        if ($check_account_password['partners_soc_type_id'] != 0) {
            $has_password = false;
        } else {
            tep_logoff();
        }
    } else {
        $has_password = true;
    }
} elseif (tep_session_is_registered('customer_id')) {
    $check_account_password_query = tep_db_query('select p.customers_password, pi.customers_soc_type_id from ' . TABLE_CUSTOMERS . ' as p join ' . TABLE_CUSTOMERS_INFO . ' as pi on(p.customers_id = pi.customers_info_id and p.customers_id = ' . (int)$customer_id . ')');
    $check_account_password = tep_db_fetch_array($check_account_password_query);
    if (empty($check_account_password['customers_password'])) {
        if ($check_account_password['customers_soc_type_id'] != 0) {
            $has_password = false;
        } else {
            tep_logoff();
        }
    } else {
        $has_password = true;
    }
}
?>

<div class="contentContainer">
    <h2><?php echo MY_ACCOUNT_TITLE; ?></h2>

    <div class="contentText">
        <ul class="accountLinkList">
            <li><span class="ui-icon ui-icon-person accountLinkListEntry"></span><?php echo '<a href="' . tep_href_link(ACCOUNT_EDIT_URL, '', 'SSL') . '">' . MY_ACCOUNT_INFORMATION . '</a>'; ?></li>
            <?php if (!$PARTNER) {?>
                <li><span class="ui-icon ui-icon-home accountLinkListEntry"></span><?php echo '<a href="' . tep_href_link(ADDRESS_BOOK_URL, '', 'SSL') . '">' . MY_ACCOUNT_ADDRESS_BOOK . '</a>'; ?></li>
            <?php }?>
            <li><span class="ui-icon ui-icon-key accountLinkListEntry"></span><?php echo '<a href="' . tep_href_link(ACCOUNT_PASSWORD_URL, '', 'SSL') . '">' . ($has_password ? MY_ACCOUNT_PASSWORD : MY_ACCOUNT_PASSWORD_SET) . '</a>'; ?></li>
        </ul>
    </div>

    <?php if (!$PARTNER) {?>
    <h2><?php echo MY_ORDERS_TITLE; ?></h2>

    <div class="contentText">
        <ul class="accountLinkList">
            <li><span class="ui-icon ui-icon-cart accountLinkListEntry"></span><?php echo '<a href="' . tep_href_link(ACCOUNT_HISTORY_URL, '', 'SSL') . '">' . MY_ORDERS_VIEW . '</a>'; ?></li>
        </ul>
    </div>

    <!--<h2><?php /*echo EMAIL_NOTIFICATIONS_TITLE; */?></h2>

    <div class="contentText">
        <ul class="accountLinkList">
            <li><span class="ui-icon ui-icon-mail-closed accountLinkListEntry"></span><?php /*echo '<a href="' . tep_href_link(ACCOUNT_NEWSLETTERS_URL, '', 'SSL') . '">' . EMAIL_NOTIFICATIONS_NEWSLETTERS . '</a>'; */?></li>
            <li><span class="ui-icon ui-icon-heart accountLinkListEntry"></span><?php /*echo '<a href="' . tep_href_link(ACCOUNT_NOTIFICATIONS_URL, '', 'SSL') . '">' . EMAIL_NOTIFICATIONS_PRODUCTS . '</a>'; */?></li>
        </ul>
    </div>-->
    <?php
    } if ($PARTNER && tep_session_is_registered('partner_key')) {
    ?>
    <div class="contentText">
        <ul class="accountLinkList">
            <li><span class="ui-icon ui-icon-heart accountLinkListEntry"></span><?php echo '<a href="' . tep_href_link(PARTNERS_ANALYTIC_URL, '', 'SSL') . '">' . PARTNER_ANALYTIC . '</a>'; ?></li>
            <li><span class="ui-icon ui-icon-key accountLinkListEntry"></span><?php echo PARTNER_KEY . tep_draw_input_field('p', '?p=' . $partner_key, 'onclick="this.select();" readonly'); ?></li>
            <li><span class="ui-icon ui-icon-key accountLinkListEntry"></span><a href="<?php echo tep_href_link(PARTNERS_MARKETING_MATERIALS_URL); ?>"><?php echo PARTNER_MARKETING_MATERIALS; ?></a></li>
        </ul>
    </div>
    <?php
    }
    ?>
</div>
