<h1 class="page-main-h1"><?php echo HEADING_TITLE; ?></h1>
<div class="page-content-cont">
    <div class="page-content-cont-inner">
        <?php
        if ($cart->count_contents() > 0):
            if ($messageStack->size('create_account') > 0) {
                echo $messageStack->output('create_account');
            }
            ?>

            <div class="contentContainer" id="full-cart">
                <h2><?php echo TABLE_HEADING_PRODUCTS; ?></h2>

                <div class="contentText">

                    <?php
                    echo tep_draw_form('shopping', tep_href_link(SHOPPING_URL, '', 'SSL'), 'post', '', true) . tep_draw_hidden_field('action', 'process');
                    $any_out_of_stock = 0;
                    $products = $cart->get_products();
                    ?>

                    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="jg-basket-products">
                    </table>

                    <p align="right"><strong><?php echo SUB_TITLE_SUB_TOTAL; ?>$<span
                                class="jg-basket-total-price"></span></strong>
                    </p>

                    <?php
                    if ($any_out_of_stock == 1) {
                        if (STOCK_ALLOW_CHECKOUT == 'true') {
                            ?>

                            <p class="stockWarning" align="center"><?php echo OUT_OF_STOCK_CAN_CHECKOUT; ?></p>

                            <?php
                        } else {
                            ?>

                            <p class="stockWarning" align="center"><?php echo OUT_OF_STOCK_CANT_CHECKOUT; ?></p>

                            <?php
                        }
                    }
                    ?>

                    <!---------------------------------------------- shipping ----------------------------------->

                    <script type="text/javascript">
                        var selecteds, selectedp;

                        function selectRowEffects(object, buttonSelect) {
                            var id = 'defaultSelecteds';
                            if (!selecteds) {
                                if (document.getElementById) {
                                    selecteds = document.getElementById(id);
                                } else {
                                    selecteds = document.all[id];
                                }
                            }

                            if (selecteds) selecteds.className = 'moduleRow';
                            object.className = 'moduleRowSelected';
                            selecteds = object;

// one button is not an array
                            if (document.shopping.shipping[0]) {
                                document.shopping.shipping[buttonSelect].checked = true;
                            } else {
                                document.shopping.shipping.checked = true;
                            }
                        }

                        function selectRowEffectp(object, buttonSelect) {
                            var id = 'defaultSelectedp';
                            if (!selectedp) {
                                if (document.getElementById) {
                                    selectedp = document.getElementById(id);
                                } else {
                                    selectedp = document.all[id];
                                }
                            }

                            if (selectedp) selectedp.className = 'moduleRow';
                            object.className = 'moduleRowSelected';
                            selectedp = object;

// one button is not an array
                            if (document.shopping.payment[0]) {
                                document.shopping.payment[buttonSelect].checked = true;
                            } else {
                                document.shopping.payment.checked = true;
                            }
                        }

                        function rowOverEffect(object) {
                            if (object.className == 'moduleRow') object.className = 'moduleRowOver';
                        }

                        function rowOutEffect(object) {
                            if (object.className == 'moduleRowOver') object.className = 'moduleRow';
                        }
                        //--></script>

                    <?php
                    if (tep_count_shipping_modules() > 0) {
                        ?>

                        <h2><?php echo TABLE_HEADING_SHIPPING_METHOD; ?></h2>

                        <?php
                        if (sizeof($quotes) > 1 && sizeof($quotes[0]) > 1) {
                            ?>

                            <div class="contentText">
                                <div style="float: right;">
                                    <?php echo '<strong>' . TITLE_PLEASE_SELECT . '</strong>'; ?>
                                </div>

                                <?php echo TEXT_CHOOSE_SHIPPING_METHOD; ?>
                            </div>

                            <?php
                        } elseif ($free_shipping == false) {
                            ?>

                            <div class="contentText">
                                <?php echo TEXT_ENTER_SHIPPING_INFORMATION; ?>
                            </div>

                            <?php
                        }
                        ?>

                        <div class="contentText">
                            <table border="0" width="100%" cellspacing="0" cellpadding="2">

                                <?php
                                if ($free_shipping == true) {
                                    ?>

                                    <tr>
                                        <td>
                                            <strong><?php echo FREE_SHIPPING_TITLE; ?></strong>&nbsp;<?php echo $quotes[$i]['icon']; ?>
                                        </td>
                                    </tr>
                                    <tr id="defaultSelecteds" class="moduleRowSelected"
                                        onmouseover="rowOverEffect(this)"
                                        onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, 0, 's')">
                                        <td style="padding-left: 15px;"><?php echo sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) . tep_draw_hidden_field('shipping', 'free_free'); ?></td>
                                    </tr>

                                    <?php
                                } else {
                                    $radio_buttons = 0;
                                    for ($i = 0, $n = sizeof($quotes); $i < $n; $i++) {
                                        ?>

                                        <tr>
                                            <td colspan="3">
                                                <strong><?php echo $quotes[$i]['module']; ?></strong>&nbsp;<?php if (isset($quotes[$i]['icon']) && tep_not_null($quotes[$i]['icon'])) {
                                                    echo $quotes[$i]['icon'];
                                                } ?></td>
                                        </tr>

                                        <?php
                                        if (isset($quotes[$i]['error'])) {
                                            ?>

                                            <tr>
                                                <td colspan="3"><?php echo $quotes[$i]['error']; ?></td>
                                            </tr>

                                            <?php
                                        } else {
                                            for ($j = 0, $n2 = sizeof($quotes[$i]['methods']); $j < $n2; $j++) {
// set the radio button to be checked if it is the method chosen
                                                $checked = (($quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'] == $shipping['id']) ? true : ($shipping['id'] ? false : !($i + $j)));

                                                if (($checked == true) || ($n == 1 && $n2 == 1)) {
                                                    echo '      <tr id="defaultSelecteds" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffects(this, ' . $radio_buttons . ')">' . "\n";
                                                } else {
                                                    echo '      <tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffects(this, ' . $radio_buttons . ')">' . "\n";
                                                }
                                                ?>

                                                <td width="75%"
                                                    style="padding-left: 15px;"><?php echo $quotes[$i]['methods'][$j]['title']; ?></td>

                                                <?php
                                                if (($n > 1) || ($n2 > 1)) {
                                                    ?>

                                                    <td id="quotes-<?php echo $i . '-' . $j; ?>"><?php echo $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))); ?></td>
                                                    <td align="right"><?php echo tep_draw_radio_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'], $checked); ?></td>

                                                    <?php
                                                } else {
                                                    ?>

                                                    <td align="right" colspan="2"
                                                        id="quotes-<?php echo $i . '-' . $j; ?>"><?php echo $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))) . tep_draw_hidden_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id']); ?></td>

                                                    <?php
                                                }
                                                ?>

                                                </tr>

                                                <?php
                                                $radio_buttons++;
                                            }
                                        }
                                    }
                                }
                                ?>

                            </table>
                        </div>

                        <?php
                    }
                    ?>

                    <!------------------------------------------------------------------------------------------->

                    <!--------------------------------------checkout payment ------------------------------------>

                    <?php echo $payment_modules->javascript_validation(); ?>

                    <?php
                    if (isset($_GET['payment_error']) && is_object(${$_GET['payment_error']}) && ($error = ${$_GET['payment_error']}->get_error())) {
                        ?>

                        <div class="contentText">
                            <?php echo '<strong>' . tep_output_string_protected($error['title']) . '</strong>'; ?>

                            <p class="messageStackError"><?php echo tep_output_string_protected($error['error']); ?></p>
                        </div>

                        <?php
                    }
                    ?>

                    <h2><?php echo TABLE_HEADING_PAYMENT_METHOD; ?></h2>

                    <?php
                    $selection = $payment_modules->selection();

                    if (sizeof($selection) > 1) {
                        ?>

                        <div class="contentText">
                            <div style="float: right;">
                                <?php echo '<strong>' . TITLE_PLEASE_SELECT . '</strong>'; ?>
                            </div>

                            <?php echo TEXT_SELECT_PAYMENT_METHOD; ?>
                        </div>

                        <?php
                    } elseif ($free_shipping == false) {
                        ?>

                        <div class="contentText">
                            <?php echo TEXT_ENTER_PAYMENT_INFORMATION; ?>
                        </div>

                        <?php
                    }
                    ?>

                    <div class="contentText">

                        <?php
                        $radio_buttons = 0;
                        for ($i = 0, $n = sizeof($selection); $i < $n; $i++) {
                            ?>

                            <table border="0" width="100%" cellspacing="0" cellpadding="2">

                                <?php
                                if (($i == 0)) {
                                    echo '      <tr id="defaultSelectedp" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffectp(this, ' . $radio_buttons . ')">' . "\n";
                                } else {
                                    echo '      <tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffectp(this, ' . $radio_buttons . ')">' . "\n";
                                }
                                ?>

                                <td><strong><?php echo $selection[$i]['module']; ?></strong></td>
                                <td align="right">

                                    <?php
                                    //if (sizeof($selection) > 1) {
                                    echo tep_draw_radio_field('payment', $selection[$i]['id'], $i == 0);
                                    /*} else {
                                        echo tep_draw_hidden_field('payment', $selection[$i]['id']);
                                    }*/
                                    ?>

                                </td>
                                </tr>

                                <?php
                                if (isset($selection[$i]['error'])) {
                                    ?>

                                    <tr>
                                        <td colspan="2"><?php echo $selection[$i]['error']; ?></td>
                                    </tr>

                                    <?php
                                } elseif (isset($selection[$i]['fields']) && is_array($selection[$i]['fields'])) {
                                    ?>

                                    <tr>
                                        <td colspan="2">
                                            <table border="0" cellspacing="0" cellpadding="2">

                                                <?php
                                                for ($j = 0, $n2 = sizeof($selection[$i]['fields']); $j < $n2; $j++) {
                                                    ?>

                                                    <tr>
                                                        <td><?php echo $selection[$i]['fields'][$j]['title']; ?></td>
                                                        <td><?php echo $selection[$i]['fields'][$j]['field']; ?></td>
                                                    </tr>

                                                    <?php
                                                }
                                                ?>

                                            </table>
                                        </td>
                                    </tr>

                                    <?php
                                }
                                ?>

                            </table>

                            <?php
                            $radio_buttons++;
                        }
                        ?>

                    </div>

                    <h2><?php echo TABLE_HEADING_COMMENTS; ?></h2>

                    <div class="contentText">
                        <?php echo tep_draw_textarea_field('comments', 'soft', '60', '5', $comments); ?>
                        </dpt>


                    </div>
                    <!------------------------------------------------------------------------------------------->

                    <!-----------------------------------------address---------------------------------------->
                    <?php
                    if (!isset($process)) $process = false;

                    if (tep_session_is_registered('customer_id')) {
                        $address_query = tep_db_query('select c.customers_telephone, c.customers_email_address, ab.* from ' . TABLE_CUSTOMERS . ' as c join ' . TABLE_ADDRESS_BOOK . ' as ab on(c.customers_id = ab.customers_id) where ab.address_book_id = ' . intval($customer_default_address_id));
                        $addr_arr = tep_db_fetch_array($address_query);
                    }
                    ?>

                    <div class="contentText">
                        <table border="0" width="100%" cellspacing="2" cellpadding="2">

                            <?php
                            ?>

                            <tr>
                                <td class="fieldKey"><?php echo ENTRY_FIRST_NAME; ?></td>
                                <td class="fieldValue"><?php echo tep_draw_input_field('firstname', empty($addr_arr['entry_firstname']) ? '' : $addr_arr['entry_firstname']) . '&nbsp;' . (tep_not_null(ENTRY_FIRST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_FIRST_NAME_TEXT . '</span>' : ''); ?></td>
                            </tr>
                            <?php
                            if (ACCOUNT_LAST_NAME == 'true') {
                                ?>
                                <tr>
                                    <td class="fieldKey"><?php echo ENTRY_LAST_NAME; ?></td>
                                    <td class="fieldValue"><?php echo tep_draw_input_field('lastname', empty($addr_arr['entry_lastname']) ? '' : $addr_arr['entry_lastname']) . '&nbsp;' . (tep_not_null(ENTRY_LAST_NAME_TEXT) ? '<span class="inputRequirement">' . ENTRY_LAST_NAME_TEXT . '</span>' : ''); ?></td>
                                </tr>
                                <?php
                            }
                            ?>

                            <tr>
                                <td class="fieldKey"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                                <td class="fieldValue"><?php echo tep_draw_input_field('email_address', empty($addr_arr['customers_email_address']) ? '' : $addr_arr['customers_email_address']) . '&nbsp;' . (tep_not_null(ENTRY_EMAIL_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_EMAIL_ADDRESS_TEXT . '</span>' : ''); ?></td>
                            </tr>

                            <?php

                            if (ACCOUNT_COMPANY == 'true') {
                                ?>

                                <tr>
                                    <td class="fieldKey"><?php echo ENTRY_COMPANY; ?></td>
                                    <td class="fieldValue"><?php echo tep_draw_input_field('company', empty($addr_arr['entry_company']) ? '' : $addr_arr['entry_company']) . '&nbsp;' . (tep_not_null(ENTRY_COMPANY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COMPANY_TEXT . '</span>' : ''); ?></td>
                                </tr>

                                <?php
                            }

                            if (ACCOUNT_STREET_ADDRESS == 'true') {
                                ?>
                                <tr>
                                    <td class="fieldKey"><?php echo ENTRY_STREET_ADDRESS; ?></td>
                                    <td class="fieldValue"><?php echo tep_draw_input_field('street_address', empty($addr_arr['entry_street_address']) ? '' : $addr_arr['entry_street_address']) . '&nbsp;' . (tep_not_null(ENTRY_STREET_ADDRESS_TEXT) ? '<span class="inputRequirement">' . ENTRY_STREET_ADDRESS_TEXT . '</span>' : ''); ?></td>
                                </tr>
                                <?php
                            }

                            if (ACCOUNT_SUBURB == 'true') {
                                ?>

                                <tr>
                                    <td class="fieldKey"><?php echo ENTRY_SUBURB; ?></td>
                                    <td class="fieldValue"><?php echo tep_draw_input_field('suburb', empty($addr_arr['entry_suburb']) ? '' : $addr_arr['entry_suburb']) . '&nbsp;' . (tep_not_null(ENTRY_SUBURB_TEXT) ? '<span class="inputRequirement">' . ENTRY_SUBURB_TEXT . '</span>' : ''); ?></td>
                                </tr>
                                <?php
                            }

                            if (ACCOUNT_POST_CODE == 'true') {
                                ?>
                                <tr>
                                    <td class="fieldKey"><?php echo ENTRY_POST_CODE; ?></td>
                                    <td class="fieldValue"><?php echo tep_draw_input_field('postcode', empty($addr_arr['entry_postcode']) ? '' : $addr_arr['entry_postcode']) . '&nbsp;' . (tep_not_null(ENTRY_POST_CODE_TEXT) ? '<span class="inputRequirement">' . ENTRY_POST_CODE_TEXT . '</span>' : ''); ?></td>
                                </tr>
                                <?php
                            }

                            if (ACCOUNT_CITY == 'true') {
                                ?>
                                <tr>
                                    <td class="fieldKey"><?php echo ENTRY_CITY; ?></td>
                                    <td class="fieldValue"><?php echo tep_draw_input_field('city', empty($addr_arr['entry_city']) ? '' : $addr_arr['entry_city']) . '&nbsp;' . (tep_not_null(ENTRY_CITY_TEXT) ? '<span class="inputRequirement">' . ENTRY_CITY_TEXT . '</span>' : ''); ?></td>
                                </tr>
                                <?php
                            }

                            if (ACCOUNT_STATE == 'true') {
                                ?>

                                <tr>
                                    <td class="fieldKey"><?php echo ENTRY_STATE; ?></td>
                                    <td class="fieldValue">
                                        <?php
                                        if (!empty($addr_arr['entry_country_id'])) {
                                            $zones_array = array();
                                            $zones_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$addr_arr['entry_country_id'] . "' order by zone_name");

                                            while ($zones_values = tep_db_fetch_array($zones_query)) {
                                                $zones_array[] = array('id' => $zones_values['zone_id'], 'text' => $zones_values['zone_name']);
                                            }
                                            echo tep_draw_pull_down_menu('state', $zones_array, empty($addr_arr['entry_zone_id']) ? 0 : $addr_arr['entry_zone_id']);
                                        } else {
                                            echo tep_draw_input_field('state', empty($addr_arr['entry_state']) ? '' : $addr_arr['entry_state']);
                                        }

                                        if (tep_not_null(ENTRY_STATE_TEXT)) echo '&nbsp;<span class="inputRequirement">' . ENTRY_STATE_TEXT . '</span>';
                                        ?>
                                    </td>
                                </tr>

                                <?php
                            }

                            if (ACCOUNT_COUNTRY == 'true') {
                                ?>
                                <tr>
                                    <td class="fieldKey"><?php echo ENTRY_COUNTRY; ?></td>
                                    <td class="fieldValue country"><?php echo tep_get_country_list('country', STORE_COUNTRY, empty($addr_arr['entry_country_id']) ? 0 : $addr_arr['entry_country_id']) . '&nbsp;' . (tep_not_null(ENTRY_COUNTRY_TEXT) ? '<span class="inputRequirement">' . ENTRY_COUNTRY_TEXT . '</span>' : ''); ?></td>
                                </tr>
                                <?php
                            }
                            ?>

                            <?php
                            if (ACCOUNT_TELEPHONE_NUMBER == 'true') {
                                ?>
                                <tr>
                                    <td class="fieldKey"><?php echo ENTRY_TELEPHONE_NUMBER; ?></td>
                                    <td class="fieldValue"><?php echo tep_draw_input_field('telephone', empty($addr_arr['customers_telephone']) ? '' : $addr_arr['customers_telephone']) . '&nbsp;' . (tep_not_null(ENTRY_TELEPHONE_NUMBER_TEXT) ? '<span class="inputRequirement">' . ENTRY_TELEPHONE_NUMBER_TEXT . '</span>' : ''); ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                    </div>
                    <!------------------------------------------------------------------------------------------->

                    <?php
                    echo tep_draw_button(IMAGE_BUTTON_CONFIRM_ORDER, 'check', null, 'primary');
                    ?>
                    </form>
                </div>
            </div>

        <?php else: ?>
            <div class="contentText">
                <?= TEXT_CART_EMPTY; ?>
                <p align="right"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link()); ?></p>
            </div>
        <?php endif; ?>
    </div>
</div>