<?php if (tep_db_num_rows($product_info_query) < 1): ?>

  <div class="contentContainer" xmlns="http://www.w3.org/1999/html">
    <div class="contentText">
		<?php echo TEXT_PRODUCT_NOT_FOUND; ?>
    </div>

    <div style="float: right;">
		<?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link()); ?>
    </div>
  </div>

<?php else:

	//tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1 where products_id = '" . (int)$item . "' and language_id = '" . (int)$languages_id . "'");

	//$product_info_query = tep_db_query("select p.products_id, pd.products_name, pd.products_description, p.products_model, p.products_quantity, p.products_image, pd.products_url, p.products_price, p.products_tax_class_id, p.products_date_added, p.products_date_available, p.manufacturers_id from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd using(products_id) where p.products_status = '1' and p.products_id = '" . (int)$item . "' and pd.language_id = '" . (int)$languages_id . "'");
	//$product_info = tep_db_fetch_array($product_info_query);

	tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1 where products_id = '" . (int)$products_id . "' and language_id = '" . (int)$languages_id . "'");

	if ($new_price = tep_get_products_special_price($product_info['products_id'])) {
		$products_price = '<del>' . $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])) . '</del> <span class="productSpecialPrice">' . $currencies->display_price($new_price, tep_get_tax_rate($product_info['products_tax_class_id'])) . '</span>';
	} else {
		$products_price = $currencies->display_price($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id']));
	}
	//echo $products_price;

	if (tep_not_null($product_info['products_model'])) {
		$products_name = $product_info['products_name'] . '<br /><span class="smallText">[' . $product_info['products_model'] . ']</span>';
	} else {
		$products_name = $product_info['products_name'];
	}

	$all_prods_query = tep_db_query('select p.products_id, pd.products_name from ' . TABLE_PRODUCTS . ' as p join ' . TABLE_PRODUCTS_DESCRIPTION . ' as pd on(p.products_id = pd.products_id and pd.language_id = ' . intval($languages_id) . ') where p.products_status = 1');
	$first_prod = null;
	$last_prod = null;
	$prev_prod = null;
	$left_neighbor = null;
	$right_neighbor = null;
	while ($prod = tep_db_fetch_array($all_prods_query)) {
		if (!$first_prod) $first_prod = $prod;
		$last_prod = $prod;
		if ($prod['products_id'] == $product_info['products_id']) {
			$left_neighbor = $prev_prod;
		}

		if ($prev_prod && $prev_prod['products_id'] == $product_info['products_id']) {
			$right_neighbor = $prod;
		}
		$prev_prod = $prod;
	}

	if (!$left_neighbor) $left_neighbor = $last_prod;
	if (!$right_neighbor) $right_neighbor = $first_prod;

	?>
  <div id="prod-container">
    <a href="<?= tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($left_neighbor['products_name'])) ?>"
       class="slider-arrow-left">&lt;</a>
    <a href="<?= tep_href_link(PRODUCT_URL . '/' . tep_clean_text_int($right_neighbor['products_name'])) ?>"
       class="slider-arrow-right">&gt;</a>
    <div class="prod-head-cont">
      <div class="prod-left-img"><?php echo tep_image(DIR_WS_IMAGES_PRODUCTS . $product_info['products_image'] . '-1' . IMAGE_EXTENSION, addslashes($product_info['products_name']), null, null, 'hspace="5" vspace="5" id="image-' . $product_info['products_id'] . '"'); ?></div>
      <div class="prod-left-img">
        <h1><?= $products_name ?><sup>®</sup></h1>
        <h2><?= $product_info['products_short_desc'] ?></h2>
        <p>
			<?= $product_info['products_description'] ?>
        </p>
		  <?php
		  /*if (!tep_session_is_registered('partner_id')):*/
			  echo tep_draw_form('cart_quantity', tep_href_link('', 'action=buy_now&'), 'post', 'id="form-' . $product_info['products_id'] . '"');
			  ?>
            <div class="prod-buy-info">
              <input class="numCounter" type="text" data-price="<?= $product_info['products_price']; ?>"
                     name="products_count" maxlength="4" value="1"/>
				    <?= tep_draw_hidden_field('products_id', $product_info['products_id']); ?>
              <button class="dena-btn dena-btn-blue buy-shopify">
                <!--<i class="dena-btn-icon"></i>-->
                BUY NOW
              </button>
                <div class="vitamines-btn-2 dena-btn dena-btn-blue" onclick="addToCart(<?=$product_info['shopify_id']?>, this)">Add to CART</div>
            </div>

            </form>

              <?php //if (WITH_SHOPPING_CART): ?>
                <script>
                    $(document).ready(function () {

                      $('.buy-shopify').click(function () {

                        var shopClient = ShopifyBuy.buildClient({
                          apiKey: '89e2a646a16e81afd559fa49b3ebb17f',
                          domain: 'vitaminized.myshopify.com',
                          appId: '6'
                        });

                        var cart;
                        shopClient.createCart().then(function (newCart) {
                          cart = newCart;
                          // do something with updated cart
                        });

                        // fetch a product using resource id
                        shopClient.fetchProduct('<?= $product_info['shopify_id'] ?>')
                          .then(function (product) {
                            console.log(product);
                            cart.addVariants({
                              variant: product.selectedVariant,
                              quantity: $('input[name="products_count"]').val(),
                            }).then(function (cart) {
                              // do something with updated cart
                              window.location.href = cart.checkoutUrl;
                            });
                          })
                          .catch(function () {
                            console.log('Request failed');
                          });
                      });

                    });

                  $('#form-<?php echo (int)$product_info['products_id'];?>').submit(function () {
                    return false;
                  });

                </script>
              <?php //endif; ?>
		  <?php /*endif; */?>

        <div class="soc-shares">
          <span>SHARE:</span>
          <div class="g-plusone" data-size="medium" data-annotation="none"
               data-href="<?= htmlentities($og_url) ?>"></div>
          <span>&nbsp;&nbsp;</span>
          <div class="fb-like" data-href="<?= htmlentities($og_url) ?>" data-layout="button_count"
               data-action="recommend" data-show-faces="true" data-share="true"></div>
        </div>
      </div>
    </div>
    <div class="prod-body-cont">

		<?php
		$products_name = $product_info['products_name'];
		?>

      <div class="clear"></div>

		<?php
		// if (!tep_session_is_registered('partner_id')) end

		/*if ($product_info['products_date_available'] > date('Y-m-d H:i:s')) {
		?>

		<p style="text-align: center;"><?php echo sprintf(TEXT_DATE_AVAILABLE, tep_date_long($product_info['products_date_available'])); ?></p>

	<?php
	}*/
		?>

		<?php

		$pi_content = '<div id="pInfoTabs">';
		$pi_head = '<ul>';
		$pi_body = '<div>';

		$products_info = array();
		$products_info_query = tep_db_query('select pid.products_info_title, p2pi.products_info_text, tid.content from ' . TABLE_PRODUCTS_INFO . ' as pi join ' . TABLE_PRODUCTS_INFO_DESCRIPTION . ' as pid on(pi.products_info_id = pid.products_info_id and pi.products_info_status = "1" and pid.language_id = ' . (int)$languages_id . ') join ' . TABLE_PRODUCTS_TO_PRODUCTS_INFO . ' as p2pi on(p2pi.products_info_id = pi.products_info_id and p2pi.language_id = ' . (int)$languages_id . ' and p2pi.products_id = ' . (int)$products_id . ') left join ' . TABLE_TABS_INFO . ' as ti on (ti.products_id = ' . intval($products_id) . ' and ti.status = 1 and ti.products_info_id = pi.products_info_id) left join ' . TABLE_TABS_INFO_DESC . ' as tid on(tid.tabs_info_id = ti.id and tid.languages_id = ' . (int)$languages_id . ') order by pi.sort_order');
		if (tep_db_num_rows($products_info_query)) {
			while ($products_info = tep_db_fetch_array($products_info_query)) {
				if ($products_info['products_info_text'] != '') {
					$pi_head .= '<li data-tab="' . tep_clean_text_int($products_info['products_info_title']) . '"><a href="#' . tep_clean_text_int($products_info['products_info_title']) . '">' . $products_info['products_info_title'] . '</a><i></i></li>';
					$pi_body .= '<div id="' . tep_clean_text_int($products_info['products_info_title']) . '" class="contentTextMain">' . $products_info['products_info_text'] . '</div>';
				} elseif ($products_info['content']) {
					$tab_content = json_decode($products_info['content'], true);
					if ($tab_content['title']) {
						$pi_head .= '<li data-tab="' . tep_clean_text_int($products_info['products_info_title']) . '"><a href="#">' . $products_info['products_info_title'] . '</a><i></i></li>';
						$pi_body .= '<div id="' . tep_clean_text_int($products_info['products_info_title']) . '" class="contentTextMain">
								<div class="advantages">
									<h3>' . $tab_content['title'] . '</h3>';

						$odd = true;
						foreach ($tab_content['body'] as $item_id => $item) {
							if (!$item['title']) continue;
							$odd = !$odd;
							$pi_body .= '
									<div class="advantages-cont' . ($odd ? ' advantages-cont-right' : '') . '">
										<div class="advantages-head">
											<h4>' . $item['title'] . '</h4>
										</div>
										<div class="advantages-left-cont">
											<p>' . strip_tags($item['text']) . '</p>
										</div>' .
								($item['image'] ? tep_image(DIR_WS_IMAGES_TABS_INFO . $item['image'], '', '', '', 'class="advantages-right-cont"') : '')
								. '<ul>
											<li><img src="images/capsule-icon.png" alt="capsule"/><span>' . $item['count'] . '</span></li>
											<li><img src="images/times-icon.png" alt="times"/><span>' . $item['frequency'] . '</span></li>
											<li><img src="images/monts-icon.png" alt="monts"/><span>' . $item['duration'] . '</span></li>
										</ul>
									</div>';
						}

						$pi_body .= '</div></div>';
					}
				}
			}
		}

		//----reviews----
		if (!tep_session_is_registered('partner_id')) {
			$pi_head .= '<li data-tab="reviews"><a>Reviews</a><i></i></li>';

			ob_start();

			require(DIR_FS_CATALOG . FILENAME_PRODUCT_REVIEWS);
			$reviews_body = ob_get_contents();
			ob_end_clean();

			$pi_body .= '<div id="reviews">' . $reviews_body . '</div>';
		}

		//----end reviews

		$pi_head .= '</ul>';
		$pi_body .= '</div>';

		$pi_content .= $pi_head . $pi_body . '</div>';

		echo $pi_content;
		?>

    </div>


  </div>

<?php endif; ?>

