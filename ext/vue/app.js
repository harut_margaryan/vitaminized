/**
 * Created by hmargary on 4/14/2017.
 */

var state = {
	//date_from: new Date(1492167920 * 1000),
	//date_to: new Date(1492167920 * 1000),
	referal_id: 49786574987,
	gridData: [
		{id: 123456, name: "Harut", price: "50", count: 5, date: 1492167920},
		{id: 123456, name: "Harut", price: "50", count: 5, date: 1492161920},
		{id: 123456, name: "Harut", price: 50.5, count: 5, date: 1492161920},
		{id: 123456, name: "Harut", price: "50", count: 5, date: 1492061920}
	],
};


$(document).ready(function () {

	setTimeout(function () {
		app.showDashboard = true;
	}, 0);

	const app = new Vue({
		el: "#app",
		data: {
			message: "Hello world!",
			state: {
				date_from: Vue.moment().add("month", -1).toDate(),
				date_to: Vue.moment().toDate(),
        referal_id: state.referal_id
			},
			showDashboard: false
		},
		methods: {

		},
		computed: {
			filterGridByDate:  function () {
        var self = this;
					return state.gridData.filter(function (el) {
						return self.state.date_from.getTime() <= el.date * 1000 && self.state.date_to.getTime() >= el.date *1000;
					}, 100);
			},
			balance: function() {
				return 10.56;
			}
		},
		components: {datepicker: vueComponents.Datepicker}

	});

});