!function($) {
    window.JgTabs = function (selector) {
        this.ok = false;
        var tabs_container = $(selector);
        if (tabs_container.length) tabs_container = tabs_container.eq(0); else return;
        this.tabs_container = tabs_container;
        this.tabs_container.addClass('jg-tabs-container');
        var tabs_head = tabs_container.children('ul');
        if (tabs_head.length) tabs_head = tabs_head.eq(0); else return;
        this.tabs_head = tabs_head;
        this.tabs_head.addClass('jg-tabs-head');
        var tabs_body = tabs_container.children('div');
        if (tabs_body.length) tabs_body = tabs_body.eq(0); else return;
        this.tabs_body = tabs_body;
        this.tabs_body.addClass('jg-tabs-body');
        var tabId = window.location.hash.substr(1);
        if (!tabId || ~tabId.indexOf('&') || ~tabId.indexOf('=')) {
            tabId = this.tabs_head.children('li:first-child').eq(0).data('tab');
        }
        this.showTab(tabId);
        var self_this = this;

        function onTabsClick (event) {
            var tg = $(event.target);
            if (!tg.data('tab')) {
                tg = tg.parents('[data-tab]');
                if (!tg.length) {
                    return false;
                }
                tg = tg.eq(0);
            }
            var tabId = tg.data('tab');
            self_this.showTab(tabId);
            return false;
        }

        this.tabs_head.on('click', onTabsClick);
        this.ok = true;
        return this;
    };

    JgTabs.prototype.showTab = function (tabId) {
        var tabBody = this.tabs_body.children('#' + tabId);
        if (!tabBody.length) {
            tabBody = this.tabs_body.children(':first-child');
            tabId = tabBody.id;
        }
        var tabHead = this.tabs_head.children('li[data-tab="' + tabId + '"]');
        if (!tabHead.length) {
            tabHead = this.tabs_body.children(':first-child');
            tabId = tabHead.data('tab');
        }
        $.map(this.tabs_body.children('div'), function (elem){$(elem).hide();});
        this.tabs_body.children().removeClass('jg-tabs-body-active');
        this.tabs_head.children().removeClass('jg-tabs-head-active');
        tabBody.fadeIn();
        tabBody.addClass('jg-tabs-body-active');
        tabHead.addClass('jg-tabs-head-active');
        this.afterTabChange && this.afterTabChange(tabId);
    };
}(jQuery);