$.fn.jgCounter = function (arg1, arg2) {
    var $this, input;
    if (arg1 === 'value' || arg1 === 'min' || arg1 === 'max' || arg1 === 'step' || arg1 === 'default' || arg1 === 'reset') {
        if (arg1 === 'reset') {
            this.each(function () {
                if (this.jg_counter) {
                    reset(this.jg_counter);
                }
            });
        } else {
            if (arg2 === undefined) {
                $this = this.eq(0);
                input = $this.get(0);
                if (!input.jg_counter) return false;
                return get(input.jg_counter, arg1);
            } else if (arg2) {
                this.each(function () {
                    if (!this.jg_counter) return;
                    set(this.jg_counter, {arg1: arg2});
                });
            }
        }
    }

    if (arg2 === undefined) {
        this.each(function () {
            var $this = $(this);
            if (this.jg_counter) {
                set(this.jg_counter, arg1);
            } else {
                if ($this.is('input[type="text"]')) {
                    create($this, arg1);
                }
            }
        });
    }
    
    /***-   private function   -***/

    function create ($this, options) {
        var input = $this.get(0);
        if (input.jg_counter) reset(input.jg_counter);
        var _ = input.jg_counter = {};

        _.settings = $.extend({
            min : undefined,
            max : undefined,
            default : 1,
            step: 1
        }, options);

        _.val = _.settings.value;
        _.$input = $this;
        _.$input.addClass('jg-counter-input');

        _.$cont = $('<div>');
        _.$cont.addClass('jg-counter-cont');
        _.$cont = _.$input.wrap(_.$cont).parent();

        _.$incdec = $('<div>');
        _.$incdec.addClass('jg-counter-incdec');

        _.$inc = $('<button type="button">');
        _.$inc.addClass('jg-counter-inc');
        _.$incdec.append(_.$inc);
		
        _.$incIcon = $('<i>');
        _.$inc.append(_.$incIcon);

        _.$dec = $('<button type="button">');
        _.$dec.addClass('jg-counter-dec');
        _.$incdec.append(_.$dec);
		
        _.$decIcon = $('<i>');
        _.$dec.append(_.$decIcon);

        _.$cont.append(_.$incdec);

        _.$input.change(onChange);
        _.$input.keyup(onChange);
        _.$input.on('input', onChange);
        _.$incdec.click(onIncDec);
        _.$input.focusout(onInputFocusOut);

        set(_, _.settings);
        set(_, {value : _.settings.default});
    }

    function set(_, opts) {
        if (!opts) return;

        if (opts.value !== undefined) {
            if (+opts.value === _.val && +opts.value === +_.$input.val()) return;

            if (opts.value === '') {
                _.val = _.settings.default;
            } else {
                var newVal = parseInt(+opts.value);
                if (isNaN(newVal)) {
                    _.$input.val(_.val);
                } else {
                    newVal = newVal > _.settings.max ? _.val : newVal;
                    newVal = newVal < _.settings.min ? _.val : newVal;
                    _.val = newVal;
                    _.$input.val(newVal || '');
                }
            }
        }

        if (!isNaN(+opts.default)) {
            var newDef = +opts.default;
            if (!isNaN(newDef) && newDef <= _.settings.max && newDef >= _.settings.min) {
                _.settings.default = newDef;
                _.$input.attr('placeholder', _.settings.default);
            }
        }

        if (!isNaN(+opts.min)) {
            var newMin = +opts.min || 0;
            if (_.settings.max != undefined && newMin > _.settings.max) {
                newMin = _.settings.max;
            }

            _.settings.min = newMin;
            if (_.val < _.settings.min) set(_, {value : _.settings.min});
        }

        if (!isNaN(+opts.max)) {
            var newMax = +opts.max || 0;
            if (_.settings.min != undefined && newMax < _.settings.min) {
                newMax = _.settings.min;
            }

            _.settings.max = newMax;
            if (_.val > _.settings.max) set(_, {value : _.settings.max});
        }

        if (!isNaN(+opts.step)) {
            _.settings.step = +opts.step || 1;
        }

        if (_.val <= _.settings.min) {
            _.$dec.prop('disabled', true);
        } else {
            _.$dec.prop('disabled', false);
        }

        if (_.val >= _.settings.max) {
            _.$inc.prop('disabled', true);
        } else {
            _.$inc.prop('disabled', false);
        }
    }

    function get(_, opt) {
        switch (opt) {
            case 'value':
                return _.val;
            case 'min':
                return _.settings.min;
            case 'max':
                return _.settings.max;
            case 'step':
                return _.settings.step;
            case 'default':
                return _.settings.default;
            default:
                return false;
        }
    }

    function onIncDec (e) {
        var $target = $(e.target), $this = $(this);		
        var cont = $this.parent().children('input').get(0);
        if (!cont.jg_counter) return;
        var _ = cont.jg_counter;
		
		var $button = $target;
		if ($button.parent().get(0) !== $this.get(0)) {
			$button = $button.parents('.jg-counter-incdec > button').eq(0);
		}
        var newVal;
        if ($button.hasClass('jg-counter-inc') && _.settings.max !== undefined) {
            newVal = _.val + _.settings.step > _.settings.max ? _.settings.max: _.val + _.settings.step;
        } else if ($button.hasClass('jg-counter-dec') && _.settings.min !== undefined) {
            newVal = _.val - _.settings.step < _.settings.min ? _.settings.min: _.val - _.settings.step;
        }
        var oldVal = _.val;
        set(_, {value: newVal});
        _.val !== oldVal && _.$input.trigger('jgCounterUserChange');
    }

    function onChange () {
        if (this.jg_counter) {
            var _ = this.jg_counter;
            var oldVal = _.val;
            set(_, {value : _.$input.val()});
            _.val !== oldVal && _.$input.trigger('jgCounterUserChange');
        }
    }

    function onInputFocusOut () {
        if (this.jg_counter) {
            set(this.jg_counter, {value : this.jg_counter.val});
        }
    }

    function reset (_) {

        _.$input.off('focusout');
        _.$incdec.off('click');
        _.$input.off('input');
        _.$input.off('keyup');
        _.$input.off('change');

        _.$input.change(onChange);
        _.$input.keyup(onChange);
        _.$input.on('input', onChange);
        _.$incdec.click(onIncDec);
        _.$input.focusout(onInputFocusOut);
    }

    /***--***/

    return this;
};