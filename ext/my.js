/***   global variables */
var loginPopup, myCart, loginTabs, JgRatingRanger, showLoginPopup;
/* global variables   ***/
$(document).ready(function () {

    refreshCard();
    /***   JgCounter   ***/
    function JgCounter(elem) {
        this.ok = false;
        this.input = $(elem);
        if (!this.input.length) return this;
        var self_this = this;

        /***-   private function   -***/
        function draw() {
            self_this.container = $('<span>');
            self_this.container.addClass('jg-counter-container');
            self_this.incButton = $('<span>+</span>');
            self_this.incButton.addClass('jg-counter-inc-button');
            self_this.decButton = $('<span>-</span>');
            self_this.decButton.addClass('jg-counter-dec-button');
            self_this.container = self_this.input.wrap(self_this.container).parent();
            self_this.container.append(self_this.incButton);
            self_this.container.append(self_this.decButton);
            if (self_this.price = self_this.input.data('price')) {
                self_this.price_container = $('<span>');
                self_this.price_container.addClass('jg-counter-price-container');
                self_this.container.append(self_this.price_container);
            }
            self_this.input.change(onChange);
            self_this.input.keyup(onChange);
            self_this.container.click(onIncDec);
            if (self_this.price_container) updatePrice();
        }

        function onIncDec(event) {
            var className = event.target.className;
            var cur_count = parseInt(self_this.input.val()) || 1;
            switch (className) {
                case 'jg-counter-inc-button':
                    self_this.input.val(cur_count + 1);
                    break;
                case 'jg-counter-dec-button':
                    self_this.input.val(cur_count > 1 ? cur_count - 1 : 1);
                    break;
                default :
                    return;
            }
            if (self_this.price_container) updatePrice();
        }

        function onChange() {
            self_this.input.val(parseInt(self_this.input.val()) || 1);
            if (self_this.price_container) updatePrice();
        }

        function updatePrice() {
            self_this.price_container.html('$' + (Math.round((self_this.input.val() * self_this.price) * 100) / 100));
        }

        /***--***/
        draw();
        this.ok = true;
        return this
    }

    /******/

    /***   JgRatingRanger   ***/

    JgRatingRanger = function (elem) {
        this.ok = false;
        this.container = $(elem);
        var maxRating = 5;
        if (!this.container.length) return this;
        this.x = 0;
        this.current_rating = 0;
        this.rating = 0;
        var self_this = this;
        this.star_width = 0;

        /***-   private functions   -***/

        function updateStars(i) {
            if (!i) i = self_this.rating;
            self_this.current_rating = i;
            for (var j = 0; j < 6; ++j) {
                self_this.rating_start[j].hide();
            }
            self_this.rating_start[i].show();
        }

        function draw() {
            self_this.rating_start = [];
            for (var i = 0; i <= maxRating; ++i) {
                self_this.rating_start[i] = $('<img>');
                self_this.rating_start[i].attr('src', '/images/stars_' + i + '.gif');
                self_this.container.append(self_this.rating_start[i]);
            }
            updateStars();
            self_this.hidden = $('<input type="hidden" name="rating" value="0">');
            self_this.container.append(self_this.hidden);
            self_this.x = self_this.container.offset().left;
            self_this.star_width = 59 / 5;
            self_this.container.mouseenter(onMouseEnter);
            self_this.container.mousemove(onMouseMove);
            self_this.container.mouseleave(onMouseLeave);
            self_this.container.click(onClick);
        }

        function onMouseMove(event) {
            var left_offset = event.pageX - self_this.x;
            var stars_count = parseInt(left_offset / self_this.star_width + 1);
            if (stars_count > 0 && stars_count < 6) {
                updateStars(stars_count);
            }
        }

        function onMouseEnter(event) {
            if (!self_this.x) self_this.x = self_this.container.offset().left;
        }

        function onClick() {
            self_this.rating = self_this.current_rating;
            self_this.hidden.val(self_this.rating);
            updateStars();
        }

        function onMouseLeave() {
            updateStars();
        }

        /***--***/

        draw();
        this.ok = true;
        return this;
    };

    JgRatingRanger.prototype.reset = function () {
        this.current_rating = 0;
        this.rating = 0;
        for (var j = 0; j < 6; ++j) {
            this.rating_start[j].hide();
        }
        this.rating_start[0].show();
        this.hidden.val(0);
    }

    /******/

    //--- JgTabs {

    function JgTabs(selector) {
        this.ok = false;
        var tabs_container = $(selector);
        if (tabs_container.length) tabs_container = tabs_container.eq(0); else return;
        this.tabs_container = tabs_container;
        this.tabs_container.addClass('jg-tabs-container');
        var tabs_head = tabs_container.children('ul');
        if (tabs_head.length) tabs_head = tabs_head.eq(0); else return;
        this.tabs_head = tabs_head;
        this.tabs_head.addClass('jg-tabs-head');
        var tabs_body = tabs_container.children('div');
        if (tabs_body.length) tabs_body = tabs_body.eq(0); else return;
        this.tabs_body = tabs_body;
        this.tabs_body.addClass('jg-tabs-body');
        var tabId = window.location.hash.substr(1);
        if (!tabId || ~tabId.indexOf('&') || ~tabId.indexOf('=')) {
            tabId = this.tabs_head.children('li:first-child').eq(0).data('tab');
        }
        this.showTab(tabId, 'init');
        var self_this = this;

        function onTabsClick(event) {
            var tg = $(event.target);
            if (!tg.data('tab')) {
                tg = tg.parents('[data-tab]');
                if (!tg.length) {
                    return false;
                }
                tg = tg.eq(0);
            }
            var tabId = tg.data('tab');
            self_this.showTab(tabId, 'click');
            return false;
        }

        this.tabs_head.on('click', onTabsClick);
        this.ok = true;
        return this;
    }


    JgTabs.prototype.showTab = function (tabId, from) {
        var tabBody = this.tabs_body.children('#' + tabId);
        if (!tabBody.length) {
            tabBody = this.tabs_body.children(':first-child');
            tabId = tabBody.id;
        }
        var tabHead = this.tabs_head.children('li[data-tab="' + tabId + '"]');
        if (!tabHead.length) {
            tabHead = this.tabs_body.children(':first-child');
            tabId = tabHead.data('tab');
        }
        $.map(this.tabs_body.children('div'), function (elem) {
            $(elem).hide();
        });
        this.tabs_body.children().removeClass('jg-tabs-body-active');
        this.tabs_head.children().removeClass('jg-tabs-head-active');
        tabBody.fadeIn();
        tabBody.addClass('jg-tabs-body-active');
        tabHead.addClass('jg-tabs-head-active');
        this.afterTabChange && this.afterTabChange(tabId);
        //window.location.hash = tabId
        history.replaceState(undefined, undefined, window.location.origin + window.location.pathname +"#"+tabId);

        if(from == "init") {
            if (location.hash) {
                setTimeout(function() {
                    window.scrollTo(0, 0);
                }, 0);
            }
        }

      prepareShowMore();
    };
    //   }

    /*** JgPopup */

    function JgPopup(selector) {
        var popupContent = $(selector);
        if (!popupContent.length) return this;
        this.selector = selector;
        this.popup_content = popupContent.eq(0);
        this.popup_container = $('<div>');
        this.popup_container.addClass('jg-popup-container');
        this.popup_box = $('<div>');
        this.popup_box.addClass('jg-popup-box');
        this.background = $('<div>');
        this.background.addClass('jg-popup-background');
        this.closeButton = $('<div>');
        this.closeButton.addClass('jg-popup-close-button');
        this.popup_container.append(this.closeButton);
        this.popup_box.append(this.popup_content);
        this.popup_container.append(this.popup_box);
        this.popup_container.append(this.background);
        $(document.body).append(this.popup_container);
        var safe_this = this;

        /***   event handlers */

        function onKeyDown(event) {
            if (event.which == 27) {
                safe_this.hidePopup();
            }
        }

        function onCloseButtonClick() {
            safe_this.hidePopup();
        }

        /* event handlers   ***/

        this.closeButton.on('click', onCloseButtonClick);
        $(document).on("keydown", onKeyDown);

        return this;
    }

    JgPopup.prototype.showPopup = function () {
        this.popup_container.show();
    };

    JgPopup.prototype.hidePopup = function () {
        this.popup_container.hide();
    };

    /* JgPopup   ***/


    /***   loginFormsInit */
    function loginFormsInit(url) {
        var signInErrorStack = $('#signInMessages');

        function signInFormBefore() {
            $('#signInForm #tdb1').hide();
            $('#signInForm .login-loading').show();
        }

        function signInFormSuccess(response) {
            if (response.error !== undefined) {
                $('#signInForm .login-loading').hide();
                $('#signInForm #tdb1').show();
                signInErrorStack.hide();
                signInErrorStack.html(response.error);
                signInErrorStack.fadeIn();
            } else if (response.url !== undefined) {
                location.href = response.url;
            } else if (response.ok) {
                location.reload(true);
            }

        }


        var createAccountErrorStack = $('#createAccountMessages');

        function signUpFormBefore() {
            $('#createAccountForm #tdb2').hide();
            $('#createAccountForm .login-loading').show();
        }

        function createAccountFormSuccess(response) {
            if (response.error !== undefined) {
                $('#createAccountForm .login-loading').hide();
                $('#createAccountForm #tdb2').show();
                createAccountErrorStack.hide();
                createAccountErrorStack.html(response.error);
                createAccountErrorStack.fadeIn();
            } else if (response.url !== undefined) {
                location.href = response.url;
            } else if (response.ok) {
                location.reload(true);
            }
        }

        var passwordResetErrorStack = $('#passwordResetMessages');

        function passwordResetFormSuccess(response) {
            if (response.error !== undefined) {
                passwordResetErrorStack.html(response.error);
            } else {
                if (response.password_reset_initiated) {
                    $('#pass_res').hide();
                    $('#pass_res_init').show();
                }
            }
        }

        var signInFormOptions = {
            beforeSubmit: signInFormBefore,
            success: signInFormSuccess,
            data: {'url': url},
            dataType: 'json'
        };
        var createAccountFormOptions = {
            beforeSubmit: signUpFormBefore,
            success: createAccountFormSuccess,
            data: {'url': url},
            dataType: 'json'
        };
        var passwordResetFormOptions = {
            success: passwordResetFormSuccess,
            dataType: 'json'
        };

        $('#signInForm').ajaxForm(signInFormOptions);
        $('#createAccountForm').ajaxForm(createAccountFormOptions);
        $('#passwordResetForm').ajaxForm(passwordResetFormOptions);

        /***   facebook */

        var onfbclick = function (event) {
            FB.login(function (response) {
                if (response.authResponse) {
                    window.location.href = global_settings.dir + '/facebook_login?url=' + encodeURIComponent(url);
                }
            }, {scope: 'email, user_birthday, user_location, user_work_history, user_hometown'});
        };

        document.getElementById('fb-login-btn1').onclick = onfbclick;
        document.getElementById('fb-login-btn2').onclick = onfbclick;
    }

    /* loginFormsInit ***/

    /***   runOnSuccess */
    function runOnSuccess(data, tabId, url) {
        $(document.body).append(data);
        loginTabs = new JgTabs('#loginTabs');
        loginPopup = new JgPopup('#loginTabs');
        var loginTabsBody = loginTabs.tabs_body.children('div');
        for (var i = 0; i < loginTabsBody.length; ++i) {
            loginTabsBody.eq(i).css('height', '100%');
            /*loginTabsBody.eq(i).customScrollbar({
             skin: "default-skin",
             hScroll: false,
             updateOnWindowResize: true,
             preventDefaultScroll: true
             });*/

        }
        /*loginTabs.afterTabChange = function (toId) {
         $("#" + toId).customScrollbar("resize", false);
         };*/
        loginTabs.tabs_container.css('height', '100%');
        loginTabs.tabs_body.css('height', '100%');

        loginFormsInit(url);
        /*  facebook ***/
        if (tabId) showLoginPopup(tabId);
    }

    /* runOnSuccess   ***/

    /***   showLoginPopup */

    showLoginPopup = function (tabId, url) {
        if (!loginPopup) {
            $.ajax(global_settings.dir + "/login?ajax",
                {
                    dataType: "html",
                    success: function (data) {
                        runOnSuccess(data, tabId, url);
                    }
                }
            );
            return false;
        }
        loginPopup.showPopup();
        loginTabs.showTab(tabId);
        return false;
    };
    /* showLoginPopup   ***/


    /***   JgShoppingCart */
    function JgShoppingCart(button_id, products_container_id) {
        this.ok = false;
        var cartButton = $(button_id);
        if (!cartButton.length) return;
        var productsContainer = $(products_container_id);
        if (!productsContainer.length) return;
        this.cartButton = cartButton;
        this.productsContainer = productsContainer;
        this.count = 0;
        this.products = [];
        this.can_add = true;
        this.totalPrice = 0;
        var self_this = this;

        /***   private functions */
        function onCartClick() {
            self_this.productsContainer.parent().fadeToggle();
            return false;
        }

        function draw() {
            self_this.productsContainer.addClass('jg-shopping-cart-products');
            self_this.cartButton.on('click', onCartClick);
            self_this.table = $('<table>');
            self_this.total = $('<div>');
            var cur_prod;
            for (var i = 0; i < self_this.products.length; ++i) {
                if (self_this.products[i]) cur_prod = self_this.products[i]; else continue;
                cur_prod.draw();
            }
            self_this.productsContainer.append(self_this.table);
            self_this.productsContainer.append(self_this.total);
            self_this.buttonText = self_this.cartButton.find('span.ui-button-text')
        }

        (function loadProducts() {
            $.ajax('?action=give_products', {
                dataType: 'json',
                success: function (data) {
                    draw();
                    var newProd;
                    for (var i = 0; i < data.length; ++i) {
                        newProd = new JgProductsInCart(data[i].id, data[i].name, data[i].price, data[i].quantity, data[i].link);
                        if (newProd.ok) {
                            self_this.products.push(newProd);
                            self_this.count += parseInt(data[i].quantity);
                            self_this.totalPrice = Math.round((self_this.totalPrice + newProd.price * newProd.quantity) * 100) / 100;
                        }
                    }
                    self_this.total.html('$' + self_this.totalPrice);
                }
            });
        })();
        /* private functions   ***/
        this.ok = true;
    }

    JgShoppingCart.prototype.addProduct = function (id, name, price, quantity, link) {
        if (!this.can_add) return false;
        this.can_add = false;
        id = parseInt(id);
        quantity = parseInt(quantity);
        this.link = link;
        var cur_prod, self_this = this;
        for (var i = 0; i < this.products.length; ++i) {
            if (this.products[i]) cur_prod = this.products[i]; else continue;
            if (id === cur_prod.id) {
                addOnServer(id, cur_prod.quantity + quantity, function (data) {
                    if (!data.ok) return;
                    self_this.count += quantity;
                    animateAdding(function () {
                        self_this.can_add = true;
                        cur_prod.update(quantity);
                        self_this.totalPrice = Math.round((self_this.totalPrice + cur_prod.price * quantity) * 100) / 100;
                        self_this.total.html('$' + self_this.totalPrice);
                        var str = self_this.buttonText.html();
                        var re = /(^[^(]*)/;
                        var start_str = re.exec(str);
                        self_this.buttonText.html(start_str[1] + '(' + self_this.count + ')');
                    });
                });
                return false;
            }
        }
        var newProduct = new JgProductsInCart(id, name, price, quantity, link);
        if (newProduct.ok) {
            self_this.products.push(newProduct);
            addOnServer(id, quantity, function (data) {
                if (!data.ok) return;
                self_this.count += quantity;
                animateAdding(function () {
                    self_this.can_add = true;
                    self_this.totalPrice = Math.round((self_this.totalPrice + newProduct.price * newProduct.quantity) * 100) / 100;
                    self_this.total.html('$' + self_this.totalPrice);
                    var str = self_this.buttonText.html();
                    var re = /(^[^(]*)/;
                    var start_str = re.exec(str);
                    self_this.buttonText.html(start_str[1] + '(' + self_this.count + ')');
                });
            });
        }

        /***   private functions */

        function animateAdding(callback) {
            var products_image = $('#image-' + id);
            if (!products_image.length) return callback();
            var targetView = {};
            targetView.x = self_this.cartButton.offset().left;
            targetView.y = self_this.cartButton.offset().top;
            targetView.height = self_this.cartButton.innerHeight();
            var avatar = products_image.clone();
            var startView = {};
            startView.x = products_image.offset().left;
            startView.y = products_image.offset().top;
            startView.width = products_image.innerWidth();
            startView.height = products_image.innerHeight();

            avatar.css(
                {
                    'position': 'absolute',
                    'zIndex': '2',
                    'left': startView.x,
                    'top': startView.y,
                    'width': startView.width,
                    'height': startView.height
                }
            );
            $(document.body).append(avatar);
            avatar.animate(
                {
                    'left': targetView.x,
                    'top': targetView.y,
                    'width': targetView.height * (startView.width / startView.height),
                    'height': targetView.height,
                    'opacity': 0.5
                },
                500,
                function () {
                    avatar.remove();
                    callback();
                }
            );
            return false;
        }

        function addOnServer(id, quantity, callback) {
            $.ajax('?action=add_product', {
                    data: {
                        'ajax': '',
                        'products_id': id,
                        'products_quantity': quantity
                    },
                    async: false,
                    dataType: 'json',
                    type: 'post',
                    success: callback
                }
            );
        }

        /* private functions   ***/

        return false;
    };

    JgShoppingCart.prototype.removeProduct = function (id, quantity) {
        if (!this.can_add) return false;
        this.can_add = false;
        id = parseInt(id);
        quantity = parseInt(quantity);
        var cur_prod;
        for (var i = 0; i < this.products.length; ++i) {
            if (this.products[i]) cur_prod = this.products[i]; else continue;
            if (id === cur_prod.id) {
                if (cur_prod.quantity <= quantity || quantity === 0) {
                    var self_this = this;
                    $.ajax(
                        '?action=remove_product&products_id=' + id,
                        {
                            data: {
                                ajax: ''
                            },
                            dataType: 'json',
                            type: 'post',
                            success: function (data) {
                                self_this.can_add = true;
                                if (!data.ok) return;
                                self_this.count -= cur_prod.quantity;
                                self_this.totalPrice = Math.round((self_this.totalPrice - cur_prod.price * cur_prod.quantity) * 100) / 100;
                                cur_prod.remove();
                                delete self_this.products[i];
                                self_this.total.html('$' + self_this.totalPrice);
                                var str = self_this.buttonText.html();
                                var re = /(^[^(]*)/;
                                var start_str = re.exec(str);
                                self_this.buttonText.html(start_str[1] + '(' + self_this.count + ')');
                            }
                        }
                    );
                } else {
                    cur_prod.update(-quantity);
                    this.count += cur_prod.quantity;
                    this.totalPrice = Math.round((this.totalPrice - cur_prod.price * (-quantity)) * 100) / 100
                }
                return false;
            }
        }
        return false;
    };

    /* JgShoppingCart   ***/

    /***   JgProductsInCart */

    function JgProductsInCart(id, name, price, quantity, link) {
        this.ok = false;
        if (id !== undefined) this.id = parseInt(id); else return;
        if (name !== undefined) this.name = name; else return;
        if (price !== undefined) this.price = parseFloat(price); else return;
        if (link !== undefined) this.link = link; else return;
        this.quantity = parseInt(quantity) || 1;
        this.draw();
        this.ok = true;
    }

    JgProductsInCart.prototype.remove = function () {
        this.row.remove();
        delete this.row;
    };

    JgProductsInCart.prototype.update = function (quantity) {
        quantity = parseInt(quantity);
        this.quantity += parseInt(quantity) || 1;
        this.draw();
    };

    JgProductsInCart.prototype.draw = function () {
        var newRow = $('<tr>');
        newRow.append($('<td><a href="' + this.link + '">' + this.name + '</a></td>'));
        newRow.append($('<td style="text-align: right">$' + this.price + '</td>'));
        newRow.append($('<td style="text-align: center">x</td>'));
        newRow.append($('<td style="text-align: left">' + this.quantity + '</td>'));
        var remove_filed = $('<td style="text-align: center"><span class="jg-shopping-cart-remove-product"></span></td>');
        var self_this = this;
        remove_filed.click(function () {
            myCart.removeProduct(self_this.id, 0);
        });
        newRow.append(remove_filed);
        if (this.row) this.row.replaceWith(newRow); else myCart.table.append(newRow);
        this.row = newRow;
    };

    /* JgProductsInCart   ***/


    myCart = new JgShoppingCart('#myCart', '#cart-products-container');
    loginTabs = new JgTabs('#loginTabs');
    if (loginTabs.ok) {
        loginFormsInit();
    }
    loginTabs = new JgTabs('#pInfoTabs');
    var alCounters = $('.numCounter');
    alCounters.map(function (i, elem) {
        new JgCounter(elem);
    });


    //$('#right-col-hidden').click(function() {$(document.body).removeClass('hidden-right-col');$(window).resize();});
    $('#hide-right-col').click(function () {
        $(document.body).toggleClass('hidden-right-col');
    });

    // Harut's addition
  //prepareShowMore();


  $('#search-form').submit(function () {
    if ($.trim(this.keywords.value).length < 3) return false;
  });



});

var shopClient = ShopifyBuy.buildClient({
  apiKey: '89e2a646a16e81afd559fa49b3ebb17f',
  domain: 'vitaminized.myshopify.com',
  appId: '6'
});

var cart;
/*shopClient.createCart().then(function (newCart) {
  cart = newCart;
  // do something with updated cart
});*/

if(localStorage.getItem('lastCartId')) {
  shopClient.fetchCart(localStorage.getItem('lastCartId')).then(function(remoteCart) {
    cart = remoteCart;
    cartLineItemCount = cart.lineItems.length;
    //renderCartItems();
  });
} else {
  shopClient.createCart().then(function (newCart) {
    cart = newCart;
    localStorage.setItem('lastCartId', cart.id);
    cartLineItemCount = 0;
  });
}

function buyNowQuick(key) {
  shopClient.fetchProduct(key)
.then(function (product) {
    console.log(product);
    cart.createLineItemsFromVariants({
      variant: product.selectedVariant,
      quantity: 1
    }).then(function (cart) {
      // do something with updated cart
      window.location.href = cart.checkoutUrl;
    });
  })
    .catch(function () {
      console.log('Request failed');
    });
  return false;
}

function prepareShowMore() {
  var regex = /\[more\]([\s\S]*)\[endmore\]/g;

  var data = $('#scientific').html();

  if(data) {
	  data =  data.replace(/\[more\]/g, '<div class="text-content short-text">');
	  data =  data.replace(/\[endshort\]/g, '</div><div class="text-content long-text">');
	  data =  data.replace(/\[endmore\]/g, '</div><div class="show-more"><a href="#">Show more</a></div>');
	  $('#scientific').html(data);

	  $('.long-text').hide();

	  $(".show-more a").on("click", function() {
		  var $link = $(this);
		  var $content = $link.parent().prev("div.text-content");
		  var linkText = $link.text();

		  var shortText = $(this).parent().prev().prev();
		  var longText = $(this).parent().prev();

		  shortText.toggle();
		  if(longText.is(":visible")) {
			  $(window).scrollTop($content.offset().top - 200);
			  longText.slideUp('fast');
		  }
		  else {
			  longText.slideDown('fast');
		  }

		  $link.text(getShowLinkText(linkText));

		  return false;
	  });
  }


  function getShowLinkText(currentText) {
    var newText = '';

    if (currentText.toUpperCase() === "SHOW MORE") {
      newText = "Show less";
    } else {
      newText = "Show more";
    }

    return newText;
  }
}


function validateName() {

  var name = document.getElementsByName('name')[0].value;

  if(name.length == 0) {
    producePrompt('Name is required', 'name-error' , '#fd9393');
    return false;
  }

  /*if (!name.match(/^[A-Za-z]*\s{1}[A-Za-z]*$/)) {
    producePrompt('First and last name, please.','name-error', 'red');
    return false;
  }*/

  producePrompt('Valid', 'name-error', '#73ad05');
  return true;

}

function validateEmail() {

  var email = document.getElementsByName('email')[0].value;
  if (email.length == 0) {
    producePrompt('Email Invalid', 'email-error', '#fd9393');
    return false;
  }

  if (!email.match(/^[A-Za-z\._\-[0-9]*[@][A-Za-z]*[\.][a-z]{2,4}$/)) {
    producePrompt('Email Invalid', 'email-error', '#fd9393');
    return false;
  }

  producePrompt('Valid', 'email-error', '#73ad05');
  return true;

}

function validateMessage() {
  var message = document.getElementsByName('message')[0].value;
  var required = 30;
  var left = required - message.length;

  if (left > 0) {
    producePrompt(left + ' more characters required', 'message-error', '#fd9393');
    return false;
  }

  producePrompt('Valid', 'message-error', '#73ad05');
  return true;

}

function sendFeedback() {
  $('.error-message').css('display', 'inline-table');
  if (!validateName() || !validateEmail() || !validateMessage()) {
    jsShow('submit-error');
    producePrompt('Please fix errors to send an email', 'submit-error', '#fd9393');
    //setTimeout(function(){jsHide('submit-error');}, 3000);
    return false;
  }
  else {

    var name = document.getElementsByName('name')[0].value;
    var email = document.getElementsByName('email')[0].value;
    var message = document.getElementsByName('message')[0].value;

    $('.feedback-button').html('<img src="images/loader.gif"/>');
    $('.feedback-button').attr('disabled', 'disabled');
    $.ajax({
      url: global_settings.ajax_url + '/feedback',
      method: "POST",
      data: {name:name, email:email, message:message},
      success: function (resp) {
        var timeout = 3000;
        if(resp.success) {
          producePrompt('Message has been sent successfully', 'submit-error', '#73ad05');
          var timeout = 5000;
          document.getElementById("feedbackForm").reset();
        }
        else {
          producePrompt('There is some error, please try later', 'submit-error', '#fd9393');
        }
        $('.feedback-button').html('SEND');
        $('.feedback-button').attr('disabled', false);
        $('.error-message').hide();
        jsShow('submit-error');
        setTimeout(function(){
          jsHide('submit-error');

        }, timeout);

      }

    });

  }
}

function jsShow(id) {
  document.getElementById(id).style.display = 'block';
}

function jsHide(id) {
  document.getElementById(id).style.display = 'none';
}

function producePrompt(message, promptLocation, color) {
  document.getElementById(promptLocation).innerHTML = message;
  document.getElementById(promptLocation).style.color = color;
}

function addToCart(shopify_id, el) {

  shopClient.fetchProduct(shopify_id)
    .then(function (product) {
      cart.createLineItemsFromVariants({
        variant: product.selectedVariant,
        quantity: 1
      }).then(function (cart) {
        refreshCard();
      });
    })
    .catch(function () {
      console.log('Request failed');
    });

    var oldVal =  $(el).html();
    $(el).html("ADDED TO CART");
    setTimeout(function () {
      $(el).html(oldVal);
    }, 1500);

}

function refreshCard() {
    var cardView = $('.cart-quantity');

    if (cart.lineItems.length == 0) {
        cardView.html("");
    }
    else {
      cardView.html("("+ cart.lineItems.length + ")");
    }
}

function onCardClear() {
  cart.clearLineItems();
  refreshCard();
}

/* Shopify cart functions*/

/* Find Cart Line Item By Variant Id
 ============================================================ */
function findCartItemByVariantId(variantId) {
  return cart.lineItems.filter(function (item) {
    return (item.variant_id === variantId);
  })[0];
}

/* Update product variant quantity in cart
 ============================================================ */
function updateQuantity(fn, variantId) {
  var variant = product.variants.filter(function (variant) {
    return (variant.id === variantId);
  })[0];
  var quantity;
  var cartLineItem = findCartItemByVariantId(variant.id);
  if (cartLineItem) {
    quantity = fn(cartLineItem.quantity);
    updateVariantInCart(cartLineItem, quantity);
  }
}

/* Decrease quantity amount by 1
 ============================================================ */
function decrementQuantity(variantId) {
  updateQuantity(function(quantity) {
    return quantity - 1;
  }, variantId);
}

/* Increase quantity amount by 1
 ============================================================ */
function incrementQuantity(variantId) {
  updateQuantity(function(quantity) {
    return quantity + 1;
  }, variantId);
}
