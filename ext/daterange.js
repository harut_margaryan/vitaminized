!function($) {
    window.DateRange = function(options) {
        this.settings = $.extend({
            dateFrom: new Date(0),
            minDate: new Date(0),
            dateTo: new Date(),
            maxDate: new Date(),
            dateFormat: 'dd.mm.yy',
            numberOfMonths: 2,
            changeMonth: true,
            minRange: 0,
            events: {}
        }, options);

        this.$container = $(this.settings.container);

        this.$range_from = $('<input type="text">');
        this.$range_to = $('<input type="text">');

        this.$container.append(this.$range_from);
        this.$container.append(this.$range_to);

        var _this = this;

        this.$range_from.datepicker({
            dateFormat: this.settings.dateFormat,
            minDate: this.settings.minDate,
            changeMonth: this.settings.changeMonth,
            numberOfMonths: this.settings.numberOfMonths,
            onSelect: function (selectedDate) {
                _this.from($.datepicker.parseDate(_this.settings.dateFormat, selectedDate));
                _this.$range_from.blur();
                _this.settings.events.onChange && _this.settings.events.onChange(_this.settings.dateFrom, _this.settings.dateTo);
            }
        });

        this.$range_to.datepicker({
            dateFormat: this.settings.dateFormat,
            maxDate: this.settings.maxDate,
            changeMonth: this.settings.changeMonth,
            numberOfMonths: this.settings.numberOfMonths,
            onSelect: function (selectedDate) {
                _this.to($.datepicker.parseDate(_this.settings.dateFormat, selectedDate));
                _this.$range_to.blur();
                _this.settings.events.onChange && _this.settings.events.onChange(_this.settings.dateFrom, _this.settings.dateTo);
            }
        });

        this.from(this.settings.dateFrom).to(this.settings.dateTo);

        return this;
    };

    DateRange.prototype.from = function (newDate) {
        this.settings.dateFrom = newDate;
        this.$range_from.datepicker('setDate', this.settings.dateFrom);
        this.$range_to.datepicker('option', 'minDate', new Date(this.settings.dateFrom.getTime() + this.settings.minRange * 3600 * 24 * 1000));
        return this;
    };

    DateRange.prototype.to = function (newDate) {
        this.settings.dateTo = newDate;
        this.$range_to.datepicker('setDate', this.settings.dateTo);
        this.$range_from.datepicker('option', 'maxDate', new Date(this.settings.dateTo.getTime() - this.settings.minRange * 3600 * 24 * 1000));
        return this;
    };

    DateRange.prototype.onChange = function (f) {
        this.settings.events.onChange = f;
    };
}(jQuery);