$(function() {

    /** helpers **/

    if (!Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
            if (typeof this !== "function") {
                // closest thing possible to the ECMAScript 5
                // internal IsCallable function
                throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
            }

            var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP = function () {},
                fBound = function () {
                    return fToBind.apply(this instanceof fNOP && oThis
                            ? this
                            : oThis,
                        aArgs.concat(Array.prototype.slice.call(arguments)));
                };

            fNOP.prototype = this.prototype;
            fBound.prototype = new fNOP();

            return fBound;
        };
    }

    function is_scalar(val) {
        return (/boolean|number|string/).test(typeof val);
    }

    function className(v) {
        return _.settings.classPrefix + v;
    }

    function dataName(v) {
        return _.settings.dataPrefix ? _.settings.dataPrefix + ucfirst(v) : v;
    }

    function ucfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function is_bool(val) {
        return typeof val === 'boolean';
    }

    function is_valid_prod_id(val) {
        return is_scalar(val) && !is_bool(val);
    }

    function is_valid_prod_html(val) {
        return is_scalar(val) && !is_bool(val);
    }

    function toDash(text) {
        return text.replace(new RegExp('([a-z0-9])([A-Z])', 'g'), function($0, $1, $2) {
            return $1 + '-' + $2.toLowerCase()
        });
    }

    function toCamel(text) {
        return $.camelCase(text);
    }

    function is_object(val) {
        return Object.prototype.toString.call(val) === '[object Object]';
    }

    function is_array(val) {
        return val instanceof Array;
    }
	
	function numberFormat(number) {
		return number_format(number, _.settings.numberFormat.decimals, _.settings.numberFormat.decPoint, _.settings.numberFormat.thousSep);
	}
	
	function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
		// 
		// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// +	 bugfix by: Michael White (http://crestidg.com)
	
		var i, j, kw, kd, km;
	
		// input sanitation & defaults
		if( isNaN(decimals = Math.abs(decimals)) ){
			decimals = 2;
		}
		if( dec_point == undefined ){
			dec_point = ",";
		}
		if( thousands_sep == undefined ){
			thousands_sep = ".";
		}
	
		i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
	
		if( (j = i.length) > 3 ){
			j = j % 3;
		} else{
			j = 0;
		}
	
		km = (j ? i.substr(0, j) + thousands_sep : "");
		kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
		//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
		kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
	
	
		return km + kw + kd;
	}



    /** ??? **/

    function countProdsTotalQty() {
        _.totalQty = 0;
        for (var prop in _.products) {
            if (_.products.hasOwnProperty(prop) && _.products[prop].qty) {
                _.totalQty += _.products[prop].qty;
            }
        }
    }

    function animateFly(prodObj, onAnimateFlyComplete) {
        var $fly = null;
        if (_.$flyTo) {
            if (prodObj.adderId) {
                $fly = $('.' + className('fly') + '[data-' + toDash(dataName('adder-id')) + '="' + prodObj.adderId + '"]');
            }
            if ((!$fly || !$fly.length) && prodObj.prodId) {
                $fly = $('.' + className('fly') + '[data-' + toDash(dataName('prod-id')) + '="' + prodObj.prodId + '"]');
            }
        }
        if (!$fly || !$fly.length){
            //cant animate without fly elem
            onAnimateFlyComplete(false);
            return;
        }

        $fly = $fly.eq(0);

        var targetView = {};
        targetView.x = _.$flyTo.offset().left;
        targetView.y = _.$flyTo.offset().top;
        targetView.height = _.$flyTo.innerHeight();
        var avatar = $fly.clone();
        var startView = {};
        startView.x = $fly.offset().left;
        startView.y = $fly.offset().top;
        startView.width = $fly.innerWidth();
        startView.height = $fly.innerHeight();

        avatar.css(
            {
                'position' : 'absolute',
                'zIndex' : '2',
                'left' : startView.x,
                'top' : startView.y,
                'width' : startView.width,
                'height' : startView.height
            }
        );
        $(document.body).append(avatar);
        avatar.animate(
            {
                'left' : targetView.x,
                'top' : targetView.y,
                'width' : targetView.height * (startView.width / startView.height),
                'height' : targetView.height,
                'opacity' : 0.2
            },
            500,
            function () {
                avatar.remove();
                onAnimateFlyComplete(true);
            }
        );
    }

    function addProd(prodObj) {
        !is_object(_.products) && (_.products = {});
        if (!is_valid_prod_id(prodObj.id)) return false;

        prodObj.id += '';
        prodObj.qty = parseInt(prodObj.qty) || 1;
        prodObj.qty < 1 && (prodObj.qty = 1);
        prodObj.html = !prodObj.html || (typeof prodObj.html != 'string') ? null : prodObj.html;
        if (prodObj.html) {
            prodObj.$elem = $(prodObj.html);
            !prodObj.$elem.length && (prodObj.$elem = null);
        } else {
            prodObj.$elem = null
        }

        var existing_prod = getProd(prodObj.id);
        if (existing_prod === false) {
			var $prodTotalPrice = $('.' + className('prod-total-price') + '[data-' + toDash(dataName('prodId')) + '="' + prodObj.id + '"]');
			var price = 0;
			if ($prodTotalPrice.length) {
				price = parseInt($prodTotalPrice.data(dataName('prodPrice')));
				if (price < 0 || !price) {
					$prodTotalPrice = null;
					price = 0;
				}
			} else {
				$prodTotalPrice = null;
			}
			
            var newProd = {
                id : prodObj.id,
                qty : prodObj.qty,
                $elem : prodObj.$elem,
				price : price,
				$totalPrice : $prodTotalPrice
            };
            _.products[newProd.id] = newProd;
        } else {
            existing_prod.qty = parseInt(existing_prod.qty) || 0;
            existing_prod.qty < 0 && (existing_prod.qty = 0);
            existing_prod.qty += prodObj.qty;
        }

        countProdsTotalQty();

        return true;
    }

    function updateProd(prodObj) {
        !is_object(_.products) && (_.products = {});
        if (!is_valid_prod_id(prodObj.id)) return false;

        prodObj.id += '';
        prodObj.qty = parseInt(prodObj.qty) || 1;
        prodObj.qty < 1 && (prodObj.qty = 1);

        var existing_prod = getProd(prodObj.id);
        if (existing_prod === false) {
			var $prodTotalPrice = $('.' + className('prod-total-price') + '[data-' + toDash(dataName('prodId')) + '="' + prodObj.id + '"]');
			var price = 0;
			if ($prodTotalPrice.length) {
				price = parseInt($prodTotalPrice.data(dataName('prodPrice')));
				if (price < 0 || !price) {
					$prodTotalPrice = null;
					price = 0;
				}
			} else {
				$prodTotalPrice = null;
			}
			
            var newProd = {
                id : prodObj.id,
                qty : prodObj.qty,
				price : price,
				$totalPrice : $prodTotalPrice
            };
            _.products[newProd.id] = newProd;
        } else {
            existing_prod.qty = prodObj.qty;
        }

        countProdsTotalQty();

        return true;
    }

    function removeProd(prodObj) {
        !is_object(_.products) && (_.products = {});
        if (!is_valid_prod_id(prodObj.id)) return false;

        prodObj.id += '';

        if (!getProd(prodObj.id)) return false;
        delete _.products[prodObj.id];
        countProdsTotalQty();

        return true;
    }

    function getProd(id) {
        if (!is_valid_prod_id(id)) return false;
        id += '';
        return _.products[id] || false;
    }
	
	function getTotalPrice() {
		var $totalPrice = $('.' + className('total-price'));
		var totalPrice = 0;
		if ($totalPrice.length) {
			$totalPrice = $totalPrice;
		} else {
			$totalPrice = null;
		}
		
		for(var id in _.products) {
			if (_.products.hasOwnProperty(id) && _.products[id].price) {
				totalPrice += _.products[id].price * _.products[id].qty;
			}
		}
		return {$totalPrice : $totalPrice, totalPrice : totalPrice};
	}
	
	function updateTotalPrice() {
		var obj = getTotalPrice();
		obj.$totalPrice && obj.$totalPrice.html(numberFormat(obj.totalPrice));
	}
	
	function updateProdTotalPrice(id) {
		var prod = getProd(id);
		prod && prod.$totalPrice && prod.$totalPrice.html(numberFormat(prod.price * prod.qty));
	}

    function reDrawBasket() {
        _.$totalQty && _.$totalQty.html(_.totalQty);
        if (_.$products) {
            _.$products.empty();
            for(var prop in _.products) {
                if (_.products.hasOwnProperty(prop)) {					
                    if (_.products[prop].$elem) {
                        _.$products.append(_.products[prop].$elem);
                    }
                }
            }
			
			if (_.products['prepend']) _.$products.prepend(_.products['prepend']);
			if (_.products['postpend']) _.$products.append(_.products['postpend']);
        }
		
		for(var prop in _.products) {
			if (_.products.hasOwnProperty(prop)) {
				var $prodTotalPrice = $('.' + className('prod-total-price') + '[data-' + toDash(dataName('prodId')) + '="' + prop + '"]');
				var price = 0;
				if ($prodTotalPrice.length) {
					price = parseInt($prodTotalPrice.data(dataName('prodPrice')));
					if (price < 0 || !price) {
						$prodTotalPrice = null;
						price = 0;
					}
				} else {
					$prodTotalPrice = null;
				}
				_.products[prop].$totalPrice = $prodTotalPrice;
				_.products[prop].price = price;
				updateProdTotalPrice(prop);
			}
		}

        $('.' + className('qty')).each(function() {
            var $this = $(this);
            var prodId = $this.data(dataName('prodId'));
            var prod;
            if (prodId) {
                if (prod = getProd(prodId)) {
                    $this.jgCounter('reset');
                    $this.jgCounter(_.settings.jgCounter);
					$this.jgCounter({value: prod.qty});
                }
            }
        });
        initMaxQtys();
        updateTotalPrice();
    }

    function initAdders() {
        $(document).on('click', '.' + className('adder'), onAdderClick);
    }

    function initUpdaters() {
        $(document).on('jgCounterUserChange', '.' + className('updater'), onUpdaterChange);
    }

    function initRemovers() {
        $(document).on('click', '.' + className('remover'), onRemoverClick);
    }

    function initCounters() {
        $('.' + className('updater-qty') + ', .' + className('adder-qty')).each(function() {
            var $this = $(this);
            if (($this.hasClass(className('updater-qty')) && $this.data(dataName('updaterId'))) || ($this.hasClass(className('adder-qty')) && $this.data(dataName('adderId')))) {
                $this.jgCounter(_.settings.jgCounter);
            }
        });
    }

    function initMaxQtys() {
        $('.' + className('prod-max-qty')).each(function() {
            var $this = $(this);
            var prodId = $this.data(dataName('prodId'));
            if (!prodId) return;
            var prod = getProd(prodId);
            var maxUpdaterQty = parseInt(+$this.val()) || 100;
            var maxAdderQty = maxUpdaterQty;
            if (prod) {
                maxAdderQty -= prod.qty;
            }
            updateUpdatersMax(prodId, maxUpdaterQty);
            updateAddersMax(prodId, maxAdderQty);
        });
    }

    function updateAddersMax(prodId, max) {
        var $adders = $('.' + className('adder') + '[data-' + toDash(dataName('prodId')) + '="' + prodId + '"]');
        $adders.length && $adders.each(function() {
            var $this = $(this);
            if (max < 1) {
                $this.prop('disabled', true);
            } else {
                $this.prop('disabled', false);
            }
        });
        var $addersQty = $('.' + className('adder-qty') + '[data-' + toDash(dataName('prodId')) + '="' + prodId + '"]');
        $addersQty.length && $addersQty.each(function() {
            var $this = $(this);
            if (max < 1) {
                $this.jgCounter({max : 1});
                $this.prop('disabled', true);
            } else {
                $this.jgCounter({max : max});
                $this.prop('disabled', false);
            }
        });
    }

    function updateUpdatersMax(prodId, max) {
        var $updatersQty = $('.' + className('updater-qty') + '[data-' + toDash(dataName('prodId')) + '="' + prodId + '"]');
        $updatersQty.length && $updatersQty.each(function() {
            var $this = $(this);
            if (max < 1) {
                $this.jgCounter({max : 1});
                $this.prop('disabled', true);
            } else {
                $this.jgCounter({max : max});
                $this.prop('disabled', false);
            }
        });
    }

    function initElems() {
        initAdders();
        initUpdaters();
        initRemovers();
        initCounters();
        initMaxQtys();
    }

    /** event handlers **/

    function onAdderClick() {
        if (_.settings.loading) return;
        _.settings.loading = true;

        var $this = $(this);

        var prodId = $this.data(dataName('prodId')), adderId = $this.data(dataName('adderId')), qty = 1;
        if (!prodId) {
            _.settings.loading = false;
            return;
        }

        var $qty = $('.' + className('adder-qty') + '[data-' + toDash(dataName('adderId')) + '="' + adderId + '"]');
        if ($qty.length) {
            $qty = $qty.eq(0);
            qty = parseInt($qty.jgCounter('value'));
            qty = qty > 0 ? qty : 1;
        } else {
            $qty = null;
        }

        var infoObj = {$adder : $this, $qty : $qty, id: prodId, qty : qty};
        _.events.beforeProductAdding && _.events.beforeProductAdding(infoObj);
        ajaxAdd(infoObj, function (success) {
            if (success) {
                animateFly({prodId : prodId, adderId : adderId}, function (success) {
                    infoObj.$qty.jgCounter({value : 1});
                    reDrawBasket();
                    _.events.afterProductAdding && _.events.afterProductAdding(infoObj, true);
                    _.settings.loading = false;
                });
            } else {
                _.events.afterProductAdding && _.events.afterProductAdding(infoObj, false);
                _.settings.loading = false;
            }
        });
    }

    function onUpdaterChange() {
        clearTimeout(_.onJgCounterUserChangeTimer);
        _.onJgCounterUserChangeTimer = setTimeout(onUpdaterChangeTime.bind(this), _.settings.updateDelay);
    }

    function onUpdaterChangeTime() {
        if (_.settings.loading) return;
        _.settings.loading = true;

        var $this = $(this);

        var prodId = $this.data(dataName('prodId')), updaterId = $this.data(dataName('updaterId')), qty = 1;
        if (!prodId || !getProd(prodId)) {
            _.settings.loading = false;
            return;
        }



        if ($this.hasClass(className('updater-qty'))) {
            var $qty = $this;
        } else {
            $qty = $('.' + className('updater-qty') + '[data-' + toDash(dataName('updaterId')) + '="' + updaterId + '"]');
        }

        if ($qty.length) {
            $qty = $qty.eq(0);
            qty = parseInt($qty.jgCounter('value'));
            qty = qty > 0 ? qty : 1;
        } else {
            $qty = null;
        }

        var infoObj = {$updater : $this, $qty : $qty, id: prodId, qty : qty};
        _.events.beforeProductUpdating && _.events.beforeProductUpdating(infoObj);
        ajaxUpdate(infoObj, function (success, response) {
            if (success) {
                var updated = updateProd(infoObj);
                updated && reDrawBasket();
				infoObj.qty = qty;
                _.events.afterProductUpdating && _.events.afterProductUpdating(infoObj, updated, response);
            } else {
                _.events.afterProductUpdating && _.events.afterProductUpdating(infoObj, false, response);
            }
            _.settings.loading = false;
        });
    }

    function onRemoverClick() {
        if (_.settings.loading) return;
        _.settings.loading = true;

        var $this = $(this);

        var prodId = $this.data(dataName('prodId')), adderId = $this.data(dataName('adderId')), qty = 1;
        if (!prodId) {
            _.settings.loading = false;
            return;
        }

        var infoObj = {$remover : $this, id : prodId};
		_.events.beforeProductRemoving && _.events.beforeProductRemoving(infoObj);
        ajaxRemove(infoObj, function (success) {
            if (success) {
                removeProd(infoObj) && reDrawBasket();
            }
			_.events.afterProductRemoving && _.events.afterProductRemoving(_.totalQty);
            _.settings.loading = false;
        });
    }

    /** ajax functions **/

    function ajaxAdd(data, onAddComplete) {
        var success = false;
        if (!_.settings.addURL) {
            onAddComplete(success);
            return;
        }
        $.ajax({
            complete : function() {
                onAddComplete(success);
            },
            context : _,
            data : {
                id : data.id,
                qty : data.qty
            },
            dataType : 'json',
            success : function (resp) {
                addProd(resp) && (success = true);
            },
            type : 'POST',
            url : _.settings.addURL
        });
    }

    function ajaxUpdate(data, onUpdateComplete) {
        var success = false;
        var response = '';
        if (!_.settings.updateURL) {
            onUpdateComplete(success, response);
            return;
        }
        $.ajax({
            complete : function() {
                onUpdateComplete(success, response);
            },
            context : _,
            data : {
                id : data.id,
                qty : data.qty
            },
            dataType : 'json',
            success : function (resp) {
                if (resp) {
                    success = true;
                    response = resp;
                }
            },
            type : 'POST',
            url : _.settings.updateURL
        });
    }

    function ajaxRemove(data, onRemoveComplete) {
        var success = false;
        if (!_.settings.removeURL) {
            onRemoveComplete(success);
            return;
        }
        $.ajax({
            complete : function() {
                onRemoveComplete(success);
            },
            context : _,
            data : {
                id : data.id
            },
            dataType : 'text',
            success : function (resp) {
                if (resp === "OK") {
                    success = true;
                }
            },
            type : 'POST',
            url : _.settings.removeURL
        });
    }

    function ajaxLoad(data, onLoadComplete) {
        var success = false;
        if (!_.settings.loadURL) {
            onLoadComplete(success);
            return;
        }
        $.ajax({
            complete : function() {
                onLoadComplete(success);
            },
            context : _,
            dataType : 'json',
            data : data,
            success : function (resp) {
                if (is_object(resp)) {
                    for (var prop in resp) {
                        if (resp.hasOwnProperty(prop)) {
							if (prop === 'prepend' || prop === 'postpend') {
								_.products[prop] = $(resp[prop]);
							} else {
                            	addProd(resp[prop]) && (success = true);
							}
                        }
                    }
                }
            },
            type : 'POST',
            url : _.settings.loadURL
        });
    }

    /** init **/

    var _ = {};
    _.settings = $.extend({
        dataPrefix : 'jgBasket',
        classPrefix : 'jg-basket-',
        updateDelay : 300,
		jgCounter : {
			max : 100,
			min : 1,
			step : 1,
			default : 1
		},
		numberFormat : {
			decimals : 2,
			decPoint : '.',
			thousSep : ' '		
		}
    }, window.JgBasketSettings);
    _.products = {};
    _.totalQty = 0;
    _.events = {};
    if (_.settings.events) {
        _.events = _.settings.events;
    }
    _.onJgCounterUserChangeTimer = 0;

    _.$flyTo = $('.' + className('fly-to'));
    _.$flyTo = _.$flyTo.length ? _.$flyTo.eq(0) : null;

    _.$totalQty = $('.' + className('total-qty'));
    !_.$totalQty.length && (_.$totalQty = null);

    _.$products = $('.' + className('products'));
    !_.$products.length && (_.$products = null);

    initElems();

    if (_.settings.loadURL) {
        _.settings.loading = true;
		_.events.beforeProductLoading && _.events.beforeProductLoading();
        ajaxLoad({}, function(success) {
            success && reDrawBasket();
            _.settings.loading = false;
			_.events.afterProductLoading && _.events.afterProductLoading(success);
        });
    }
});