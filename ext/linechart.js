!function($) {
    $.fn.lineChart = function (options) {
        var settings = $.extend({
            datepicker : true,
            title : 'Title',
            minRange : 24 * 3600000 * 30
        }, options);
        var vars = {};
        vars.container = this;
        vars.chart_container = $('<div>');
        vars.container.append(vars.chart_container);
        vars.chart_container.height(400);
        vars.container.addClass('loading');

        $.getJSON(settings.url, null, function (response) {
            vars.container.removeClass('loading');
            if (!response.series) {
                vars.container.empty();
                return;
            }
            settings = $.extend(settings, response);

            vars.chart_container.highcharts('StockChart', {
                rangeSelector : {
                    buttons: [{
                        type: 'month',
                        count: 3,
                        text: '3m'
                    },  {
                        type: 'month',
                        count: 6,
                        text: '6m'
                    }, {
                        type: 'year',
                        count: 1,
                        text: '1y'
                    }, {
                        type: 'all',
                        text: 'All'
                    }],
                    selected : 5,
                    inputEnabled: !settings.datepicker
                },
                title : {
                    text : settings.title
                },
                tooltip : settings.tooltip || {},
                xAxis : {
                    events : {
                        afterSetExtremes : function (e) {
                            e.min = parseInt(e.min);
                            e.max = parseInt(e.max);
                            vars.container.addClass('loading');
                            $.getJSON(settings.url + (~settings.url.indexOf('?') ? '&' : '?') + 'start=' + Math.round(e.min) + '&end='+ Math.round(e.max), function(response) {
                                settings = $.extend(settings, response);
                                for (var i = 0; i < settings.series.length; i++) {
                                    var curs = settings.series[i];
                                    vars.chart.series[i].setData(curs.data);
                                }
                                settings.dateFrom = new Date(e.min);
                                settings.dateTo = new Date(e.max);
                                if (settings.datepicker && (settings.maxDate.getTime() - settings.minDate.getTime() > settings.minRange)) {
                                    vars.date_ranger.from(settings.dateFrom).to(settings.dateTo);
                                }
                                vars.container.removeClass('loading');
                            });
                        }
                    },
                    minRange : settings.minRange,
                    allowDecimals : false
                },
                yAxis: {
                    floor: 0
                },
                scrollbar: {
                    liveRedraw: false
                },
                navigator : {
                    adaptToUpdatedData: false
                },
                series : settings.series
            });

            vars.chart = vars.chart_container.highcharts();

            var extremes = vars.chart.xAxis[0].getExtremes();
            settings.minDate = settings.dateFrom = new Date(extremes.min);
            settings.maxDate = settings.dateTo = new Date(extremes.max);

            if (settings.datepicker && (settings.maxDate.getTime() - settings.minDate.getTime() > settings.minRange)) {
                vars.date_range_container = $('<div style="float: right;overflow: hidden;">');
                vars.date_range_container.append('<span style="margin: 0 10px;"></span>');
                vars.date_ranger = new DateRange({
                    dateFrom : settings.minDate,
                    minDate : settings.minDate,
                    dateTo : settings.maxDate,
                    maxDate : settings.maxDate,
                    dateFormat : 'dd.mm.yy',
                    numberOfMonths : 2,
                    changeMonth : true,
                    minRange : 30,
                    container : vars.date_range_container.get(0)
                });
                vars.date_ranger.onChange(function (from, to) {
                    if (from) {
                        from.setHours(0,0,0,0);
                        from = from.getTime();
                    } else {
                        from = '';
                    }

                    if (to) {
                        to.setHours(0,0,0,0);
                        to = to.getTime() + 3600 * 24 * 1000 - 1;
                    } else {
                        to = '';
                    }

                    vars.chart.xAxis[0].setExtremes(from, to);
                });

                vars.container.css('overflow', 'hidden');
                vars.container.prepend('<div style="clear: both;"></div>');
                vars.container.prepend(vars.date_range_container);
            }
        });
    };
}(jQuery);