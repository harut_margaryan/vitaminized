!function($){
    $.fn.barChart = function (options) {
        var settings = $.extend({
            title : 'Title'
        }, options);
        settings.container = this;
        settings.container.height(400);
        settings.container.addClass('loading');

        $.getJSON(settings.url, null, function (response) {
            settings.container.removeClass('loading');
            settings = $.extend(settings, response);
            settings.container.highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: settings.title
                },
                xAxis: {
                    categories: settings.categories
                },
                yAxis: {
                    min: 0
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: settings.series
            });
        });
    };
}(jQuery);