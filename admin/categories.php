<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

$action = (isset($_GET['action']) ? $_GET['action'] : '');

$current_category_id = (empty($_GET['cID']) ? '' : $_GET['cID']);

if (tep_not_null($action)) {
    switch ($action) {
        case 'insert_category':
        case 'update_category':
            if (isset($_POST['categories_id'])) $categories_id = tep_db_prepare_input($_POST['categories_id']);
            $sort_order = tep_db_prepare_input($_POST['sort_order']);
            $parent_id = 0;
            if (!empty($_POST['pID'])) $parent_id = tep_db_prepare_input($_POST['pID']);

            $sql_data_array = array('sort_order' => (int)$sort_order);

            if ($action == 'insert_category') {
                $insert_sql_data = array('parent_id' => $parent_id,
                    'date_added' => 'now()');

                $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

                tep_db_perform(TABLE_CATEGORIES, $sql_data_array);

                $categories_id = tep_db_insert_id();
            } elseif ($action == 'update_category') {
                $update_sql_data = array('last_modified' => 'now()');

                $sql_data_array = array_merge($sql_data_array, $update_sql_data);

                if (!empty($_POST['pID'])) $sql_data_array['parent_id'] = tep_db_prepare_input($_POST['pID']);

                tep_db_perform(TABLE_CATEGORIES, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "'");
            }

            $languages = tep_get_languages();
            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                $categories_name_array = $_POST['categories_name'];

                $language_id = $languages[$i]['id'];

                $sql_data_array = array('categories_name' => $categories_name_array[$language_id]);

                if ($action == 'insert_category') {
                    $insert_sql_data = array('categories_id' => $categories_id,
                        'language_id' => $languages[$i]['id']);

                    $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

                    tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array);
                } elseif ($action == 'update_category') {
                    tep_db_perform(TABLE_CATEGORIES_DESCRIPTION, $sql_data_array, 'update', "categories_id = '" . (int)$categories_id . "' and language_id = '" . (int)$languages[$i]['id'] . "'");
                }
            }

            $categories_image = new upload('categories_image');
            $categories_image->set_destination(DIR_FS_CATALOG_IMAGES);

            if ($categories_image->parse() && $categories_image->save()) {
                tep_db_query("update " . TABLE_CATEGORIES . " set categories_image = '" . tep_db_input($categories_image->filename) . "' where categories_id = '" . (int)$categories_id . "'");
            }

            if (USE_CACHE == 'true') {
                tep_reset_cache_block('categories');
                tep_reset_cache_block('also_purchased');
            }

            tep_redirect(tep_href_link(FILENAME_CATEGORIES, 'cID=' . $categories_id));
            break;
        case 'delete_category_confirm':
            if (isset($_POST['categories_id'])) {
                $categories_id = tep_db_prepare_input($_POST['categories_id']);

                $categories = tep_get_category_tree($categories_id, '', '0', '', true);
                /*$products = array();
                $products_delete = array();

                for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
                    $product_ids_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$categories[$i]['id'] . "'");

                    while ($product_ids = tep_db_fetch_array($product_ids_query)) {
                        $products[$product_ids['products_id']]['categories'][] = $categories[$i]['id'];
                    }
                }

                reset($products);
                while (list($key, $value) = each($products)) {
                    $category_ids = '';

                    for ($i=0, $n=sizeof($value['categories']); $i<$n; $i++) {
                        $category_ids .= "'" . (int)$value['categories'][$i] . "', ";
                    }
                    $category_ids = substr($category_ids, 0, -2);

                    $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$key . "' and categories_id not in (" . $category_ids . ")");
                    $check = tep_db_fetch_array($check_query);
                    if ($check['total'] < '1') {
                        $products_delete[$key] = $key;
                    }
                }*/

// removing categories can be a lengthy process
                tep_set_time_limit(0);
                for ($i=0, $n=sizeof($categories); $i<$n; $i++) {
                    tep_remove_category($categories[$i]['id']);
                }

                /*reset($products_delete);
                while (list($key) = each($products_delete)) {
                    tep_remove_product($key);
                }*/
            }

            if (USE_CACHE == 'true') {
                tep_reset_cache_block('categories');
                tep_reset_cache_block('also_purchased');
            }

            tep_redirect(tep_href_link(FILENAME_CATEGORIES));
            break;
    }
}

// check if the catalog image directory exists
if (is_dir(DIR_FS_CATALOG_IMAGES)) {
    if (!tep_is_writable(DIR_FS_CATALOG_IMAGES)) $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
} else {
    $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
}

require(DIR_WS_INCLUDES . 'template_top.php');
?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
                    <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="smallText" align="right">
                                    <?php
                                    echo tep_draw_form('search', FILENAME_CATEGORIES, '', 'get');
                                    echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search');
                                    echo tep_hide_session_id() . '</form>';
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="smallText" align="right">
                                    <?php
                                    echo tep_draw_form('goto', FILENAME_CATEGORIES, '', 'get');
                                    echo HEADING_TITLE_FILTER . ' ' . tep_draw_pull_down_menu('cID', tep_get_category_tree(), $current_category_id, 'onchange="this.form.submit();"');
                                    echo tep_hide_session_id() . '</form>';
                                    ?>
                                </td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr class="dataTableHeadingRow">
                                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CATEGORIES; ?></td>
                                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                            </tr>
                            <?php
                            $categories_count = 0;
                            $rows = 0;
                            $where_str = '';
                            if (isset($_GET['search'])) {
                                $search = tep_db_prepare_input($_GET['search']);
                                $where_str = " and cd.categories_name like '%" . tep_db_input($search) . "%'";
                            }
                            $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.categories_image, c.parent_id, c.sort_order, c.date_added, c.last_modified from " . TABLE_CATEGORIES . " c join " . TABLE_CATEGORIES_DESCRIPTION . " cd using(categories_id) where cd.language_id = '" . (int)$languages_id . "'" . $where_str . " and parent_id = '0' order by c.sort_order ASC, cd.categories_name DESC");
                            $categories_sort = array();
                            while($cat = tep_db_fetch_array($categories_query)) {
                                $categories_sort[] = $cat;
                                $subcategories_query = tep_db_query("select c.categories_id, cd.categories_name, c.categories_image, c.parent_id, c.sort_order, c.date_added, c.last_modified from " . TABLE_CATEGORIES . " c join " . TABLE_CATEGORIES_DESCRIPTION . " cd using(categories_id) where cd.language_id = '" . (int)$languages_id . "'" . $where_str . " and parent_id = '" . $cat['categories_id'] . "' order by c.sort_order ASC, cd.categories_name DESC");
                                while($subcategory = tep_db_fetch_array($subcategories_query)) {
                                    $categories_sort[] = $subcategory;
                                }
                            }
                            foreach($categories_sort as $categories) {
                                $categories_count++;
                                $rows++;
// Get parent_id for subcategories if search
                                $parent_id = $categories['parent_id'];

                                if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $categories['categories_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
                                    $category_childs = array('childs_count' => tep_childs_in_category_count($categories['categories_id']));
                                    $category_products = array('products_count' => tep_products_in_category_count($categories['categories_id']));

                                    $cInfo_array = array_merge($categories, $category_childs, $category_products);
                                    $cInfo = new objectInfo($cInfo_array);
                                }

                                if (isset($cInfo) && is_object($cInfo) && ($categories['categories_id'] == $cInfo->categories_id) ) {
                                    echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_CATEGORIES, 'cID=' . $categories['categories_id'] . '&action=edit_category') . '\'">' . "\n";
                                } else {
                                    echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_CATEGORIES, 'cID=' . $categories['categories_id']) . '\'">' . "\n";
                                }

                                $spacing = ($parent_id == 0 ? "" : "&nbsp;&nbsp;&nbsp;");
                                ?>
                                <td class="dataTableContent"><?php echo $spacing . '<a href="' . tep_href_link(FILENAME_CATEGORIES, tep_get_path($categories['categories_id'])) . '">' . tep_image(DIR_WS_ICONS . 'folder.gif', ICON_FOLDER) . '</a>&nbsp;<strong>' . $categories['categories_name'] . '</strong>'; ?></td>
                                <td class="dataTableContent" align="right"><?php if (isset($cInfo) && is_object($cInfo) && ($categories['categories_id'] == $cInfo->categories_id) ) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_CATEGORIES, 'cID=' . $categories['categories_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                                </tr>
                            <?php
                            }

                            ?>
                            <tr>
                                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        <tr>
                                            <td class="smallText"><?php echo TEXT_CATEGORIES . '&nbsp;' . $categories_count; ?></td>
                                            <td align="right" class="smallText"><?php if (!isset($_GET['search'])) echo tep_draw_button(IMAGE_NEW_CATEGORY, 'plus', tep_href_link(FILENAME_CATEGORIES, 'action=new_category')); ?></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                    <?php
                    $heading = array();
                    $contents = array();
                    switch ($action) {
                        case 'new_category':
                            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_CATEGORY . '</strong>');

                            $contents = array('form' => tep_draw_form('newcategory', FILENAME_CATEGORIES, 'action=insert_category', 'post', 'enctype="multipart/form-data"'));
                            $contents[] = array('text' => TEXT_NEW_CATEGORY_INTRO);

                            $category_inputs_string = '';
                            $languages = tep_get_languages();
                            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                                $category_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']');
                            }

                            $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_NAME . $category_inputs_string);
                            $main_categories_query = tep_db_query("select c.categories_id, cd.categories_name from " . TABLE_CATEGORIES . " c join " . TABLE_CATEGORIES_DESCRIPTION . " cd on(c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "' and c.parent_id = '0') order by c.sort_order, cd.categories_name");
                            $main_categories_arr = array();
                            $main_categories_arr[] = array('id' => '0', 'text' => 'top');
                            while($main_category = tep_db_fetch_array($main_categories_query)) {
                                $main_categories_arr[] =  array('id' => $main_category['categories_id'], 'text' => '&nbsp;&nbsp;&nbsp;' . $main_category['categories_name']);
                            }
                            $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_PARENT . '<br />' . tep_draw_pull_down_menu('pID', $main_categories_arr));
                            $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_IMAGE . '<br />' . tep_draw_file_field('categories_image'));
                            $contents[] = array('text' => '<br />' . TEXT_SORT_ORDER . '<br />' . tep_draw_input_field('sort_order', '', 'size="2"'));
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_CATEGORIES)));
                            break;
                        case 'edit_category':
                            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_CATEGORY . '</strong>');

                            $main_categories_query = tep_db_query("select c.categories_id, cd.categories_name from " . TABLE_CATEGORIES . " c join " . TABLE_CATEGORIES_DESCRIPTION . " cd on(c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "' and c.parent_id = '0') order by c.sort_order, cd.categories_name");
                            $main_categories_arr = array();
                            $main_categories_arr[] = array('id' => '0', 'text' => 'top');
                            while($main_category = tep_db_fetch_array($main_categories_query)) {
                                $main_categories_arr[] =  array('id' => $main_category['categories_id'], 'text' => '&nbsp;&nbsp;&nbsp;' . $main_category['categories_name']);
                            }


                            $contents = array('form' => tep_draw_form('categories', FILENAME_CATEGORIES, 'action=update_category', 'post', 'enctype="multipart/form-data"') . tep_draw_hidden_field('categories_id', $cInfo->categories_id));
                            $contents[] = array('text' => TEXT_EDIT_INTRO);

                            $category_inputs_string = '';
                            $languages = tep_get_languages();
                            for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                                $category_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('categories_name[' . $languages[$i]['id'] . ']', tep_get_category_name($cInfo->categories_id, $languages[$i]['id']));
                            }
                            $contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_NAME . $category_inputs_string);
                            $contents[] = array('text' => '<br />' . TEXT_CATEGORIES_PARENT . '<br />' . tep_draw_pull_down_menu('pID', $main_categories_arr, $cInfo->parent_id));
                            $contents[] = array('text' => '<br />' . tep_image(DIR_WS_CATALOG_IMAGES . $cInfo->categories_image, $cInfo->categories_name) . '<br />' . DIR_WS_CATALOG_IMAGES . '<br /><strong>' . $cInfo->categories_image . '</strong>');
                            $contents[] = array('text' => '<br />' . TEXT_EDIT_CATEGORIES_IMAGE . '<br />' . tep_draw_file_field('categories_image'));
                            $contents[] = array('text' => '<br />' . TEXT_EDIT_SORT_ORDER . '<br />' . tep_draw_input_field('sort_order', $cInfo->sort_order, 'size="2"'));
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_CATEGORIES, 'cID=' . $cInfo->categories_id)));
                            break;
                        case 'delete_category':
                            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_CATEGORY . '</strong>');
                            $contents = array('form' => tep_draw_form('categories', FILENAME_CATEGORIES, 'action=delete_category_confirm') . tep_draw_hidden_field('categories_id', $cInfo->categories_id));
                            $contents[] = array('text' => TEXT_DELETE_CATEGORY_INTRO);
                            $contents[] = array('text' => '<br /><strong>' . $cInfo->categories_name . '</strong>');
                            if ($cInfo->childs_count > 0) $contents[] = array('text' => '<br />' . sprintf(TEXT_DELETE_WARNING_CHILDS, $cInfo->childs_count));
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_CATEGORIES, 'cID=' . $cInfo->categories_id)));
                            break;
                        default:
                            if ($rows > 0) {
                                if (isset($cInfo) && is_object($cInfo)) { // category info box contents
                                    $category_path_string = '';
                                    $category_path = tep_generate_category_path($cInfo->categories_id);
                                    for ($i=(sizeof($category_path[0])-1); $i>0; $i--) {
                                        $category_path_string .= $category_path[0][$i]['id'] . '_';
                                    }
                                    $category_path_string = substr($category_path_string, 0, -1);

                                    $heading[] = array('text' => '<strong>' . $cInfo->categories_name . '</strong>');

                                    $contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_CATEGORIES,'cID=' . $cInfo->categories_id . '&action=edit_category')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_CATEGORIES, 'cID=' . $cInfo->categories_id . '&action=delete_category')));
                                    $contents[] = array('text' => '<br />' . TEXT_DATE_ADDED . ' ' . tep_date_short($cInfo->date_added));
                                    if (tep_not_null($cInfo->last_modified)) $contents[] = array('text' => TEXT_LAST_MODIFIED . ' ' . tep_date_short($cInfo->last_modified));
                                    //$contents[] = array('text' => '<br />' . tep_info_image($cInfo->categories_image, $cInfo->categories_name, HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT) . '<br />' . $cInfo->categories_image);
                                    $contents[] = array('text' => '<br />' . TEXT_SUBCATEGORIES . ' ' . $cInfo->childs_count . '<br />' . TEXT_PRODUCTS . ' ' . $cInfo->products_count);
                                }
                            } else { // create category/product info
                                $heading[] = array('text' => '<strong>' . EMPTY_CATEGORY . '</strong>');

                                $contents[] = array('text' => TEXT_NO_CHILD_CATEGORIES);
                            }
                            break;
                    }

                    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                        echo '            <td width="25%" valign="top">' . "\n";

                        $box = new box;
                        echo $box->infoBox($heading, $contents);

                        echo '            </td>' . "\n";
                    }
                    ?>
                </tr>
            </table></td>
    </tr>
    </table>
<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>