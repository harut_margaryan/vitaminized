<!-- TinyMCE -->
<script type="text/javascript" src="includes/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
        selector: "textarea.tinymce",
		//paste_as_text: true,
        plugins: [ "autolink link lists charmap preview searchreplace code fullscreen table paste" ],

        toolbar1: "cut copy paste | bold italic underline strikethrough | bullist numlist | subscript superscript | charmap",
        toolbar2: "undo redo | table | searchreplace | link unlink | removeformat | preview fullscreen code",
        toolbar3: "",

		content_css: "includes/content.css",
		statusbar: true,
        menubar: false,
        toolbar_items_size: 'small'
});
// | alignleft aligncenter alignright alignjustify
</script>
<!-- /TinyMCE -->
