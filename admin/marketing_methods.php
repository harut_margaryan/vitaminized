<?php

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
    switch ($action) {
        case 'setflag':
            if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
                if (isset($_GET['mmID'])) {
                    $status = $_GET['flag'];
                    if ($status == 1) {
                        tep_db_query("update " . TABLE_MARKETING_METHODS . " set method_status = b'1' where method_id = '" . (int)$_GET['mmID'] . "'");
                    } elseif ($status == 0) {
                        tep_db_query("update " . TABLE_MARKETING_METHODS . " set method_status = b'0' where method_id = '" . (int)$_GET['mmID'] . "'");
                    }
                }
            }

            tep_redirect(tep_href_link(FILENAME_MARKETING_METHODS, 'mmID=' . $_GET['mmID']));
            break;
        case 'save':
            if (isset($_GET['mmID'])) $mm_id = (int)$_GET['mmID'];

            $languages = tep_get_languages();
            $mm_names_array = $_POST['method_name'];

            $sql_data_mm = array('sort_order' => (isset($_POST['sort_order']) ? (int)$_POST['sort_order'] : 1));

            tep_db_perform(TABLE_MARKETING_METHODS, $sql_data_mm, 'update', "method_id = " . (int)$mm_id);

            for ($i = 0, $n = sizeof($languages); $i < $n; ++$i) {
                $language_id = $languages[$i]['id'];

                $sql_data_mm = array('method_name' => $mm_names_array[$language_id],
                    'language_id' => $language_id);

                tep_db_perform(TABLE_MARKETING_METHODS_INFO, $sql_data_mm, 'update', "language_id = '" . (int)$language_id . "' and method_id = " . (int)$mm_id);
            }


            tep_redirect(tep_href_link(FILENAME_MARKETING_METHODS, 'mmID=' . (int)$mm_id));
            break;
    }
}

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr class="dataTableHeadingRow">
                                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_METHOD; ?></td>
                                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                            </tr>
                            <?php
                            $mm_query = tep_db_query("select m.method_id, m.method_status, m.sort_order, mi.method_name from " . TABLE_MARKETING_METHODS . " as m join " . TABLE_MARKETING_METHODS_INFO . " as mi on(m.method_id = mi.method_id and mi.language_id = '" . (int)$languages_id . "') order by m.sort_order");

                            while ($mm = tep_db_fetch_array($mm_query)) {
                                if ((!isset($_GET['mmID']) || (isset($_GET['mmID']) && ($_GET['mmID'] == $mm['method_id']))) && !isset($mmInfo)) {
                                    $mmInfo = new objectInfo($mm);
                                }

                                if (isset($mmInfo) && is_object($mmInfo) && ($mm['method_id'] == $mmInfo->method_id)) {
                                    echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_MARKETING_METHODS, 'mmID=' . $mmInfo->method_id . '&action=edit') . '\'">' . "\n";
                                } else {
                                    echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_MARKETING_METHODS, 'mmID=' . $mm['method_id']) . '\'">' . "\n";
                                }
                                ?>
                                <td class="dataTableContent"><?php echo $mm['method_name'];?></td>
                                <td class="dataTableContent" align="center">
                                    <?php
                                    if ($mm['method_status'] == '1') {
                                        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_MARKETING_METHODS, 'action=setflag&flag=0&mmID=' . $mm['method_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
                                    } else {
                                        echo '<a href="' . tep_href_link(FILENAME_MARKETING_METHODS, 'action=setflag&flag=1&mmID=' . $mm['method_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                                    }
                                    ?>
                                </td>
                                <td class="dataTableContent" align="right"><?php if (isset($mmInfo) && is_object($mmInfo) && ($mm['method_id'] == $mmInfo->method_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_MARKETING_METHODS, 'mmID=' . $mm['method_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </table></td>
                    <?php
                    $heading = array();
                    $contents = array();

                    switch ($action) {
                        case 'edit':
                            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_PRODUCTS_INFO . '</strong>');

                            $contents = array('form' => tep_draw_form('mm', FILENAME_MARKETING_METHODS, 'mmID=' . $mmInfo->method_id  . '&action=save'));
                            $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);

                            $mm_inputs_string = '';
                            $languages = tep_get_languages();
                            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                                $mm_names_query = tep_db_query('select mi.method_name from ' . TABLE_MARKETING_METHODS . ' as m join ' . TABLE_MARKETING_METHODS_INFO . ' as mi on(m.method_id = ' . (int)$mmInfo->method_id . ' and m.method_id = mi.method_id and mi.language_id = ' . (int)$languages[$i]['id'] . ')');
                                $mm_name = tep_db_num_rows($mm_names_query) == 1 ? tep_db_fetch_array($mm_names_query) : array('method_name' => '');
                                $mm_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('method_name[' . $languages[$i]['id'] . ']', $mm_name['method_name']);
                            }

                            $contents[] = array('text' => '<br />' . TEXT_INFO_METHOD_NAME . $mm_inputs_string);
                            $contents[] = array('text' => '<br />' . TEXT_EDIT_SORT_ORDER . '<br />' . tep_draw_input_field('sort_order', $mmInfo->sort_order, 'size="2"'));
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_MARKETING_METHODS, 'mmID=' . $mmInfo->method_id)));
                            break;
                        default:
                            if (isset($mmInfo) && is_object($mmInfo)) {
                                $heading[] = array('text' => '<strong>' . $mmInfo->method_name . '</strong>');

                                $contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_MARKETING_METHODS, 'mmID=' . $mmInfo->method_id . '&action=edit')));

                                $mm_inputs_string = '';
                                $languages = tep_get_languages();

                                for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                                    $mm_names_query = tep_db_query('select mi.method_name from ' . TABLE_MARKETING_METHODS . ' as m join ' . TABLE_MARKETING_METHODS_INFO . ' as mi on(m.method_id = ' . (int)$mmInfo->method_id . ' and m.method_id = mi.method_id and mi.language_id = ' . (int)$languages[$i]['id'] . ')');
                                    $mm_name = tep_db_num_rows($mm_names_query) == 1 ? tep_db_fetch_array($mm_names_query) : array('method_name' => '');
                                    $mm_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . $mm_name['method_name'];
                                }

                                $contents[] = array('text' => $mm_inputs_string);
                            }
                            break;
                    }

                    if ( (tep_not_null($heading)) && (tep_not_null($contents))) {
                        echo '            <td width="25%" valign="top">' . "\n";

                        $box = new box;
                        echo $box->infoBox($heading, $contents);

                        echo '            </td>' . "\n";
                    }
                    ?>
                </tr>
            </table></td>
    </tr>
</table>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
