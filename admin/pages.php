<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
	switch ($action) {
		case 'setflag':
			if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
				if (isset($_GET['pID'])) {
					tep_set_page_status($_GET['pID'], $_GET['flag']);
				}
			}

			tep_redirect(tep_href_link(FILENAME_PAGES, 'pID=' . $_GET['pID']));
			break;
		case 'delete_page_confirm':
			if (isset($_POST['pages_id'])) {
				$page_id = intval($_POST['pages_id']);

				$page_query = tep_db_query('select pages_image from ' . TABLE_PAGES . ' where pages_id = ' . intval($pages_id));

				if (tep_db_num_rows($page_query)) {

					$page_info = tep_db_fetch_array($page_query);

					@unlink(DIR_FS_CATALOG_IMAGES_PAGES . $page_info['pages_image']);

					tep_db_query("delete from " . TABLE_PAGES . " where pages_id = " . (int)$page_id);
					tep_db_query("delete from " . TABLE_PAGES_DESCRIPTION . " where pages_id = " . (int)$page_id);
					tep_db_query("update " . TABLE_PAGES . " set parents_id = 0 where parents_id = " . (int)$page_id);
				}

			}

			tep_redirect(tep_href_link(FILENAME_PAGES));
			break;
		case 'insert_page':
		case 'update_page':
			if (isset($_GET['pID'])) $pages_id = intval($_GET['pID']);

			$sql_data_array = array(
				'parents_id' => empty($_POST['parents_id']) ? 0 : (int)$_POST['parents_id'],
				'pages_sort_order' => empty($_POST['pages_sort_order']) ? 1 : intval($_POST['pages_sort_order'])
			);

			$error_message = false;

			$parents_id = empty($_POST['parents_id']) ? 0 : tep_db_prepare_input($_POST['parents_id']);

			/*if ($_FILES['pages_image']['tmp_name'] != '') {
				list($width_orig, $height_orig, $image_format) = @getimagesize($_FILES['pages_image']['tmp_name']);
				if ($width_orig < MIDDLE_IMAGE_WIDTH || $height_orig < MIDDLE_IMAGE_HEIGHT) {
					$error_message .= 'Error: Image size must be not less then ' . MIDDLE_IMAGE_WIDTH . 'x' . MIDDLE_IMAGE_HEIGHT . 'px' . '<br/>';
				}
			}*/
			/*if (!$parents_id) {
				$error_message .= 'Error: Select pages' . '<br/>';
			}*/

			if ($error_message) {
				$messageStack->add_session($error_message, 'error');
				tep_redirect(tep_href_link(FILENAME_PAGES, 'pID=' . $pages_id . '&action=' . ($action == 'insert_page'? 'new_page' : 'edit_page')));
			}
			if ($action == 'insert_page') {

				tep_db_perform(TABLE_PAGES, $sql_data_array);
				$pages_id = tep_db_insert_id();

				/*if (is_array($parents_id)) {
					foreach ($parents_id as $curr_cat_id) {
						tep_db_query("insert into " . TABLE_PAGES_TO_CATEGORIES . " (pages_id, pages_id) values ('" . (int)$pages_id . "', '" . (int)$curr_cat_id . "')");
					}
				} else {
					tep_db_query("insert into " . TABLE_PAGES_TO_CATEGORIES . " (pages_id, pages_id) values ('" . (int)$pages_id . "', '0')");
				}*/
				//tep_db_query("insert into " . TABLE_PAGES_TO_CATEGORIES . " (pages_id, pages_id) values ('" . (int)$pages_id . "', '" . (int)$parents_id . "')");
			} elseif ($action == 'update_page') {

				tep_db_perform(TABLE_PAGES, $sql_data_array, 'update', "pages_id = '" . (int)$pages_id . "'");

				//tep_db_query("update " . TABLE_PAGES_TO_CATEGORIES . " set pages_id = '" . (int)$parents_id . "' where pages_id = '" . (int)$pages_id . "'");
			}

			/*
			require(DIR_WS_CLASSES . 'gallery.php');

			$gall = new CGallery();
			if ($_FILES['pages_image']['tmp_name'] != '') {
				$with_logo = (isset($_POST['with_logo']) && ($_POST['with_logo'] == 'on')) ? 1 : 0;
				$name = substr(tep_clean_text_int(tep_translit(tep_db_prepare_input($_POST['pages_title'][1]) ? tep_db_prepare_input($_POST['pages_title'][1]) : '')), 0, 20) . '-p' . (int)$pages_id;
				$sql_data_array['pages_image'] = DIR_WS_PHOTOS . $name;
				tep_db_perform(TABLE_PAGES, $sql_data_array, 'update', "pages_id = '" . (int)$pages_id . "'");
				$pic_query = tep_db_query("select pages_image from " . TABLE_PAGES . " where pages_id = '" . (int)$pages_id . "'");
				$page_pic = tep_db_fetch_array($pic_query);
				if ($page_pic['pages_image'] != DIR_WS_PHOTOS . $name && tep_not_null($page_pic['pages_image'])) {
					$image_location =  DIR_FS_CATALOG_IMAGES . $page_pic['pages_image'];
					if (file_exists($image_location . '-1' . IMAGE_EXTENSION)) @unlink($image_location . '-1' . IMAGE_EXTENSION);
					if (file_exists($image_location . '-2' . IMAGE_EXTENSION)) @unlink($image_location . '-2' . IMAGE_EXTENSION);
					//						if (file_exists($image_location . '-3' . $ext)) @unlink($image_location . '-3' . $ext);
				}
				$gall->addPhoto($_FILES['pages_image']['tmp_name'], DIR_FS_PHOTOS . $name, IMAGE_EXTENSION, $with_logo, '', MIDDLE_IMAGE_WIDTH, '', SMALL_IMAGE_WIDTH);
				$gall->name = '';
			}*/

			if (!empty($_FILES['pages_image']['tmp_name'])) {
				$tmp_name = $_FILES['pages_image']['tmp_name'];
				if (is_uploaded_file($tmp_name)) {
					$name = 'p' . intval($pages_id);
					$image_ext = '.' . pathinfo($_FILES['pages_image']['name'], PATHINFO_EXTENSION);
					$name .= $image_ext;

					$image_location = DIR_FS_CATALOG_IMAGES_PAGES . $name;

					if (file_exists($image_location)) @unlink($image_location);
					if (file_exists($image_location)) @unlink($image_location);

					move_uploaded_file($tmp_name, $image_location);


					tep_db_query('update ' . TABLE_PAGES . ' set pages_image = "' . tep_db_input($name) . '" where pages_id = ' . $pages_id);
				}
			}

			$languages = tep_get_languages();
			for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
				$language_id = $languages[$i]['id'];

				$sql_data_array = array('pages_title' => tep_db_prepare_input($_POST['pages_title'][$language_id]),
					'pages_description' => tep_db_prepare_input($_POST['pages_description'][$language_id]),
					'meta_keyword' => tep_db_prepare_input($_POST['meta_keyword'][$language_id]),
					'meta_description' => tep_db_prepare_input($_POST['meta_description'][$language_id])
				);

				if ($action == 'insert_page') {
					$insert_sql_data = array('pages_id' => $pages_id,
						'language_id' => $language_id);

					$sql_data_array = array_merge($sql_data_array, $insert_sql_data);

					tep_db_perform(TABLE_PAGES_DESCRIPTION, $sql_data_array);
				} elseif ($action == 'update_page') {
					tep_db_perform(TABLE_PAGES_DESCRIPTION, $sql_data_array, 'update', "pages_id = '" . (int)$pages_id . "' and language_id = '" . (int)$language_id . "'");
				}

				/*$pages_info_ids_query = tep_db_query('select pages_info_id from ' . TABLE_PAGES_INFO);
				$pages_has_info = tep_db_num_rows($pages_info_ids_query) > 0;

				if ($pages_has_info) {
					while ($pages_info_id = tep_db_fetch_array($pages_info_ids_query)) {
						$pi_text = isset($_POST['pages_info_text'][$language_id][$pages_info_id['pages_info_id']]) ? tep_db_prepare_input($_POST['pages_info_text'][$language_id][$pages_info_id['pages_info_id']]) : '';

						if ($action == 'insert_page') {
							$sql_data_array = array('pages_info_id' => (int)($pages_info_id['pages_info_id']),
								'pages_id' => (int)$pages_id,
								'language_id' => $language_id,
								'pages_info_text' => $pi_text);
							tep_db_perform(TABLE_PAGES_TO_PAGES_INFO, $sql_data_array);
						} elseif ($action == 'update_page') {
							$chec_pinfo_row = tep_db_query('select * from ' . TABLE_PAGES_TO_PAGES_INFO . ' where'
								. ' pages_info_id = ' . (int)($pages_info_id['pages_info_id'])
								. ' and pages_id = ' . (int)$pages_id
								. ' and language_id = ' . $language_id
							);

							if (tep_db_num_rows($chec_pinfo_row)) {
								$sql_data_array = array('pages_info_text' => $pi_text);
								tep_db_perform(TABLE_PAGES_TO_PAGES_INFO, $sql_data_array, 'update', "pages_id = '" . (int)$pages_id . "' and language_id = '" . (int)$language_id . "' and pages_info_id = " . (int)($pages_info_id['pages_info_id']));
							} else {
								$sql_data_array = array('pages_info_id' => (int)($pages_info_id['pages_info_id']),
									'pages_id' => (int)$pages_id,
									'language_id' => $language_id,
									'pages_info_text' => $pi_text);
								tep_db_perform(TABLE_PAGES_TO_PAGES_INFO, $sql_data_array);
							}
						}

					}
				}*/
			}

			/*$pi_sort_order = 0;
			$piArray = array(0);

			foreach ($_FILES as $key => $value) {
// Update existing large page images
				if (preg_match('/^pages_image_large_([0-9]+)$/', $key, $matches)) {
					$pi_sort_order++;

					$sql_data_array = array('htmlcontent' => tep_db_prepare_input($_POST['pages_image_htmlcontent_' . $matches[1]]),
						'sort_order' => $pi_sort_order);

					$t = new upload($key);
					$t->set_destination(DIR_FS_CATALOG_IMAGES);
					if ($t->parse() && $t->save()) {
						$sql_data_array['image'] = tep_db_prepare_input($t->filename);
					}

					tep_db_perform(TABLE_PAGES_IMAGES, $sql_data_array, 'update', "pages_id = '" . (int)$pages_id . "' and id = '" . (int)$matches[1] . "'");

					$piArray[] = (int)$matches[1];
				} elseif (preg_match('/^pages_image_large_new_([0-9]+)$/', $key, $matches)) {
// Insert new large page images
					$sql_data_array = array('pages_id' => (int)$pages_id,
						'htmlcontent' => tep_db_prepare_input($_POST['pages_image_htmlcontent_new_' . $matches[1]]));

					$t = new upload($key);
					$t->set_destination(DIR_FS_CATALOG_IMAGES);
					if ($t->parse() && $t->save()) {
						$pi_sort_order++;

						$sql_data_array['image'] = tep_db_prepare_input($t->filename);
						$sql_data_array['sort_order'] = $pi_sort_order;

						tep_db_perform(TABLE_PAGES_IMAGES, $sql_data_array);

						$piArray[] = tep_db_insert_id();
					}
				}
			}

			$page_images_query = tep_db_query("select image from " . TABLE_PAGES_IMAGES . " where pages_id = '" . (int)$pages_id . "' and id not in (" . implode(',', $piArray) . ")");
			if (tep_db_num_rows($page_images_query)) {
				while ($page_images = tep_db_fetch_array($page_images_query)) {
					$duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_PAGES_IMAGES . " where image = '" . tep_db_input($page_images['image']) . "'");
					$duplicate_image = tep_db_fetch_array($duplicate_image_query);

					if ($duplicate_image['total'] < 2) {
						if (file_exists(DIR_FS_CATALOG_IMAGES . $page_images['image'])) {
							@unlink(DIR_FS_CATALOG_IMAGES . $page_images['image']);
						}
					}
				}

				tep_db_query("delete from " . TABLE_PAGES_IMAGES . " where pages_id = '" . (int)$pages_id . "' and id not in (" . implode(',', $piArray) . ")");
			}

			if (USE_CACHE == 'true') {
				tep_reset_cache_block('pages');
				tep_reset_cache_block('also_purchased');
			}*/

			tep_redirect(tep_href_link(FILENAME_PAGES, 'pID=' . $pages_id));
			break;
		/*case 'copy_to_confirm':
			if (isset($_POST['pages_id']) && isset($_POST['pages_id'])) {
				$pages_id = tep_db_prepare_input($_POST['pages_id']);
				$pages_id = tep_db_prepare_input($_POST['pages_id']);
				$parent_cat_id_array = tep_generate_page_path($pages_id, 'page');
				//$parent_cat_id = end($parent_cat_id_array)['id'];

				$page_query = tep_db_query("select pages_quantity, pages_misspellings, pages_model, pages_image, pages_price, pages_date_available, pages_weight, pages_tax_class_id, manufacturers_id from " . TABLE_PAGES . " where pages_id = '" . (int)$pages_id . "'");
				$page = tep_db_fetch_array($page_query);

				tep_db_query("insert into " . TABLE_PAGES . " (pages_quantity, pages_misspellings, pages_model,pages_image, pages_price, pages_date_added, pages_date_available, pages_weight, pages_status, pages_tax_class_id, manufacturers_id) values ('" . tep_db_input($page['pages_quantity']) . "', '" . tep_db_input($page['pages_misspellings']) . "', '" . tep_db_input($page['pages_model']) . "', '" . tep_db_input($page['pages_image']) . "', '" . tep_db_input($page['pages_price']) . "',  now(), " . (empty($page['pages_date_available']) ? "null" : "'" . tep_db_input($page['pages_date_available']) . "'") . ", '" . tep_db_input($page['pages_weight']) . "', '0', '" . (int)$page['pages_tax_class_id'] . "', '" . (int)$page['manufacturers_id'] . "')");
				$dup_pages_id = tep_db_insert_id();

				$description_query = tep_db_query("select language_id, pages_title, pages_description, pages_url from " . TABLE_PAGES_DESCRIPTION . " where pages_id = '" . (int)$pages_id . "'");
				while ($description = tep_db_fetch_array($description_query)) {
					tep_db_query("insert into " . TABLE_PAGES_DESCRIPTION . " (pages_id, language_id, pages_title, pages_description, pages_url, pages_viewed) values ('" . (int)$dup_pages_id . "', '" . (int)$description['language_id'] . "', '" . tep_db_input($description['pages_title']) . "', '" . tep_db_input($description['pages_description']) . "', '" . tep_db_input($description['pages_url']) . "', '0')");
				}

				$page_images_query = tep_db_query("select image, htmlcontent, sort_order from " . TABLE_PAGES_IMAGES . " where pages_id = '" . (int)$pages_id . "'");
				while ($page_images = tep_db_fetch_array($page_images_query)) {
					tep_db_query("insert into " . TABLE_PAGES_IMAGES . " (pages_id, image, htmlcontent, sort_order) values ('" . (int)$dup_pages_id . "', '" . tep_db_input($page_images['image']) . "', '" . tep_db_input($page_images['htmlcontent']) . "', '" . tep_db_input($page_images['sort_order']) . "')");
				}

				tep_db_query("insert into " . TABLE_PAGES_TO_CATEGORIES . " (pages_id, pages_id) values ('" . (int)$dup_pages_id . "', '" . (int)$pages_id . "')");
				$pages_id = $dup_pages_id;

				$pages_info_query = tep_db_query('select pages_info_id, language_id, pages_info_text from ' . TABLE_PAGES_TO_PAGES_INFO . ' where pages_id = "' . (int)$pages_id . '"');
				while($pages_info = tep_db_fetch_array($pages_info_query)) {
					tep_db_query("insert into " . TABLE_PAGES_TO_PAGES_INFO . " (pages_info_id, pages_id, language_id, pages_info_text) values ('" . (int)$pages_info['pages_info_id'] . "', '" . (int)$pages_id . "', '" . (int)$pages_info['language_id'] . "', '" . $pages_info['pages_info_text'] . "')");
				}

				if (USE_CACHE == 'true') {
					tep_reset_cache_block('pages');
					tep_reset_cache_block('also_purchased');
				}
			}

			tep_redirect(tep_href_link(FILENAME_PAGES, 'cPath=' . $pages_id . '&pID=' . $pages_id));
			break;*/
	}
}

// check if the catalog image directory exists
/*if (is_dir(DIR_FS_CATALOG_IMAGES)) {
	if (!tep_is_writable(DIR_FS_CATALOG_IMAGES)) $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
} else {
	$messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
}*/

require(DIR_WS_INCLUDES . 'template_top.php');
?>
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
	<tr>
		<td><table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td class="pageHeading"><a href="<?php echo tep_href_link(FILENAME_PAGES);?>" style="display: block; text-decoration: none;"><?php echo HEADING_TITLE; ?></td>
					<td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
					<td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td class="smallText" align="right">
									<?php
									echo tep_draw_form('search', FILENAME_PAGES, '', 'get');
									echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search');
									echo tep_hide_session_id() . '</form>';
									?>
								</td>
							</tr>
						</table></td>
				</tr>
			</table></td>
	</tr>
	<tr>
	<td><table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
	<td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
			<tr class="dataTableHeadingRow">
				<td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PAGES; ?></td>
				<td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
				<td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
			</tr>
			<?php
			$rows = 0;
			$where_str = '';
			if (isset($_GET['search'])) {
				$search = tep_db_prepare_input($_GET['search']);
				$where_str = " and pd.pages_title like '%" . tep_db_input($search) . "%'";
			}
			$pages_query = tep_db_query("select p.pages_id, p.pages_image, pd.pages_title, p.parents_id, p.pages_status, p.pages_sort_order from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) where pd.language_id = '" . (int)$languages_id . "'" . $where_str . " and parents_id = '0' order by p.pages_sort_order ASC, pd.pages_title DESC");
			$pages_sort = array();
			while($page = tep_db_fetch_array($pages_query)) {
				$pages_sort[] = $page;
				$subpages_query = tep_db_query("select p.pages_id, p.pages_image, pd.pages_title, p.parents_id, p.pages_status, p.pages_sort_order from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd using(pages_id) where pd.language_id = '" . (int)$languages_id . "'" . $where_str . " and parents_id = '" . $page['pages_id'] . "' order by p.pages_sort_order ASC, pd.pages_title DESC");
				while($subpage = tep_db_fetch_array($subpages_query)) {
					$pages_sort[] = $subpage;
				}
			}
			foreach($pages_sort as $pages) {
				$rows++;
// Get parents_id for subpages if search
				$parents_id = $pages['parents_id'];

				if ((!isset($_GET['pID']) || (isset($_GET['pID']) && ($_GET['pID'] == $pages['pages_id']))) && !isset($pInfo) && (substr($action, 0, 3) != 'new')) {
					$page_childs = array('childs_count' => tep_childs_in_page_count($pages['pages_id']));

					$pInfo_array = array_merge($pages, $page_childs);
					$pInfo = new objectInfo($pInfo_array);
				}

				if (isset($pInfo) && is_object($pInfo) && ($pages['pages_id'] == $pInfo->pages_id) ) {
					echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PAGES, 'pID=' . $pages['pages_id'] . '&action=edit_page') . '\'">' . "\n";
				} else {
					echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PAGES, 'pID=' . $pages['pages_id']) . '\'">' . "\n";
				}

				$spacing = ($parents_id == 0 ? "" : "&nbsp;&nbsp;&nbsp;");
				?>
<!--				<td class="dataTableContent" align="right">--><?php //if (isset($pInfo) && is_object($pInfo) && ($pages['pages_id'] == $pInfo->pages_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_PAGES, 'cPath=' . $cPath . '&pID=' . $pages['pages_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?><!--&nbsp;</td>-->
				<td class="dataTableContent"><?php echo $spacing . '<a href="' . tep_href_link(FILENAME_PAGES, tep_get_path($pages['pages_id'])) . '">' . tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PAGE) . '</a>&nbsp;<strong>' . $pages['pages_title'] . '</strong>'; ?></td>
				<td class="dataTableContent" align="center">
					<?php
					if ($pages['pages_status'] == '1') {
						echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_PAGES, 'action=setflag&flag=0&pID=' . $pages['pages_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
					} else {
						echo '<a href="' . tep_href_link(FILENAME_PAGES, 'action=setflag&flag=1&pID=' . $pages['pages_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
					}
					?>
				</td>
				<td class="dataTableContent" align="right"><?php if (isset($pInfo) && is_object($pInfo) && ($pages['pages_id'] == $pInfo->pages_id) ) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_PAGES, 'pID=' . $pages['pages_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
				</tr>
			<?php
			}
			/*$rows = 0;
			$filter_str =  " and p2c.pages_id = '" . (int)$current_parent_page_id . "'";
			if ($current_parent_page_id == 0) {
				$filter_str = '';
			} else {
				$subpages_query = tep_db_query("select pages_id from " . TABLE_PAGES . " where parents_id = '" . (int)$current_parent_page_id . "'");
				if (tep_db_num_rows($subpages_query) > 0 ) {
					$filter_str = ' and (';
					while($subpages = tep_db_fetch_array($subpages_query)) {
						$filter_str .= " p2c.pages_id = '" . (int)$subpages['pages_id'] . "' || ";
					}
					$filter_str .= '0)';
				}
			}



			if (isset($_GET['search'])) {
				$pages_query = tep_db_query("select p.pages_id, pd.pages_title, p.pages_status from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd on (p.pages_id = pd.pages_id and pd.language_id = '" . (int)$languages_id . "') where pd.pages_title like '%" . tep_db_input($_GET['search']) . "%' order by pd.pages_title");
				//$pages_query = tep_db_query("select p.pages_id, pd.pages_title, p.pages_quantity, p.pages_misspellings, p.pages_image, p.pages_price, p.pages_date_added, p.pages_last_modified, p.pages_date_available, p.pages_status from " . TABLE_PAGES . " p, " . TABLE_PAGES_DESCRIPTION . " pd where p.pages_id = pd.pages_id and pd.language_id = '" . (int)$languages_id . "' and pd.pages_title like '%" . tep_db_input($search) . "%' order by pd.pages_title");
			} else {
				$pages_query = tep_db_query("select p.pages_id, pd.pages_title, p.pages_status from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd on (p.pages_id = pd.pages_id and pd.language_id = '" . (int)$languages_id . "') order by p.pages_sort_order, pd.pages_title");
			}
			while ($pages = tep_db_fetch_array($pages_query)) {
				$rows++;

// Get pages_id for page if search
				//if (isset($_GET['search'])) $cPath = $pages['pages_id'];

				if ( (!isset($_GET['pID']) || (isset($_GET['pID']) && ($_GET['pID'] == $pages['pages_id']))) && !isset($pInfo) && (substr($action, 0, 3) != 'new')) {
// find out the rating average from customer reviews
					$reviews_query = tep_db_query("select (avg(reviews_rating) / 5 * 100) as average_rating from " . TABLE_REVIEWS . " where pages_id = '" . (int)$pages['pages_id'] . "'");
					$reviews = tep_db_fetch_array($reviews_query);
					$pInfo_array = array_merge($pages, $reviews);
					$pInfo = new objectInfo($pInfo_array);
				}

				if (isset($pInfo) && is_object($pInfo) && ($pages['pages_id'] == $pInfo->pages_id) ) {
					echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PAGES, 'cPath=' . $cPath . '&pID=' . $pages['pages_id'] . '&action=edit_page') . '\'">' . "\n";
				} else {
					echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PAGES, 'cPath=' . $cPath . '&pID=' . $pages['pages_id']) . '\'">' . "\n";
				}
				?>
				<td class="dataTableContent"><?php echo '<a href="' . tep_href_link(FILENAME_PAGES, 'cPath=' . $cPath . '&pID=' . $pages['pages_id'] . '&action=page_preview') . '">' . tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>&nbsp;' . $pages['pages_title']; ?></td>
				<td class="dataTableContent" align="center">
					<?php
					if ($pages['pages_status'] == '1') {
						echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_PAGES, 'action=setflag&flag=0&pID=' . $pages['pages_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
					} else {
						echo '<a href="' . tep_href_link(FILENAME_PAGES, 'action=setflag&flag=1&pID=' . $pages['pages_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
					}
					?>
				</td>
				<td class="dataTableContent" align="right"><?php if (isset($pInfo) && is_object($pInfo) && ($pages['pages_id'] == $pInfo->pages_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_PAGES, 'cPath=' . $cPath . '&pID=' . $pages['pages_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
				</tr>
			<?php
			}*/
			?>
			<tr>
				<td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
						<tr>
							<td align="right" class="smallText"><?php if (!isset($_GET['search'])) echo tep_draw_button(IMAGE_NEW_PAGE, 'plus', tep_href_link(FILENAME_PAGES, 'action=new_page')); ?>&nbsp;</td>
						</tr>
					</table></td>
			</tr>
		</table></td>
	<?php
	$heading = array();
	$contents = array();
	switch ($action) {
		case 'delete_page':
			$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_PAGE . '</strong>');

			$contents = array('form' => tep_draw_form('pages', FILENAME_PAGES, 'action=delete_page_confirm') . tep_draw_hidden_field('pages_id', $pInfo->pages_id));
			$contents[] = array('text' => TEXT_DELETE_PAGE_INTRO);
			$contents[] = array('text' => '<br /><strong>' . $pInfo->pages_title . '</strong>');

			$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PAGES, 'pID=' . $pInfo->pages_id)));
			break;
		/*case 'copy_to':
			$heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_COPY_TO . '</strong>');

			$contents = array('form' => tep_draw_form('copy_to', FILENAME_PAGES, 'action=copy_to_confirm&cPath=' . $cPath) . tep_draw_hidden_field('pages_id', $pInfo->pages_id));
			$contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
			$contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . tep_output_generated_page_path($pInfo->pages_id, 'page') . '</strong>');
			$contents[] = array('text' => '<br />' . TEXT_CATEGORIES . '<br />' . tep_draw_pull_down_menu('pages_id', tep_get_page_tree('0', '', '0')));
			$contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_COPY, 'copy', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PAGES, 'cPath=' . $cPath . '&pID=' . $pInfo->pages_id)));
			break;*/
		case 'new_page':
			break;
		case 'edit_page':
			break;
		default:
			if ($rows > 0) {
				if (isset($pInfo) && is_object($pInfo)) { // page info box contents
					$heading[] = array('text' => '<strong>' . $pInfo->pages_title . '</strong>');

					$contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_PAGES, 'pID=' . $pInfo->pages_id . '&action=edit_page')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_PAGES, 'pID=' . $pInfo->pages_id . '&action=delete_page')));
//					$contents[] = array('text' => '<br />' . tep_info_image($pInfo->pages_image . '-2' . IMAGE_EXTENSION, $pInfo->pages_title, SMALL_IMAGE_WIDTH) . '<br />' . $pInfo->pages_image . '-2' . IMAGE_EXTENSION);
				}
			} else { // create page/page info
				$heading[] = array('text' => '<strong>empty</strong>');

				$contents[] = array('text' => TEXT_NO_CHILD_PAGES);
			}
			break;
	}
	if ($action == 'new_page' || $action == 'edit_page') {
		echo '<script>stopUnload = true;</script>';
		echo '</tr><tr><td width="450" valign="top" id="pageInfo">' . "\n";
		$parameters = array(
			'pages_title' => '',
			'pages_description' => '',
//			'pages_url' => '',
			'pages_id' => '',
			'pages_sort_order' => '0',
			'parents_id' => '0'
		);

		$pInfo = new objectInfo($parameters);

		if (!empty($_GET['pID']) && empty($_POST) && substr($action, 0 , 3) != 'new') {
			$page_query = tep_db_query("select pd.pages_title, pd.pages_description, p.* from " . TABLE_PAGES . " p, " . TABLE_PAGES_DESCRIPTION . " pd where p.pages_id = '" . (int)$_GET['pID'] . "' and p.pages_id = pd.pages_id and pd.language_id = '" . (int)$languages_id . "'");
			$page = tep_db_fetch_array($page_query);

			/*$page_pages_query = tep_db_query('select pages_id from pages_to_pages where pages_id = ' . (int)$_GET['pID']);
			$page['pages_id'] = array();
			while ($curr_cat = tep_db_fetch_array($page_pages_query)) {
				$page['pages_id'][] = $curr_cat['pages_id'];
			}*/

			$pInfo->objectInfo($page);



			/*$page_images_query = tep_db_query("select id, image, htmlcontent, sort_order from " . TABLE_PAGES_IMAGES . " where pages_id = '" . (int)$page['pages_id'] . "' order by sort_order");
			while ($page_images = tep_db_fetch_array($page_images_query)) {
				$pInfo->pages_larger_images[] = array('id' => $page_images['id'],
					'image' => $page_images['image'],
					'htmlcontent' => $page_images['htmlcontent'],
					'sort_order' => $page_images['sort_order']);
			}*/
		}

		/*$manufacturers_array = array(array('id' => '', 'text' => TEXT_NONE));
		$manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . " order by manufacturers_name");
		while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
			$manufacturers_array[] = array('id' => $manufacturers['manufacturers_id'],
				'text' => $manufacturers['manufacturers_name']);
		}

		$tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
		$tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
		while ($tax_class = tep_db_fetch_array($tax_class_query)) {
			$tax_class_array[] = array('id' => $tax_class['tax_class_id'],
				'text' => $tax_class['tax_class_title']);
		}*/

		$languages = tep_get_languages();

		$form_action = $action == 'edit_page' ? 'update_page' : 'insert_page';

		echo tep_draw_form('new_page', FILENAME_PAGES, 'action=' . $form_action . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : ''), 'post', 'enctype="multipart/form-data"'); ?>
		<table border="0" width="100%" cellspacing="0" cellpadding="2" style="background-color: #DEE4E8;">
			<tr class="infoBoxHeading">
				<td class="infoBoxHeading"><table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td><?php echo $action == 'new_page' ? TEXT_NEW_PAGE : TEXT_EDIT_PAGE; ?></td>
							<!--                            <td class="dataTableHeadingContent" align="right">--><?php //echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?><!--</td>-->
						</tr>
					</table></td>
			</tr>
			<tr>
				<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
			</tr>
			<tr>
				<td><table border="0" cellspacing="0" cellpadding="2" style="width: 100%;">
						<tr>
							<td class="main"><?php
								$main_pages_query = tep_db_query("select p.pages_id, pd.pages_title from " . TABLE_PAGES . " p join " . TABLE_PAGES_DESCRIPTION . " pd on(p.pages_id = pd.pages_id and pd.language_id = '" . (int)$languages_id . "' and p.parents_id = '0') order by p.pages_sort_order, pd.pages_title");
								$main_pages_arr = array();
								$main_pages_arr[] = array('id' => '0', 'text' => 'top');
								while($main_page = tep_db_fetch_array($main_pages_query)) {
									if ($pInfo->pages_id == $main_page['pages_id']) continue;
									$main_pages_arr[] =  array('id' => $main_page['pages_id'], 'text' => '&nbsp;&nbsp;&nbsp;' . $main_page['pages_title']);
								}
								echo TEXT_PAGES_PARENT . '<br />' . tep_draw_pull_down_menu('parents_id', $main_pages_arr, $pInfo->parents_id);
								?>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div id="tabs">
									<?php
									$tabs = '<ul>';
									$tabs_content = '<div>';

									for ($i=0, $n=sizeof($languages); $i < $n; $i++) {
										$tabs .= '<li data-tab="tabs-' . $languages[$i]['id'] . '"><a>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</a></li>';

										$tabs_content .= '
                                    <div id="tabs-' . $languages[$i]['id'] . '">
                                        <table width="100%">
                                            <tr>
                                                <td class="main">' . TEXT_PAGES_TITLE . '<span style="color: #ff0000;">*</span></td>
                                                <td class="main">' . tep_draw_input_field('pages_title[' . $languages[$i]['id'] . ']', (empty($pInfo->pages_id) ? '' : tep_get_pages_title($pInfo->pages_id, $languages[$i]['id']))) . '</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="main" valign="top">' . TEXT_PAGES_DESCRIPTION . '<br/>
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td colspan="2" class="main">' . tep_draw_textarea_field('pages_description[' . $languages[$i]['id'] . ']', 'soft', '70', '15', (empty($pInfo->pages_id) ? '' : tep_get_pages_description($pInfo->pages_id, $languages[$i]['id'])), 'class="tinymce"') . '</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="main">Meta Keywords<span style="color: #ff0000;">*</span></td>
                                                <td class="main">' . tep_draw_input_field('meta_keyword[' . $languages[$i]['id'] . ']', (empty($pInfo->pages_id) ? '' : tep_get_pages_meta_keyword($pInfo->pages_id, $languages[$i]['id']))) . '</td>
                                            </tr>
                                            <tr>
                                                <td class="main">Meta Description<span style="color: #ff0000;">*</span></td>
                                                <td class="main">' . tep_draw_input_field('meta_description[' . $languages[$i]['id'] . ']', (empty($pInfo->pages_id) ? '' : tep_get_pages_meta_description($pInfo->pages_id, $languages[$i]['id']))) . '</td>
                                            </tr>';
										$tabs_content .= '
                                        </table>
                                    </div>';
									}
									$tabs .= '</ul>';
									$tabs_content .= '</div>';
									echo $tabs . $tabs_content;
									?>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
						</tr>
						<tr>
							<td class="main"><?= TEXT_PAGES_IMAGE ?></td>
							<td class="main"><?= (!empty($pInfo->pages_image) ? tep_info_image(DIR_WS_PAGES_IMAGES . $pInfo->pages_image, '', SMALL_IMAGE_WIDTH) : '') . '<br/>' .  tep_draw_file_field('pages_image') ?></td>
						</tr>
						<tr>
							<td colspan="2" class="main">
								<?php
								echo '<br />' . TEXT_SORT_ORDER . '<br />' . tep_draw_input_field('pages_sort_order', empty($pInfo->pages_sort_order) ? 0 : $pInfo->pages_sort_order, 'size="2"');
								?>
							</td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
			</tr>
			<tr>
				<td class="smallText" align="right"><?php echo tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PAGES, (isset($_GET['pID']) ? 'pID=' . $_GET['pID'] : ''))); ?></td>
			</tr>
		</table>

		</form>
		<script type="text/javascript">
			var page_form = document.forms["new_page"];
			function validateForm() {
				var message_stack = '';
				<?php for ($i=0, $n=sizeof($languages); $i<$n; $i++) { ?>
				if(page_form.elements['pages_title[<?php echo $languages[$i]['id']; ?>]'].value == '')
					message_stack += 'Fill Pages Title in <?php echo $languages[$i]['name'];?>\n';
				<?php } ?>
				var checked_flag = false;
				<?php if ($action == 'new_page') {?>
				<?php }?>
				if (message_stack != '') { alert(message_stack); return false;} else {
					stopUnload = false;
				}
			}
			page_form.onsubmit = validateForm;
		</script>
		<?php
		echo '            </td>' . "\n";
	} elseif ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
		echo '            <td width="25%" valign="top">' . "\n";

		$box = new box;
		echo $box->infoBox($heading, $contents);

		echo '            </td>' . "\n";
	}
	?>
	</tr>
	</table></td>
	</tr>
	</table>
<?php


require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>