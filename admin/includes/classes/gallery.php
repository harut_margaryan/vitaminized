<?php
class CGallery
{
	function CGallery()
    {
    }
	
    function addPhoto($filePath,$name,$ext='.jpg',$watermark=0,$b_height='',$b_width='',$m_height='',$m_width='',$s_height='',$s_width='')
    {
		if($b_width || $b_height) $this->resizeImage($filePath,$filePath,$b_height,$b_width);
		if($watermark) $this->watermarkImage($filePath,0,1);
        move_uploaded_file($filePath,$name.'-1'.$ext);
		if($m_width || $m_height) $this->resizeImage($name.'-1'.$ext,$name.'-2'.$ext,$m_height,$m_width);
		if($s_width || $s_height) $this->resizeImage($name.'-1'.$ext,$name.'-3'.$ext,$s_height,$s_width);
    }

///////////////////////
	function resizeImage($from, $to, $toH, $toW)
	{
        list($width_orig, $height_orig, $image_format) = @getimagesize($from);
		
//		if($image_format != 2) { echo "Unsupported picture format. Must be .jpg"; die; } 

		if ($width_orig < $toW || $height_orig < $toH) return;
        
        if ($toH && ($width_orig < $height_orig)) {
            $toW = ($toH / $height_orig) * $width_orig;
        } else {
            $toH = ($toW / $width_orig) * $height_orig;
        }	
		
        $image_p = imagecreatetruecolor($toW, $toH);

		if($image_format == 2)  
		  $image = @imagecreatefromjpeg($from);
		elseif($image_format == 3)  
		  $image = @imagecreatefrompng($from);
		elseif($image_format == 1)  
		  $image = @imagecreatefromgif($from);
		else
		  die();  
#        $image = imagecreatefromjpeg($from);

        // convert all to jpg with white bg
        $white = imagecolorallocate($image_p, 255, 255, 255);
        imagefilledrectangle($image_p, 0, 0, $toW, $toH, $white);

        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $toW, $toH, $width_orig, $height_orig);

        imagejpeg($image_p, $to, 90);

#        imagejpeg($image_p,$to, 95);

        imagedestroy($image_p);
        imagedestroy($image);	
	}
	
    function watermarkImage($filename, $text = '1', $logo = '0')  #if $text=1 -> text-wm 
    {
		  define('WATERMARK_MARGIN', '0'); // margin
		  define('TEXT_SHADOW', '1'); // 1 - yes / 0 - no
		  $wm_text = 'text';
		  $wm_filename = DIR_FS_CATALOG . DIR_WS_IMAGES . 'store_logo.png';
		  
		  $watermark_align_h = 'right'; // left / right / center
		  $watermark_align_v = 'bottom'; // top / bottom / center
		  
		  $lst = getimagesize($filename);
		  $image_width = $lst[0];
		  $image_height = $lst[1];
		  $image_format = $lst[2];
		 
		  if($image_format == 2)  
			$image = @imagecreatefromjpeg($filename);
		  elseif($image_format == 3)  
			$image = @imagecreatefrompng($filename);
		  elseif($image_format == 1)  
			$image = @imagecreatefromgif($filename);
		  else
			die();  
		 
		  if(!$image) 
			die();
		  
		  if ($text == '1') { // text

		    $text_color = imagecolorallocate ($image, 255, 255, 255); 
#		    $bg_color = imagecolorallocatealpha ($image, 0, 0, 0, 90); 

		    $font = 'arialamb.ttf';
		    $font_size = '9';
			
		    $text_box = imagettfbbox ($font_size, 0, $font, $wm_text);
#		    $text_height = $text_box[3]-$text_box[5];
		    $text_height = $font_size;
		    $text_width = $text_box[2]-$text_box[0];

		    $wt_y = WATERMARK_MARGIN;
		    if ($watermark_align_v == 'top') {
		      $wt_y = WATERMARK_MARGIN;
		    } elseif ($watermark_align_v == 'bottom') {
		      $wt_y = $image_height-$text_height-WATERMARK_MARGIN;
		    } elseif ($watermark_align_v == 'center') {
		      $wt_y = (int)($image_height/2-$text_height/2);
		    }
		
		    $wt_x = WATERMARK_MARGIN;
		    if ($watermark_align_h == 'left') {
		      $wt_x = WATERMARK_MARGIN;
		    } elseif ($watermark_align_h == 'right') {
		      $wt_x = $image_width-$text_width-WATERMARK_MARGIN;
		    } elseif ($watermark_align_h == 'center') {
		      $wt_x = (int)($image_width/2-$text_width/2);
		    }

#            imagefilledrectangle ($image, 0, $image_height-$font_size-(2*WATERMARK_MARGIN), $image_width, $image_height, $bg_color);

		    if (TEXT_SHADOW == '1')
              imagettftext($image, $font_size, 0, $wt_x+1, $wt_y+$font_size+1, 0, $font, $wm_text);
            imagettftext($image, $font_size, 0, $wt_x, $wt_y+$font_size, $text_color, $font, $wm_text);
		
		  }
		  
		  if ($logo == '1') { //image
		  
		      $watermark_align_h = 'right'; // left / right / center
		      $watermark_align_v = 'bottom'; // top / bottom / center

			  $wm_lst = getimagesize($wm_filename);
			  $wm_image_width = $wm_lst[0];
			  $wm_image_height = $wm_lst[1];
			  $wm_image_format = $wm_lst[2];
			  
			  if($wm_image_format == 3)
				$watermark = @imagecreatefrompng($wm_filename);
			  elseif($wm_image_format == 1)
				$watermark = @imagecreatefromgif($wm_filename);
			  else
				die();
					
			  if(!$watermark)
				die();
			 
			  $wm_image_y = WATERMARK_MARGIN;
			  if ($watermark_align_v == 'top') {
				$wm_image_y = WATERMARK_MARGIN;
			  } elseif ($watermark_align_v == 'bottom') {
				$wm_image_y = $image_height-$wm_image_height-WATERMARK_MARGIN; //////////!!!-3 x
			  } elseif ($watermark_align_v == 'center') {
				$wm_image_y = (int)($image_height/2-$wm_image_height/2);
			  }
			
			  $wm_image_x = WATERMARK_MARGIN;
			  if ($watermark_align_h == 'left') {
				$wm_image_x = WATERMARK_MARGIN+2; //////////!!!+2
			  } elseif ($watermark_align_h == 'right') {
				$wm_image_x = $image_width-$wm_image_width-WATERMARK_MARGIN;
			  } elseif ($watermark_align_h == 'center') {
				$wm_image_x = (int)($image_width/2-$wm_image_width/2);
			  }
			
              imagecopy($image, $watermark,  $wm_image_x, $wm_image_y, 0, 0, $wm_image_width, $wm_image_height);
			  imagedestroy($watermark);
		  }

		  if ($image_format == 2) {
			imagejpeg($image, $filename, 95);
		  } elseif ($image_format == 3) {
			imagepng($image, $filename, 95);
		  }
		
		  imagedestroy($image);
		  
#		  chmod("$filename",0666);
    }
}
?>