<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

// define the database table names used in the project
define('TABLE_ACTION_RECORDER', 'action_recorder');
define('TABLE_ADDRESS_BOOK', 'address_book');
define('TABLE_ADDRESS_FORMAT', 'address_format');
define('TABLE_ADMINISTRATORS', 'administrators');
define('TABLE_BANNERS', 'banners');
define('TABLE_BANNERS_HISTORY', 'banners_history');
define('TABLE_CATEGORIES', 'categories');
define('TABLE_CATEGORIES_DESCRIPTION', 'categories_description');
define('TABLE_CONFIGURATION', 'configuration');
define('TABLE_CONFIGURATION_GROUP', 'configuration_group');
define('TABLE_COUNTRIES', 'countries');
define('TABLE_CURRENCIES', 'currencies');
define('TABLE_CUSTOMERS', 'customers');
define('TABLE_CUSTOMERS_TO_PARTNERS', 'customers_to_partners');
define('TABLE_PARTNERS', 'partners');
define('TABLE_CUSTOMERS_BASKET', 'customers_basket');
define('TABLE_CUSTOMERS_BASKET_ATTRIBUTES', 'customers_basket_attributes');
define('TABLE_CUSTOMERS_INFO', 'customers_info');
define('TABLE_PARTNERS_INFO', 'partners_info');
define('TABLE_LANGUAGES', 'languages');
define('TABLE_MANUFACTURERS', 'manufacturers');
define('TABLE_MANUFACTURERS_INFO', 'manufacturers_info');
define('TABLE_MARKETING_METHODS', 'marketing_methods');
define('TABLE_MARKETING_METHODS_INFO', 'marketing_methods_info');
define('TABLE_MARKETING_POSTS', 'marketing_posts');
define('TABLE_MARKETING_POSTS_TAGS', 'marketing_posts_tags');
define('TABLE_MARKETING_BANNERS', 'marketing_banners');
define('TABLE_MARKETING_BANNERS_TAGS', 'marketing_banners_tags');
define('TABLE_MARKETING_PICS', 'marketing_pics');
define('TABLE_MARKETING_PICS_TAGS', 'marketing_pics_tags');
define('TABLE_MARKETING_MAILS', 'marketing_mails');
define('TABLE_MARKETING_MAILS_TAGS', 'marketing_mails_tags');
define('TABLE_MARKETING_SHARES', 'marketing_shares');
define('TABLE_MARKETING_SHARES_TAGS', 'marketing_shares_tags');
define('TABLE_MARKETING_STATUSES', 'marketing_statuses');
define('TABLE_MARKETING_STATUSES_TAGS', 'marketing_statuses_tags');
define('TABLE_MARKETING_TAGS', 'marketing_tags');
define('TABLE_MARKETING_TAGS_INFO', 'marketing_tags_info');
define('TABLE_NEWSLETTERS', 'newsletters');
define('TABLE_ORDERS', 'orders');
define('TABLE_ORDERS_PRODUCTS', 'orders_products');
define('TABLE_ORDERS_PRODUCTS_ATTRIBUTES', 'orders_products_attributes');
define('TABLE_ORDERS_PRODUCTS_DOWNLOAD', 'orders_products_download');
define('TABLE_ORDERS_STATUS', 'orders_status');
define('TABLE_ORDERS_STATUS_HISTORY', 'orders_status_history');
define('TABLE_ORDERS_TOTAL', 'orders_total');
define('TABLE_PARTNERS_HISTORY', 'partners_history');
define('TABLE_PAGES', 'pages');
define('TABLE_PAGES_DESCRIPTION', 'pages_description');
define('TABLE_PRODUCTS', 'products');
define('TABLE_PRODUCTS_ATTRIBUTES', 'products_attributes');
define('TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD', 'products_attributes_download');
define('TABLE_PRODUCTS_DESCRIPTION', 'products_description');
define('TABLE_PRODUCTS_IMAGES', 'products_images');
define('TABLE_PRODUCTS_INFO', 'products_info');
define('TABLE_PRODUCTS_INFO_DESCRIPTION', 'products_info_description');
define('TABLE_PRODUCTS_NOTIFICATIONS', 'products_notifications');
define('TABLE_PRODUCTS_OPTIONS', 'products_options');
define('TABLE_PRODUCTS_OPTIONS_VALUES', 'products_options_values');
define('TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS', 'products_options_values_to_products_options');
define('TABLE_PRODUCTS_TO_CATEGORIES', 'products_to_categories');
define('TABLE_PRODUCTS_TO_PRODUCTS_INFO', 'products_to_products_info');
define('TABLE_REVIEWS', 'reviews');
define('TABLE_SEC_DIRECTORY_WHITELIST', 'sec_directory_whitelist');
define('TABLE_SESSIONS', 'sessions');
define('TABLE_SPECIALS', 'specials');
define('TABLE_TABS_INFO', 'tabs_info');
define('TABLE_TABS_INFO_DESC', 'tabs_info_desc');
define('TABLE_TAX_CLASS', 'tax_class');
define('TABLE_TAX_RATES', 'tax_rates');
define('TABLE_GEO_ZONES', 'geo_zones');
define('TABLE_ZONES_TO_GEO_ZONES', 'zones_to_geo_zones');
define('TABLE_VISITS', 'visits');
define('TABLE_WHOS_ONLINE', 'whos_online');
define('TABLE_ZONES', 'zones');
?>
