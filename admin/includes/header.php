<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

  if ($messageStack->size > 0) {
    echo $messageStack->output();
  }
?>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr class="headerBar">
      <td class="headerBarContent">&nbsp;&nbsp;<?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT, '', 'NONSSL') . '" class="headerLink">' . HEADER_TITLE_ADMINISTRATION . '</a>'; ?></td>
      <?php
      if (tep_session_is_registered('admin')) {
          if (tep_session_is_registered('admin')) {
              $cl_box_groups = array();

              if ($admin['id'] == 2) include(DIR_WS_BOXES . 'configuration.php');
              include(DIR_WS_BOXES . 'catalog.php');
              include(DIR_WS_BOXES . 'modules.php');
              include(DIR_WS_BOXES . 'accounts.php');
              include(DIR_WS_BOXES . 'taxes.php');
              include(DIR_WS_BOXES . 'localization.php');
              include(DIR_WS_BOXES . 'marketing.php');
              include(DIR_WS_BOXES . 'reports.php');
              if ($admin['id'] == 2) include(DIR_WS_BOXES . 'tools.php');
              ?>

              <div id="adminAppMenu">

                  <?php
                  foreach ($cl_box_groups as $groups) {
                      echo '<td class="headerBarContent"><h3>' . $groups['heading'] . '</h3>' .
                          '<ul>';

                      foreach ($groups['apps'] as $app) {
                          echo '<li><a href="' . $app['link'] . '">' . $app['title'] . '</a></li>';
                      }

                      echo '</ul></td>';
                  }
                  ?>

              </div>
          <?php
          }
      }
      ?>
      <td class="headerBarContent"><?php echo (tep_session_is_registered('admin') ? 'Logged in as: ' . $admin['username']  . ' (<a href="' . tep_href_link(FILENAME_LOGIN, 'action=logoff') . '" class="headerLink">Logoff</a>)' : ''); ?>&nbsp;&nbsp;</td>
  </tr>
</table>