<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Arto
 * Date: 02.10.13
 * Time: 17:34
 * To change this template use File | Settings | File Templates.
 */

function tep_htmlentities_decode($text) {
    $text = str_replace(
        array(
            '&quot;','&#039;','&#37;','&euro;','&amp;','&lt;','&gt;','&laquo;','&raquo;','&lsquo;','&rsquo;','&ldquo;','&rdquo;','&bdquo;','&Prime;','&not;','&ndash;','&#58;','&#43;','&#45;','&nbsp;','&ordf;','&bull;','&deg;','&reg;','&mdash;',
        ),
        array(
            '"',"'",'%','€','&','<','>','«','»','‘','’','"','"','"','"','','-',':','+','-',' ','`','.','','','-',
        ),
        $text );
    return html_entity_decode($text);
}

function tep_clean_text_int($str) {	// cleaning text-names - for url

    $str = tep_htmlentities_decode($str);
    $str = preg_replace("#[^(\w)|(\x7F-\xFF)|(\s)]#", " ", $str);
    $str = trim($str);
    $str = preg_replace('#[-_ ]+#', '-', $str);//tep_htmlentities_decode($text)
    return tep_strtolower_int($str,CHARSET);
}

function tep_strtolower_int($str){
    return mb_convert_case($str, MB_CASE_LOWER);
}

function tep_translit($str) {

    $tr = array(
        ":"=>".","՞"=>"","`"=>":","՝"=>":","ª"=>":","՜"=>"","՛"=>"",

        "Ա"=>"A","Բ"=>"B","Գ"=>"G","Դ"=>"D",
        "Ե"=>"E","Զ"=>"Z","Է"=>"E","Ը"=>"Y","Թ"=>"T",
        "Ժ"=>"Zh","Ի"=>"I","Լ"=>"L","Խ"=>"X","Ծ"=>"Ts",
        "Կ"=>"K","Հ"=>"H","Ձ"=>"Dz","Ղ"=>"Gh","Ճ"=>"Tch",
        "Մ"=>"M","Յ"=>"Y","Ն"=>"N","Շ"=>"Sh","Ո"=>"O",
        "Չ"=>"Ch","Պ"=>"P","Ջ"=>"J","Ռ"=>"R","Ս"=>"S",
        "Վ"=>"V","Տ"=>"T","Ր"=>"R","Ց"=>"C","Ու"=>"U",
        "ՈՒ"=>"U","Փ"=>"Ph","Ք"=>"Q","ԵՎ"=>"Ev","Եվ"=>"Ev",
        "Եւ"=>"Ev","Օ"=>"O","Ֆ"=>"F",

        "ա"=>"a","բ"=>"b","գ"=>"g","դ"=>"d",
        "ե"=>"e","զ"=>"z","է"=>"e","ը"=>"y","թ"=>"t",
        "ժ"=>"zh","ի"=>"i","լ"=>"l","խ"=>"x","ծ"=>"ts",
        "կ"=>"k","հ"=>"h","ձ"=>"dz","ղ"=>"gh","ճ"=>"tch",
        "մ"=>"m","յ"=>"y","ն"=>"n","շ"=>"sh","ո"=>"o",
        "չ"=>"ch","պ"=>"p","ջ"=>"j","ռ"=>"r","ս"=>"s",
        "վ"=>"v","տ"=>"t","ր"=>"r","ց"=>"c","ու"=>"u",
        "փ"=>"ph","ք"=>"q","եվ"=>"ev","եւ"=>"ev",
        "և"=>"ev","օ"=>"o","ֆ"=>"f",

        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ё"=>"Yo","Ж"=>"Zh","З"=>"Z",
        "И"=>"I","Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M",
        "Н"=>"N","О"=>"O","П"=>"P","Р"=>"R","С"=>"S",
        "Т"=>"T","У"=>"U","Ф"=>"F","Х"=>"X","Ц"=>"C",
        "Ч"=>"Ch","Ш"=>"Sh","Щ"=>"Shch","Ъ"=>"",
        "Ы"=>"I","Ь"=>"","Э"=>"E","Ю"=>"Yu","Я"=>"Ya",

        "а"=>"a","б"=>"b","в"=>"v","г"=>"g",
        "д"=>"d","е"=>"e","ё"=>"yo","ж"=>"zh","з"=>"z",
        "и"=>"i","й"=>"y","к"=>"k","л"=>"l","м"=>"m",
        "н"=>"n","о"=>"o","п"=>"p","р"=>"r","с"=>"s",
        "т"=>"t","у"=>"u","ф"=>"f","х"=>"x","ц"=>"c",
        "ч"=>"ch","ш"=>"sh","щ"=>"shch","ъ"=>"",
        "ы"=>"i","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
    );
    return strtr($str,$tr);
}