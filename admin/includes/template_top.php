<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<meta name="robots" content="noindex,nofollow">
<title><?php echo TITLE; ?></title>
<base href="<?php echo HTTP_SERVER . DIR_WS_ADMIN; ?>" />
<!--[if IE]><script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/flot/excanvas.min.js'); ?>"></script><![endif]-->
<link rel="stylesheet" type="text/css" href="<?php echo tep_catalog_href_link('ext/jquery/ui/smoothness/jquery-ui-1.10.4.min.css'); ?>">
<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/jquery/jquery-1.11.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/jquery/ui/jquery-ui-1.10.4.min.js'); ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo tep_catalog_href_link('ext/jg/tabs/jg-tabs.css'); ?>">
<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/jg/tabs/jg-tabs.js'); ?>"></script>

<script type="text/javascript">
// fix jQuery 1.8.0 and jQuery UI 1.8.22 bug with dialog buttons; http://bugs.jqueryui.com/ticket/8484
if ( $.attrFn ) { $.attrFn.text = true; }
</script>

<?php
  if (tep_not_null(JQUERY_DATEPICKER_I18N_CODE)) {
?>
<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/jquery/ui/i18n/jquery.ui.datepicker-' . JQUERY_DATEPICKER_I18N_CODE . '.js'); ?>"></script>
<script type="text/javascript">
$.datepicker.setDefaults($.datepicker.regional['<?php echo JQUERY_DATEPICKER_I18N_CODE; ?>']);
</script>
<?php
  }

if ('stats_customers_activity.php' === $current_page) {
?>
	<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/highcharts.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/daterange.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/linechart.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/barchart.js'); ?>"></script>
<?php
}

?>

<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/swfobject.js'); ?>"></script>

<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/flot/jquery.flot.js'); ?>"></script>
<script type="text/javascript" src="<?php echo tep_catalog_href_link('ext/flot/jquery.flot.time.js'); ?>"></script>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script>
    $(function() {
        $('.prodInfoTabs').each(function() {
            new JgTabs('#' + this.id);
        });
        new JgTabs('#tabs');
    });
</script>
<?php require('tinymce.php'); ?>
<?php //require('ckeditor.php'); ?>
<script type="text/javascript" src="includes/general.js"></script>
<script>
    stopUnload = false;
</script>
</head>
<body>

<?php require(DIR_WS_INCLUDES . 'header.php'); ?>

<div id="contentText">
