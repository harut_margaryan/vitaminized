<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
?>

</div>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>

<script>
    window.onbeforeunload = function() {
        if (stopUnload) return 'Are you sure you want to do it?';
    };
</script>

</body>
</html>
