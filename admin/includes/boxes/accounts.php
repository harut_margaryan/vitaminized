<?php
$cl_box_groups[] = array(
    'heading' => BOX_HEADING_ACCOUNTS,
    'apps' => array(
        array(
            'code' => FILENAME_CUSTOMERS,
            'title' => BOX_ACCOUNTS_CUSTOMERS,
            'link' => tep_href_link(FILENAME_CUSTOMERS)
        ),
        array(
            'code' => FILENAME_PARTNERS,
            'title' => BOX_ACCOUNTS_PARTNERS,
            'link' => tep_href_link(FILENAME_PARTNERS)
        ),
        array(
            'code' => FILENAME_PARTNERS_HISTORY,
            'title' => BOX_ACCOUNTS_PARTNERS_HISTORY,
            'link' => tep_href_link(FILENAME_PARTNERS_HISTORY)
        ),
        array(
            'code' => FILENAME_ORDERS,
            'title' => BOX_ACCOUNTS_ORDERS,
            'link' => tep_href_link(FILENAME_ORDERS)
        )
    )
);