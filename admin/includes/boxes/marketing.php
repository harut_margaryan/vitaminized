<?php
//$methods_array = array(array('id' => '0', 'text' => TEXT_SELECT));
$methods_name = array();
$methods_query = tep_db_query("select method_id, method_name from " . TABLE_MARKETING_METHODS . " join " . TABLE_MARKETING_METHODS_INFO . " using(method_id) where language_id = 1 order by sort_order");
while($methods = tep_db_fetch_array($methods_query)){
//    $methods_array[] = array('id' => $methods['method_id'], 'text' => $methods['method_name']);
    $methods_name[$methods['method_id']] = $methods['method_name'];
}

$cl_box_groups[] = array(
    'heading' => BOX_HEADING_MARKETING,
    'apps' => array(
        array(
            'code' => FILENAME_MARKETING_METHODS,
            'title' => BOX_MARKETING_METHODS,
            'link' => tep_href_link(FILENAME_MARKETING_METHODS)
        ),
        array(
            'code' => FILENAME_MARKETING_MATERIALS . '?type=1',
            'title' => $methods_name[1],
            'link' => tep_href_link(FILENAME_MARKETING_MATERIALS, 'type=1')
        ),
        array(
            'code' => FILENAME_MARKETING_MATERIALS . '?type=2',
            'title' => $methods_name[2],
            'link' => tep_href_link(FILENAME_MARKETING_MATERIALS, 'type=2')
        ),
        array(
            'code' => FILENAME_MARKETING_MATERIALS . '?type=3',
            'title' => $methods_name[3],
            'link' => tep_href_link(FILENAME_MARKETING_MATERIALS, 'type=3')
        ),
        array(
            'code' => FILENAME_MARKETING_MATERIALS . '?type=4',
            'title' => $methods_name[4],
            'link' => tep_href_link(FILENAME_MARKETING_MATERIALS, 'type=4')
        ),
        array(
            'code' => FILENAME_MARKETING_MATERIALS . '?type=5',
            'title' => $methods_name[5],
            'link' => tep_href_link(FILENAME_MARKETING_MATERIALS, 'type=5')
        ),
        array(
            'code' => FILENAME_MARKETING_MATERIALS . '?type=6',
            'title' => $methods_name[6],
            'link' => tep_href_link(FILENAME_MARKETING_MATERIALS, 'type=6')
        ),
        array(
            'code' => FILENAME_MARKETING_TAGS,
            'title' => BOX_MARKETING_TAGS,
            'link' => tep_href_link(FILENAME_MARKETING_TAGS)
        )
    )
);