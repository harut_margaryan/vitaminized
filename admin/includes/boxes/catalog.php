<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
$apps = array();

$apps[] = array(
	'code' => FILENAME_CATEGORIES,
	'title' => BOX_CATALOG_CATEGORIES,
	'link' => tep_href_link(FILENAME_CATEGORIES)
);
$apps[] = array(
	'code' => FILENAME_PRODUCTS,
	'title' => BOX_CATALOG_PRODUCTS,
	'link' => tep_href_link(FILENAME_PRODUCTS)
);
if ($admin['id'] == 2)
	$apps[] = array(
		'code' => FILENAME_PRODUCTS_ATTRIBUTES,
		'title' => BOX_CATALOG_CATEGORIES_PRODUCTS_ATTRIBUTES,
		'link' => tep_href_link(FILENAME_PRODUCTS_ATTRIBUTES)
	);
if ($admin['id'] == 2)
	$apps[] = array(
		'code' => FILENAME_MANUFACTURERS,
		'title' => BOX_CATALOG_MANUFACTURERS,
		'link' => tep_href_link(FILENAME_MANUFACTURERS)
	);
$apps[] = array(
	'code' => FILENAME_REVIEWS,
	'title' => BOX_CATALOG_REVIEWS,
	'link' => tep_href_link(FILENAME_REVIEWS)
);
if ($admin['id'] == 2)
	$apps[] = array(
		'code' => FILENAME_SPECIALS,
		'title' => BOX_CATALOG_SPECIALS,
		'link' => tep_href_link(FILENAME_SPECIALS)
	);
$apps[] = array(
	'code' => FILENAME_PRODUCTS_INFO,
	'title' => BOX_CATALOG_PRODUCTS_INFO,
	'link' => tep_href_link(FILENAME_PRODUCTS_INFO)
);
$apps[] = array(
	'code' => FILENAME_PAGES,
	'title' => BOX_CATALOG_PAGES,
	'link' => tep_href_link(FILENAME_PAGES)
);
$apps[] = array(
	'code' => FILENAME_TABS_INFO,
	'title' => BOX_CATALOG_TABS_INFO,
	'link' => tep_href_link(FILENAME_TABS_INFO)
);
if ($admin['id'] == 2)
	$apps[] = array(
		'code' => FILENAME_PRODUCTS_EXPECTED,
		'title' => BOX_CATALOG_PRODUCTS_EXPECTED,
		'link' => tep_href_link(FILENAME_PRODUCTS_EXPECTED)
	);

$cl_box_groups[] = array(
	'heading' => BOX_HEADING_CATALOG,
	'apps' => $apps
);
?>
