<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Pages');
define('HEADING_TITLE_SEARCH', 'Search:');
define('HEADING_TITLE_FILTER', 'Filter:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_PAGES', 'Pages');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');

define('TEXT_NEW_PAGE', 'New Page');
define('TEXT_EDIT_PAGE', 'Edit Page');
define('TEXT_PARENT', 'Parent:');
define('TEXT_PAGES', 'Pages:');
define('TEXT_PAGES_PARENT', 'Pages parent:');
define('TEXT_IMAGE_NONEXISTENT', 'IMAGE DOES NOT EXIST');
define('TEXT_NO_CHILD_PAGES', 'Please insert a new page in this level.');

define('TEXT_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_EDIT_SORT_ORDER', 'Sort Order:');

define('TEXT_INFO_HEADING_DELETE_PAGE', 'Delete Page');

define('TEXT_DELETE_PAGE_INTRO', 'Are you sure you want to permanently delete this page?');

define('TEXT_DELETE_WARNING_CHILDS', '<strong>WARNING:</strong> There are %s (child-)pages still linked to this category!');
define('TEXT_DELETE_WARNING_PAGES', '<strong>WARNING:</strong> There are %s pages still linked to this category!');

define('TEXT_SORT_ORDER', 'Sort Order:');

define('TEXT_PAGES_STATUS', 'Pages Status:');
define('TEXT_PAGES_TITLE', 'Pages Title:');
define('TEXT_PAGES_DESCRIPTION', 'Pages Description:');
define('TEXT_PAGES_IMAGE', 'Pages Image:');
define('TEXT_PAGES_IMAGE_WITH_LOGO', 'Width Logo');
define('TEXT_PAGES_MAIN_IMAGE', 'Main Image');
define('TEXT_PAGES_LARGE_IMAGE', 'Large Image');
define('TEXT_PAGES_LARGE_IMAGE_HTML_CONTENT', 'HTML Content (for popup)');
define('TEXT_PAGES_URL', 'Pages URL:');
define('TEXT_PAGES_URL_WITHOUT_HTTP', '<small>(without http://)</small>');

define('ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE', 'Error: Catalog images directory is not writeable: ' . DIR_FS_CATALOG_IMAGES);
define('ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST', 'Error: Catalog images directory does not exist: ' . DIR_FS_CATALOG_IMAGES);
?>