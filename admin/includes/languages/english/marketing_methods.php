<?php

define('HEADING_TITLE', 'Marketing Methods');

define('TABLE_HEADING_METHOD', 'Method');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_METHOD_NAME', 'Method Name:');
define('TEXT_INFO_HEADING_EDIT_PRODUCTS_INFO', 'Edit Method');
define('TEXT_EDIT_SORT_ORDER', 'Sort Order:');
?>
