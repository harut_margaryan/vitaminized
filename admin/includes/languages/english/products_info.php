<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Products Information');

define('TABLE_HEADING_PRODUCTS_INFO', 'Products Information');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_PRODUCTS_INFO_NAME', 'Products Information Title:');
define('TEXT_INFO_INSERT_INTRO', 'Please enter the new Products Information with its related data');
define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to delete this Products Information?');
define('TEXT_INFO_HEADING_NEW_PRODUCTS_INFO', 'New Products Information');
define('TEXT_INFO_HEADING_EDIT_PRODUCTS_INFO', 'Edit Products Information');
define('TEXT_INFO_HEADING_DELETE_PRODUCTS_INFO', 'Delete Products Information');
define('TEXT_EDIT_SORT_ORDER', 'Sort Order:');
?>