<?php
define('HEADING_TITLE', 'Partners History');
define('HEADING_TITLE_SEARCH', 'Search:');

define('TABLE_HEADING_PARTNER', 'Partner');
define('TABLE_HEADING_CUSTOMER', 'Customer');
define('TABLE_HEADING_PAYMENT_DATE', 'Payment Date');
define('TABLE_HEADING_REFERRAL_REFERER', 'Referral from');
define('TABLE_HEADING_ORDER', 'Order');
define('TABLE_HEADING_AFFILIATES_COMMISSION', 'Affiliates Commission');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_DATE_ACCOUNT_CREATED', 'Account Created:');
define('TEXT_DATE_ACCOUNT_LAST_MODIFIED', 'Last Modified:');
define('TEXT_INFO_DATE_LAST_LOGON', 'Last Logon:');
define('TEXT_INFO_NUMBER_OF_LOGONS', 'Number of Logons:');
define('TEXT_INFO_COUNTRY', 'Country:');
define('TEXT_INFO_NUMBER_OF_REVIEWS', 'Number of Reviews:');
define('TEXT_DELETE_INTRO', 'Are you sure you want to delete this partner?');
define('TEXT_DELETE_REVIEWS', 'Delete %s review(s)');
define('TEXT_INFO_HEADING_DELETE_PARTNER', 'Delete Partner');
define('TYPE_BELOW', 'Type below');
define('PLEASE_SELECT', 'Select One');