<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
    switch ($action) {
        case 'setflag':
            if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
                if (isset($_GET['tiID'])) {
					$status = $_GET['flag'];
					if ($status == '1') {
						tep_db_query("update " . TABLE_TABS_INFO . " set status = '1' where id = '" . (int)$_GET['tiID'] . "'");
					} else {
						tep_db_query("update " . TABLE_TABS_INFO . " set status = '0' where id = '" . (int)$_GET['tiID'] . "'");
					}
                }
            }

            tep_redirect(tep_href_link(FILENAME_TABS_INFO, 'tiID=' . $_GET['tiID']));
            break;
        case 'insert_tab_info':
        case 'update_tab_info':
            if (isset($_GET['tiID'])) $tabs_info_id = intval($_GET['tiID']);

            $sql_data_array = array(
				'products_id' => empty($_POST['products_id']) ? 0 : (int)($_POST['products_id']),
				'products_info_id' => empty($_POST['products_info_id']) ? 0 : (int)($_POST['products_info_id'])
			);

			/*$tabs_info_image = new upload('tabs_info_image');
            $tabs_info_image->set_destination(DIR_FS_CATALOG_IMAGES);
            if ($tabs_info_image->parse() && $tabs_info_image->save()) {
                $sql_data_array['tabs_info_image'] = tep_db_prepare_input($tabs_info_image->filename);
            }*/

            $error_message = '';

			if (!$sql_data_array['products_id']) {
				$error_message .= 'Error: select product<br/>';
			}
			if (!$sql_data_array['products_info_id']) {
				$error_message .= 'Error: select tab<br/>';
			}

            if ($error_message) {
                $messageStack->add_session($error_message, 'error');
                tep_redirect(tep_href_link(FILENAME_TABS_INFO, 'tiID=' . $tabs_info_id . '&action=' . ($action == 'insert_tab_info'? 'new_tab_info' : 'edit_tab_info')));
            }
            if ($action == 'insert_tab_info') {

                tep_db_perform(TABLE_TABS_INFO, $sql_data_array);
                $tabs_info_id = tep_db_insert_id();


            } elseif ($action == 'update_tab_info') {

                tep_db_perform(TABLE_TABS_INFO, $sql_data_array, 'update', "id = '" . (int)$tabs_info_id . "'");

            }

			$sql_data_array = array(
				'tabs_info_id' => $tabs_info_id,
				'content' => empty($_POST['content']) ? array() : $_POST['content']
			);

			$languages = tep_get_languages();


			if (!empty($_FILES['image'])) {
				foreach ($languages as $k => $v) {
					if (is_array($_FILES['image']['tmp_name'][$v['id']])) {
						foreach($_FILES['image']['tmp_name'][$v['id']] as $item_id => $tmp_name) {
							if (!is_uploaded_file($tmp_name)) continue;
							$with_logo = (isset($_POST['with_logo']) && ($_POST['with_logo'] == 'on')) ? 1 : 0;
							$name = 'p' . intval($_POST['products_id']) . '-t' . intval($_POST['products_info_id']) . '-i' . intval($item_id) . '-l' . intval($v['id']);
							$image_ext = '.' . pathinfo($_FILES['image']['name'][$v['id']][$item_id], PATHINFO_EXTENSION);
							$sql_data_array['content'][$v['id']]['body'][$item_id]['image'] = $name . $image_ext;

							$image_location =  DIR_FS_CATALOG_IMAGES_TABS_INFO . $name . $image_ext;

							if (file_exists($image_location)) @unlink($image_location);
							if (file_exists($image_location)) @unlink($image_location);

							move_uploaded_file($tmp_name, $image_location);
						}
					}
				}
			}


            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                $language_id = $languages[$i]['id'];

				if (!empty($sql_data_array['content'][$language_id])) {

					$sql_data = $sql_data_array;
					$sql_data['content'] = json_encode($sql_data_array['content'][$language_id]);
					$sql_data['languages_id'] = $language_id;

					$check_row_query = tep_db_query('select tabs_info_id from ' . TABLE_TABS_INFO_DESC . ' where tabs_info_id = ' . intval($tabs_info_id) . ' and languages_id = ' . intval($language_id));
					if(tep_db_num_rows($check_row_query)) {
						tep_db_perform(TABLE_TABS_INFO_DESC, $sql_data, 'update', 'tabs_info_id = ' . intval($tabs_info_id) . ' and languages_id = ' . intval($language_id));
					} else {
						tep_db_perform(TABLE_TABS_INFO_DESC, $sql_data);
					}
				}
            }

            tep_redirect(tep_href_link(FILENAME_TABS_INFO, 'tiID=' . $tabs_info_id));
            break;
    }
}

// check if the catalog image directory exists
if (is_dir(DIR_FS_CATALOG_IMAGES)) {
    if (!tep_is_writable(DIR_FS_CATALOG_IMAGES)) $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
} else {
    $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
}

require(DIR_WS_INCLUDES . 'template_top.php');
?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pageHeading"><a href="<?php echo tep_href_link(FILENAME_TABS_INFO);?>" style="display: block; text-decoration: none;"><?php echo HEADING_TITLE; ?></td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
                    <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
							<!--<tr>
                                <td class="smallText" align="right">
                                    <?php
                                    echo tep_draw_form('search', FILENAME_TABS_INFO, '', 'get');
                                    echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search');
                                    echo tep_hide_session_id() . '</form>';
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="smallText" align="right">
                                    <?php
                                    echo tep_draw_form('filter', FILENAME_TABS_INFO, '', 'get');
                                    echo HEADING_TITLE_FILTER . ' ' . tep_draw_pull_down_menu('cPath', tep_get_category_tree(), $current_category_id, 'onchange="this.form.submit();"');
                                    echo tep_hide_session_id() . '</form>';
                                    ?>
                                </td>
                            </tr>-->
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
    <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tr>
    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS_INFO; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
            </tr>
            <?php
            $rows = 0;
            $tabs_info_query = tep_db_query("select ti.id, ti.status, pd.products_id, pd.products_name, pid.products_info_id, pid.products_info_title from " . TABLE_TABS_INFO . " as ti join " . TABLE_PRODUCTS_DESCRIPTION . " as pd on(ti.products_id = pd.products_id and pd.language_id = 1) join " . TABLE_PRODUCTS_INFO_DESCRIPTION . " as pid on(ti.products_info_id = pid.products_info_id and pid.language_id = 1)");

            /*if (isset($_GET['search'])) {
                $tabs_info_query = tep_db_query("select ti.id, tid.tabs_info_name, ti.tabs_info_quantity, ti.tabs_info_misspellings, ti.tabs_info_image, ti.tabs_info_price, ti.tabs_info_date_added, ti.tabs_info_last_modified, ti.tabs_info_date_available, ti.tabs_info_status from " . TABLE_TABS_INFO . " p join " . TABLE_TABS_INFO_DESCRIPTION . " pd on (p.id = tid.id and tid.language_id = '" . (int)$languages_id . "') where tid.tabs_info_name like '%" . tep_db_input($_GET['search']) . "%' order by tid.tabs_info_name");
                //$tabs_info_query = tep_db_query("select ti.id, tid.tabs_info_name, ti.tabs_info_quantity, ti.tabs_info_misspellings, ti.tabs_info_image, ti.tabs_info_price, ti.tabs_info_date_added, ti.tabs_info_last_modified, ti.tabs_info_date_available, ti.tabs_info_status from " . TABLE_TABS_INFO . " p, " . TABLE_TABS_INFO_DESCRIPTION . " pd where ti.id = tid.id and tid.language_id = '" . (int)$languages_id . "' and tid.tabs_info_name like '%" . tep_db_input($search) . "%' order by tid.tabs_info_name");
            } else {
                $tabs_info_query = tep_db_query("select ti.id, tid.tabs_info_name, ti.tabs_info_quantity, ti.tabs_info_misspellings, ti.tabs_info_image, ti.tabs_info_price, ti.tabs_info_date_added, ti.tabs_info_last_modified, ti.tabs_info_date_available, ti.tabs_info_status from " . TABLE_TABS_INFO . " p join " . TABLE_TABS_INFO_DESCRIPTION . " pd on (p.id = tid.id and tid.language_id = '" . (int)$languages_id . "') join " . TABLE_TABS_INFO_TO_CATEGORIES . " p2c on (p.id = p2c.id" . $filter_str . ") group by id order by tid.tabs_info_name");
            }*/
            while ($tabs_info = tep_db_fetch_array($tabs_info_query)) {
                $rows++;

// Get categories_id for tab_info if search
                //if (isset($_GET['search'])) $cPath = $tabs_info['categories_id'];

                if ( (!isset($_GET['tiID']) || (isset($_GET['tiID']) && ($_GET['tiID'] == $tabs_info['id']))) && !isset($tiInfo) && (substr($action, 0, 3) != 'new')) {
// find out the rating average from customer reviews
                    $tiInfo = new objectInfo($tabs_info);
                }

                if (isset($tiInfo) && is_object($tiInfo) && ($tabs_info['id'] == $tiInfo->id) ) {
                    echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_TABS_INFO, 'tiID=' . $tabs_info['id'] . '&action=edit_tab_info') . '\'">' . "\n";
                } else {
                    echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_TABS_INFO, 'tiID=' . $tabs_info['id'] . '&action=edit_tab_info') . '\'">' . "\n";
                }
                ?>
                <td class="dataTableContent"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCTS, 'pID=' . $tabs_info['products_id']) . '">' . $tabs_info['products_name'] . '</a>'; ?></td>
                <td class="dataTableContent"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $tabs_info['products_info_id']) . '">' . $tabs_info['products_info_title'] . '</a>'; ?></td>
                <td class="dataTableContent" align="center">
                    <?php
                    if ($tabs_info['status'] == '1') {
                        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_TABS_INFO, 'action=setflag&flag=0&tiID=' . $tabs_info['id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
                    } else {
                        echo '<a href="' . tep_href_link(FILENAME_TABS_INFO, 'action=setflag&flag=1&tiID=' . $tabs_info['id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                    }
                    ?>
                </td>
                <td class="dataTableContent" align="right"><?php if (isset($tiInfo) && is_object($tiInfo) && ($tabs_info['id'] == $tiInfo->id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_TABS_INFO, 'tiID=' . $tabs_info['id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                </tr>
            <?php
            }
            ?>
            <tr>
                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                        <tr>
                            <td align="right" class="smallText"><?php if (!isset($_GET['search'])) echo tep_draw_button(IMAGE_NEW_TAB_INFO, 'plus', tep_href_link(FILENAME_TABS_INFO, 'cPath=' . $cPath . '&action=new_tab_info')); ?>&nbsp;</td>
                        </tr>
                    </table></td>
            </tr>
        </table></td>
    <?php
    $heading = array();
    $contents = array();
    switch ($action) {
        case 'new_tab_info':
            break;
        case 'edit_tab_info':
            break;
        case 'tab_info_preview':
            break;
        default:
            if ($rows > 0) {
                if (isset($tiInfo) && is_object($tiInfo)) { // tab_info info box contents
                    //$heading[] = array('text' => '<strong>' . tep_get_tabs_info_name($tiInfo->id, $languages_id) . '</strong>');

                    $contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_TABS_INFO, 'tiID=' . $tiInfo->id . '&action=edit_tab_info')));
                }
            } else { // create category/tab_info info
                $heading[] = array('text' => '<strong>' . EMPTY_CATEGORY . '</strong>');

                $contents[] = array('text' => TEXT_NO_CHILD_PRODUCTS);
            }
            break;
    }
    if ($action == 'new_tab_info' || $action == 'edit_tab_info') {
        echo '<script>stopUnload = true;</script>';
        echo '            <td width="450" valign="top" id="tab_infoInfo">' . "\n";

        $languages = tep_get_languages();

        $languages_ids = array();
        foreach($languages as &$v) {
            $languages_ids[] = $v['id'];
        }
        $parameters = array(
			'id' => '',
            'products_id' => '',
            'products_info_id' => '',
			'content' => array_fill_keys($languages_ids, array('title' => '', 'body' => array()))
        );

        $tiInfo = new objectInfo($parameters);


        if (!empty($_GET['tiID']) && empty($_POST) && substr($action, 0 , 3) != 'new') {

			$tab_info_query = tep_db_query('select * from ' . TABLE_TABS_INFO . ' as ti join ' . TABLE_TABS_INFO_DESC . ' as tid on(ti.id = tid.tabs_info_id) where ti.id = ' . intval($_GET['tiID']));
			if (tep_db_num_rows($tab_info_query)) {
				$tab_info['id'] = intval($_GET['tiID']);
				while ($tab_inf = tep_db_fetch_array($tab_info_query)) {
					$tab_info['products_id'] = $tab_inf['products_id'];
					$tab_info['products_info_id'] = $tab_inf['products_info_id'];
					$tab_info['content'][$tab_inf['languages_id']] = json_decode($tab_inf['content'], true);
				}

				$tiInfo->objectInfo($tab_info);
			}
        }

        $form_action = $action == 'edit_tab_info' ? 'update_tab_info' : 'insert_tab_info';
        ?>
    <?php echo tep_draw_form('new_tab_info', FILENAME_TABS_INFO, (isset($_GET['tiID']) ? 'tiID=' . $_GET['tiID'] : '') . '&action=' . $form_action, 'post', 'enctype="multipart/form-data"'); ?>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr class="infoBoxHeading">
                <td class="infoBoxHeading"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><?php echo $action == 'new_tab_info' ? TEXT_NEW_TAB_INFO : TEXT_EDIT_TAB_INFO; ?></td>
                            <!--                            <td class="dataTableHeadingContent" align="right">--><?php //echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?><!--</td>-->
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
            <tr>
                <td><table style="width: 100%" border="0" cellspacing="0" cellpadding="2">
                        <tr>
                            <td class="main"><?php echo TEXT_PRODUCTS; ?><span style="color: #ff0000;">*</span></td>
                            <td class="main"><?php
								$all_prods_query = tep_db_query('select products_id, products_name from ' . TABLE_PRODUCTS_DESCRIPTION . ' where language_id = 1');
								echo '<select name="products_id">';
								echo '<option value="0">--select product--</option>';
                                while ($prod = tep_db_fetch_array($all_prods_query)) {
                                    if (!empty($_GET['tiID']) && $prod['products_id'] == $tiInfo->products_id) {
                                        $checked = true;
                                    } else {
										$checked = false;
									}
                                    echo '<option value="' . $prod['products_id'] . '"' . ($checked ? ' selected="selected"' : '') . '>' . $prod['products_name'] . '</option>';
                                }
								echo '</select>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="main"><?php echo TEXT_PRODUCTS_INFO; ?><span style="color: #ff0000;">*</span></td>
                            <td class="main"><?php
								$all_tabs_query = tep_db_query('select products_info_id, products_info_title from ' . TABLE_PRODUCTS_INFO_DESCRIPTION . ' where language_id = 1');
								echo '<select name="products_info_id">';
								echo '<option value="0">--select tab--</option>';
                                while ($tab = tep_db_fetch_array($all_tabs_query)) {
                                    if (!empty($_GET['tiID']) && $tab['products_info_id'] == $tiInfo->products_info_id) {
                                        $checked = true;
                                    } else {
										$checked = false;
									}
                                    echo '<option value="' . $tab['products_info_id'] . '"' . ($checked ? ' selected="selected"' : '') . '>' . $tab['products_info_title'] . '</option>';
                                }
								echo '</select>';
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="tabs">
                                    <?php
                                    $tabs = '<ul>';
                                    $tabs_content = '<div>';

                                    for ($i=0, $n=sizeof($languages); $i < $n; $i++) {
										$tab_info_content = $tiInfo->content[$languages[$i]['id']];
                                        $tabs .= '<li data-tab="tabs-' . $languages[$i]['id'] . '"><a>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</a></li>';

                                        $tabs_content .= '
                                    <div id="tabs-' . $languages[$i]['id'] . '">
                                        <table width="100%">
                                            <tr>
                                                <td class="main">' . TEXT_TABS_INFO_TITLE . '</td>
                                                <td class="main">' . tep_draw_input_field('content[' . $languages[$i]['id'] . '][title]', (empty($tiInfo->id) ? '' : $tab_info_content['title'])) . '</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="main" valign="top">' . '<br/>
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">';
										for ($j = 0; $j < 10; ++$j) {
											$tabs_content .= '
											<tr>
												<td class="main">' . TEXT_TABS_INFO_HEADING . '</td>
												<td class="main">' . tep_draw_input_field('content[' . $languages[$i]['id'] . '][body][' . $j . '][title]', (empty($tab_info_content['body'][$j]['title']) ? '' : $tab_info_content['body'][$j]['title'])) . '</td>
											</tr>';
											$tabs_content .= '
											<tr>
												<td colspan="2" class="main">' . TEXT_TABS_INFO_TEXT . tep_draw_textarea_field('content[' . $languages[$i]['id'] . '][body][' . $j . '][text]', 'soft', '70', '15', (empty($tab_info_content['body'][$j]['text']) ? '' : $tab_info_content['body'][$j]['text'])) . '</td>
											</tr>';
											$tabs_content .= '
											<tr>
												<td class="main">' . TEXT_TABS_INFO_IMAGE . '</td>
												<td class="main">' . (!empty($tab_info_content['body'][$j]['image']) ? tep_info_image(DIR_WS_TABS_INFO_IMAGES . $tab_info_content['body'][$j]['image'], '', SMALL_IMAGE_WIDTH) . tep_draw_hidden_field('content[' . $languages[$i]['id'] . '][body][' . $j . '][image]', $tab_info_content['body'][$j]['image']): '') . '<br/>' .  tep_draw_file_field('image[' . $languages[$i]['id'] . '][' . $j . ']') . '</td>
											</tr>';
											$tabs_content .= '
											<tr>
												<td class="main">' . TEXT_TABS_INFO_COUNT . '</td>
												<td class="main">' . tep_draw_input_field('content[' . $languages[$i]['id'] . '][body][' . $j . '][count]', (empty($tab_info_content['body'][$j]['count']) ? '' : $tab_info_content['body'][$j]['count'])) . '</td>
											</tr>';
											$tabs_content .= '
											<tr>
												<td class="main">' . TEXT_TABS_INFO_FREQUENCY . '</td>
												<td class="main">' . tep_draw_input_field('content[' . $languages[$i]['id'] . '][body][' . $j . '][frequency]', (empty($tab_info_content['body'][$j]['frequency']) ? '' : $tab_info_content['body'][$j]['frequency'])) . '</td>
											</tr>';
											$tabs_content .= '
											<tr>
												<td class="main">' . TEXT_TABS_INFO_DURATION . '</td>
												<td class="main">' . tep_draw_input_field('content[' . $languages[$i]['id'] . '][body][' . $j . '][duration]', (empty($tab_info_content['body'][$j]['duration']) ? '' : $tab_info_content['body'][$j]['duration'])) . '</td>
											</tr>';
											$tabs_content .= '
											<tr>
												<td class="main" colspan="2" style="height: 20px;"></td>
											</tr>
											<tr>
												<td class="main" colspan="2" style="height: 20px; border-top: 2px dashed #000;"></td>
											</tr>
											';
										}
										$tabs_content .= '
													</table>
                                                </td>
                                            </tr>';
                                        $tabs_content .= '
                                        </table>
                                    </div>';
                                    }
                                    $tabs .= '</ul>';
                                    $tabs_content .= '</div>';
                                    echo $tabs . $tabs_content;
                                    ?>
                                </div>
                            </td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
            <tr>
                <td class="smallText" align="right"><?php echo tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_TABS_INFO, (isset($_GET['tiID']) ? '&tiID=' . $_GET['tiID'] : ''))); ?></td>
            </tr>
        </table>

        </form>
        <script type="text/javascript">
            var tab_info_form = document.forms["new_tab_info"];
            function validateForm() {
                var message_stack = '';
				if ('0' == tab_info_form.elements['products_id'].value) message_stack += 'Select Product\n';
				if ('0' == tab_info_form.elements['products_info_id'].value) message_stack += 'Select Tab\n';
                if (message_stack != '') { alert(message_stack); return false;} else {
                    stopUnload = false;
                }
            }
            tab_info_form.onsubmit = validateForm;
        </script>
        <?php
        echo '            </td>' . "\n";
    } elseif ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
        echo '            <td width="25%" valign="top">' . "\n";

        $box = new box;
        echo $box->infoBox($heading, $contents);

        echo '            </td>' . "\n";
    }
    ?>
    </tr>
    </table></td>
    </tr>
    </table>
<?php


require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>