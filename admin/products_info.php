<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

if (tep_not_null($action)) {
    switch ($action) {
        case 'setflag':
            if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
                if (isset($_GET['piID'])) {
                    tep_set_products_info_status($_GET['piID'], $_GET['flag']);
                }
            }

            tep_redirect(tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $_GET['piID']));
            break;
        case 'insert':
        case 'save':
            if (isset($_GET['piID'])) $products_info_id = tep_db_prepare_input($_GET['piID']);

            $languages = tep_get_languages();
            $products_info_titles_array = $_POST['products_info_title'];

            $sql_data_pi = array('sort_order' => (isset($_POST['sort_order']) ? (int)$_POST['sort_order'] : 1));

            if ($action == 'insert') {
                $sql_data_pi['products_info_status'] = 1;
                tep_db_perform(TABLE_PRODUCTS_INFO, $sql_data_pi);
                $products_info_id = tep_db_insert_id();
            } elseif ($action == 'save') {
                tep_db_perform(TABLE_PRODUCTS_INFO, $sql_data_pi, 'update', "products_info_id = " . $products_info_id);
            }

        $ps_query = tep_db_query('select products_id from ' . TABLE_PRODUCTS);

            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                $language_id = $languages[$i]['id'];

                $sql_data_pid = array('products_info_title' => tep_db_prepare_input($products_info_titles_array[$language_id]),
                    'language_id' => $language_id);


                if ($action == 'insert') {

                    $sql_data_pid['products_info_id'] = $products_info_id;

                    tep_db_perform(TABLE_PRODUCTS_INFO_DESCRIPTION, $sql_data_pid);

                    tep_db_data_seek($ps_query, 0);
                    while($p = tep_db_fetch_array($ps_query)) {
                            $ins_data = array(
                                'products_info_id' => (int)$products_info_id,
                                'products_id' => (int)$p['products_id'],
                                'language_id' => (int)$languages[$i]['id'],
                                'products_info_text' => '');

                            tep_db_perform(TABLE_PRODUCTS_TO_PRODUCTS_INFO, $ins_data);
                    }
                } elseif ($action == 'save') {
                    tep_db_perform(TABLE_PRODUCTS_INFO_DESCRIPTION, $sql_data_pid, 'update', "language_id = '" . (int)$language_id . "' and products_info_id = " . $products_info_id);
                }
            }

            tep_redirect(tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $products_info_id));
            break;
        case 'deleteconfirm':
            $piID = tep_db_prepare_input($_GET['piID']);

            tep_db_query("delete from " . TABLE_PRODUCTS_INFO . " where products_info_id = '" . (int)($piID) . "'");
            tep_db_query("delete from " . TABLE_PRODUCTS_INFO_DESCRIPTION . " where products_info_id = '" . (int)($piID) . "'");
            tep_db_query("delete from " . TABLE_PRODUCTS_TO_PRODUCTS_INFO . " where products_info_id = '" . (int)($piID) . "'");


            tep_redirect(tep_href_link(FILENAME_PRODUCTS_INFO));
            break;
        case 'delete':
            /*$piID = tep_db_prepare_input($_GET['piID']);

            $status_query = tep_db_query("select count(*) as count from " . TABLE_ORDERS . " where orders_status = '" . (int)$piID . "'");
            $status = tep_db_fetch_array($status_query);

            $remove_status = true;
            if ($piID == DEFAULT_ORDERS_STATUS_ID) {
                $remove_status = false;
                $messageStack->add(ERROR_REMOVE_DEFAULT_ORDER_STATUS, 'error');
            } elseif ($status['count'] > 0) {
                $remove_status = false;
                $messageStack->add(ERROR_STATUS_USED_IN_ORDERS, 'error');
            } else {
                $history_query = tep_db_query("select count(*) as count from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_status_id = '" . (int)$piID . "'");
                $history = tep_db_fetch_array($history_query);
                if ($history['count'] > 0) {
                    $remove_status = false;
                    $messageStack->add(ERROR_STATUS_USED_IN_HISTORY, 'error');
                }
            }*/
            break;
    }
}

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr class="dataTableHeadingRow">
                                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS_INFO; ?></td>
                                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                            </tr>
                            <?php
                            $products_info_query = tep_db_query("select pi.products_info_id, pi.products_info_status, pi.sort_order, pid.products_info_title  from " . TABLE_PRODUCTS_INFO . " as pi join " . TABLE_PRODUCTS_INFO_DESCRIPTION . " as pid on(pi.products_info_id = pid.products_info_id and language_id = '" . (int)$languages_id . "') order by pi.sort_order");

                            while ($products_info = tep_db_fetch_array($products_info_query)) {
                                if ((!isset($_GET['piID']) || (isset($_GET['piID']) && ($_GET['piID'] == $products_info['products_info_id']))) && !isset($piInfo) && (substr($action, 0, 3) != 'new')) {
                                    $piInfo = new objectInfo($products_info);
                                }

                                if (isset($piInfo) && is_object($piInfo) && ($products_info['products_info_id'] == $piInfo->products_info_id)) {
                                    echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $piInfo->products_info_id . '&action=edit') . '\'">' . "\n";
                                } else {
                                    echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $products_info['products_info_id']) . '\'">' . "\n";
                                }
                                ?>
                                <td class="dataTableContent"><?php echo $products_info['products_info_title'];?></td>
                                <td class="dataTableContent" align="center">
                                    <?php
                                    if ($products_info['products_info_status'] == '1') {
                                        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_PRODUCTS_INFO, 'action=setflag&flag=0&piID=' . $products_info['products_info_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
                                    } else {
                                        echo '<a href="' . tep_href_link(FILENAME_PRODUCTS_INFO, 'action=setflag&flag=1&piID=' . $products_info['products_info_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                                    }
                                    ?>
                                </td>
                                <td class="dataTableContent" align="right"><?php if (isset($piInfo) && is_object($piInfo) && ($products_info['products_info_id'] == $piInfo->products_info_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $products_info['products_info_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        <?php
                                        if (empty($action)) {
                                            ?>
                                            <tr>
                                                <td class="smallText" colspan="2" align="right"><?php echo tep_draw_button(IMAGE_INSERT, 'plus', tep_href_link(FILENAME_PRODUCTS_INFO, 'action=new')); ?></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </table></td>
                            </tr>
                        </table></td>
                    <?php
                    $heading = array();
                    $contents = array();

                    switch ($action) {
                        case 'new':
                            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_NEW_PRODUCTS_INFO . '</strong>');

                            $contents = array('form' => tep_draw_form('status', FILENAME_PRODUCTS_INFO, 'action=insert'));
                            $contents[] = array('text' => TEXT_INFO_INSERT_INTRO);

                            $products_info_inputs_string = '';
                            $languages = tep_get_languages();
                            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                                $products_info_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('products_info_title[' . $languages[$i]['id'] . ']');
                            }

                            $contents[] = array('text' => '<br />' . TEXT_INFO_PRODUCTS_INFO_NAME . $products_info_inputs_string);
                            $contents[] = array('text' => '<br />' . TEXT_EDIT_SORT_ORDER . '<br />' . tep_draw_input_field('sort_order', 1, 'size="2"'));
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PRODUCTS_INFO)));
                            break;
                        case 'edit':
                            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT_PRODUCTS_INFO . '</strong>');

                            $contents = array('form' => tep_draw_form('product_info', FILENAME_PRODUCTS_INFO, 'piID=' . $piInfo->products_info_id  . '&action=save'));
                            $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);

                            $products_info_inputs_string = '';
                            $languages = tep_get_languages();
                            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                                $products_info_titles_query = tep_db_query('select pid.products_info_title from ' . TABLE_PRODUCTS_INFO . ' as pi join ' . TABLE_PRODUCTS_INFO_DESCRIPTION . ' as pid on(pi.products_info_id = ' . (int)$piInfo->products_info_id . ' and pi.products_info_id = pid.products_info_id and pid.language_id = ' . (int)$languages[$i]['id'] . ')');
                                $products_info_title = tep_db_num_rows($products_info_titles_query) == 1 ? tep_db_fetch_array($products_info_titles_query) : array('products_info_title' => '');
                                $products_info_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('products_info_title[' . $languages[$i]['id'] . ']', $products_info_title['products_info_title']);
                            }

                            $contents[] = array('text' => '<br />' . TEXT_INFO_PRODUCTS_INFO_NAME . $products_info_inputs_string);
                            $contents[] = array('text' => '<br />' . TEXT_EDIT_SORT_ORDER . '<br />' . tep_draw_input_field('sort_order', $piInfo->sort_order, 'size="2"'));
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $piInfo->products_info_id)));
                            break;
                        case 'delete':
                            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_PRODUCTS_INFO . '</strong>');

                            $contents = array('form' => tep_draw_form('status', FILENAME_PRODUCTS_INFO, 'piID=' . $piInfo->products_info_id  . '&action=deleteconfirm'));
                            $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
                            $contents[] = array('text' => '<br /><strong>' . $piInfo->products_info_title . '</strong>');
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $piInfo->products_info_id)));
                            break;
                        default:
                            if (isset($piInfo) && is_object($piInfo)) {
                                $heading[] = array('text' => '<strong>' . $piInfo->products_info_title . '</strong>');

                                $contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $piInfo->products_info_id . '&action=edit')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_PRODUCTS_INFO, 'piID=' . $piInfo->products_info_id . '&action=delete')));

                                $products_info_inputs_string = '';
                                $languages = tep_get_languages();
                                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                                    $products_info_titles_query = tep_db_query('select pid.products_info_title from ' . TABLE_PRODUCTS_INFO . ' as pi join ' . TABLE_PRODUCTS_INFO_DESCRIPTION . ' as pid on(pi.products_info_id = ' . (int)$piInfo->products_info_id . ' and pi.products_info_id = pid.products_info_id and pid.language_id = ' . (int)$languages[$i]['id'] . ')');
                                    $products_info_title = tep_db_num_rows($products_info_titles_query) == 1 ? tep_db_fetch_array($products_info_titles_query) : array('products_info_title' => '');
                                    $products_info_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . $products_info_title['products_info_title'];
                                }

                                $contents[] = array('text' => $products_info_inputs_string);
                            }
                            break;
                    }

                    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                        echo '            <td width="25%" valign="top">' . "\n";

                        $box = new box;
                        echo $box->infoBox($heading, $contents);

                        echo '            </td>' . "\n";
                    }
                    ?>
                </tr>
            </table></td>
    </tr>
</table>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
