<?php

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

$error = false;
$processed = false;

require(DIR_WS_INCLUDES . 'template_top.php');
?>


<table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><?php echo tep_draw_form('search', FILENAME_PARTNERS_HISTORY, '', 'get'); ?>
                        <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                        <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
                        <td class="smallText" align="right"><?php echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search'); ?></td>
                        <?php echo tep_hide_session_id(); ?></form></tr>
                </table></td>
        </tr>
        <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">
                            <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                <tr class="dataTableHeadingRow">
                                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PARTNER; ?></td>
                                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMER; ?></td>
                                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PAYMENT_DATE; ?></td>
                                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_REFERRAL_REFERER; ?></td>
                                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ORDER; ?></td>
                                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_AFFILIATES_COMMISSION; ?></td>
                                </tr>
                                <?php
                                $search = '';
                                $p_filter = '';
                                if (isset($_GET['pID'])) $p_filter = ' and ph.partners_id = ' . (int)$_GET['pID'];
                                if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
                                    $keywords = tep_db_input(tep_db_prepare_input($_GET['search']));
                                    $search = "where p.partners_lastname like '%" . $keywords . "%' or p.partners_firstname like '%" . $keywords . "%' or p.partners_email_address like '%" . $keywords . "%'";
                                    $search .= " or c.customers_lastname like '%" . $keywords . "%' or c.customers_firstname like '%" . $keywords . "%' or c.customers_email_address like '%" . $keywords . "%'";
                                }
                                $partners_history_query_raw = "select ph.customers_from_url, ph.date_added, ph.affiliates_commission, ph.orders_id, p.partners_id, p.partners_firstname, p.partners_lastname, c.customers_id, c.customers_firstname, c.customers_lastname from " . TABLE_PARTNERS_HISTORY . " as ph join " . TABLE_PARTNERS . " as p on(p.partners_id = ph.partners_id" . $p_filter . ") join " . TABLE_CUSTOMERS . " as c on(c.customers_id = ph.customers_id) " . $search . " order by ph.date_added desc";
                                $partners_history_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $partners_history_query_raw, $partners_history_query_numrows);
                                $partners_history_query = tep_db_query($partners_history_query_raw);
                                while ($partners_history = tep_db_fetch_array($partners_history_query)) {
                                ?>
                                    <tr class="dataTableRow">
                                    <td class="dataTableContent"><a href="<?php echo tep_href_link(FILENAME_PARTNERS, 'pID=' . $partners_history['partners_id']);?>"><?php echo $partners_history['partners_firstname'] . ' ' . $partners_history['partners_lastname'];?></a></td>
                                    <td class="dataTableContent"><a href="<?php echo tep_href_link(FILENAME_CUSTOMERS, 'cID=' . $partners_history['customers_id']);?>"><?php echo $partners_history['customers_firstname'] . ' ' . $partners_history['customers_lastname'];?></a></td>
                                    <td class="dataTableContent"><?php echo $partners_history['date_added']; ?></td>
                                    <td class="dataTableContent"><?php echo (string)parse_url($partners_history['customers_from_url'], PHP_URL_HOST); ?></td>
                                    <td class="dataTableContent"><a href="<?php echo tep_href_link(FILENAME_ORDERS, 'oID=' . $partners_history['orders_id']);?>">Go to order</a></td>
                                    <td class="dataTableContent"><?php echo '$' . round($partners_history['affiliates_commission'], 2); ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                                <tr>
                                    <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                            <tr>
                                                <td class="smallText" valign="top"><?php echo $partners_history_split->display_count($partners_history_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PARTNERS_HISTORY); ?></td>
                                                <td class="smallText" align="right"><?php echo $partners_history_split->display_links($partners_history_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'info', 'x', 'y', 'phID'))); ?></td>
                                            </tr>
                                            <?php
                                            if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
                                                ?>
                                                <tr>
                                                    <td class="smallText" align="right" colspan="2"><?php echo tep_draw_button(IMAGE_RESET, 'arrowrefresh-1-w', tep_href_link(FILENAME_PARTNERS_HISTORY)); ?></td>
                                                </tr>
                                            <?php
                                            }
                                            ?>
                                        </table></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
</table>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
