<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');

$error = false;
$processed = false;

if (tep_not_null($action)) {
    switch ($action) {
        /*case 'update':
            $partners_id = tep_db_prepare_input($_GET['pID']);
            $partners_firstname = tep_db_prepare_input($_POST['partners_firstname']);
            $partners_lastname = tep_db_prepare_input($_POST['partners_lastname']);
            $partners_email_address = tep_db_prepare_input($_POST['partners_email_address']);            

            if (strlen($partners_firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
                $error = true;
                $entry_firstname_error = true;
            } else {
                $entry_firstname_error = false;
            }

            if (ACCOUNT_LAST_NAME == 'true') {
                if (strlen($partners_lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
                    $error = true;
                    $entry_lastname_error = true;
                } else {
                    $entry_lastname_error = false;
                }
            }            

            if (strlen($partners_email_address) < ENTRY_EMAIL_ADDRESS_MIN_LENGTH) {
                $error = true;
                $entry_email_address_error = true;
            } else {
                $entry_email_address_error = false;
            }

            if (!tep_validate_email($partners_email_address)) {
                $error = true;
                $entry_email_address_check_error = true;
            } else {
                $entry_email_address_check_error = false;
            }            

            $check_email = tep_db_query("select partners_email_address from " . TABLE_PARTNERS . " where partners_email_address = '" . tep_db_input($partners_email_address) . "' and partners_id != '" . (int)$partners_id . "'");
            if (tep_db_num_rows($check_email)) {
                $error = true;
                $entry_email_address_exists = true;
            } else {
                $entry_email_address_exists = false;
            }

            if ($error == false) {

                $sql_data_array = array('partners_firstname' => $partners_firstname,
                    'partners_email_address' => $partners_email_address);

                if (ACCOUNT_LAST_NAME == 'true') $sql_data_array['partners_lastname'] = $partners_lastname;               

                tep_db_perform(TABLE_PARTNERS, $sql_data_array, 'update', "partners_id = '" . (int)$partners_id . "'");

                tep_db_query("update " . TABLE_PARTNERS_INFO . " set partners_info_date_account_last_modified = now() where partners_info_id = '" . (int)$partners_id . "'");
                

                tep_redirect(tep_href_link(FILENAME_PARTNERS, tep_get_all_get_params(array('pID', 'action')) . 'pID=' . $partners_id));

            } else if ($error == true) {
                $pInfo = new objectInfo($_POST);
                $processed = true;
            }

            break;
        case 'deleteconfirm':
            $partners_id = tep_db_prepare_input($_GET['pID']);            

            tep_db_query("delete from " . TABLE_PARTNERS . " where partners_id = '" . (int)$partners_id . "'");
            tep_db_query("delete from " . TABLE_PARTNERS_INFO . " where partners_info_id = '" . (int)$partners_id . "'");

            tep_redirect(tep_href_link(FILENAME_PARTNERS, tep_get_all_get_params(array('pID', 'action'))));
            break;*/
        default:
            $partners_query = tep_db_query("select partners_id, partners_firstname, partners_lastname, partners_email_address from " . TABLE_PARTNERS . " where partners_id = '" . (int)$_GET['pID'] . "'");
            $partners = tep_db_fetch_array($partners_query);
            $pInfo = new objectInfo($partners);
    }
}

require(DIR_WS_INCLUDES . 'template_top.php');

/*if ($action == 'edit' || $action == 'update') {
    ?>
    <script type="text/javascript"><!--

        function check_form() {
            var error = 0;
            var error_message = "<?php echo JS_ERROR; ?>";

            var partners_firstname = document.partners.partners_firstname.value;
            <?php if (ACCOUNT_LAST_NAME == 'true') echo 'var partners_lastname = document.partners.partners_lastname.value;' . "\n"; ?>
            var partners_email_address = document.partners.partners_email_address.value;

            if (partners_firstname.length < <?php echo ENTRY_FIRST_NAME_MIN_LENGTH; ?>) {
                error_message = error_message + "<?php echo JS_FIRST_NAME; ?>";
                error = 1;
            }

            <?php if (ACCOUNT_LAST_NAME == 'true') { ?>
            if (partners_lastname.length < <?php echo ENTRY_LAST_NAME_MIN_LENGTH; ?>) {
                error_message = error_message + "<?php echo JS_LAST_NAME; ?>";
                error = 1;
            }
            <?php } ?>

            if (partners_email_address.length < <?php echo ENTRY_EMAIL_ADDRESS_MIN_LENGTH; ?>) {
                error_message = error_message + "<?php echo JS_EMAIL_ADDRESS; ?>";
                error = 1;
            }

            if (error == 1) {
                alert(error_message);
                return false;
            } else {
                return true;
            }
        }
        //--></script>
<?php
}*/
?>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
/*if ($action == 'edit' || $action == 'update') {
    ?>
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
    </tr>
    <tr><?php echo tep_draw_form('partners', FILENAME_PARTNERS, tep_get_all_get_params(array('action')) . 'action=update', 'post', 'onsubmit="return check_form();"'); ?>
        <td class="formAreaTitle"><?php echo CATEGORY_PERSONAL; ?></td>
    </tr>
    <tr>
        <td class="formArea">
            <table border="0" cellspacing="2" cellpadding="2">
                <tr>
                    <td class="main"><?php echo ENTRY_FIRST_NAME; ?></td>
                    <td class="main">
                        <?php
                        if ($error == true) {
                            if ($entry_firstname_error == true) {
                                echo tep_draw_input_field('partners_firstname', $pInfo->partners_firstname, 'maxlength="32"') . '&nbsp;' . ENTRY_FIRST_NAME_ERROR;
                            } else {
                                echo $pInfo->partners_firstname . tep_draw_hidden_field('partners_firstname');
                            }
                        } else {
                            echo tep_draw_input_field('partners_firstname', $pInfo->partners_firstname, 'maxlength="32"', true);
                        }
                        ?></td>
                </tr>
                <?php
                if (ACCOUNT_LAST_NAME == 'true') {
                    ?>
                    <tr>
                        <td class="main"><?php echo ENTRY_LAST_NAME; ?></td>
                        <td class="main">
                            <?php
                            if ($error == true) {
                                if ($entry_lastname_error == true) {
                                    echo tep_draw_input_field('partners_lastname', $pInfo->partners_lastname, 'maxlength="32"') . '&nbsp;' . ENTRY_LAST_NAME_ERROR;
                                } else {
                                    echo $pInfo->partners_lastname . tep_draw_hidden_field('partners_lastname');
                                }
                            } else {
                                echo tep_draw_input_field('partners_lastname', $pInfo->partners_lastname, 'maxlength="32"', true);
                            }
                            ?></td>
                    </tr>
                <?php
                }
                ?>
                <tr>
                    <td class="main"><?php echo ENTRY_EMAIL_ADDRESS; ?></td>
                    <td class="main">
                        <?php
                        if ($error == true) {
                            if ($entry_email_address_error == true) {
                                echo tep_draw_input_field('partners_email_address', $pInfo->partners_email_address, 'maxlength="96"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR;
                            } elseif ($entry_email_address_check_error == true) {
                                echo tep_draw_input_field('partners_email_address', $pInfo->partners_email_address, 'maxlength="96"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_CHECK_ERROR;
                            } elseif ($entry_email_address_exists == true) {
                                echo tep_draw_input_field('partners_email_address', $pInfo->partners_email_address, 'maxlength="96"') . '&nbsp;' . ENTRY_EMAIL_ADDRESS_ERROR_EXISTS;
                            } else {
                                echo $partners_email_address . tep_draw_hidden_field('partners_email_address');
                            }
                        } else {
                            echo tep_draw_input_field('partners_email_address', $pInfo->partners_email_address, 'maxlength="96"', true);
                        }
                        ?></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td align="right" class="smallText"><?php echo tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PARTNERS, tep_get_all_get_params(array('action')))); ?></td>
    </tr></form>
<?php
} else {*/
    ?>
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr><?php echo tep_draw_form('search', FILENAME_PARTNERS, '', 'get'); ?>
                    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
                    <td class="smallText" align="right"><?php echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search'); ?></td>
                    <?php echo tep_hide_session_id(); ?></form></tr>
            </table></td>
    </tr>
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr class="dataTableHeadingRow">
                                <?php if (ACCOUNT_LAST_NAME == 'true') echo '<td class="dataTableHeadingContent">' . TABLE_HEADING_LASTNAME . '</td>';?>
                                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FIRSTNAME; ?></td>
                                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACCOUNT_CREATED; ?></td>
                                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_PARTNER_KEY; ?></td>
                                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_PARTNER_HISTORY; ?></td>
                                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                            </tr>
                            <?php
                            $search = '';
                            if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
                                $keywords = tep_db_input(tep_db_prepare_input($_GET['search']));
                                $search = "where partners_lastname like '%" . $keywords . "%' or partners_firstname like '%" . $keywords . "%' or partners_email_address like '%" . $keywords . "%'";
                            }
                            $partners_query_raw = "select p.partners_id, p.partners_key, p.partners_lastname, p.partners_firstname, p.partners_email_address from " . TABLE_PARTNERS . " as p join " . TABLE_PARTNERS_INFO . " as pi on(p.partners_id = pi.partners_info_id) " . $search . " order by pi.partners_info_date_account_created desc, p.partners_lastname, p.partners_firstname";
                            $partners_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $partners_query_raw, $partners_query_numrows);
                            $partners_query = tep_db_query($partners_query_raw);
                            while ($partners = tep_db_fetch_array($partners_query)) {
                                $info_query = tep_db_query("select partners_info_date_account_created as date_account_created, partners_info_date_account_last_modified as date_account_last_modified, partners_info_date_of_last_logon as date_last_logon, partners_info_number_of_logons as number_of_logons from " . TABLE_PARTNERS_INFO . " where partners_info_id = '" . $partners['partners_id'] . "'");
                                $info = tep_db_fetch_array($info_query);

                                if ((!isset($_GET['pID']) || (isset($_GET['pID']) && ($_GET['pID'] == $partners['partners_id']))) && !isset($pInfo)) {
                                    $pInfo_array = array_merge($partners, $info);
                                    $pInfo = new objectInfo($pInfo_array);
                                }

                                if (isset($pInfo) && is_object($pInfo) && ($partners['partners_id'] == $pInfo->partners_id)) {
                                    echo '          <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PARTNERS, tep_get_all_get_params(array('pID', 'action')) . 'pID=' . $pInfo->partners_id/* . '&action=edit'*/) . '\'">' . "\n";
                                } else {
                                    echo '          <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PARTNERS, tep_get_all_get_params(array('pID')) . 'pID=' . $partners['partners_id']) . '\'">' . "\n";
                                }
                                ?>
                                <?php if (ACCOUNT_LAST_NAME == 'true') echo '<td class="dataTableContent">' . $partners['partners_lastname'] . '</td>'; ?>
                                <td class="dataTableContent"><?php echo $partners['partners_firstname']; ?></td>
                                <td class="dataTableContent" align="right"><?php echo tep_date_short($info['date_account_created']); ?></td>
                                <td class="dataTableContent" align="right"><?php echo $partners['partners_key']; ?></td>
                                <td class="dataTableContent" align="right"><a href="<?php echo tep_href_link(FILENAME_PARTNERS_HISTORY, 'pID=' . $partners['partners_id']); ?>">View Partners History</a></td>
                                <td class="dataTableContent" align="right"><?php if (isset($pInfo) && is_object($pInfo) && ($partners['partners_id'] == $pInfo->partners_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_PARTNERS, tep_get_all_get_params(array('pID')) . 'pID=' . $partners['partners_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        <tr>
                                            <td class="smallText" valign="top"><?php echo $partners_split->display_count($partners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_PARTNERS); ?></td>
                                            <td class="smallText" align="right"><?php echo $partners_split->display_links($partners_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'info', 'x', 'y', 'pID'))); ?></td>
                                        </tr>
                                        <?php
                                        if (isset($_GET['search']) && tep_not_null($_GET['search'])) {
                                            ?>
                                            <tr>
                                                <td class="smallText" align="right" colspan="2"><?php echo tep_draw_button(IMAGE_RESET, 'arrowrefresh-1-w', tep_href_link(FILENAME_PARTNERS)); ?></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </table></td>
                            </tr>
                        </table></td>
                    <?php
                    $heading = array();
                    $contents = array();

                    switch ($action) {
                        /*case 'confirm':
                            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_PARTNER . '</strong>');

                            $contents = array('form' => tep_draw_form('partners', FILENAME_PARTNERS, tep_get_all_get_params(array('pID', 'action')) . 'pID=' . $pInfo->partners_id . '&action=deleteconfirm'));
                            $contents[] = array('text' => TEXT_DELETE_INTRO . '<br /><br /><strong>' . $pInfo->partners_firstname . ' ' . $pInfo->partners_lastname . '</strong>');
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PARTNERS, tep_get_all_get_params(array('pID', 'action')) . 'pID=' . $pInfo->partners_id)));
                            break;*/
                        default:
                            if (isset($pInfo) && is_object($pInfo)) {
                                $heading[] = array('text' => '<strong>' . $pInfo->partners_firstname . ' ' . $pInfo->partners_lastname . '</strong>');

                                $contents[] = array('align' => 'center', 'text' => /*tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_PARTNERS, tep_get_all_get_params(array('pID', 'action')) . 'pID=' . $pInfo->partners_id . '&action=edit')) . */tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_PARTNERS, tep_get_all_get_params(array('pID', 'action')) . 'pID=' . $pInfo->partners_id . '&action=confirm')));
                                $contents[] = array('text' => '<br />' . TEXT_DATE_ACCOUNT_CREATED . ' ' . tep_date_short($pInfo->date_account_created));
                                $contents[] = array('text' => '<br />' . TEXT_DATE_ACCOUNT_LAST_MODIFIED . ' ' . tep_date_short($pInfo->date_account_last_modified));
                                $contents[] = array('text' => '<br />' . TEXT_INFO_DATE_LAST_LOGON . ' '  . tep_date_short($pInfo->date_last_logon));
                                $contents[] = array('text' => '<br />' . TEXT_INFO_NUMBER_OF_LOGONS . ' ' . $pInfo->number_of_logons);
                            }
                            break;
                    }

                    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                        echo '            <td width="25%" valign="top">' . "\n";

                        $box = new box;
                        echo $box->infoBox($heading, $contents);

                        echo '            </td>' . "\n";
                    }
                    ?>
                </tr>
            </table></td>
    </tr>
<?php
//}
?>
</table>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
