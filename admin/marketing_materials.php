<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$type = (isset($_GET['type']) ? $_GET['type'] : '1');
$action = (isset($_GET['action']) ? $_GET['action'] : '');
$material_id = (isset($_GET['mpID']) ? (int)$_GET['mpID'] : 0);
$products_id = (isset($_GET['pID']) ? (int)$_GET['pID'] : -1);
$languages_id = (isset($_GET['lID']) ? (int)$_GET['lID'] : 0);
$tags_id = (isset($_GET['tID']) ? (int)$_GET['tID'] : 0);
$page = (isset($_GET['page']) ? (int)$_GET['page'] : 1);

$url = 'type=' . $type . '&page=' . $page . ($languages_id ? '&lID=' . $languages_id : '') . ($products_id != -1 ? '&pID=' . $products_id : '') . ($tags_id ? '&tID=' . $tags_id : '');
if(empty($_GET['page']) && $material_id) $url = 'type=' . $type;
$TABLE_MATERIAL = '';
$TABLE_MATERIAL_TAGS = '';

switch($type) {
    case 2: //banners
        $TABLE_MATERIAL = TABLE_MARKETING_BANNERS;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_BANNERS_TAGS;
        break;
    case 3: //pics
        $TABLE_MATERIAL = TABLE_MARKETING_PICS;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_PICS_TAGS;
        break;
    case 4: //mails
        $TABLE_MATERIAL = TABLE_MARKETING_MAILS;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_MAILS_TAGS;
        break;
    case 5: //statuses
        $TABLE_MATERIAL = TABLE_MARKETING_STATUSES;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_STATUSES_TAGS;
        break;
    case 6: //shares
        $TABLE_MATERIAL = TABLE_MARKETING_SHARES;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_SHARES_TAGS;
        break;
    default: // posts
        $TABLE_MATERIAL = TABLE_MARKETING_POSTS;
        $TABLE_MATERIAL_TAGS = TABLE_MARKETING_POSTS_TAGS;
        break;
}

if (tep_not_null($action)) {
    switch ($action) {
        case 'setflag':
            if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
                if (isset($_GET['mpID'])) {
                    $status = $_GET['flag'];
                    if ($status == 1) {
                        tep_db_query("update " . $TABLE_MATERIAL . " set material_status = b'1' where material_id = '" . (int)$_GET['mpID'] . "'");
                    } elseif ($status == 0) {
                        tep_db_query("update " . $TABLE_MATERIAL . " set material_status = b'0' where material_id = '" . (int)$_GET['mpID'] . "'");
                    }
                }
            }

            tep_redirect(tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $_GET['mpID'] . '&' . $url));
            break;
        case 'insert':
        case 'save':

            $error_message = '';
            switch($type) {
                case 2: //banners
                    $sql_data_pi = array(
                        'products_id' => (int)$_POST['products_id'],
                        'languages_id' => (int)$_POST['languages_id'],
                        'material_title' => $_POST['material_title'],
                        'material_width' => (int)$_POST['material_width'],
                        'material_height' => (int)$_POST['material_height'],
                        'material_sort' => (int)$_POST['material_sort']
                    );
                    if (!isset($_POST['products_id']) || $_POST['products_id'] == -1 || empty($_POST['languages_id']) || empty($_POST['material_title']) || empty($_POST['material_width']) || empty($_POST['material_height']) || ( $action == 'insert' && (empty($_FILES['material_file']['tmp_name']) || !is_uploaded_file($_FILES['material_file']['tmp_name'])))) {
                        $error_message = 'Fill Required Fields';
                    }
                    break;
                case 3: //pics
                    $sql_data_pi = array(
                        'products_id' => (int)$_POST['products_id'],
                        'material_title' => $_POST['material_title'],
                        'material_sort' => (int)$_POST['material_sort']
                    );
                    if (!isset($_POST['products_id']) || $_POST['products_id'] == -1 || empty($_POST['material_title']) || ( $action == 'insert' && (empty($_FILES['material_pic']['tmp_name']) || !is_uploaded_file($_FILES['material_pic']['tmp_name'])))) {
                        $error_message = 'Fill Required Fields';
                    }
                    break;
                case 4: //mails
                    $sql_data_pi = array(
                        'products_id' => (int)$_POST['products_id'],
                        'languages_id' => (int)$_POST['languages_id'],
                        'material_title' => $_POST['material_title'],
                        'material_html' => $_POST['material_html'],
                        'material_sort' => (int)$_POST['material_sort']
                    );
                    if (!isset($_POST['products_id']) || $_POST['products_id'] == -1 || empty($_POST['languages_id']) || empty($_POST['material_title']) || empty($_POST['material_html']) || ( $action == 'insert' && (empty($_FILES['material_pic']['tmp_name']) || !is_uploaded_file($_FILES['material_pic']['tmp_name'])))) {
                        $error_message = 'Fill Required Fields';
                    }
                    break;
                case 5: //statuses
                    $sql_data_pi = array(
                        'products_id' => (int)$_POST['products_id'],
                        'languages_id' => (int)$_POST['languages_id'],
                        'material_title' => $_POST['material_title'],
                        'material_sort' => (int)$_POST['material_sort']
                    );
                    if (!isset($_POST['products_id']) || $_POST['products_id'] == -1 || empty($_POST['languages_id']) || empty($_POST['material_title'])) {
                        $error_message = 'Fill Required Fields';
                    }
                    break;
                case 6: //shares
                    $sql_data_pi = array(
                        'products_id' => (int)$_POST['products_id'],
                        'languages_id' => (int)$_POST['languages_id'],
                        'material_title' => $_POST['material_title'],
                        'material_desc' => $_POST['material_desc'],
                        'material_sort' => (int)$_POST['material_sort']
                    );
                    if (!isset($_POST['products_id']) || $_POST['products_id'] == -1 || empty($_POST['languages_id']) || empty($_POST['material_title']) || empty($_POST['material_desc']) || ( $action == 'insert' && (empty($_FILES['material_pic']['tmp_name']) || !is_uploaded_file($_FILES['material_pic']['tmp_name'])))) {
                        $error_message = 'Fill Required Fields';
                    }
                    break;
                default: // posts
                    $sql_data_pi = array(
                        'products_id' => (int)$_POST['products_id'],
                        'languages_id' => (int)$_POST['languages_id'],
                        'material_title' => $_POST['material_title'],
                        'material_text' => $_POST['material_text'],
                        'material_sort' => (int)$_POST['material_sort']
                    );
                    if (!isset($_POST['products_id']) || $_POST['products_id'] == -1 || empty($_POST['languages_id']) || empty($_POST['material_title']) || empty($_POST['material_text']) || empty($_POST['tags']) || !is_array($_POST['tags'])) {
                        $error_message = 'Fill Required Fields';
                    }
                    break;
            }

            if ($error_message) {
                $messageStack->add_session($error_message, 'error');
                tep_redirect(tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $material_id . '&' . $url . '&action=' . ($action == 'insert'? 'new' : 'edit')));
            }

            if ($action == 'insert') {
                $sql_data_pi['material_status'] = 1;
                tep_db_perform($TABLE_MATERIAL, $sql_data_pi);
                $material_id = tep_db_insert_id();
            } elseif ($action == 'save') {
                tep_db_perform($TABLE_MATERIAL, $sql_data_pi, 'update', "material_id = " . (int)$material_id);
            }

            if (!empty($_FILES['material_file']['tmp_name']) || !empty($_FILES['material_pic']['tmp_name'])) {
                switch ($type) {
                    case 2: //banners
                        $file = 'banner-' . $material_id . '.' . pathinfo($_FILES['material_file']['name'], PATHINFO_EXTENSION);
                        if ($action == 'save') {
                            @unlink(DIR_FS_MARKETING_BANNERS . $file);
                        }
                        move_uploaded_file($_FILES['material_file']['tmp_name'], DIR_FS_MARKETING_BANNERS . $file);
                        tep_db_query('update ' . $TABLE_MATERIAL . ' set material_file = "' . tep_db_input($file) . '" where material_id = ' . (int)$material_id);
                        break;
                    case 3: //pics
                        $file = 'pic-' . $material_id . '.' . pathinfo($_FILES['material_pic']['name'], PATHINFO_EXTENSION);
                        if ($action == 'save') {
                            @unlink(DIR_FS_MARKETING_PICS . $file);
                        }
                        move_uploaded_file($_FILES['material_pic']['tmp_name'], DIR_FS_MARKETING_PICS . $file);
                        tep_db_query('update ' . $TABLE_MATERIAL . ' set material_pic = "' . tep_db_input($file) . '" where material_id = ' . (int)$material_id);
                        break;
                    case 4: //mails
                        $file = 'mail-' . $material_id . '.' . pathinfo($_FILES['material_pic']['name'], PATHINFO_EXTENSION);
                        if ($action == 'save') {
                            @unlink(DIR_FS_MARKETING_MAILS . $file);
                        }
                        move_uploaded_file($_FILES['material_pic']['tmp_name'], DIR_FS_MARKETING_MAILS . $file);
                        tep_db_query('update ' . $TABLE_MATERIAL . ' set material_pic = "' . tep_db_input($file) . '" where material_id = ' . (int)$material_id);
                        break;
                    case 5: //statuses
                        break;
                    case 6: //shares
                        $file = 'share-' . $material_id . '.' . pathinfo($_FILES['material_pic']['name'], PATHINFO_EXTENSION);
                        if ($action == 'save') {
                            @unlink(DIR_FS_MARKETING_SHARES . $file);
                        }
                        move_uploaded_file($_FILES['material_pic']['tmp_name'], DIR_FS_MARKETING_SHARES . $file);
                        tep_db_query('update ' . $TABLE_MATERIAL . ' set material_pic = "' . tep_db_input($file) . '" where material_id = ' . (int)$material_id);
                        break;
                    default: // posts
                        break;
                }
            }

            if ($action == 'save') {
                tep_db_query('delete from ' . $TABLE_MATERIAL_TAGS . ' where material_id = ' . (int)$material_id);
            }

            if (!empty($_POST['tags']) && is_array($_POST['tags'])) {
                $insert_values = '';
                for($i = 0, $n = sizeof($_POST['tags']); $i < $n; ++$i) {
                    $insert_values .= '(' . (int)$material_id . ', ' . (int)$_POST['tags'][$i] . '), ';
                }
                $insert_values = substr($insert_values, 0, -2);
                tep_db_query('insert into ' . $TABLE_MATERIAL_TAGS . ' (material_id, tag_id) values' . $insert_values);
            }
            
            tep_redirect(tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $material_id . '&' . $url));
            break;
        case 'deleteconfirm':
            tep_db_query("delete from " . $TABLE_MATERIAL . " where material_id = '" . (int)$material_id . "'");

            tep_redirect(tep_href_link(FILENAME_MARKETING_MATERIALS, $url));
            break;
    }
}

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
<tr>
    <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td class="pageHeading"><?php echo $methods_name[$type]; ?></td>
                <td class="smallText" align="right">
                    <?php
                    $products_array = array(array('id' => '-1', 'text' => TEXT_SELECT));
                    $products_array[] = array('id' => '0', 'text' => TEXT_GENERAL);
                    $products_query = tep_db_query("select products_id, products_name from " . TABLE_PRODUCTS . " join " . TABLE_PRODUCTS_DESCRIPTION . " using(products_id) where language_id = 1 order by products_ordered");
                    while($products = tep_db_fetch_array($products_query)){
                        $products_array[] = array('id' => $products['products_id'], 'text' => $products['products_name']);
                    }

                    $tags_array = array(array('id' => '0', 'text' => TEXT_SELECT));
                    $tags_query = tep_db_query("select tag_id, tag_name from " . TABLE_MARKETING_TAGS . " join " . TABLE_MARKETING_TAGS_INFO . " using(tag_id) where language_id = 1 order by sort_order");
                    while($tags = tep_db_fetch_array($tags_query)){
                        $tags_array[] = array('id' => $tags['tag_id'], 'text' => $tags['tag_name']);
                    }

                    $languages_array = array(array('id' => '0', 'text' => TEXT_SELECT));
                    $languages_query = tep_db_query("select languages_id, name from " . TABLE_LANGUAGES . " order by sort_order");
                    while($languages = tep_db_fetch_array($languages_query)){
                        $languages_array[] = array('id' => $languages['languages_id'], 'text' => $languages['name']);
                    }


                    echo tep_draw_form('news', FILENAME_MARKETING_MATERIALS, '', 'get');
                    echo TEXT_PRODUCTS_LIST . ' ' . tep_draw_pull_down_menu('pID', $products_array, $products_id, 'style="width:150px" onChange="this.form.submit();"');
                    echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                    echo TEXT_TAGS_LIST . ' ' . tep_draw_pull_down_menu('tID', $tags_array, $tags_id, 'style="width:150px" onChange="this.form.submit();"');
                    echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                    echo TEXT_LANGUAGES . ' ' . tep_draw_pull_down_menu('lID', $languages_array, $languages_id, 'style="width:150px" onChange="this.form.submit();"');
                    echo tep_draw_hidden_field('type', $type);
                    //echo tep_draw_hidden_field('jAdminID', (isset($_GET['jAdminID']) ? $_GET['jAdminID'] : ''));
                    echo '</form>';

                    ?></td>
            </tr>
        </table></td>
</tr>
<tr>
<td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
<tr>
<td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr class="dataTableHeadingRow">
            <td class="dataTableHeadingContent"><?php echo $methods_name[$type]; ?></td>
            <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS_2; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION_2; ?>&nbsp;</td>
        </tr>
        <?php
        $where_str = '';
        if($products_id != -1) $where_str .= ' and products_id = ' . (int)$products_id;
        if($languages_id) $where_str .= ' and languages_id = ' . (int)$languages_id;
        if($tags_id) $where_str .= ' and tag_id = ' . (int)$tags_id;
        $where_str = $where_str ? ' where ' . substr($where_str, 4) : '';
        if (empty($_GET['page']) && $material_id) $where_str = ' where material_id = ' . (int)$material_id;
        $material_query_raw = "select * from " . $TABLE_MATERIAL . ($tags_id ? ' join ' . $TABLE_MATERIAL_TAGS . ' using(material_id)' : '') . $where_str . " order by material_sort";
        $material_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $material_query_raw, $material_numrows);
        $material_query = tep_db_query($material_query_raw);

        while ($material = tep_db_fetch_array($material_query)) {
            if ((!isset($_GET['mpID']) || (isset($_GET['mpID']) && ($_GET['mpID'] == $material['material_id']))) && !isset($piInfo) && (substr($action, 0, 3) != 'new')) {
                $piInfo = new objectInfo($material);
            }

            if (isset($piInfo) && is_object($piInfo) && ($material['material_id'] == $piInfo->material_id)) {
                echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $piInfo->material_id . '&' . $url . '&action=edit') . '\'">' . "\n";
            } else {
                echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $material['material_id'] . '&' . $url) . '\'">' . "\n";
            }
            ?>
            <td class="dataTableContent"><?php echo $material['material_title'];?></td>
            <td class="dataTableContent" align="center">
                <?php
                if ($material['material_status'] == '1') {
                    echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_MARKETING_MATERIALS, 'action=setflag&flag=0&mpID=' . $material['material_id'] . '&' . $url) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
                } else {
                    echo '<a href="' . tep_href_link(FILENAME_MARKETING_MATERIALS, 'action=setflag&flag=1&mpID=' . $material['material_id']) . '&' . $url . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                }
                ?>
            </td>
            <td class="dataTableContent" align="right"><?php if (isset($piInfo) && is_object($piInfo) && ($material['material_id'] == $piInfo->material_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $material['material_id']) . '&' . $url . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
            </tr>
        <?php
        }
        ?>
        <tr>
            <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                    <?php
                    if (empty($action)) {
                        ?>
                        <tr>
                            <td class="smallText" colspan="2" align="right"><?php echo tep_draw_button(IMAGE_INSERT, 'plus', tep_href_link(FILENAME_MARKETING_MATERIALS, 'action=new' . '&' . $url)); ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr>
                        <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                <tr>
                                    <td class="smallText" valign="top"><?php echo $material_split->display_count($material_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></td>
                                    <td class="smallText" align="right"><?php echo $material_split->display_links($material_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                                </tr>
                            </table></td>
                    </tr>
                </table></td>
        </tr>
    </table></td>
<?php
$heading = array();
$contents = array();

switch ($action) {
    case 'new':
        $heading[] = array('text' => '<strong>' . $methods_name[$type] . '</strong>');

        $contents = array('form' => tep_draw_form($TABLE_MATERIAL, FILENAME_MARKETING_MATERIALS, 'action=insert&' . $url, 'post', 'enctype="multipart/form-data"'));
        switch($type) {
            case 2: //banners
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', '', 'style="width:300px"'));
                $contents[] = array('text' => '<br />' . TEXT_FILE . '*<br />' . tep_draw_file_field('material_file'));
                $contents[] = array('text' => '<br />' . TEXT_WIDTH . '*<br />' . tep_draw_input_field('material_width', '', 'maxlength="4" style="width:70px"'));
                $contents[] = array('text' => '<br />' . TEXT_HEIGHT . '*<br />' . tep_draw_input_field('material_height', '', 'maxlength="4" style="width:70px"'));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', 0, 'style="width:50px"'));
                break;
            case 3: //pics
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', '', 'style="width:300px"'));
                $contents[] = array('text' => '<br />' . TEXT_FILE . '*<br />' . tep_draw_file_field('material_pic'));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', 0, 'style="width:50px"'));
                break;
            case 4: //mails
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', '', 'style="width:300px"'));
                $contents[] = array('text' => '<br />' . TEXT_FILE . '*<br />' . tep_draw_file_field('material_pic'));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TEXT . '*<br />' . tep_draw_textarea_field('material_html', 'soft', '70', '15', '', ''));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', 0, 'style="width:50px"'));
                break;
            case 5: //statuses
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_textarea_field('material_title', 'soft', '70', '15', '', ''));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', 0, 'style="width:50px"'));
                break;
            case 6: //shares
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', '', 'style="width:300px"'));
                $contents[] = array('text' => '<br />' . TEXT_FILE . '*<br />' . tep_draw_file_field('material_pic'));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TEXT . '*<br />' . tep_draw_textarea_field('material_desc', 'soft', '70', '15', '', ''));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', 0, 'style="width:50px"'));
                break;
            default: // posts
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', '', 'style="width:300px"'));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TEXT . '*<br />' . tep_draw_textarea_field('material_text', 'soft', '70', '15', '', 'class="tinymce"'));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', 0, 'style="width:50px"'));
                break;
        }

        $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_MARKETING_MATERIALS, $url)));
        break;
    case 'edit':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_EDIT . '</strong>');

        $contents = array('form' => tep_draw_form($TABLE_MATERIAL, FILENAME_MARKETING_MATERIALS, 'mpID=' . $piInfo->material_id . '&' . $url  . '&action=save', 'post', 'enctype="multipart/form-data"'));

        switch($type) {
            case 2: //banners
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $piInfo->products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $piInfo->languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type, $piInfo->material_id));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', $piInfo->material_title, 'style="width:300px"'));

				$banner_width = min($piInfo->material_width, 300);
				$banner_height = intval($piInfo->material_height / $piInfo->material_width * $banner_width);
	            switch(pathinfo($piInfo->material_file, PATHINFO_EXTENSION)) {
		            case 'jpg':
		            case 'png':
		            case 'gif':
		                $contents[] = array('text' =>  '<img src="' . HTTP_SERVER . '/' . DIR_WS_MARKETING_BANNERS . $piInfo->material_file . '" alt="' . $piInfo->material_title . '" title="' . $piInfo->material_title . '" width="' . $banner_width . '" height="' . $banner_height . '"/>');
			            break;
		            case 'swf':
			            $contents[] = array('text' => '<div id="banner"></div>');
			            $contents[] = array('text' => '<script type="text/javascript">window.swfobject.embedSWF("' . HTTP_SERVER . '/' . DIR_WS_MARKETING_BANNERS . $piInfo->material_file . '", "banner", "' . $banner_width . '", "' . $banner_height . '", "9.0.0");</script>');
			            break;
	            }


                $contents[] = array('text' => '<br />' . TEXT_FILE . '*<br />' . tep_draw_file_field('material_file'));
                $contents[] = array('text' => '<br />' . TEXT_WIDTH . '*<br />' . tep_draw_input_field('material_width', $piInfo->material_width, 'maxlength="4" style="width:70px"'));
                $contents[] = array('text' => '<br />' . TEXT_HEIGHT . '*<br />' . tep_draw_input_field('material_height', $piInfo->material_height, 'maxlength="4" style="width:70px"'));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', $piInfo->material_sort, 'style="width:50px"'));
                break;
            case 3: //pics
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $piInfo->products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type, $piInfo->material_id));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', $piInfo->material_title, 'style="width:300px"'));
                $contents[] = array('text' => '<br />' . tep_image(DIR_WS_MARKETING_PICS . $piInfo->material_pic, $piInfo->material_title, MIDDLE_IMAGE_WIDTH));
                $contents[] = array('text' => '<br />' . TEXT_FILE . '*<br />' . tep_draw_file_field('material_pic'));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', $piInfo->material_sort, 'style="width:50px"'));
                break;
            case 4: //mails
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $piInfo->products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $piInfo->languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type, $piInfo->material_id));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', $piInfo->material_title, 'style="width:300px"'));
                $contents[] = array('text' => '<br />' . tep_image(DIR_WS_MARKETING_MAILS . $piInfo->material_pic, $piInfo->material_title, MIDDLE_IMAGE_WIDTH));
                $contents[] = array('text' => '<br />' . TEXT_FILE . '*<br />' . tep_draw_file_field('material_pic'));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TEXT . '*<br />' . tep_draw_textarea_field('material_html', 'soft', '70', '15', $piInfo->material_html, ''));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', $piInfo->material_sort, 'style="width:50px"'));
                break;
            case 5: //statuses
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $piInfo->products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $piInfo->languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type, $piInfo->material_id));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_textarea_field('material_title', 'soft', '70', '15', $piInfo->material_title, ''));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', $piInfo->material_sort, 'style="width:50px"'));
                break;
            case 6: //shares
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $piInfo->products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $piInfo->languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type, $piInfo->material_id));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', $piInfo->material_title, 'style="width:300px"'));
                $contents[] = array('text' => '<br />' . tep_image(DIR_WS_MARKETING_SHARES . $piInfo->material_pic, $piInfo->material_title, MIDDLE_IMAGE_WIDTH));
                $contents[] = array('text' => '<br />' . TEXT_FILE . '*<br />' . tep_draw_file_field('material_pic'));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TEXT . '*<br />' . tep_draw_textarea_field('material_desc', 'soft', '70', '15', $piInfo->material_desc, ''));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', $piInfo->material_sort, 'style="width:50px"'));
                break;
            default: // posts
                $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_LIST . '*<br />' . tep_draw_pull_down_menu('products_id', $products_array, $piInfo->products_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_LANGUAGES . '*<br />' . tep_draw_pull_down_menu('languages_id', $languages_array, $piInfo->languages_id, 'style="width:150px"'));
                $contents[] = array('text' => '<br />' . TEXT_MARKETING_TAGS . '<br />' . tep_draw_tags_list($type, $piInfo->material_id));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TITLE . '*<br />' . tep_draw_input_field('material_title', $piInfo->material_title, 'style="width:300px"'));
                $contents[] = array('text' => '<br />' . TEXT_INFO_TEXT . '*<br />' . tep_draw_textarea_field('material_text', 'soft', '70', '15', $piInfo->material_text, 'id="material_text" class="tinymce"'));
                $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('material_sort', $piInfo->material_sort, 'style="width:50px"'));
                break;
        }



        $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $piInfo->material_id . '&' . $url)));
        break;
    case 'delete':
        $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE . '</strong>');

        $contents = array('form' => tep_draw_form($TABLE_MATERIAL, FILENAME_MARKETING_MATERIALS, 'mpID=' . $piInfo->material_id . '&' . $url  . '&action=deleteconfirm'));
        $contents[] = array('text' => '<br /><strong>' . $piInfo->material_title . '</strong>');
        $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $piInfo->material_id . '&' . $url)));
        break;
    default:
        if (isset($piInfo) && is_object($piInfo)) {
            $heading[] = array('text' => '<strong>' . $piInfo->material_title . '</strong>');

            $contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $piInfo->material_id . '&' . $url . '&action=edit')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_MARKETING_MATERIALS, 'mpID=' . $piInfo->material_id . '&' . $url . '&action=delete')));
        }
        break;
}

if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
}
?>
</tr>
</table></td>
</tr>
</table>
<script>
    $(document.forms['<?php echo $TABLE_MATERIAL;?>']).submit(function () {
    <?php
    switch($type) {
        case 2: //banners
        ?>
            if (this.products_id.value == -1 || this.languages_id.value == 0 || this.material_title.value == '' || this.material_width.value == '' || this.material_height.value == ''<?php echo ($action == 'new' ? " || this.material_file.value == ''" : "");?>) {
                alert('Fill required fields');
                return false;
            }
        <?php
            break;
        case 3: //pics
        ?>
            if (this.products_id.value == -1<?php echo ($action == 'new' ? " || this.material_pic.value == ''" : "");?> || this.material_title.value == '') {
                alert('Fill required fields');
                return false;
            }
        <?php
            break;
        case 4: //mails
        ?>
            if (this.products_id.value == -1 || this.languages_id.value == 0 || this.material_title.value == '' || this.material_html.value == ''<?php echo ($action == 'new' ? " || this.material_pic.value == ''" : "");?>) {
                alert('Fill required fields');
                return false;
            }
        <?php
            break;
        case 5: //statuses
        ?>
            if (this.products_id.value == -1 || this.languages_id.value == 0 || this.material_title.value == '') {
                alert('Fill required fields');
                return false;
            }
        <?php
            break;
        case 6: //shares
        ?>
            if (this.products_id.value == -1 || this.languages_id.value == 0 || this.material_title.value == '' || this.material_desc.value == ''<?php echo ($action == 'new' ? " || this.material_pic.value == ''" : "");?>) {
                alert('Fill required fields');
                return false;
            }
        <?php
            break;
        default: // posts
            ?>
            if (this.products_id.value == -1 || this.languages_id.value == 0 || this.material_title.value == '' || tinyMCE.get('material_text').getContent() == '') {
                alert('Fill required fields');
                return false;
            }
            <?php
            break;
    }
    ?>
    });
</script>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
