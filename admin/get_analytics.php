<?php

empty($_GET['for']) && exit;
$for = $_GET['for'];
require('includes/application_top.php');
!tep_session_is_registered('admin') && tep_exit();
switch($for) {
    case 'activity':
        if (!tep_session_is_registered('admin')) exit();
        $result = '{}';
        $date_filter = '';
        if (isset($_GET['start']) && isset($_GET['end']) && is_numeric($_GET['start']) && is_numeric($_GET['end']) && $_GET['start'] <= $_GET['end']) {
            $start_date = intval($_GET['start'] / 1000 - 24 * 3600);
            $end_date = intval($_GET['end'] / 1000 + 24 * 3600);

            $start_date = strtotime(date('Y-m-d', $start_date));
            $end_date = strtotime(date('Y-m-d', $end_date));

            $range = $end_date - $start_date;

            $date_filter = ' where v.first_online > "' . date('Y-m-d', $start_date) . '" and v.first_online < "' . date('Y-m-d', $end_date) . '"';
            if ($range > 12 * 30 * 24 * 3600) { // 12 months
                $date_format = "%Y-%m-01 00:00:00";
	            $by = 'months';
            } else {
                $date_format = "%Y-%m-%d 00:00:00";
	            $by = 'days';
            }
        } else {
            $date_format = "%Y-%m-01 00:00:00";
	        $by = 'months';
        }

        $visits_query = tep_db_query('SELECT UNIX_TIMESTAMP(DATE_FORMAT(v.first_online, "' . $date_format . '")) AS visits_date, COUNT(DISTINCT v.visit_id) AS global_visits, COUNT(DISTINCT o.visit_id) AS global_buys, COUNT(DISTINCT v.visit_id, v.partners_id) AS partner_visits, COUNT(DISTINCT o.visit_id, v.partners_id) AS partner_buys FROM ' . TABLE_VISITS . ' AS v LEFT JOIN ' . TABLE_ORDERS . ' AS o ON(o.visit_id = v.visit_id)' . $date_filter . ' GROUP BY visits_date ORDER BY visits_date ASC');
        if ($rows = tep_db_num_rows($visits_query)) {
            $js_data_global_visits = array();
            $js_data_partner_visits = array();
            $js_data_global_purchases = array();
            $js_data_partner_purchases = array();
            while($visit = tep_db_fetch_array($visits_query)) {
	            $prev_date = (float)$visit['visits_date'];
	            $js_data_global_visits[] = array($prev_date * 1000, (int)$visit['global_visits']);
	            $js_data_partner_visits[] = array($prev_date * 1000, (int)$visit['partner_visits']);
	            $js_data_global_purchases[] = array($prev_date * 1000, (int)$visit['global_buys']);
	            $js_data_partner_purchases[] = array($prev_date * 1000, (int)$visit['partner_buys']);
            }

	        switch ($by) {
		        case 'months':
			        $prev_date += 32 * 3600 * 24;
			        $prev_date = strtotime(date('Y-m-01', $prev_date));
			        break;
		        case 'days':
			        $prev_date += 3600 * 24;
			        break;
	        }

	        $js_data_global_visits[] = array($prev_date * 1000, 0);
	        $js_data_partner_visits[] = array($prev_date * 1000, 0);
	        $js_data_global_purchases[] = array($prev_date * 1000, 0);
	        $js_data_partner_purchases[] = array($prev_date * 1000, 0);

	        $result = array(
		        'title' => TEXT_ACTIVITY_TITLE,
		        'series' => array(
			        array(
				        'name' => TEXT_GLOBAL_VISITS,
				        'data' => $js_data_global_visits
			        ),
			        array(
				        'name' => TEXT_PARTNERS_VISITS,
				        'data' => $js_data_partner_visits
			        ),
			        array(
				        'name' => TEXT_GLOBAL_BUYS,
				        'data' => $js_data_global_purchases
			        ),
			        array(
				        'name' => TEXT_PARTNERS_BUYS,
				        'data' => $js_data_partner_purchases
			        )
		        )
	        );
	        $result = json_encode($result);

        }

        if (isset($_GET['callback'])) {
            $result = $_GET['callback'] . '(' . $result . ');';
        }
        exit($result);
        break;

	case 'methods':
		$res = '{}';
		$methods_query = tep_db_query('SELECT/* IFNULL(v.method_id, 0) AS method_id,*/ COUNT(DISTINCT ph.visit_id) AS buys,  COUNT(DISTINCT v.visit_id) AS visits, IFNULL(mi.method_name, "' . TEXT_METHOD_NAME_OTHER . '") AS method_name FROM ' . TABLE_VISITS . ' AS v LEFT JOIN ' . TABLE_MARKETING_METHODS_INFO . ' AS mi ON(v.method_id = mi.method_id AND mi.language_id = ' . (int)$languages_id . ') LEFT JOIN ' . TABLE_PARTNERS_HISTORY . ' AS ph ON(ph.visit_id = v.visit_id) GROUP BY (v.method_id)');
		if ($rows = tep_db_num_rows($methods_query)) {
			$js_data_visits = '[';
			$js_data_purchases = '[';
			$js_data_methods = '[';
			while($method = tep_db_fetch_array($methods_query)) {
				$js_data_visits .= '{"name":"' . $method['method_name'] . '","y":' . $method['visits'] . '},';
				$js_data_purchases .= '{"name":"' . $method['method_name'] . '","y":' . $method['buys'] . '},';
				$js_data_methods .= '"' . $method['method_name'] . '",';
			}

			$js_data_methods[strlen($js_data_methods) - 1] = ']';
			$js_data_visits[strlen($js_data_visits) - 1] = ']';
			$js_data_purchases[strlen($js_data_purchases) - 1] = ']';
			$result = '{"title":"' . TEXT_METHODS_TITLE . '","series":[{"name":"' . TEXT_VISITS . '","data":' . $js_data_visits . '},{"name":"' . TEXT_BUYS . '","data":' . $js_data_purchases . '}],"categories":' . $js_data_methods . '}';
		}
		if (isset($_GET['callback'])) {
			$result = $_GET['callback'] . '(' . $result . ');';
		}
		exit($result);
		break;

	case 'products':
		$result = '{}';

		$products_query = tep_db_query(
            'SELECT ' .
                'v.products_id, ' .
                'v.products_name, ' .
                'v.visits, ' .
                'b.buys ' .
            'FROM (' .
                'SELECT ' .
                    'pd.products_id, ' .
                    'pd.products_name, ' .
                    'COUNT(DISTINCT v.visit_id) AS visits ' .
                'FROM ' . TABLE_PRODUCTS_DESCRIPTION . ' AS pd ' .
                'LEFT JOIN ' . TABLE_VISITS . ' AS v ' .
                    'ON(' .
                        'pd.products_id = v.products_id' .
                    ') ' .
                'WHERE ' .
                    'pd.language_id = ' . (int)$languages_id . ' ' .
                'GROUP BY(pd.products_id)' .
            ') AS v ' .
            'LEFT JOIN (' .
                'SELECT ' .
                    'op.products_id, ' .
                    'SUM(op.products_quantity) AS buys ' .
                'FROM ' . TABLE_PARTNERS_HISTORY . ' AS ph ' .
                'JOIN ' . TABLE_ORDERS_PRODUCTS . ' AS op ' .
                    'ON(' .
                        'ph.orders_id = op.orders_id ' .
                    ') ' .
                'GROUP BY(op.products_id)' .
            ') AS b ' .
                'ON(v.products_id = b.products_id) ' .
            'WHERE (' .
                'v.visits > 0 ' .
                'OR b.buys > 0' .
            ')'
        );

		if ($rows = tep_db_num_rows($products_query)) {
			$js_data_visits = '[';
			$js_data_purchases = '[';
			$js_data_products = '[';
			while ($products = tep_db_fetch_array($products_query)) {
				$js_data_visits .= '{"name":"' . $products['products_name'] . '","y":' . $products['visits'] . '},';
				$js_data_purchases .= '{"name":"' . $products['products_name'] . '","y":' . $products['buys'] . '},';
				$js_data_products .= '"' . $products['products_name'] . '",';
			}

			$js_data_products[strlen($js_data_products) - 1] = ']';
			$js_data_visits[strlen($js_data_visits) - 1] = ']';
			$js_data_purchases[strlen($js_data_purchases) - 1] = ']';

			$result = '{"title":"' . TEXT_PRODUCTS_TITLE . '","series":[{"name":"' . TEXT_VISITS . '","data":' . $js_data_visits . '},{"name":"' . TEXT_BUYS . '","data":' . $js_data_purchases . '}],"categories":' . $js_data_products . '}';
		}

		if (isset($_GET['callback'])) {
			$result = $_GET['callback'] . '(' . $result . ');';
		}
		exit($result);
		break;

	case 'dashboard_customers':
		if (!tep_session_is_registered('admin')) exit();
		$result = '{}';
		$date_filter = '';
		$date_from = false;
		if (isset($_GET['start']) && isset($_GET['end']) && is_numeric($_GET['start']) && is_numeric($_GET['end']) && $_GET['start'] <= $_GET['end']) {
			$start_date = intval($_GET['start'] / 1000 - 24 * 3600);
			$end_date = intval($_GET['end'] / 1000 + 24 * 3600);

			$start_date = strtotime(date('Y-m-d', $start_date));
			$end_date = strtotime(date('Y-m-d', $end_date));

			$range = $end_date - $start_date;

			$date_filter = ' where customers_info_date_account_created > "' . date('Y-m-d', $start_date) . '" and customers_info_date_account_created < "' . date('Y-m-d', $end_date) . '"';
			if ($range > 12 * 30 * 24 * 3600) { // 12 months
				$date_format = "%Y-%m-01 00:00:00";
				$by = 'months';
			} else {
				$date_format = "%Y-%m-%d 00:00:00";
				$by = 'days';
			}

			$date_from = $start_date;
		} else {
			$by = 'months';
			$date_format = "%Y-%m-01 00:00:00";
		}

		$visits_query = tep_db_query('SELECT UNIX_TIMESTAMP(DATE_FORMAT(customers_info_date_account_created, "' . $date_format . '")) AS date_col, COUNT(DISTINCT customers_info_id) AS customers FROM ' . TABLE_CUSTOMERS_INFO . '' . $date_filter . ' GROUP BY date_col ORDER BY date_col ASC');
		$prev_date = false;
		if ($rows = tep_db_num_rows($visits_query)) {
			$js_data_customers = array();
			while($visit = tep_db_fetch_array($visits_query)) {
				$date = intval($visit['date_col']);
				if ($prev_date) {
					switch ($by) {
						case 'months':
							while ($prev_date < $date - 32 * 3600 * 24) {
								$prev_date += 32 * 3600 * 24;
								$prev_date = strtotime(date('Y-m-01', $prev_date));
								$js_data_customers[] = array($prev_date * 1000, 0);
							}
							break;
						case 'days':
							while ($prev_date < $date - 3600 * 24) {
								$prev_date += 3600 * 24;
								$js_data_customers[] = array($prev_date * 1000, 0);
							}
							break;
					}
				} elseif ($date_from && $date > $date_from) {
					switch ($by) {
						case 'months':
							while ($date > $date_from + 1) {
								$date_from += 32 * 3600 * 24;
								$date_from = strtotime(date('Y-m-01', $date_from));
								$js_data_customers[] = array($date_from * 1000, 0);
							}
							break;
						case 'days':
							while ($date > $date_from + 1) {
								$date_from += 3600 * 24;
								$js_data_customers[] = array($date_from * 1000, 0);
							}
							break;
					}
				}
				$js_data_customers[] = array(floatval($date) * 1000, (int)$visit['customers']);
				$prev_date = $date;
			}

			switch ($by) {
				case 'months':
					$prev_date += 32 * 3600 * 24;
					$prev_date = strtotime(date('Y-m-01', $prev_date));
					$js_data_customers[] = array($prev_date * 1000, 0);
					break;
				case 'days':
					$prev_date += 3600 * 24;
					$js_data_customers[] = array($prev_date * 1000, 0);
					break;
			}

			$result = array(
				'title' => TEXT_CUSTOMERS_TITLE,
				'series' => array(
					array(
						'name' => TEXT_CUSTOMERS_COUNT,
						'data' => $js_data_customers
					)
				)
			);
			$result = json_encode($result);

		}

		if (isset($_GET['callback'])) {
			$result = $_GET['callback'] . '(' . $result . ');';
		}
		exit($result);
		break;

	case 'dashboard_revenue':
		if (!tep_session_is_registered('admin')) exit();
		$result = '{}';
		$date_filter = '';
		$date_from = false;
		if (isset($_GET['start']) && isset($_GET['end']) && is_numeric($_GET['start']) && is_numeric($_GET['end']) && $_GET['start'] <= $_GET['end']) {
			$start_date = intval($_GET['start'] / 1000 - 24 * 3600);
			$end_date = intval($_GET['end'] / 1000 + 24 * 3600);

			$start_date = strtotime(date('Y-m-d', $start_date));
			$end_date = strtotime(date('Y-m-d', $end_date));

			$range = $end_date - $start_date;

			$date_filter = ' and date_purchased > "' . date('Y-m-d', $start_date) . '" and date_purchased < "' . date('Y-m-d', $end_date) . '"';
			if ($range > 12 * 30 * 24 * 3600) { // 12 months
				$date_format = "%Y-%m-01 00:00:00";
				$by = 'months';
			} else {
				$date_format = "%Y-%m-%d 00:00:00";
				$by = 'days';
			}

			$date_from = $start_date;
		} else {
			$by = 'months';
			$date_format = "%Y-%m-01 00:00:00";
		}

		$visits_query = tep_db_query('SELECT UNIX_TIMESTAMP(DATE_FORMAT(o.date_purchased, "' . $date_format . '")) AS date_col, SUM(ot.value) AS orders FROM ' . TABLE_ORDERS . ' as o JOIN ' . TABLE_ORDERS_TOTAL .  ' as ot on(o.orders_id = ot.orders_id) where ot.class = "ot_total"' . $date_filter . ' GROUP BY date_col ORDER BY date_col ASC');
		$prev_date = false;
		if ($rows = tep_db_num_rows($visits_query)) {
			$js_data_revenue = array();
			while($visit = tep_db_fetch_array($visits_query)) {
				$date = intval($visit['date_col']);
				if ($prev_date) {
					switch ($by) {
						case 'months':
							while ($prev_date < $date - 32 * 3600 * 24) {
								$prev_date += 32 * 3600 * 24;
								$prev_date = strtotime(date('Y-m-01', $prev_date));
								$js_data_revenue[] = array($prev_date * 1000, 0);
							}
							break;
						case 'days':
							while ($prev_date < $date - 3600 * 24) {
								$prev_date += 3600 * 24;
								$js_data_revenue[] = array($prev_date * 1000, 0);
							}
							break;
					}
				} elseif ($date_from && $date > $date_from) {
					switch ($by) {
						case 'months':
							while ($date > $date_from + 1) {
								$date_from += 32 * 3600 * 24;
								$date_from = strtotime(date('Y-m-01', $date_from));
								$js_data_revenue[] = array($date_from * 1000, 0);
							}
							break;
						case 'days':
							while ($date > $date_from + 1) {
								$date_from += 3600 * 24;
								$js_data_revenue[] = array($date_from * 1000, 0);
							}
							break;
					}
				}
				$js_data_revenue[] = array(floatval($date) * 1000, (int)$visit['orders']);
				$prev_date = $date;
			}

			switch ($by) {
				case 'months':
					$prev_date += 32 * 3600 * 24;
					$prev_date = strtotime(date('Y-m-01', $prev_date));
					$js_data_revenue[] = array($prev_date * 1000, 0);
					break;
				case 'days':
					$prev_date += 3600 * 24;
					$js_data_revenue[] = array($prev_date * 1000, 0);
					break;
			}

			$result = array(
				'title' => TEXT_REVENUE_TITLE,
				'series' => array(
					array(
						'name' => TEXT_REVENUE,
						'data' => $js_data_revenue
					)
				)
			);
			$result = json_encode($result);

		}

		if (isset($_GET['callback'])) {
			$result = $_GET['callback'] . '(' . $result . ');';
		}
		exit($result);
		break;
}