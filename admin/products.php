<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
require('includes/application_top.php');


require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

$action = (isset($_GET['action']) ? $_GET['action'] : '');

$current_category_id = (empty($_GET['cPath']) ? '' : $_GET['cPath']);

if (tep_not_null($action)) {
    switch ($action) {
        case 'setflag':
            if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
                if (isset($_GET['pID'])) {
                    tep_set_product_status($_GET['pID'], $_GET['flag']);
                }

                if (USE_CACHE == 'true') {
                    tep_reset_cache_block('also_purchased');
                }
            }

            tep_redirect(tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $_GET['cPath'] . '&pID=' . $_GET['pID']));
            break;
        case 'delete_product_confirm':
            if (isset($_POST['products_id']) && isset($_POST['product_categories']) && is_array($_POST['product_categories'])) {
                $product_id = tep_db_prepare_input($_POST['products_id']);
                $product_categories = $_POST['product_categories'];

                for ($i=0, $n=sizeof($product_categories); $i<$n; $i++) {
                    tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$product_id . "' and categories_id = '" . (int)$product_categories[$i] . "'");
                }

                tep_db_query("delete from " . TABLE_PRODUCTS_TO_PRODUCTS_INFO . " where products_id = '" . (int)$product_id . "'");


                $product_categories_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$product_id . "'");
                $product_categories = tep_db_fetch_array($product_categories_query);

                if ($product_categories['total'] == '0') {
                    tep_remove_product($product_id);
                }
            }

            if (USE_CACHE == 'true') {
                tep_reset_cache_block('categories');
                tep_reset_cache_block('also_purchased');
            }

            tep_redirect(tep_href_link(FILENAME_PRODUCTS));
            break;
        case 'insert_product':
        case 'update_product':
            if (isset($_GET['pID'])) $products_id = tep_db_prepare_input($_GET['pID']);
            $products_date_available = empty($_POST['products_date_available'])? 'null' : strtotime(tep_db_prepare_input($_POST['products_date_available']));

            if ($products_date_available === false) {
                $products_date_available = 'null';
            }

            //$products_date_available = (time() < $products_date_available) ? strtotime($products_date_available) : 'null';

            $sql_data_array = array('products_quantity' => empty($_POST['products_quantity']) ? 0 : (int)tep_db_prepare_input($_POST['products_quantity']),
                'products_misspellings' => empty($_POST['products_misspellings']) ? '' : tep_db_prepare_input($_POST['products_misspellings']),
                'products_price' => empty($_POST['products_price']) ? 0 : floatval($_POST['products_price']),
                'products_date_available' => $products_date_available === 'null' ? 'null' : date('Y-m-d', $products_date_available),
                'products_weight' => empty($_POST['products_weight']) ? 0 : (float)tep_db_prepare_input($_POST['products_weight']),
                'products_status' => empty($_POST['products_status']) ? 1 : tep_db_prepare_input($_POST['products_status']),
                'products_tax_class_id' => empty($_POST['products_tax_class_id']) ? 0 : tep_db_prepare_input($_POST['products_tax_class_id']));

            /*$products_image = new upload('products_image');
            $products_image->set_destination(DIR_FS_CATALOG_IMAGES);
            if ($products_image->parse() && $products_image->save()) {
                $sql_data_array['products_image'] = tep_db_prepare_input($products_image->filename);
            }*/

            $error_message = false;

            $parent_category_id = empty($_POST['parent_category_id']) ? 0 : tep_db_prepare_input($_POST['parent_category_id']);

            if ($_FILES['products_image']['tmp_name'] != '') {
                list($width_orig, $height_orig, $image_format) = @getimagesize($_FILES['products_image']['tmp_name']);
                if ($width_orig < MIDDLE_IMAGE_WIDTH || $height_orig < MIDDLE_IMAGE_HEIGHT) {
                    $error_message .= 'Error: Image size must be not less then ' . MIDDLE_IMAGE_WIDTH . 'x' . MIDDLE_IMAGE_HEIGHT . 'px' . '<br/>';
                }
            }
            if (!$parent_category_id) {
                $error_message .= 'Error: Select categories' . '<br/>';
            }

            if ($error_message) {
                $messageStack->add_session($error_message, 'error');
                tep_redirect(tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $products_id . '&action=' . ($action == 'insert_product'? 'new_product' : 'edit_product')));
            }
            if ($action == 'insert_product') {
                $insert_sql_data = array('products_date_added' => 'now()');

                $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

                tep_db_perform(TABLE_PRODUCTS, $sql_data_array);
                $products_id = tep_db_insert_id();

                if (is_array($parent_category_id)) {
                    foreach ($parent_category_id as $curr_cat_id) {
                        tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id . "', '" . (int)$curr_cat_id . "')");
                    }
                } else {
                    tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id . "', '0')");
                }
                //tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id . "', '" . (int)$parent_category_id . "')");
            } elseif ($action == 'update_product') {
                $update_sql_data = array('products_last_modified' => 'now()');

                $sql_data_array = array_merge($sql_data_array, $update_sql_data);

                tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "'");

                if (is_array($parent_category_id)) {
                    tep_db_query('delete from ' . TABLE_PRODUCTS_TO_CATEGORIES . ' where products_id = ' . (int)$products_id);
                    foreach ($parent_category_id as $curr_cat_id) {
                        tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id . "', '" . (int)$curr_cat_id . "')");
                    }
                } else {
                    tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$products_id . "', '0')");
                }

                //tep_db_query("update " . TABLE_PRODUCTS_TO_CATEGORIES . " set categories_id = '" . (int)$parent_category_id . "' where products_id = '" . (int)$products_id . "'");
            }

            require(DIR_WS_CLASSES . 'gallery.php');

            $gall = new CGallery();
            if ($_FILES['products_image']['tmp_name'] != '') {
                $with_logo = (isset($_POST['with_logo']) && ($_POST['with_logo'] == 'on')) ? 1 : 0;
                $name = substr(tep_clean_text_int(tep_translit(tep_db_prepare_input($_POST['products_name'][1]) ? tep_db_prepare_input($_POST['products_name'][1]) : '')), 0, 20) . '-p' . (int)$products_id;
                $sql_data_array['products_image'] = $name;
                tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "'");
                $pic_query = tep_db_query("select products_image from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
                $prod_pic = tep_db_fetch_array($pic_query);
                if ($prod_pic['products_image'] != $name && tep_not_null($prod_pic['products_image'])) {
                    $image_location =  DIR_FS_CATALOG_IMAGES . $prod_pic['products_image'];
                    if (file_exists($image_location . '-1' . IMAGE_EXTENSION)) @unlink($image_location . '-1' . IMAGE_EXTENSION);
                    if (file_exists($image_location . '-2' . IMAGE_EXTENSION)) @unlink($image_location . '-2' . IMAGE_EXTENSION);
                    //						if (file_exists($image_location . '-3' . $ext)) @unlink($image_location . '-3' . $ext);
                }
                $gall->addPhoto($_FILES['products_image']['tmp_name'], DIR_FS_CATALOG_IMAGES_PRODUCTS . $name, IMAGE_EXTENSION, $with_logo, '', MIDDLE_IMAGE_WIDTH, '', SMALL_IMAGE_WIDTH);
                $gall->name = '';
            }

            $languages = tep_get_languages();
            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                $language_id = $languages[$i]['id'];

                $sql_data_array = array(
                    'products_name' => tep_db_prepare_input($_POST['products_name'][$language_id]),
                    'meta_keyword' => tep_db_prepare_input($_POST['meta_keyword'][$language_id]),
                    'meta_title' => tep_db_prepare_input($_POST['meta_title'][$language_id]),
                    'meta_description' => tep_db_prepare_input($_POST['meta_description'][$language_id]),
                    'products_short_desc' => tep_db_prepare_input($_POST['products_short_desc'][$language_id]),
                    'products_description' => tep_db_prepare_input($_POST['products_description'][$language_id])
                    //'products_url' => tep_db_prepare_input($_POST['products_url'][$language_id])
                );

                if ($action == 'insert_product') {
                    $insert_sql_data = array('products_id' => $products_id,
                        'language_id' => $language_id);

                    $sql_data_array = array_merge($sql_data_array, $insert_sql_data);

                    tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array);
                } elseif ($action == 'update_product') {
                    tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "' and language_id = '" . (int)$language_id . "'");
                }

                $products_info_ids_query = tep_db_query('select products_info_id from ' . TABLE_PRODUCTS_INFO);
                $products_has_info = tep_db_num_rows($products_info_ids_query) > 0;

                if ($products_has_info) {
                    while ($products_info_id = tep_db_fetch_array($products_info_ids_query)) {
                        $pi_text = isset($_POST['products_info_text'][$language_id][$products_info_id['products_info_id']]) ? tep_db_prepare_input($_POST['products_info_text'][$language_id][$products_info_id['products_info_id']]) : '';

                        if ($action == 'insert_product') {
                            $sql_data_array = array('products_info_id' => (int)($products_info_id['products_info_id']),
                                'products_id' => (int)$products_id,
                                'language_id' => $language_id,
                                'products_info_text' => $pi_text);
                            tep_db_perform(TABLE_PRODUCTS_TO_PRODUCTS_INFO, $sql_data_array);
                        } elseif ($action == 'update_product') {
                            $chec_pinfo_row = tep_db_query('select * from ' . TABLE_PRODUCTS_TO_PRODUCTS_INFO . ' where'
                                . ' products_info_id = ' . (int)($products_info_id['products_info_id'])
                                . ' and products_id = ' . (int)$products_id
                                . ' and language_id = ' . $language_id
                            );

                            if (tep_db_num_rows($chec_pinfo_row)) {
                                $sql_data_array = array('products_info_text' => $pi_text);
                                tep_db_perform(TABLE_PRODUCTS_TO_PRODUCTS_INFO, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "' and language_id = '" . (int)$language_id . "' and products_info_id = " . (int)($products_info_id['products_info_id']));
                            } else {
                                $sql_data_array = array('products_info_id' => (int)($products_info_id['products_info_id']),
                                    'products_id' => (int)$products_id,
                                    'language_id' => $language_id,
                                    'products_info_text' => $pi_text);
                                tep_db_perform(TABLE_PRODUCTS_TO_PRODUCTS_INFO, $sql_data_array);
                            }
                        }

                    }
                }
            }

            $pi_sort_order = 0;
            $piArray = array(0);

            foreach ($_FILES as $key => $value) {
// Update existing large product images
                if (preg_match('/^products_image_large_([0-9]+)$/', $key, $matches)) {
                    $pi_sort_order++;

                    $sql_data_array = array('htmlcontent' => tep_db_prepare_input($_POST['products_image_htmlcontent_' . $matches[1]]),
                        'sort_order' => $pi_sort_order);

                    $t = new upload($key);
                    $t->set_destination(DIR_FS_CATALOG_IMAGES);
                    if ($t->parse() && $t->save()) {
                        $sql_data_array['image'] = tep_db_prepare_input($t->filename);
                    }

                    tep_db_perform(TABLE_PRODUCTS_IMAGES, $sql_data_array, 'update', "products_id = '" . (int)$products_id . "' and id = '" . (int)$matches[1] . "'");

                    $piArray[] = (int)$matches[1];
                } elseif (preg_match('/^products_image_large_new_([0-9]+)$/', $key, $matches)) {
// Insert new large product images
                    $sql_data_array = array('products_id' => (int)$products_id,
                        'htmlcontent' => tep_db_prepare_input($_POST['products_image_htmlcontent_new_' . $matches[1]]));

                    $t = new upload($key);
                    $t->set_destination(DIR_FS_CATALOG_IMAGES);
                    if ($t->parse() && $t->save()) {
                        $pi_sort_order++;

                        $sql_data_array['image'] = tep_db_prepare_input($t->filename);
                        $sql_data_array['sort_order'] = $pi_sort_order;

                        tep_db_perform(TABLE_PRODUCTS_IMAGES, $sql_data_array);

                        $piArray[] = tep_db_insert_id();
                    }
                }
            }

            $product_images_query = tep_db_query("select image from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int)$products_id . "' and id not in (" . implode(',', $piArray) . ")");
            if (tep_db_num_rows($product_images_query)) {
                while ($product_images = tep_db_fetch_array($product_images_query)) {
                    $duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_IMAGES . " where image = '" . tep_db_input($product_images['image']) . "'");
                    $duplicate_image = tep_db_fetch_array($duplicate_image_query);

                    if ($duplicate_image['total'] < 2) {
                        if (file_exists(DIR_FS_CATALOG_IMAGES . $product_images['image'])) {
                            @unlink(DIR_FS_CATALOG_IMAGES . $product_images['image']);
                        }
                    }
                }

                tep_db_query("delete from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int)$products_id . "' and id not in (" . implode(',', $piArray) . ")");
            }

            if (USE_CACHE == 'true') {
                tep_reset_cache_block('categories');
                tep_reset_cache_block('also_purchased');
            }

            tep_redirect(tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $products_id));
            break;
        case 'copy_to_confirm':
            if (isset($_POST['products_id']) && isset($_POST['categories_id'])) {
                $products_id = tep_db_prepare_input($_POST['products_id']);
                $categories_id = tep_db_prepare_input($_POST['categories_id']);
                $parent_cat_id_array = tep_generate_category_path($products_id, 'product');
                //$parent_cat_id = end($parent_cat_id_array)['id'];

                $product_query = tep_db_query("select products_quantity, products_misspellings, products_model, products_image, products_price, products_date_available, products_weight, products_tax_class_id, manufacturers_id from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
                $product = tep_db_fetch_array($product_query);

                tep_db_query("insert into " . TABLE_PRODUCTS . " (products_quantity, products_misspellings, products_model,products_image, products_price, products_date_added, products_date_available, products_weight, products_status, products_tax_class_id, manufacturers_id) values ('" . tep_db_input($product['products_quantity']) . "', '" . tep_db_input($product['products_misspellings']) . "', '" . tep_db_input($product['products_model']) . "', '" . tep_db_input($product['products_image']) . "', '" . tep_db_input($product['products_price']) . "',  now(), " . (empty($product['products_date_available']) ? "null" : "'" . tep_db_input($product['products_date_available']) . "'") . ", '" . tep_db_input($product['products_weight']) . "', '0', '" . (int)$product['products_tax_class_id'] . "', '" . (int)$product['manufacturers_id'] . "')");
                $dup_products_id = tep_db_insert_id();

                $description_query = tep_db_query("select language_id, products_name, products_description, products_url from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$products_id . "'");
                while ($description = tep_db_fetch_array($description_query)) {
                    tep_db_query("insert into " . TABLE_PRODUCTS_DESCRIPTION . " (products_id, language_id, products_name, products_description, products_url, products_viewed) values ('" . (int)$dup_products_id . "', '" . (int)$description['language_id'] . "', '" . tep_db_input($description['products_name']) . "', '" . tep_db_input($description['products_description']) . "', '" . tep_db_input($description['products_url']) . "', '0')");
                }

                $product_images_query = tep_db_query("select image, htmlcontent, sort_order from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int)$products_id . "'");
                while ($product_images = tep_db_fetch_array($product_images_query)) {
                    tep_db_query("insert into " . TABLE_PRODUCTS_IMAGES . " (products_id, image, htmlcontent, sort_order) values ('" . (int)$dup_products_id . "', '" . tep_db_input($product_images['image']) . "', '" . tep_db_input($product_images['htmlcontent']) . "', '" . tep_db_input($product_images['sort_order']) . "')");
                }

                tep_db_query("insert into " . TABLE_PRODUCTS_TO_CATEGORIES . " (products_id, categories_id) values ('" . (int)$dup_products_id . "', '" . (int)$categories_id . "')");
                $products_id = $dup_products_id;

                $products_info_query = tep_db_query('select products_info_id, language_id, products_info_text from ' . TABLE_PRODUCTS_TO_PRODUCTS_INFO . ' where products_id = "' . (int)$products_id . '"');
                while($products_info = tep_db_fetch_array($products_info_query)) {
                    tep_db_query("insert into " . TABLE_PRODUCTS_TO_PRODUCTS_INFO . " (products_info_id, products_id, language_id, products_info_text) values ('" . (int)$products_info['products_info_id'] . "', '" . (int)$products_id . "', '" . (int)$products_info['language_id'] . "', '" . $products_info['products_info_text'] . "')");
                }

                if (USE_CACHE == 'true') {
                    tep_reset_cache_block('categories');
                    tep_reset_cache_block('also_purchased');
                }
            }

            tep_redirect(tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $categories_id . '&pID=' . $products_id));
            break;
    }
}

// check if the catalog image directory exists
if (is_dir(DIR_FS_CATALOG_IMAGES)) {
    if (!tep_is_writable(DIR_FS_CATALOG_IMAGES)) $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_NOT_WRITEABLE, 'error');
} else {
    $messageStack->add(ERROR_CATALOG_IMAGE_DIRECTORY_DOES_NOT_EXIST, 'error');
}

require(DIR_WS_INCLUDES . 'template_top.php');
?>
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pageHeading"><a href="<?php echo tep_href_link(FILENAME_PRODUCTS);?>" style="display: block; text-decoration: none;"><?php echo HEADING_TITLE; ?></td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
                    <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="smallText" align="right">
                                    <?php
                                    echo tep_draw_form('search', FILENAME_PRODUCTS, '', 'get');
                                    echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search');
                                    echo tep_hide_session_id() . '</form>';
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="smallText" align="right">
                                    <?php
                                    echo tep_draw_form('filter', FILENAME_PRODUCTS, '', 'get');
                                    echo HEADING_TITLE_FILTER . ' ' . tep_draw_pull_down_menu('cPath', tep_get_category_tree(), $current_category_id, 'onchange="this.form.submit();"');
                                    echo tep_hide_session_id() . '</form>';
                                    ?>
                                </td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
    <tr>
    <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tr>
    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
            </tr>
            <?php
            $rows = 0;
            $filter_str =  " and p2c.categories_id = '" . (int)$current_category_id . "'";
            if ($current_category_id == 0) {
                $filter_str = '';
            } else {
                $subcategories_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$current_category_id . "'");
                if (tep_db_num_rows($subcategories_query) > 0 ) {
                    $filter_str = ' and (';
                    while($subcategories = tep_db_fetch_array($subcategories_query)) {
                        $filter_str .= " p2c.categories_id = '" . (int)$subcategories['categories_id'] . "' || ";
                    }
                    $filter_str .= '0)';
                }
            }



            if (isset($_GET['search'])) {
                $products_query = tep_db_query("select p.products_id, pd.products_name, p.products_quantity, p.products_misspellings, p.products_image, p.products_price, p.products_date_added, p.products_last_modified, p.products_date_available, p.products_status from " . TABLE_PRODUCTS . " p join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "') where pd.products_name like '%" . tep_db_input($_GET['search']) . "%' order by pd.products_name");
                //$products_query = tep_db_query("select p.products_id, pd.products_name, p.products_quantity, p.products_misspellings, p.products_image, p.products_price, p.products_date_added, p.products_last_modified, p.products_date_available, p.products_status from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' and pd.products_name like '%" . tep_db_input($search) . "%' order by pd.products_name");
            } else {
                $products_query = tep_db_query("select p.products_id, pd.products_name, p.products_quantity, p.products_misspellings, p.products_image, p.products_price, p.products_date_added, p.products_last_modified, p.products_date_available, p.products_status from " . TABLE_PRODUCTS . " p join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "') join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on (p.products_id = p2c.products_id" . $filter_str . ") group by products_id order by pd.products_name");
            }
            while ($products = tep_db_fetch_array($products_query)) {
                $rows++;

// Get categories_id for product if search
                //if (isset($_GET['search'])) $cPath = $products['categories_id'];

                if ( (!isset($_GET['pID']) || (isset($_GET['pID']) && ($_GET['pID'] == $products['products_id']))) && !isset($pInfo) && (substr($action, 0, 3) != 'new')) {
// find out the rating average from customer reviews
                    $reviews_query = tep_db_query("select (avg(reviews_rating) / 5 * 100) as average_rating from " . TABLE_REVIEWS . " where products_id = '" . (int)$products['products_id'] . "'");
                    $reviews = tep_db_fetch_array($reviews_query);
                    $pInfo_array = array_merge($products, $reviews);
                    $pInfo = new objectInfo($pInfo_array);
                }

                if (isset($pInfo) && is_object($pInfo) && ($products['products_id'] == $pInfo->products_id) ) {
                    echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $products['products_id'] . '&action=edit_product') . '\'">' . "\n";
                } else {
                    echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $products['products_id']) . '\'">' . "\n";
                }
                ?>
                <td class="dataTableContent"><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $products['products_id'] . '&action=product_preview') . '">' . tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>&nbsp;' . $products['products_name']; ?></td>
                <td class="dataTableContent" align="center">
                    <?php
                    if ($products['products_status'] == '1') {
                        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_PRODUCTS, 'action=setflag&flag=0&pID=' . $products['products_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
                    } else {
                        echo '<a href="' . tep_href_link(FILENAME_PRODUCTS, 'action=setflag&flag=1&pID=' . $products['products_id'] . '&cPath=' . $cPath) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                    }
                    ?>
                </td>
                <td class="dataTableContent" align="right"><?php if (isset($pInfo) && is_object($pInfo) && ($products['products_id'] == $pInfo->products_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $products['products_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                </tr>
            <?php
            }
            ?>
            <tr>
                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                        <tr>
                            <td align="right" class="smallText"><?php if (!isset($_GET['search'])) echo tep_draw_button(IMAGE_NEW_PRODUCT, 'plus', tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&action=new_product')); ?>&nbsp;</td>
                        </tr>
                    </table></td>
            </tr>
        </table></td>
    <?php
    $heading = array();
    $contents = array();
    switch ($action) {
        case 'delete_product':
            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_DELETE_PRODUCT . '</strong>');

            $contents = array('form' => tep_draw_form('products', FILENAME_PRODUCTS, 'action=delete_product_confirm&cPath=' . $cPath) . tep_draw_hidden_field('products_id', $pInfo->products_id));
            $contents[] = array('text' => TEXT_DELETE_PRODUCT_INTRO);
            $contents[] = array('text' => '<br /><strong>' . $pInfo->products_name . '</strong>');

            $product_categories_string = '';
            $product_categories = tep_generate_category_path($pInfo->products_id, 'product');
            for ($i = 0, $n = sizeof($product_categories); $i < $n; $i++) {
                $category_path = '';
                for ($j = 0, $k = sizeof($product_categories[$i]); $j < $k; $j++) {
                    $category_path .= $product_categories[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
                }
                $category_path = substr($category_path, 0, -16);
                $product_categories_string .= tep_draw_checkbox_field('product_categories[]', $product_categories[$i][sizeof($product_categories[$i])-1]['id'], true) . '&nbsp;' . $category_path . '<br />';
            }
            $product_categories_string = substr($product_categories_string, 0, -4);

            $contents[] = array('text' => '<br />' . $product_categories_string);
            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id)));
            break;
        case 'copy_to':
            $heading[] = array('text' => '<strong>' . TEXT_INFO_HEADING_COPY_TO . '</strong>');

            $contents = array('form' => tep_draw_form('copy_to', FILENAME_PRODUCTS, 'action=copy_to_confirm&cPath=' . $cPath) . tep_draw_hidden_field('products_id', $pInfo->products_id));
            $contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
            $contents[] = array('text' => '<br />' . TEXT_INFO_CURRENT_CATEGORIES . '<br /><strong>' . tep_output_generated_category_path($pInfo->products_id, 'product') . '</strong>');
            $contents[] = array('text' => '<br />' . TEXT_CATEGORIES . '<br />' . tep_draw_pull_down_menu('categories_id', tep_get_category_tree('0', '', '0')));
            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_COPY, 'copy', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id)));
            break;
        case 'new_product':
            break;
        case 'edit_product':
            break;
        case 'product_preview':
            break;
        default:
            if ($rows > 0) {
                if (isset($pInfo) && is_object($pInfo)) { // product info box contents
                    $heading[] = array('text' => '<strong>' . tep_get_products_name($pInfo->products_id, $languages_id) . '</strong>');

                    $contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id . '&action=edit_product')) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id . '&action=delete_product')) . tep_draw_button(IMAGE_COPY_TO, 'copy', tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . '&pID=' . $pInfo->products_id . '&action=copy_to')));
                    $contents[] = array('text' => '<br />' . TEXT_DATE_ADDED . ' ' . tep_date_short($pInfo->products_date_added));
                    if (tep_not_null($pInfo->products_last_modified)) $contents[] = array('text' => TEXT_LAST_MODIFIED . ' ' . tep_date_short($pInfo->products_last_modified));
                    if (date('Y-m-d') < $pInfo->products_date_available) $contents[] = array('text' => TEXT_DATE_AVAILABLE . ' ' . tep_date_short($pInfo->products_date_available));
                    $contents[] = array('text' => '<br />' . tep_info_image(DIR_WS_PRODUCT_IMAGES . $pInfo->products_image . '-2' . IMAGE_EXTENSION, $pInfo->products_name, SMALL_IMAGE_WIDTH) . '<br />' . $pInfo->products_image . '-2' . IMAGE_EXTENSION);
                    $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_PRICE_INFO . ' ' . $currencies->format($pInfo->products_price) . '<br />' . TEXT_PRODUCTS_QUANTITY_INFO . ' ' . $pInfo->products_quantity);
                    $contents[] = array('text' => '<br />' . TEXT_PRODUCTS_AVERAGE_RATING . ' ' . number_format($pInfo->average_rating, 2) . '%');
                }
            } else { // create category/product info
                $heading[] = array('text' => '<strong>' . EMPTY_CATEGORY . '</strong>');

                $contents[] = array('text' => TEXT_NO_CHILD_PRODUCTS);
            }
            break;
    }
    if ($action == 'new_product' || $action == 'edit_product') {
        echo '<script>stopUnload = true;</script>';
        echo '            </tr><tr><td width="800" valign="top" id="prodInfo">' . "\n";
        $parameters = array('products_name' => '',
            'products_misspellings' => '',
            'products_description' => '',
            'products_url' => '',
            'products_id' => '',
            'products_quantity' => '',
            'products_model' => '',
            'products_image' => '',
            'products_larger_images' => array(),
            'products_price' => '',
            'products_weight' => '',
            'products_date_added' => '',
            'products_last_modified' => '',
            'products_date_available' => '',
            'products_status' => '',
            'products_tax_class_id' => '',
            'manufacturers_id' => '',
            'categories_id' => ''
        );

        $pInfo = new objectInfo($parameters);

        if (!empty($_GET['pID']) && empty($_POST) && substr($action, 0 , 3) != 'new') {
            $product_query = tep_db_query("select pd.products_name, p.products_misspellings, pd.products_description, pd.meta_keyword, pd.meta_title, pd.meta_description, pd.products_url, p.products_id, p.products_quantity, p.products_model, p.products_image, p.products_price, p.products_weight, p.products_date_added, p.products_last_modified, date_format(p.products_date_available, '%Y-%m-%d') as products_date_available, p.products_status, p.products_tax_class_id, p.manufacturers_id from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = '" . (int)$_GET['pID'] . "' and p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "'");
            $product = tep_db_fetch_array($product_query);

            $product_categories_query = tep_db_query('select categories_id from products_to_categories where products_id = ' . (int)$_GET['pID']);
            $product['categories_id'] = array();
            while ($curr_cat = tep_db_fetch_array($product_categories_query)) {
                $product['categories_id'][] = $curr_cat['categories_id'];
            }

            $pInfo->objectInfo($product);



            $product_images_query = tep_db_query("select id, image, htmlcontent, sort_order from " . TABLE_PRODUCTS_IMAGES . " where products_id = '" . (int)$product['products_id'] . "' order by sort_order");
            while ($product_images = tep_db_fetch_array($product_images_query)) {
                $pInfo->products_larger_images[] = array('id' => $product_images['id'],
                    'image' => $product_images['image'],
                    'htmlcontent' => $product_images['htmlcontent'],
                    'sort_order' => $product_images['sort_order']);
            }
        }

        $manufacturers_array = array(array('id' => '', 'text' => TEXT_NONE));
        $manufacturers_query = tep_db_query("select manufacturers_id, manufacturers_name from " . TABLE_MANUFACTURERS . " order by manufacturers_name");
        while ($manufacturers = tep_db_fetch_array($manufacturers_query)) {
            $manufacturers_array[] = array('id' => $manufacturers['manufacturers_id'],
                'text' => $manufacturers['manufacturers_name']);
        }

        $tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
        $tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
        while ($tax_class = tep_db_fetch_array($tax_class_query)) {
            $tax_class_array[] = array('id' => $tax_class['tax_class_id'],
                'text' => $tax_class['tax_class_title']);
        }

        $languages = tep_get_languages();

        if (!isset($pInfo->products_status)) $pInfo->products_status = '1';
        switch ($pInfo->products_status) {
            case '0': $in_status = false; $out_status = true; break;
            case '1':
            default: $in_status = true; $out_status = false;
        }

        $form_action = $action == 'edit_product' ? 'update_product' : 'insert_product';
        ?>
        <script type="text/javascript"><!--
            var tax_rates = new Array();
            <?php
                for ($i=0, $n=sizeof($tax_class_array); $i<$n; $i++) {
                  if ($tax_class_array[$i]['id'] > 0) {
                    echo 'tax_rates["' . $tax_class_array[$i]['id'] . '"] = ' . tep_get_tax_rate_value($tax_class_array[$i]['id']) . ';' . "\n";
                  }
                }
            ?>

            function doRound(x, places) {
                return Math.round(x * Math.pow(10, places)) / Math.pow(10, places);
            }

            function getTaxRate() {
                var selected_value = document.forms["new_product"].products_tax_class_id.selectedIndex;
                var parameterVal = document.forms["new_product"].products_tax_class_id[selected_value].value;

                if ( (parameterVal > 0) && (tax_rates[parameterVal] > 0) ) {
                    return tax_rates[parameterVal];
                } else {
                    return 0;
                }
            }

            function updateGross() {
                var taxRate = getTaxRate();
                var grossValue = document.forms["new_product"].products_price.value;

                if (taxRate > 0) {
                    grossValue = grossValue * ((taxRate / 100) + 1);
                }

                document.forms["new_product"].products_price_gross.value = doRound(grossValue, 4);
            }

            function updateNet() {
                var taxRate = getTaxRate();
                var netValue = document.forms["new_product"].products_price_gross.value;

                if (taxRate > 0) {
                    netValue = netValue / ((taxRate / 100) + 1);
                }

                document.forms["new_product"].products_price.value = doRound(netValue, 4);
            }
            //--></script>
    <?php echo tep_draw_form('new_product', FILENAME_PRODUCTS, 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : '') . '&action=' . $form_action, 'post', 'enctype="multipart/form-data"'); ?>
        <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr class="infoBoxHeading">
                <td class="infoBoxHeading"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><?php echo $action == 'new_product' ? TEXT_NEW_PRODUCT : TEXT_EDIT_PRODUCT; ?></td>
                            <!--                            <td class="dataTableHeadingContent" align="right">--><?php //echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?><!--</td>-->
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
            <tr>
                <td><table border="0" cellspacing="0" cellpadding="2" style="width: 100%;">
                        <tr>
                            <td class="main"><?php echo TEXT_PRODUCTS_CATEGORY; ?><span style="color: #ff0000;">*</span></td>
                            <td class="main"><?php
                                foreach(tep_get_category_tree('0', '', '0') as $curr_cat) {
                                    $checked = false;
                                    if (!empty($_GET['pID']) && in_array($curr_cat['id'], $pInfo->categories_id)) {
                                        $checked = true;
                                    }
                                    echo '<label>' . tep_draw_checkbox_field('parent_category_id[]', $curr_cat['id'], $checked) . $curr_cat['text'] . '</label><br/>';
                                }
                                //                                echo tep_draw_pull_down_menu('parent_category_id', tep_get_category_tree('0', '', '0'), $pInfo->categories_id);

                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div id="tabs">
                                    <?php
                                    $tabs = '<ul>';
                                    $tabs_content = '<div>';

                                    for ($i=0, $n=sizeof($languages); $i < $n; $i++) {
                                        $tabs .= '<li data-tab="tabs-' . $languages[$i]['id'] . '"><a>' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '</a></li>';

                                        $tabs_content .= '
                                    <div id="tabs-' . $languages[$i]['id'] . '">
                                        <table width="100%">
                                            <tr>
                                                <td class="main">' . TEXT_PRODUCTS_NAME . '<span style="color: #ff0000;">*</span></td>
                                                <td class="main">' . tep_draw_input_field('products_name[' . $languages[$i]['id'] . ']', (empty($pInfo->products_id) ? '' : tep_get_products_name($pInfo->products_id, $languages[$i]['id']))) . '</td>
                                            </tr>
                                            <tr>
                                                <td class="main">' . TEXT_PRODUCTS_SHORT_DESC . '</td>
                                                <td class="main">' . tep_draw_input_field('products_short_desc[' . $languages[$i]['id'] . ']', (empty($pInfo->products_id) ? '' : tep_get_products_short_desc($pInfo->products_id, $languages[$i]['id']))) . '</td>
                                            </tr>
                                            
                                             <tr>
                                                <td class="main">Meta Title</td>
                                                <td class="main">' . tep_draw_input_field('meta_title[' . $languages[$i]['id'] . ']', (empty($pInfo->products_id) ? '' : tep_get_products_meta_title($pInfo->products_id, $languages[$i]['id']))) . '</td>
                                            </tr>
                                            <tr>
                                                <td class="main">Meta Keywords</td>
                                                <td class="main">' . tep_draw_input_field('meta_keyword[' . $languages[$i]['id'] . ']', (empty($pInfo->products_id) ? '' : tep_get_products_meta_keyword($pInfo->products_id, $languages[$i]['id']))) . '</td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="main">Meta Description</td>
                                                <td class="main">' . tep_draw_input_field('meta_description[' . $languages[$i]['id'] . ']', (empty($pInfo->products_id) ? '' : tep_get_products_meta_description($pInfo->products_id, $languages[$i]['id']))) . '</td>
                                            </tr>
                                            
                                            
                                            <tr>
                                                <td colspan="2" class="main" valign="top">' . TEXT_PRODUCTS_DESCRIPTION . '<br/>
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td colspan="2" class="main">' . tep_draw_textarea_field('products_description[' . $languages[$i]['id'] . ']', 'soft', '70', '15', (empty($pInfo->products_id) ? '' : tep_get_products_description($pInfo->products_id, $languages[$i]['id'])), 'class="tinymce"') . '</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>';

                                        $products_info_query = tep_db_query('select pi.products_info_id, pid.products_info_title from ' . TABLE_PRODUCTS_INFO . ' as pi join ' . TABLE_PRODUCTS_INFO_DESCRIPTION . ' as pid on(pi.products_info_id = pid.products_info_id and pid.language_id = ' . (int)$languages[$i]['id'] . ') order by pi.sort_order');

                                        $pi_head = '';
                                        if(tep_db_num_rows($products_info_query)) {
                                            $pi_head = '<ul>';
                                            $pi_body = '<div>';
                                            $k = 1;
                                            $products_info_text = '';
                                            while($products_info = tep_db_fetch_array($products_info_query)) {
                                                if (isset($_GET['pID'])) {
                                                    $products_info_text_query = tep_db_query('select products_info_text from ' . TABLE_PRODUCTS_TO_PRODUCTS_INFO . ' where products_info_id = "' . (int)$products_info['products_info_id'] . '" and products_id = "' . (int)$_GET['pID'] . '" and language_id = "' . (int)$languages[$i]['id'] . '"');
                                                    if (tep_db_num_rows($products_info_text_query)) {
                                                        $products_info_text = tep_db_fetch_array($products_info_text_query)['products_info_text'];
                                                    }
                                                }
                                                $pi_head .= '<li data-tab="pInfoTabs-' . $k . '"><a>' . $products_info['products_info_title'] . '</a></li>';
                                                $pi_body .= '<div id="pInfoTabs-' . $k++ . '">' . tep_draw_textarea_field('products_info_text[' . (int)$languages[$i]['id'] . '][' . $products_info['products_info_id'] . ']', 'soft', '70', '15', $products_info_text, 'class="tinymce"') . '</div>';
                                            }
                                            $pi_body .= '</div>';
                                            $tabs_content .= '<tr><td class="main" colspan="2"><div class="prodInfoTabs" id="prodInfoTab-' . $languages[$i]['id'] . '">' . $pi_head . '</ul>' . $pi_body . '</div></tr>';
                                        }
                                        $tabs_content .= '
                                        </table>
                                    </div>';
                                    }
                                    $tabs .= '</ul>';
                                    $tabs_content .= '</div>';
                                    echo $tabs . $tabs_content;
                                    ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="main"><?php echo TEXT_PRODUCTS_STATUS; ?></td>
                            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_radio_field('products_status', '1', $in_status) . '&nbsp;' . TEXT_PRODUCT_AVAILABLE . '&nbsp;' . tep_draw_radio_field('products_status', '0', $out_status) . '&nbsp;' . TEXT_PRODUCT_NOT_AVAILABLE; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr>
                            <td class="main"><?php echo TEXT_PRODUCTS_DATE_AVAILABLE; ?></td>
                            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_date_available', tep_not_null($pInfo->products_date_available) ? date('d.m.Y', strtotime($pInfo->products_date_available)) : '', 'id="products_date_available"') . ''; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr bgcolor="#F0F1F1">
                            <td class="main"><?php echo TEXT_PRODUCTS_TAX_CLASS; ?></td>
                            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_pull_down_menu('products_tax_class_id', $tax_class_array, $pInfo->products_tax_class_id, 'onchange="updateGross()"'); ?></td>
                        </tr>
                        <tr bgcolor="#F0F1F1">
                            <td class="main"><?php echo TEXT_PRODUCTS_PRICE_NET; ?></td>
                            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_price', $pInfo->products_price, 'onkeyup="updateGross()"'); ?></td>
                        </tr>
                        <tr bgcolor="#F0F1F1">
                            <td class="main"><?php echo TEXT_PRODUCTS_PRICE_GROSS; ?></td>
                            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_price_gross', $pInfo->products_price, 'onkeyup="updateNet()"'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <script type="text/javascript"><!--
                            updateGross();
                            //--></script>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr>
                            <td class="main"><?php echo TEXT_PRODUCTS_QUANTITY; ?></td>
                            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_quantity', $pInfo->products_quantity); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr>
                            <td class="main" valign="top"><?php echo TEXT_PRODUCTS_IMAGE; ?><span style="color: #ff0000;">*</span></td>
                            <td class="main" style="padding-left: 30px;">
                                <div><?php echo '<strong>' . TEXT_PRODUCTS_MAIN_IMAGE . ' </strong><br />' . (tep_not_null($pInfo->products_image) ? tep_info_image(DIR_WS_PRODUCT_IMAGES . $pInfo->products_image . '-2' . IMAGE_EXTENSION, $pInfo->products_image, SMALL_IMAGE_WIDTH): '') . '<br/>' .  tep_draw_file_field('products_image') /*. tep_draw_checkbox_field('with_logo') . TEXT_PRODUCTS_IMAGE_WITH_LOGO*/; ?></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                        </tr>
                        <tr>
                            <td class="main"><?php echo TEXT_PRODUCTS_WEIGHT; ?></td>
                            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('products_weight', $pInfo->products_weight); ?></td>
                        </tr>
                        <tr>
                            <td class="main"><?php echo TEXT_PRODUCTS_MISSPELLINGS; ?></td>
                            <td class="main"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_textarea_field('products_misspellings', '10', '45', '', $pInfo->products_misspellings, 'onkeyup="updateGross()" style="resize: none;"'); ?></td>
                        </tr>

                    </table></td>
            </tr>
            <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
            <tr>
                <td class="smallText" align="right"><?php echo tep_draw_hidden_field('products_date_added', (tep_not_null($pInfo->products_date_added) ? $pInfo->products_date_added : date('Y-m-d'))) . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_PRODUCTS, 'cPath=' . $cPath . (isset($_GET['pID']) ? '&pID=' . $_GET['pID'] : ''))); ?></td>
            </tr>
        </table>

        <script type="text/javascript">
            $('#products_date_available').datepicker({
                dateFormat: 'dd.mm.yy'
            });
        </script>

        </form>
        <script type="text/javascript">
            var prod_form = document.forms["new_product"];
            function validateForm() {
                var message_stack = '';
                <?php for ($i=0, $n=sizeof($languages); $i<$n; $i++) { ?>
                if(prod_form.elements['products_name[<?php echo $languages[$i]['id']; ?>]'].value == '')
                    message_stack += 'Fill Products Name in <?php echo $languages[$i]['name'];?>\n';
                <?php } ?>
                var checked_flag = false;
                $('input[name="parent_category_id[]"]').map(function(){
                    if (this.checked) {
                        checked_flag = true;
                        return false;
                    }
                });
                if (!checked_flag) message_stack += 'Fill Product Category\n';
                <?php if ($action == 'new_product') {?>
                if(prod_form.elements.products_image.value == '')
                    message_stack += 'Fill Products Image';
                <?php }?>
                if (message_stack != '') { alert(message_stack); return false;} else {
                    stopUnload = false;
                }
            }
            prod_form.onsubmit = validateForm;
        </script>
        <?php
        echo '            </td>' . "\n";
    } elseif ($action == 'product_preview') {
        echo '            <td width="25%" valign="top">' . "\n";
        $product_query = tep_db_query("select p.products_id, pd.language_id, pd.products_name, pd.products_description, pd.products_url, p.products_quantity, p.products_model, p.products_image, p.products_price, p.products_weight, p.products_date_added, p.products_last_modified, p.products_date_available, p.products_status, p.manufacturers_id  from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and p.products_id = '" . (int)$_GET['pID'] . "'");
        $product = tep_db_fetch_array($product_query);

        $pInfo = new objectInfo($product);
        $products_image_name = $pInfo->products_image;

        $languages = tep_get_languages();
        for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
            $pInfo->products_name = tep_get_products_name($pInfo->products_id, $languages[$i]['id']);
            $pInfo->meta_keyword = tep_get_products_meta_keyword($pInfo->products_id, $languages[$i]['id']);
            $pInfo->meta_title = tep_get_products_meta_title($pInfo->products_id, $languages[$i]['id']);
            $pInfo->meta_description = tep_get_products_meta_description($pInfo->products_id, $languages[$i]['id']);
            $pInfo->products_description = tep_get_products_description($pInfo->products_id, $languages[$i]['id']);
            //$pInfo->products_url = tep_get_products_url($pInfo->products_id, $languages[$i]['id']);
            ?>
            <table border="0" width="100%" cellspacing="0" cellpadding="2">
            <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="pageHeading"><?php echo tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . $pInfo->products_name; ?></td>
                            <td class="pageHeading" align="right"><?php echo $currencies->format($pInfo->products_price); ?></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
            <tr>
                <td class="main"><?php echo tep_image(DIR_WS_CATALOG_IMAGES . $products_image_name . '-1' . IMAGE_EXTENSION, $pInfo->products_name, MIDDLE_IMAGE_WIDTH, MIDDLE_IMAGE_HEIGHT, 'align="right" hspace="5" vspace="5"') . $pInfo->products_description; ?></td>
            </tr>
            <?php
            /*if ($pInfo->products_url) {
                ?>
                <tr>
                    <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
                </tr>
                <tr>
                    <td class="main"><?php echo sprintf(TEXT_PRODUCT_MORE_INFORMATION, $pInfo->products_url); ?></td>
                </tr>
            <?php
            }*/
            ?>
            <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
            <?php
            if ($pInfo->products_date_available > date('Y-m-d')) {
                ?>
                <tr>
                    <td align="center" class="smallText"><?php echo sprintf(TEXT_PRODUCT_DATE_AVAILABLE, tep_date_long($pInfo->products_date_available)); ?></td>
                </tr>
            <?php
            } else {
                ?>
                <tr>
                    <td align="center" class="smallText"><?php echo sprintf(TEXT_PRODUCT_DATE_ADDED, tep_date_long($pInfo->products_date_added)); ?></td>
                </tr>
            <?php
            }
            ?>
            <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
            </tr>
        <?php
        }

        if (isset($_GET['origin'])) {
            $pos_params = strpos($_GET['origin'], '?', 0);
            if ($pos_params != false) {
                $back_url = substr($_GET['origin'], 0, $pos_params);
                $back_url_params = substr($_GET['origin'], $pos_params + 1);
            } else {
                $back_url = $_GET['origin'];
                $back_url_params = '';
            }
        } else {
            $back_url = FILENAME_PRODUCTS;
            $back_url_params = 'cPath=' . $cPath . '&pID=' . $pInfo->products_id;
        }
        ?>
        <tr>
            <td align="right" class="smallText"><?php echo tep_draw_button(IMAGE_BACK, 'triangle-1-w', tep_href_link($back_url, $back_url_params, 'NONSSL')); ?></td>
        </tr>
        </table>
        <?php
        echo '            </td>' . "\n";
    } elseif ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
        echo '            <td width="25%" valign="top">' . "\n";

        $box = new box;
        echo $box->infoBox($heading, $contents);

        echo '            </td>' . "\n";
    }
    ?>
    </tr>
    </table></td>
    </tr>
    </table>
<?php


require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>