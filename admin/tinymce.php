<!-- TinyMCE -->
<script type="text/javascript" src="includes/tinymce_new/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "textarea.tinymce",
        //paste_as_text: true,
        plugins: "autolink link lists charmap image preview searchreplace code " +
        "fullscreen table paste layer jbimages anchor emoticons autoresize imagetools media",

        toolbar1: "cut copy paste | bold italic underline strikethrough | bullist numlist | subscript superscript | charmap",
        toolbar2: "undo redo | table | searchreplace | link unlink media image jbimages | removeformat | preview fullscreen code",
        toolbar3: "alignleft aligncenter alignright alignjustify | anchor emoticons | styleselect | outdent indent",
        image_advtab: true,
        content_css: "includes/content.css",
        statusbar: true,
        menubar: false,
        toolbar_items_size: 'small',
        relative_urls: false

    });
</script>

