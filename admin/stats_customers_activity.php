<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td>
            <?php
            $check_visits = tep_db_query('select visit_id from ' . TABLE_VISITS . ' limit 1');
            if (tep_db_num_rows($check_visits) === 1) {
                ?>
                <div id="activity_cont"></div>
                <div id="methods_cont"></div>
                <script>
                    var activity_analytic_url = '<?php echo 'get_analytics.php?for=activity';?>';
                    var methods_analytic_url = '<?php echo 'get_analytics.php?for=methods';?>';

                    $('#activity_cont').lineChart({url: activity_analytic_url});
                    $('#methods_cont').barChart({url: methods_analytic_url});
                </script>
                <?php
                $check_products = tep_db_query(
                    'SELECT ' .
                        'v.products_id, ' .
                        'v.products_name, ' .
                        'v.visits, ' .
                        'b.buys ' .
                    'FROM (' .
                        'SELECT ' .
                            'pd.products_id, ' .
                            'pd.products_name, ' .
                            'COUNT(DISTINCT v.visit_id) AS visits ' .
                        'FROM ' . TABLE_PRODUCTS_DESCRIPTION . ' AS pd ' .
                        'LEFT JOIN ' . TABLE_VISITS . ' AS v ' .
                            'ON(' .
                                'pd.products_id = v.products_id' .
                            ') ' .
                        'WHERE ' .
                            'pd.language_id = ' . intval($languages_id) . ' ' .
                        'GROUP BY(pd.products_id)' .
                    ') AS v ' .
                    'LEFT JOIN (' .
                        'SELECT ' .
                            'op.products_id, ' .
                            'SUM(op.products_quantity) AS buys ' .
                        'FROM ' . TABLE_PARTNERS_HISTORY . ' AS ph ' .
                        'JOIN ' . TABLE_ORDERS_PRODUCTS . ' AS op ' .
                            'ON(' .
                                'ph.orders_id = op.orders_id' .
                            ') ' .
                        'GROUP BY(op.products_id) ' .
                    ') AS b ' .
                        'ON(v.products_id = b.products_id) ' .
                    'WHERE (' .
                        'v.visits > 0 ' .
                        'OR b.buys > 0' .
                    ') ' .
                    'LIMIT 1'
                );
                if (tep_db_num_rows($check_products) === 1) {
                ?>
                    <div id="products_cont"></div>
                    <script>
	                    var products_analytic_url = '<?php echo 'get_analytics.php?for=products';?>';
                        $('#products_cont').barChart({url: products_analytic_url});
                    </script>
                <?php
                }
            } else {
                echo TEXT_NO_ANALYTIC;
            }
            ?>
        </td>
    </tr>
</table>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
