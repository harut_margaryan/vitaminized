<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

$action = (isset($_GET['action']) ? $_GET['action'] : '');
$page = (isset($_GET['page']) ? (int)$_GET['page'] : 1);

$url = 'page=' . $page;

if (tep_not_null($action)) {
    switch ($action) {
        case 'setflag':
            if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
                if (isset($_GET['tID'])) {
                    $status = $_GET['flag'];
                    if ($status == 1) {
                        tep_db_query("update " . TABLE_MARKETING_TAGS . " set tag_status = b'1' where tag_id = '" . (int)$_GET['tID'] . "'");
                    } elseif ($status == 0) {
                        tep_db_query("update " . TABLE_MARKETING_TAGS . " set tag_status = b'0' where tag_id = '" . (int)$_GET['tID'] . "'");
                    }
                }
            }

            tep_redirect(tep_href_link(FILENAME_MARKETING_TAGS, 'tID=' . $_GET['tID'] . '&' . $url));
            break;
        case 'insert':
        case 'save':
            if (isset($_GET['tID'])) $tags_id = tep_db_prepare_input($_GET['tID']);

            $languages = tep_get_languages();
            $tag_names_array = $_POST['tag_name'];

            $sql_data_t = array('sort_order' => (isset($_POST['sort_order']) ? (int)$_POST['sort_order'] : 1));

            if ($action == 'insert') {
                $sql_data_t['tag_status'] = 1;
                tep_db_perform(TABLE_MARKETING_TAGS, $sql_data_t);
                $tags_id = tep_db_insert_id();
            } elseif ($action == 'save') {
                tep_db_perform(TABLE_MARKETING_TAGS, $sql_data_t, 'update', "tag_id = " . $tags_id);
            }

            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                $language_id = $languages[$i]['id'];

                $sql_data_td = array('tag_name' => $tag_names_array[$language_id],
                    'language_id' => $language_id);


                if ($action == 'insert') {

                    $sql_data_td['tag_id'] = $tags_id;

                    tep_db_perform(TABLE_MARKETING_TAGS_INFO, $sql_data_td);                    
                } elseif ($action == 'save') {
                    tep_db_perform(TABLE_MARKETING_TAGS_INFO, $sql_data_td, 'update', "language_id = '" . (int)$language_id . "' and tag_id = " . $tags_id);
                }
            }

            tep_redirect(tep_href_link(FILENAME_MARKETING_TAGS, 'tID=' . $tags_id . '&' . $url));
            break;
        case 'deleteconfirm':
            $tID = tep_db_prepare_input($_GET['tID']);

            tep_db_query("delete from " . TABLE_MARKETING_TAGS . " where tag_id = '" . (int)($tID) . "'");
            tep_db_query("delete from " . TABLE_MARKETING_TAGS_INFO . " where tag_id = '" . (int)($tID) . "'");
            tep_db_query("delete from " . TABLE_MARKETING_POSTS_TAGS . " where tag_id = '" . (int)($tID) . "'");
            tep_db_query("delete from " . TABLE_MARKETING_BANNERS_TAGS . " where tag_id = '" . (int)($tID) . "'");
            tep_db_query("delete from " . TABLE_MARKETING_PICS_TAGS . " where tag_id = '" . (int)($tID) . "'");
            tep_db_query("delete from " . TABLE_MARKETING_SHARES_TAGS . " where tag_id = '" . (int)($tID) . "'");
            tep_db_query("delete from " . TABLE_MARKETING_STATUSES_TAGS . " where tag_id = '" . (int)($tID) . "'");
            tep_db_query("delete from " . TABLE_MARKETING_MAILS_TAGS . " where tag_id = '" . (int)($tID) . "'");


            tep_redirect(tep_href_link(FILENAME_MARKETING_TAGS));
            break;
            break;
    }
}

require(DIR_WS_INCLUDES . 'template_top.php');
?>

<table border="0" width="100%" cellspacing="0" cellpadding="2">
    <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="pageHeading"><?php echo TEXT_MARKETING_TAGS; ?></td>
                    <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                            <tr class="dataTableHeadingRow">
                                <td class="dataTableHeadingContent"><?php echo TEXT_MARKETING_TAGS; ?></td>
                                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS_2; ?></td>
                                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION_2; ?>&nbsp;</td>
                            </tr>
                            <?php
                            $tags_query_raw = "select pi.tag_id, pi.tag_status, pi.sort_order, pid.tag_name  from " . TABLE_MARKETING_TAGS . " as pi join " . TABLE_MARKETING_TAGS_INFO . " as pid on(pi.tag_id = pid.tag_id and language_id = '" . (int)$languages_id . "') order by pi.sort_order";
                            $tags_split = new splitPageResults($page, MAX_DISPLAY_SEARCH_RESULTS, $tags_query_raw, $tags_numrows);
                            $tags_query = tep_db_query($tags_query_raw);

                            while ($marketing_tags = tep_db_fetch_array($tags_query)) {
                                if ((!isset($_GET['tID']) || (isset($_GET['tID']) && ($_GET['tID'] == $marketing_tags['tag_id']))) && !isset($piInfo) && (substr($action, 0, 3) != 'new')) {
                                    $piInfo = new objectInfo($marketing_tags);
                                }

                                if (isset($piInfo) && is_object($piInfo) && ($marketing_tags['tag_id'] == $piInfo->tag_id)) {
                                    echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_MARKETING_TAGS, 'tID=' . $piInfo->tag_id . '&action=edit' . '&' . $url) . '\'">' . "\n";
                                } else {
                                    echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_MARKETING_TAGS, 'tID=' . $marketing_tags['tag_id']) . '\'">' . "\n";
                                }
                                ?>
                                <td class="dataTableContent"><?php echo $marketing_tags['tag_name'];?></td>
                                <td class="dataTableContent" align="center">
                                    <?php
                                    if ($marketing_tags['tag_status'] == '1') {
                                        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_MARKETING_TAGS, 'action=setflag&flag=0&tID=' . $marketing_tags['tag_id'] . '&' . $url) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
                                    } else {
                                        echo '<a href="' . tep_href_link(FILENAME_MARKETING_TAGS, 'action=setflag&flag=1&tID=' . $marketing_tags['tag_id'] . '&' . $url) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
                                    }
                                    ?>
                                </td>
                                <td class="dataTableContent" align="right"><?php if (isset($piInfo) && is_object($piInfo) && ($marketing_tags['tag_id'] == $piInfo->tag_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_MARKETING_TAGS, 'tID=' . $marketing_tags['tag_id'] . '&' . $url) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                        <?php
                                        if (empty($action)) {
                                            ?>
                                            <tr>
                                                <td class="smallText" colspan="2" align="right"><?php echo tep_draw_button(IMAGE_INSERT, 'plus', tep_href_link(FILENAME_MARKETING_TAGS, 'action=new')); ?></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        <tr>
                                            <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                    <tr>
                                                        <td class="smallText" valign="top"><?php echo $tags_split->display_count($tags_numrows, MAX_DISPLAY_SEARCH_RESULTS, $page, TEXT_DISPLAY_NUMBER_OF_ITEMS); ?></td>
                                                        <td class="smallText" align="right"><?php echo $tags_split->display_links($tags_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $page); ?></td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                    <?php
                    $heading = array();
                    $contents = array();

                    switch ($action) {
                        case 'new':
                            $heading[] = array('text' => '<strong>' . TEXT_MARKETING_TAGS . '</strong>');
                            $contents = array('form' => tep_draw_form(TABLE_MARKETING_TAGS, FILENAME_MARKETING_TAGS, 'action=insert'));

                            $marketing_tags_inputs_string = '';
                            $languages = tep_get_languages();
                            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                                $marketing_tags_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('tag_name[' . $languages[$i]['id'] . ']');
                            }

                            $contents[] = array('text' => '<br />' . TEXT_INFO_NAME . $marketing_tags_inputs_string);
                            $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('sort_order', 1, 'size="2"'));
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_MARKETING_TAGS, $url)));
                            break;
                        case 'edit':
                            $heading[] = array('text' => '<strong>' . TEXT_MARKETING_TAGS . '</strong>');
                            $contents = array('form' => tep_draw_form(TABLE_MARKETING_TAGS, FILENAME_MARKETING_TAGS, 'tID=' . $piInfo->tag_id  . '&action=save' . '&' . $url));


                            $marketing_tags_inputs_string = '';
                            $languages = tep_get_languages();
                            for ($i=0, $n=sizeof($languages); $i<$n; $i++) {
                                $tag_names_query = tep_db_query('select pid.tag_name from ' . TABLE_MARKETING_TAGS . ' as pi join ' . TABLE_MARKETING_TAGS_INFO . ' as pid on(pi.tag_id = ' . (int)$piInfo->tag_id . ' and pi.tag_id = pid.tag_id and pid.language_id = ' . (int)$languages[$i]['id'] . ')');
                                $tag_name = tep_db_num_rows($tag_names_query) == 1 ? tep_db_fetch_array($tag_names_query) : array('tag_name' => '');
                                $marketing_tags_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . tep_draw_input_field('tag_name[' . $languages[$i]['id'] . ']', $tag_name['tag_name']);
                            }

                            $contents[] = array('text' => '<br />' . TEXT_INFO_NAME . $marketing_tags_inputs_string);
                            $contents[] = array('text' => '<br />' . TEXT_SORTORDER . '<br />' . tep_draw_input_field('sort_order', $piInfo->sort_order, 'size="2"'));
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_SAVE, 'disk', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_MARKETING_TAGS, 'tID=' . $piInfo->tag_id . '&' . $url)));
                            break;
                        case 'delete':
                            $heading[] = array('text' => '<strong>' . TEXT_MARKETING_TAGS . '</strong>');

                            $contents = array('form' => tep_draw_form(TABLE_MARKETING_TAGS, FILENAME_MARKETING_TAGS, 'tID=' . $piInfo->tag_id  . '&action=deleteconfirm' . '&' . $url));

                            $contents[] = array('text' => '<br /><strong>' . $piInfo->tag_name . '</strong>');
                            $contents[] = array('align' => 'center', 'text' => '<br />' . tep_draw_button(IMAGE_DELETE, 'trash', null, 'primary') . tep_draw_button(IMAGE_CANCEL, 'close', tep_href_link(FILENAME_MARKETING_TAGS, 'tID=' . $piInfo->tag_id . '&' . $url)));
                            break;
                        default:
                            if (isset($piInfo) && is_object($piInfo)) {
                                $heading[] = array('text' => '<strong>' . $piInfo->tag_name . '</strong>');

                                $contents[] = array('align' => 'center', 'text' => tep_draw_button(IMAGE_EDIT, 'document', tep_href_link(FILENAME_MARKETING_TAGS, 'tID=' . $piInfo->tag_id . '&action=edit' . '&' . $url)) . tep_draw_button(IMAGE_DELETE, 'trash', tep_href_link(FILENAME_MARKETING_TAGS, 'tID=' . $piInfo->tag_id . '&action=delete' . '&' . $url)));

                                $marketing_tags_inputs_string = '';
                                $languages = tep_get_languages();
                                for ($i = 0, $n = sizeof($languages); $i < $n; $i++) {
                                    $tag_names_query = tep_db_query('select pid.tag_name from ' . TABLE_MARKETING_TAGS . ' as pi join ' . TABLE_MARKETING_TAGS_INFO . ' as pid on(pi.tag_id = ' . (int)$piInfo->tag_id . ' and pi.tag_id = pid.tag_id and pid.language_id = ' . (int)$languages[$i]['id'] . ')');
                                    $tag_name = tep_db_num_rows($tag_names_query) == 1 ? tep_db_fetch_array($tag_names_query) : array('tag_name' => '');
                                    $marketing_tags_inputs_string .= '<br />' . tep_image(DIR_WS_CATALOG_LANGUAGES . $languages[$i]['directory'] . '/images/' . $languages[$i]['image'], $languages[$i]['name']) . '&nbsp;' . $tag_name['tag_name'];
                                }

                                $contents[] = array('text' => $marketing_tags_inputs_string);
                            }
                            break;
                    }

                    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
                        echo '            <td width="25%" valign="top">' . "\n";

                        $box = new box;
                        echo $box->infoBox($heading, $contents);

                        echo '            </td>' . "\n";
                    }
                    ?>
                </tr>
            </table></td>
    </tr>
</table>

<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
