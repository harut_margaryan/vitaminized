<h1><?php echo HEADING_TITLE; ?></h1>

    <?php
    if ($messageStack->size('contact') > 0) {
        echo $messageStack->output('contact');
    }

    if (isset($_GET['action']) && ($_GET['action'] == 'success')) {
        ?>

        <div class="contentContainer">
            <div class="contentText">
                <?php echo TEXT_SUCCESS; ?>
            </div>

            <div style="float: right;">
                <?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', tep_href_link()); ?>
            </div>
        </div>
        <?php
  } else {
?>

<?php echo tep_draw_form('contact_us', tep_href_link(CONTACT_US_URL, 'action=send'), 'post', '', true); ?>

<div class="contentContainer">
  <div class="contentText">
    <table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td class="fieldKey"><?php echo ENTRY_NAME; ?></td>
        <td class="fieldValue"><?php echo tep_draw_input_field('name'); ?></td>
      </tr>
      <tr>
        <td class="fieldKey"><?php echo ENTRY_EMAIL; ?></td>
        <td class="fieldValue"><?php echo tep_draw_input_field('email'); ?></td>
      </tr>
      <tr>
        <td class="fieldKey" valign="top"><?php echo ENTRY_ENQUIRY; ?></td>
        <td class="fieldValue"><?php echo tep_draw_textarea_field('enquiry', 'soft', 50, 15); ?></td>
      </tr>
    </table>
  </div>

  <div class="buttonSet">
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e', null, 'primary'); ?></span>
  </div>
</div>

</form>
<?php
    }
?>