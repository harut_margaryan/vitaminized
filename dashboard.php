<style>

  .date-filter, .date-filter-label {
    float: left;
  }

  .date-filter, .date-filter-label:not(:first-child) {
    margin-left: 15px;
  }

  .balance {
      float: right;
  }

</style>

<h1 class="page-main-h1">Dashboard</h1>

<div class="page-content-cont">

  <div class="cart-container">

    <div id="app">

      <div class="referal">My referal id is <strong>{{ state.referal_id }}</strong></div>
      <br>

      <div class="dashboard-container" v-if="showDashboard">
        <div class="date-filter-container">
          <div class="date-filter-label">
            Date From
          </div>
          <datepicker v-model="state.date_from" format="dd MMMM yyyy"
                      placeholder="Filter From Date" wrapper-class="date-filter"></datepicker>

          <div class="date-filter-label">
            Date to
          </div>
          <datepicker v-model="state.date_to" format="dd MMMM yyyy"
                      placeholder="Filter From Date" wrapper-class="date-filter"></datepicker>
          <div class="balance">
              Balance - ${{balance}}
          </div>

          <div class="clear"></div>
        </div>

        <table style="width: 100%;">
          <thead>
          <tr>
            <th>id</th>
            <th>name</th>
            <th>price</th>
            <th>count</th>
            <th>date</th>
          </tr>
          </thead>

          <tbody>
          <tr v-for="item in filterGridByDate">
            <td>{{item.id}}</td>
            <td>{{item.name}}</td>
            <td>${{ item.price  }}</td>
            <td>{{item.count}}</td>
            <td>{{ item.date * 1000 | moment("dddd, MMMM Do YYYY") }}</td>
          </tr>
          </tbody>
        </table>

      </div>
      <div class="dashboard-container" v-else >
        Loading data ...
      </div>

    </div>

  </div>
</div>


